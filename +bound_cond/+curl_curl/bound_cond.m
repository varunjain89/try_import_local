function [ E_bc ] = bound_cond( p, E_bot, E_top, E_left, E_right )
%BOUND_COND Summary of this function goes here
%   Detailed explanation goes here

[x1,w] = GLLnodes(p);
[h,~] = MimeticpolyVal(x1,p,1);

E_bc = zeros((p+1)^2, 1);

%% bc bottom edge

for i = 2:p
    node_bot = (i-1)*(p+1) + 1; 
    for x = 1:p+1 
        E_bc(node_bot) = E_bc(node_bot) + h(i,x)*E_bot(x)*w(x);
    end
end

%% bc top edge

for i = 2:p
    node_top = (p+1) * i;
    for x = 1:p+1
        E_bc(node_top) = E_bc(node_top) - h(i,x)*E_top(x)*w(x);
    end
end

%% bc left edge

for j = 2:p
    node_left = j;
    for y = 1:p+1
        E_bc(node_left) = E_bc(node_left) - h(j,y)*E_left(y)*w(y);
    end
end

%% bc right edge

for j = 2:p
    node_right = p*(p+1) + j;
    for y = 1:p+1
        E_bc(node_right) = E_bc(node_right) + h(j,y)*E_right(y)*w(y);
    end
end

% corner nodes 
bot_left  = 1;
top_left  = p+1;
bot_right = p*(p+1) + 1;
top_right = (p+1)^2;

%% bc bottom left corner
% this is a sum of (bottom + left) edge contribution

% bottom contribution 
for x = 1:p+1
    E_bc(bot_left) = E_bc(bot_left) + h(1,x)*E_bot(x)*w(x);
end

% left contribution
for y = 1:p+1
    E_bc(bot_left) = E_bc(bot_left) - h(1,y)*E_left(y)*w(y);
end

%% bc bottom right corner
% this is a sum of (bottom + right) edge contribution

% bottom contribution 
for x = 1:p+1
    E_bc(bot_right) = E_bc(bot_right) + h(p+1,x)*E_bot(x)*w(x);
end

% right contribution
for y = 1:p+1
    E_bc(bot_right) = E_bc(bot_right) + h(1,y)*E_right(y)*w(y);
end


%% bc top left corner
% this is a sum of (top + left) edge contribution

% top contribution 
for x = 1:p+1
    E_bc(top_left) = E_bc(top_left) - h(1,x)*E_top(x)*w(x);
end

% left contribution
for y = 1:p+1
    E_bc(top_left) = E_bc(top_left) - h(p+1,y)*E_left(y)*w(y);
end


%% bc top right corner
% this is a sum of (top + right) edge contribution

% top contribution 
for x = 1:p+1
    E_bc(top_right) = E_bc(top_right) - h(p+1,x)*E_top(x)*w(x);
end

% right contribution
for y = 1:p+1
    E_bc(top_right) = E_bc(top_right) + h(p+1,y)*E_right(y)*w(y);
end

end

