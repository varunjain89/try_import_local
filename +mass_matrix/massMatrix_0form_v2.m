function [M0] = massMatrix_0form_v2( p, g )
%MASSMATRIX_0FORM Summary of this function goes here
%   Detailed explanation goes here

% updated on : 2nd March, 2018
% description : this is the mass matrix for 0 forms.
% as input you provide the polynomial degree. 
% as output you get the Mass matrix for single element. 

g = g(:);

[x,w] = GLLnodes(p);
[h,~] = MimeticpolyVal(x,p,1);

h = sparse(h);

hh  = kron(h,h);
ww = kron(w,w);

hhww = bsxfun(@times, hh, ww);      % multiplying test function with weights
hhww = bsxfun(@times, hhww, g);     % multiplying test function with metric terms

M0  = hhww*hh';

end

