function [ M2 ] = M2_2( p, g )
%M2 Summary of this function goes here
%   Detailed explanation goes here

% g = g';

g = g(:);

[x,w] = GLLnodes(p);
% [x,w] = Gnodes2(p+2);

[~,e] = MimeticpolyVal(x,p,1);

invg = (1./g)';

% e = sparse(e);

ee  = kron(e,e);

% ew  = bsxfun(@times,e,w);
% ewew = kron(ew,ew);
ww = kron(w,w);

eeww = bsxfun(@times, ee, ww);    % multiplying test function with weights
eeww = bsxfun(@times, eeww, invg); % multiplying test function with metric terms

M2  = eeww*ee';

% M2 = Slice3DMult2D(eeqq, ee'); % multiplying with dof basis





end