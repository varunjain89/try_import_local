function [ M2 ] = M2_v3( ~, g,  basis,w )

% function [ M2 ] = M2_v3( p, g,  basis,w )
%M2 Summary of this function goes here
%   Detailed explanation goes here

% Modified on 16 October, 2018

% g = g';

g = g(:);

e = basis.ep;

invg = (1./g)';

% e = sparse(e);

ee  = kron(e,e);

% ew  = bsxfun(@times,e,w);
% ewew = kron(ew,ew);
ww = kron(w,w);

eeww = bsxfun(@times, ee, ww);    % multiplying test function with weights
eeww = bsxfun(@times, eeww, invg); % multiplying test function with metric terms

M2  = eeww*ee';

% M2 = Slice3DMult2D(eeqq, ee'); % multiplying with dof basis

end