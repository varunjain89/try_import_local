function [ M2 ] = M2_6(basis, g )

% created on 26 July, 2019

% g = g';

g = g(:);

e = basis.ep;
w = basis.wp;

invg = (1./g)';

ee  = kron(e,e);

ww = kron(w,w);

eeww = bsxfun(@times, ee, ww);    % multiplying test function with weights
eeww = bsxfun(@times, eeww, invg); % multiplying test function with metric terms

M2  = eeww*ee';

end