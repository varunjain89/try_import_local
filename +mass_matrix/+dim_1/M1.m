function [M1_1D] = M1(p,ep,wp)

M1_1D = zeros(p);

for i = 1:p
    for k = 1:p
        for int_x = 1:p+1
            M1_1D(i,k) = M1_1D(i,k) + ep(i,int_x) * ep(k,int_x) * wp(int_x);
        end
    end
end

end

