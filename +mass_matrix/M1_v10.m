function [M11] = M1_v10(mesh1)

% Created on : 16 April, 2019

%%
p = mesh1.p;
w = mesh1.wp;

mesh1.def_basis(p);

h = mesh1.basis.hp;
e = mesh1.basis.ep;

for i = 1:p+1
   O(i) = (i-1)*(p+1) + 1;
end

O1 = O;

for i = 1:p
    O1 = horzcat(O1, O+i);
end

% A12 = [1 5 9 13 2 6 10 14 3 7 11 15 4 8 12 16];

hex  = kron(h,e);   % basis functions in x-direction with quad pts. numbered horizontally
                    % these are also the basis functions in y-direction,
                    % but with quad pts numbered vertically. We want the
                    % quad pts to be numbered horizontally, therefore we
                    % will resructure their numbering.
hey  = hex(:,O1);

%% multiplying with metric terms and weights

ww = kron(mesh1.wp,mesh1.wp);

%%
hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights

%%

ttl_nr_el = mesh1.Kx * mesh1.Ky;

for eln = 1:ttl_nr_el
    % this part is already implicitly multi threaded (it seems) and has 
    % to go to different clusters for further improvement in efficiency
    M11(:,:,eln) = mass_matrix.M1_v9(mesh1, eln, hex, hey, hex11, hex12, hey21, hey22);
end

end

