function [ M2 ] = M2_v5_pfoil( mesh1, eln )

% function [ M2 ] = M2_v3( p, g,  basis,w )
%M2 Summary of this function goes here
%   Detailed explanation goes here

% Modified on 16 October, 2018

% g = g';

% basis = mesh1.basis;
g = mesh1.eval.metric.ggg(:,:,eln);
w = mesh1.wp;

g = g(:);

p = mesh1.p;
[x,w] = Gnodes2(p+1);
% [x2,w2] = GLLnodes(p);

[~,e] = MimeticpolyVal(x,p,1);


invg = (1./g)';

% e = sparse(e);

ee  = kron(e,e);

% ew  = bsxfun(@times,e,w);
% ewew = kron(ew,ew);
ww = kron(w,w);

eeww = bsxfun(@times, ee, ww);    % multiplying test function with weights
eeww = bsxfun(@times, eeww, invg); % multiplying test function with metric terms

M2  = eeww*ee';

% M2 = Slice3DMult2D(eeqq, ee'); % multiplying with dof basis

end