function [ M1 ] = M1_v7(mesh1, eln)
%M1 Summary of this function goes here
%   Detailed explanation goes here

% Created on : 11th Feb, 2018
% Modified on 14 October, 2018
% Modified on 16 October, 2018
% Modified on 17 November, 2018
% Modified on 20 December, 2018

p = mesh1.p;
basis = mesh1.basis;
w = mesh1.wp;

% Description : 

g11 = mesh1.eval.metric.g11(:,:,eln);
g12 = mesh1.eval.metric.g12(:,:,eln);
g22 = mesh1.eval.metric.g22(:,:,eln);

g11 = g11';
g12 = g12';
g22 = g22';

g11 = g11(:)';
g12 = g12(:)';
g22 = g22(:)';

h = basis.hp;
e = basis.ep;

for i = 1:p+1
   O(i) = (i-1)*(p+1) + 1;
end

O1 = O;

for i = 1:p
    O1 = horzcat(O1, O+i);
end

% A12 = [1 5 9 13 2 6 10 14 3 7 11 15 4 8 12 16];

hex  = kron(h,e);   % basis functions in x-direction with quad pts. numbered horizontally
                    % these are also the basis functions in y-direction,
                    % but with quad pts numbered vertically. We want the
                    % quad pts to be numbered horizontally, therefore we
                    % will resructure their numbering.
hey  = hex(:,O1);

%% multiplying with metric terms and weights

ww = kron(w,w);

%%

hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex11 = bsxfun(@times, hex11, g11); % multiplying test function with metric terms
M1_11 = hex11 * hex';

hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex12, g12); % multiplying test function with metric terms
M1_12 = hex12 * hey';

hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey21, g12); % multiplying test function with metric terms
M1_21 = hey21 * hex';

hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey22, g22); % multiplying test function with metric terms
M1_22 = hey22 * hey';

M1 = [M1_11 M1_12; M1_21 M1_22];
end