function [ M1 ] = M1_v9(mesh1, eln, hex, hey, hex11, hex12, hey21, hey22)
%M1 Summary of this function goes here
%   Detailed explanation goes here

% Created on : 11th Feb, 2018
% Modified on 14 October, 2018
% Modified on 16 October, 2018
% Modified on 17 November, 2018
% Modified on 20 December, 2018
% Modified on 06 March, 2019

% Description : 

g11 = mesh1.eval.metric.g11(:,:,eln);
g12 = mesh1.eval.metric.g12(:,:,eln);
g22 = mesh1.eval.metric.g22(:,:,eln);

g11 = g11';
g12 = g12';
g22 = g22';

g11 = g11(:)';
g12 = g12(:)';
g22 = g22(:)';

%%

hex11 = bsxfun(@times, hex11, g11); % multiplying test function with metric terms
M1_11 = hex11 * hex';

hex12 = bsxfun(@times, hex12, g12); % multiplying test function with metric terms
M1_12 = hex12 * hey';

hey21 = bsxfun(@times, hey21, g12); % multiplying test function with metric terms
M1_21 = hey21 * hex';

hey22 = bsxfun(@times, hey22, g22); % multiplying test function with metric terms
M1_22 = hey22 * hey';

M1 = [M1_11 M1_12; M1_21 M1_22];
end