function [ M1 ] = M1_v4( p, eval, eln )

%M1 Summary of this function goes here
%   Detailed explanation goes here

% Created on : 11th Feb, 2018
% Modified on 14 October, 2018
% Description : 

g11 = eval.g11(:,:,eln);
g12 = eval.g12(:,:,eln);
g22 = eval.g22(:,:,eln);

g11 = g11';
g12 = g12';
g22 = g22';

g11 = g11(:)';
g12 = g12(:)';
g22 = g22(:)';

[x,w] = GLLnodes(p);
[h,e] = MimeticpolyVal(x,p,1);

for i = 1:p+1
   O(i) = (i-1)*(p+1) + 1;
end

O1 = O;

for i = 1:p
    O1 = horzcat(O1, O+i);
end

% A12 = [1 5 9 13 2 6 10 14 3 7 11 15 4 8 12 16];

hex  = kron(h,e);   % basis functions in x-direction with quad pts. numbered horizontally
                    % these are also the basis functions in y-direction,
                    % but with quad pts numbered vertically. We want the
                    % quad pts to be numbered horizontally, therefore we
                    % will resructure their numbering.
hey  = hex(:,O1);

%% multiplying with metric terms and weights

ww = kron(w,w);

%% 
hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex11 = bsxfun(@times, hex11, g11); % multiplying test function with metric terms
M1_11 = Slice3DMult2D(hex11, hex'); % multiplying with dof basis

hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex12, g12); % multiplying test function with metric terms
M1_12 = Slice3DMult2D(hex12, hey'); % multiplying with dof basis

hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey21, g12); % multiplying test function with metric terms
M1_21 = Slice3DMult2D(hey21, hex'); % multiplying with dof basis

hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey22, g22); % multiplying test function with metric terms
M1_22 = Slice3DMult2D(hey22, hey'); % multiplying with dof basis

M1 = [M1_11 M1_12; M1_21 M1_22];

end