classdef oneForm_v2 < handle
    %ONEFORM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
%         Xcochain
%         Ycochain
        
        p
%         pf
%         eval_pf_dx_dxii
%         eval_pf_dx_deta
%         eval_pf_dy_dxii
%         eval_pf_dy_deta
%         try45
        K
        
        mesh
%         cochain
%         rec_qx
%         rec_qy
    end
    
    methods
        function obj = oneForm_v2(mesh1)
            obj.mesh = mesh1;
            obj.K = mesh1.K;
            obj.p = mesh1.p;
%             obj.el_bounds_x = mesh1.el.bounds.x;
%             obj.el_bounds_y = mesh1.el.bounds.y;
%             obj.el_mapping  = mesh1.el.mapping;
        end
        
        function qx = reconstructionX(obj)
            qx = reconstruction.reconstruct1xform_2(obj.Xcochain, obj.Ycochain, obj.p, obj.pf, obj.eval_pf_dx_dxii, obj.eval_pf_dx_deta, obj.eval_pf_dy_dxii, obj.eval_pf_dy_deta);
        end
        
        function qy = reconstructionY(obj)
            qy = reconstruction.reconstruct1yform_2(obj.Xcochain, obj.Ycochain, obj.p, obj.pf, obj.eval_pf_dx_dxii, obj.eval_pf_dx_deta, obj.eval_pf_dy_dxii, obj.eval_pf_dy_deta);
        end
        
        function [rec] = reconstruction(obj, q_h)
            
%             q_h = cochain;
            half_edges = obj.p*(obj.p+1);
            
            for eln = 1:obj.mesh.ttl_nr_el
                qx_h(:,eln) = q_h(1:half_edges,eln);
                qy_h(:,eln) = -q_h(half_edges+1:end,eln);
            end

            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx-1)*obj.K + ely;
                    
                    temp2 = reconstruction.reconstruct1xform_v4(qx_h(:,eln), qy_h(:,eln), obj.mesh, eln);
                    rec.qx(:,:,eln) = full(temp2)';
                    
                    temp2 = reconstruction.reconstruct1yform_v5(qx_h(:,eln), qy_h(:,eln), obj.mesh, eln);
                    rec.qy(:,:,eln) = full(temp2)';
                end
            end
        end
        
    end
    
end

