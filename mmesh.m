classdef mmesh < handle
    % Created on: 8th April, 2019
    % this is a class for any general mesh
    
    properties
        patch
        eval
    end
    
    methods
        function obj = mmesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta)
            obj.patch.mapping = @(xi,eta) mapping(xi,eta);
            obj.patch.dX_dxii = @(xi,eta) dX_dxii(xi,eta);
            obj.patch.dX_deta = @(xi,eta) dX_deta(xi,eta);
            obj.patch.dY_dxii = @(xi,eta) dY_dxii(xi,eta);
            obj.patch.dY_deta = @(xi,eta) dY_deta(xi,eta);
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
        
        function obj = eval_jacobian(obj,p)
            [xp, ~] = GLLnodes(p);
            [xiip, etap] = meshgrid(xp);
            obj.eval.jac.p.dx_dxii = obj.patch.dX_dxii(xiip,etap);
            obj.eval.jac.p.dx_deta = obj.patch.dX_deta(xiip,etap);
            obj.eval.jac.p.dy_dxii = obj.patch.dY_dxii(xiip,etap);
            obj.eval.jac.p.dy_deta = obj.patch.dY_deta(xiip,etap);
        end
        
    end
end

