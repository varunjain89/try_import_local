%% script settings

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% test case

test.phi = @(x,y,z) (1-x.^2).*(1-y.^2).*(1-z.^2);

test.dpdx = @(x,y,z) -2*x.*(1-y.^2).*(1-z.^2);
test.dpdy = @(x,y,z) -2*y.*(1-x.^2).*(1-z.^2);
test.dpdz = @(x,y,z) -2*z.*(1-x.^2).*(1-y.^2);

test.d2pdx2 = @(x,y,z) -2*(1-y.^2).*(1-z.^2);
test.d2pdy2 = @(x,y,z) -2*(1-x.^2).*(1-z.^2);
test.d2pdz2 = @(x,y,z) -2*(1-x.^2).*(1-y.^2);

test.fff = @(x,y,z) test.d2pdx2(x,y,z) + test.d2pdy2(x,y,z) + test.d2pdz2(x,y,z);

%% mappings

xbound = [-1 1];
ybound = [-1 1];
zbound = [-1 1];

c = 0.0;

domain.mapping = @(xii,eta,zta) mesh.dim_3.crazy_cube.mapping(xbound,ybound,zbound,c,xii,eta,zta);
domain.jacobian = @(xii,eta,zta) mesh.dim_3.crazy_cube.jacobian(xbound,ybound,zbound,c,xii,eta,zta);

%% discretization

p = 3;

%% create basis 

[x,w] = GLLnodes(p);
[h,e] = MimeticpolyVal(x,p,1);

dtize.basis.hx = h;
dtize.basis.hy = h;
dtize.basis.hz = h;

dtize.basis.ex = e;
dtize.basis.ey = e;
dtize.basis.ez = e;

dtize.weights.wx = w;
dtize.weights.wy = w;
dtize.weights.wz = w;

dtize.nodes.sx = x;
dtize.nodes.sy = x;
dtize.nodes.sz = x;

%% random variables

% nr_nodes = (p+1)^3;
% nr_edges = 2*p*(p+1)^2
nr_surfc = 3*p^2*(p+1);
nr_volum = p^3;

%% 

[dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta] = meshgrid(dtize.nodes.sx,dtize.nodes.sy,dtize.nodes.sz);
[dtize.nodes.x,dtize.nodes.y,dtize.nodes.z] = domain.mapping(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta);

dtize.jacobian = domain.jacobian(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta);

%%

basis_volume = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.ex);

www = kron(kron(dtize.weights.wz,dtize.weights.wy),dtize.weights.wx);

%% reduction of f

metric = 1;

M333 = construct_mass_matrix(basis_volume,basis_volume,www,metric);

ff_int = test.fff(dtize.nodes.x(:),dtize.nodes.y(:),dtize.nodes.z(:))';

dual_basis = M333 \ basis_volume;

dof_f = reduction(ff_int, dual_basis, www, metric);

%% reconstruction of f

pf = p+5;

[xf,wf] = GLLnodes(pf);
[hf,ef] = MimeticpolyVal(xf,p,1);

rec.basis.hfx = hf;
rec.basis.hfy = hf;
rec.basis.hfz = hf;

rec.basis.efx = ef;
rec.basis.efy = ef;
rec.basis.efz = ef;

rec.weights.wfx = wf;
rec.weights.wfy = wf;
rec.weights.wfz = wf;

dtize.nodes.sfx = xf;
dtize.nodes.sfy = xf;
dtize.nodes.sfz = xf;

wwwf = kron(kron(rec.weights.wfz,rec.weights.wfy),rec.weights.wfx);

rec_basis_volume = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.efx);

rec_fff = reconstruction.dim_3.volume(dof_f,rec_basis_volume,pf,1);

[rec.nodes.xii,rec.nodes.eta,rec.nodes.zta] = meshgrid(dtize.nodes.sfx,dtize.nodes.sfy,dtize.nodes.sfz);
[rec.nodes.x,rec.nodes.y,rec.nodes.z] = domain.mapping(rec.nodes.xii,rec.nodes.eta,rec.nodes.zta);

ext_fff = test.fff(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error = sqrt(sum((ext_fff(:)-rec_fff(:)).^2 .* wwwf'))

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec_fff,[-0 0.5],0,0)
% colorbar
% title('pressure reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff,[-0 0.5],0,0)
% colorbar
% title('pressure exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff-rec_fff,[-0 0.5],0,0)
% colorbar
% title('pressure error')

%% surface basis 

basis_surfac_xy = kron(kron(dtize.basis.hz,dtize.basis.ey),dtize.basis.ex);
basis_surfac_yz = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.hx);
basis_surfac_zx = kron(kron(dtize.basis.ez,dtize.basis.hy),dtize.basis.ex);
% basis_surfac = [basis_surfac_xy; basis_surfac_yz; basis_surfac_zx];

%% reduction of q^(n-1)

% test_func = basis_surfac;

% trial_func = basis_surfac .* www;

qqz_int = test.dpdz(dtize.nodes.x(:),dtize.nodes.y(:),dtize.nodes.z(:))';
qqx_int = test.dpdx(dtize.nodes.x(:),dtize.nodes.y(:),dtize.nodes.z(:))';
qqy_int = test.dpdy(dtize.nodes.x(:),dtize.nodes.y(:),dtize.nodes.z(:))';

dof_zz = reduction(qqz_int, basis_surfac_xy, www, metric);
dof_xx = reduction(qqx_int, basis_surfac_yz, www, metric);
dof_yy = reduction(qqy_int, basis_surfac_zx, www, metric);

% M222x = basis_surfac_yz * basis_surfac_yz';

M222x = construct_mass_matrix(basis_surfac_yz,basis_surfac_yz,www,metric);
M222y = construct_mass_matrix(basis_surfac_zx,basis_surfac_zx,www,metric);
M222z = construct_mass_matrix(basis_surfac_xy,basis_surfac_xy,www,metric);

M222 = blkdiag(M222x,M222y,M222z);
dof = [dof_xx; dof_yy; dof_zz];

dof = M222 \ dof;

% figure
% spy(M222)

%% reconstruction of q^(n-1)

dof_xx = dof(1:p^2*(p+1));
dof_yy = dof(p^2*(p+1)+1:2*p^2*(p+1));
dof_zz = dof(2*p^2*(p+1)+1:3*p^2*(p+1));

rec_basis_surfac_xy = kron(kron(rec.basis.hfz,rec.basis.efy),rec.basis.efx);
rec_basis_surfac_yz = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.hfx);
rec_basis_surfac_zx = kron(kron(rec.basis.efz,rec.basis.hfy),rec.basis.efx);

reconstruction_qzz = rec_basis_surfac_xy' * dof_zz;
reconstruction_qxx = rec_basis_surfac_yz' * dof_xx;
reconstruction_qyy = rec_basis_surfac_zx' * dof_yy;

reconstruction_qzz = reshape(reconstruction_qzz, [pf+1 pf+1 pf+1]);
reconstruction_qxx = reshape(reconstruction_qxx, [pf+1 pf+1 pf+1]);
reconstruction_qyy = reshape(reconstruction_qyy, [pf+1 pf+1 pf+1]);

ext_qxx = test.dpdx(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qyy = test.dpdy(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qzz = test.dpdz(rec.nodes.x,rec.nodes.y,rec.nodes.z);

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx-reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x error')

% --- 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy-reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y error')

% --- 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz-reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z error')

%% construct incidence matrix

E32 = incidence_matrix.dim_3.discrete_divergence(p);

%% LHS

LHS = [M222 E32'; E32 sparse(nr_volum,nr_volum)];

figure
spy(LHS)

RHS(nr_surfc+1:nr_surfc+nr_volum) = dof_f;

RHS=RHS(:);

%% solution 

X = LHS \ RHS;

%% cochains 

dof_p = X(nr_surfc+1:nr_surfc+nr_volum);

dof_p = M333 \ dof_p;

%% reconstruction pressure 

rec_ppp = reconstruction.dim_3.volume(dof_p,rec_basis_volume,pf,1);

ext_ppp = test.phi(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error = sqrt(sum((ext_ppp(:)-rec_ppp(:)).^2 .* wwwf'))

figure
slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec_ppp,[-0.5 0.5],0,0)
colorbar
title('pressure reconstruction')

figure
slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp,[-0.5 0.5],0,0)
colorbar
title('pressure exact')

figure
slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp-rec_ppp,[-0.5 0.5],0,0)
colorbar
title('pressure error')

%% reconstruction flux

