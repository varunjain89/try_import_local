function [dtize] = create_discretization_variables(p)

%% create discretization basis

[x,w] = GLLnodes(p);
[h,e] = MimeticpolyVal(x,p,1);

dtize.basis.hx = h;
dtize.basis.hy = h;
dtize.basis.hz = h;

dtize.basis.ex = e;
dtize.basis.ey = e;
dtize.basis.ez = e;

dtize.weights.wx = w;
dtize.weights.wy = w;
dtize.weights.wz = w;

dtize.nodes.sx = x;
dtize.nodes.sy = x;
dtize.nodes.sz = x;

%% create discretization nodes

[dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta] = meshgrid(dtize.nodes.sx,dtize.nodes.sy,dtize.nodes.sz);

temp1 = dtize.nodes.eta(:)';
temp2 = dtize.nodes.xii(:)';

dtize.nodes.xii = temp1;
dtize.nodes.eta = temp2;
dtize.nodes.zta = dtize.nodes.zta(:)';

[dtize.nodes.x,dtize.nodes.y,dtize.nodes.z] = domain.mapping(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta);

%% discretization jacobian

dtize.jacobian = domain.jacobian(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta);

%% discretization weights

dtize.www = kron(kron(dtize.weights.wz,dtize.weights.wy),dtize.weights.wx);

end

