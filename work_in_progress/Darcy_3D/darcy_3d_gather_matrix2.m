%% script settings

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% test case

% test 1
test1.phi = @(x,y,z) (1-x.^2).*(1-y.^2).*(1-z.^2);

test1.dpdx = @(x,y,z) -2*x.*(1-y.^2).*(1-z.^2);
test1.dpdy = @(x,y,z) -2*y.*(1-x.^2).*(1-z.^2);
test1.dpdz = @(x,y,z) -2*z.*(1-x.^2).*(1-y.^2);

test1.d2pdx2 = @(x,y,z) -2*(1-y.^2).*(1-z.^2);
test1.d2pdy2 = @(x,y,z) -2*(1-x.^2).*(1-z.^2);
test1.d2pdz2 = @(x,y,z) -2*(1-x.^2).*(1-y.^2);

% test 2
test2.phi = @(x,y,z) x.*(1-x).*(1-y.^2).*(1-z.^2);

test2.dpdx = @(x,y,z) (1-2*x).*(1-y.^2).*(1-z.^2);
test2.dpdy = @(x,y,z) -2*y.*x.*(1-x).*(1-z.^2);
test2.dpdz = @(x,y,z) -2*z.*x.*(1-x).*(1-y.^2);

test2.d2pdx2 = @(x,y,z) -2*(1-y.^2).*(1-z.^2);
test2.d2pdy2 = @(x,y,z) -2*x.*(1-x).*(1-z.^2);
test2.d2pdz2 = @(x,y,z) -2*x.*(1-x).*(1-y.^2);

test = test1;
test.fff = @(x,y,z) test.d2pdx2(x,y,z) + test.d2pdy2(x,y,z) + test.d2pdz2(x,y,z);

%% doamin definition and mapping

xbound = [0.7 2.4];
ybound = [-0.3 1.83];
% zbound = [-1.37 0];

% xbound = [-1 1];
% ybound = [-1 1];
zbound = [-1 1];

c = 0.1;

domain.mapping  = @(xii,eta,zta) mesh.dim_3.crazy_cube.mapping(xbound,ybound,zbound,c,xii,eta,zta);
domain.jacobian = @(xii,eta,zta) mesh.dim_3.crazy_cube.jacobian(xbound,ybound,zbound,c,xii,eta,zta);

%% discretization

Kx = 2;
Ky = 5;
Kz = 3;

p = 4;

el_bounds.x = GLLnodes(Kx);
el_bounds.y = GLLnodes(Ky);
el_bounds.z = GLLnodes(Kz);

element.nodes = @(xii,eta,zta,elx,ely,elz) mesh.dim_3.element.mapping_nodes(xii,eta,zta,elx,ely,elz,el_bounds);

%% create discretization nodes & basis

[x,w] = GLLnodes(p);
[h,e] = MimeticpolyVal(x,p,1);

% h = speye(p+1);

dtize.basis.hx = h;
dtize.basis.hy = h;
dtize.basis.hz = h;

dtize.basis.ex = e;
dtize.basis.ey = e;
dtize.basis.ez = e;

dtize.weights.wx = w;
dtize.weights.wy = w;
dtize.weights.wz = w;

dtize.nodes.sx = x;
dtize.nodes.sy = x;
dtize.nodes.sz = x;

%% random variables

ttl_nr_el = Kx * Ky * Kz;

nr_surfc = 3*p^2*(p+1);
nr_volum = p^3;
ttl_loc_dof = nr_surfc+nr_volum;
nr_local_lambda = 6*p^2;

nr_lambda = (Kx+1)*Ky*Kz*p^2 + (Ky+1)*Kx*Kz*p^2 + (Kz+1)*Kx*Ky*p^2;

%% create discretization jacobian and nodes

[dtize.nodes.eta, dtize.nodes.xii, dtize.nodes.zta] = meshgrid(dtize.nodes.sx,dtize.nodes.sy,dtize.nodes.sz);

dtize.nodes.xii = dtize.nodes.xii(:)';
dtize.nodes.eta = dtize.nodes.eta(:)';
dtize.nodes.zta = dtize.nodes.zta(:)';

dtize.www = kron(kron(dtize.weights.wz,dtize.weights.wy),dtize.weights.wx);

for i = 1:Kx
    for j = 1:Ky
        for k = 1:Kz
            eleid = (k-1)*Kx*Ky + (j-1)*Kx + i;
            [dtize.nodes.xii2(:,:,eleid),dtize.nodes.eta2(:,:,eleid),dtize.nodes.zta2(:,:,eleid)] = element.nodes(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta,i,j,k);
        end
    end
end

[dtize.nodes.x,dtize.nodes.y,dtize.nodes.z] = domain.mapping(dtize.nodes.xii2, dtize.nodes.eta2, dtize.nodes.zta2);

dtize.jacobian = domain.jacobian(dtize.nodes.xii2, dtize.nodes.eta2, dtize.nodes.zta2);

dXii_ds = 0.5*(el_bounds.x(2:end) - el_bounds.x(1:end-1));
dEta_dt = 0.5*(el_bounds.y(2:end) - el_bounds.y(1:end-1));
dZta_du = 0.5*(el_bounds.z(2:end) - el_bounds.z(1:end-1));

[d1, d2, d3] = meshgrid(dXii_ds,dEta_dt,dZta_du);


d1 = permute(d1,[2 1 3]);
d1 = d1(:)';

d2 = permute(d2,[2 1 3]);
d2 = d2(:)';

d3 = d3(:)';

dtize.jacobian.dXdxii = dtize.jacobian.dXdxii .* reshape(d1, [1 1 ttl_nr_el]);
dtize.jacobian.dYdxii = dtize.jacobian.dYdxii .* reshape(d1, [1 1 ttl_nr_el]);
dtize.jacobian.dZdxii = dtize.jacobian.dZdxii .* reshape(d1, [1 1 ttl_nr_el]);

dtize.jacobian.dXdeta = dtize.jacobian.dXdeta .* reshape(d2, [1 1 ttl_nr_el]);
dtize.jacobian.dYdeta = dtize.jacobian.dYdeta .* reshape(d2, [1 1 ttl_nr_el]);
dtize.jacobian.dZdeta = dtize.jacobian.dZdeta .* reshape(d2, [1 1 ttl_nr_el]);

dtize.jacobian.dXdzta = dtize.jacobian.dXdzta .* reshape(d3, [1 1 ttl_nr_el]);
dtize.jacobian.dYdzta = dtize.jacobian.dYdzta .* reshape(d3, [1 1 ttl_nr_el]);
dtize.jacobian.dZdzta = dtize.jacobian.dZdzta .* reshape(d3, [1 1 ttl_nr_el]);
    
% for eln = 1:ttl_nr_el
%     dtize.jacobian.dXdxii(:,:,eln) = dtize.jacobian.dXdxii(:,:,eln) * d1(eln);
%     dtize.jacobian.dYdxii(:,:,eln) = dtize.jacobian.dYdxii(:,:,eln) * d1(eln);
%     dtize.jacobian.dZdxii(:,:,eln) = dtize.jacobian.dZdxii(:,:,eln) * d1(eln);
%     
%     dtize.jacobian.dXdeta(:,:,eln) = dtize.jacobian.dXdeta(:,:,eln) * d2(eln);
%     dtize.jacobian.dYdeta(:,:,eln) = dtize.jacobian.dYdeta(:,:,eln) * d2(eln);
%     dtize.jacobian.dZdeta(:,:,eln) = dtize.jacobian.dZdeta(:,:,eln) * d2(eln);
%     
%     dtize.jacobian.dXdzta(:,:,eln) = dtize.jacobian.dXdzta(:,:,eln) * d3(eln);
%     dtize.jacobian.dYdzta(:,:,eln) = dtize.jacobian.dYdzta(:,:,eln) * d3(eln);
%     dtize.jacobian.dZdzta(:,:,eln) = dtize.jacobian.dZdzta(:,:,eln) * d3(eln);
% end

%% create discretization metric terms

dtize.metric.ggg = metric.dim_3.determinant_jacobian(dtize.jacobian);
dtize.metric.invg = 1 ./dtize.metric.ggg;

dtize.metric = metric.dim_3.poisson_metric(dtize.jacobian, dtize.metric.invg);
dtize.metric.ggg = metric.dim_3.determinant_jacobian(dtize.jacobian);
dtize.metric.invg = 1 ./dtize.metric.ggg;

%% create basis volume

basis_volume = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.ex);

%% reduction of f

M333 = zeros(nr_volum, nr_volum, ttl_nr_el);

for eln = 1:ttl_nr_el
    M333(:,:,eln) = construct_mass_matrix(basis_volume,basis_volume,dtize.www,dtize.metric.invg(:,:,eln));
end

ff_int = test.fff(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);

dof_f = zeros(nr_volum,1,ttl_nr_el);

for eln = 1:ttl_nr_el
    dof_f(:,1,eln) = M333(:,:,eln) \ reduction(ff_int(:,:,eln), basis_volume, dtize.www, 1);
end

%% reduction of p

% pp_int = test.phi(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
% red_p  = M333 \ reduction(pp_int, basis_volume, dtize.www, 1);

%% reconstruction

pf = p+5;

%% create reconstruction nodes & basis

[xf,wf] = GLLnodes(pf);
[hf,ef] = MimeticpolyVal(xf,p,1);

rec.basis.hfx = hf;
rec.basis.hfy = hf;
rec.basis.hfz = hf;

rec.basis.efx = ef;
rec.basis.efy = ef;
rec.basis.efz = ef;

rec.weights.wfx = wf;
rec.weights.wfy = wf;
rec.weights.wfz = wf;

rec.nodes.sfx = xf;
rec.nodes.sfy = xf;
rec.nodes.sfz = xf;

%% create reconstruction jacobian and nodes

[rec.nodes.eta,rec.nodes.xii,rec.nodes.zta] = meshgrid(rec.nodes.sfx,rec.nodes.sfy,rec.nodes.sfz);

rec.nodes.xii = rec.nodes.xii(:)';
rec.nodes.eta = rec.nodes.eta(:)';
rec.nodes.zta = rec.nodes.zta(:)';

for i = 1:Kx
    for j = 1:Ky
        for k = 1:Kz
            eleid = (k-1)*Kx*Ky + (j-1)*Kx + i;
            [rec.nodes.xii2(:,:,eleid),rec.nodes.eta2(:,:,eleid),rec.nodes.zta2(:,:,eleid)] = element.nodes(rec.nodes.xii, rec.nodes.eta, rec.nodes.zta,i,j,k);
        end
    end
end

[rec.nodes.x,rec.nodes.y,rec.nodes.z] = domain.mapping(rec.nodes.xii2,rec.nodes.eta2,rec.nodes.zta2);

rec.jacobian = domain.jacobian(rec.nodes.xii2, rec.nodes.eta2, rec.nodes.zta2);

rec.jacobian.dXdxii = rec.jacobian.dXdxii .* reshape(d1, [1 1 ttl_nr_el]);
rec.jacobian.dYdxii = rec.jacobian.dYdxii .* reshape(d1, [1 1 ttl_nr_el]);
rec.jacobian.dZdxii = rec.jacobian.dZdxii .* reshape(d1, [1 1 ttl_nr_el]);

rec.jacobian.dXdeta = rec.jacobian.dXdeta .* reshape(d2, [1 1 ttl_nr_el]);
rec.jacobian.dYdeta = rec.jacobian.dYdeta .* reshape(d2, [1 1 ttl_nr_el]);
rec.jacobian.dZdeta = rec.jacobian.dZdeta .* reshape(d2, [1 1 ttl_nr_el]);

rec.jacobian.dXdzta = rec.jacobian.dXdzta .* reshape(d3, [1 1 ttl_nr_el]);
rec.jacobian.dYdzta = rec.jacobian.dYdzta .* reshape(d3, [1 1 ttl_nr_el]);
rec.jacobian.dZdzta = rec.jacobian.dZdzta .* reshape(d3, [1 1 ttl_nr_el]);

% for eln = 1:ttl_nr_el
%     rec.jacobian.dXdxii(:,:,eln) = rec.jacobian.dXdxii(:,:,eln) * d1(eln);
%     rec.jacobian.dYdxii(:,:,eln) = rec.jacobian.dYdxii(:,:,eln) * d1(eln);
%     rec.jacobian.dZdxii(:,:,eln) = rec.jacobian.dZdxii(:,:,eln) * d1(eln);
%     
%     rec.jacobian.dXdeta(:,:,eln) = rec.jacobian.dXdeta(:,:,eln) * d2(eln);
%     rec.jacobian.dYdeta(:,:,eln) = rec.jacobian.dYdeta(:,:,eln) * d2(eln);
%     rec.jacobian.dZdeta(:,:,eln) = rec.jacobian.dZdeta(:,:,eln) * d2(eln);
%     
%     rec.jacobian.dXdzta(:,:,eln) = rec.jacobian.dXdzta(:,:,eln) * d3(eln);
%     rec.jacobian.dYdzta(:,:,eln) = rec.jacobian.dYdzta(:,:,eln) * d3(eln);
%     rec.jacobian.dZdzta(:,:,eln) = rec.jacobian.dZdzta(:,:,eln) * d3(eln);
% end

% determinant of jacobian
rec.metric.ggg = metric.dim_3.determinant_jacobian(rec.jacobian);
rec.metric.invg = 1 ./rec.metric.ggg;

rec.wwwf = kron(kron(rec.weights.wfz,rec.weights.wfy),rec.weights.wfx);

%% create reconstruction basis and nodes - II

rec_basis_volume = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.efx);

%% reconstruction of f

rec.metric.ggg = metric.dim_3.determinant_jacobian(rec.jacobian);
rec.metric.invg = 1 ./rec.metric.ggg;

for eln = 1:ttl_nr_el
    rec.fff(:,:,eln) = reconstruction.dim_3.volume(dof_f(:,:,eln),rec_basis_volume,pf,rec.metric.ggg(:,:,eln))';
end

ext_fff = test.fff(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error_fff = sqrt(sum(sum((ext_fff-rec.fff).^2 .* rec.metric.invg .* rec.wwwf)))

% reconstruction = reshape(reconstruction, [pf+1 pf+1 pf+1]);

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec.fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure reconstruction')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure exact')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff-rec.fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure error')

%% surface basis

basis_surfac_yz = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.hx);
basis_surfac_zx = kron(kron(dtize.basis.ez,dtize.basis.hy),dtize.basis.ex);
basis_surfac_xy = kron(kron(dtize.basis.hz,dtize.basis.ey),dtize.basis.ex);

% basis_surfac = [basis_surfac_xy; basis_surfac_yz; basis_surfac_zx];

%% reduction of q^(n-1)

qqx_int = test.dpdx(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
qqy_int = test.dpdy(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
qqz_int = test.dpdz(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);

red.metric.x = qqx_int .* dtize.jacobian.dXdxii + qqy_int .* dtize.jacobian.dYdxii + qqz_int .* dtize.jacobian.dZdxii;
red.metric.y = qqx_int .* dtize.jacobian.dXdeta + qqy_int .* dtize.jacobian.dYdeta + qqz_int .* dtize.jacobian.dZdeta;
red.metric.z = qqx_int .* dtize.jacobian.dXdzta + qqy_int .* dtize.jacobian.dYdzta + qqz_int .* dtize.jacobian.dZdzta;

for eln = 1:ttl_nr_el
    dof_xx(:,:,eln) = reduction(red.metric.x(:,:,eln), basis_surfac_yz, dtize.www, 1);
    dof_yy(:,:,eln) = reduction(red.metric.y(:,:,eln), basis_surfac_zx, dtize.www, 1);
    dof_zz(:,:,eln) = reduction(red.metric.z(:,:,eln), basis_surfac_xy, dtize.www, 1);
end

for eln = 1:ttl_nr_el
    M222.x1(:,:,eln) = construct_mass_matrix(basis_surfac_yz,basis_surfac_yz,dtize.www,dtize.metric.g11(:,:,eln));
    M222.x2(:,:,eln) = construct_mass_matrix(basis_surfac_yz,basis_surfac_zx,dtize.www,dtize.metric.g12(:,:,eln));
    M222.x3(:,:,eln) = construct_mass_matrix(basis_surfac_yz,basis_surfac_xy,dtize.www,dtize.metric.g13(:,:,eln));
    
    M222.y1(:,:,eln) = construct_mass_matrix(basis_surfac_zx,basis_surfac_yz,dtize.www,dtize.metric.g21(:,:,eln));
    M222.y2(:,:,eln) = construct_mass_matrix(basis_surfac_zx,basis_surfac_zx,dtize.www,dtize.metric.g22(:,:,eln));
    M222.y3(:,:,eln) = construct_mass_matrix(basis_surfac_zx,basis_surfac_xy,dtize.www,dtize.metric.g23(:,:,eln));
    
    M222.z1(:,:,eln) = construct_mass_matrix(basis_surfac_xy,basis_surfac_yz,dtize.www,dtize.metric.g31(:,:,eln));
    M222.z2(:,:,eln) = construct_mass_matrix(basis_surfac_xy,basis_surfac_zx,dtize.www,dtize.metric.g32(:,:,eln));
    M222.z3(:,:,eln) = construct_mass_matrix(basis_surfac_xy,basis_surfac_xy,dtize.www,dtize.metric.g33(:,:,eln));
end

% M222 = blkdiag(M222x1,M222y2,M222z3);

M22 = [M222.x1 M222.x2 M222.x3; M222.y1 M222.y2 M222.y3; M222.z1 M222.z2 M222.z3];

dof_q = [dof_xx; dof_yy; dof_zz];

for eln = 1:ttl_nr_el
    dof_q(:,:,eln) = M22(:,:,eln) \ dof_q(:,:,eln);
end

% figure
% spy(M222)

%% reconstruction of q^(n-1)

% dof_xx = dof_q(1:p^2*(p+1));
% dof_yy = dof_q(p^2*(p+1)+1:2*p^2*(p+1));
% dof_zz = dof_q(2*p^2*(p+1)+1:3*p^2*(p+1));

rec.basis.surfac.xy = kron(kron(rec.basis.hfz,rec.basis.efy),rec.basis.efx);
rec.basis.surfac.yz = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.hfx);
rec.basis.surfac.zx = kron(kron(rec.basis.efz,rec.basis.hfy),rec.basis.efx);

for eln = 1:ttl_nr_el
    local_jac.dXdxii = rec.jacobian.dXdxii(:,:,eln);
    local_jac.dXdeta = rec.jacobian.dXdeta(:,:,eln);
    local_jac.dXdzta = rec.jacobian.dXdzta(:,:,eln);
    local_jac.dYdxii = rec.jacobian.dYdxii(:,:,eln);
    local_jac.dYdeta = rec.jacobian.dYdeta(:,:,eln);
    local_jac.dYdzta = rec.jacobian.dYdzta(:,:,eln);
    local_jac.dZdxii = rec.jacobian.dZdxii(:,:,eln);
    local_jac.dZdeta = rec.jacobian.dZdeta(:,:,eln);
    local_jac.dZdzta = rec.jacobian.dZdzta(:,:,eln);
             
    [rec.u.ux(:,:,eln), rec.u.uy(:,:,eln), rec.u.uz(:,:,eln)] = reconstruction.dim_3.surface(dof_q(:,:,eln),local_jac,p,rec.metric.invg(:,:,eln),rec.basis.surfac);
end

% reconstruction_qzz = reshape(reconstruction_qzz, [pf+1 pf+1 pf+1]);
% reconstruction_qxx = reshape(reconstruction_qxx, [pf+1 pf+1 pf+1]);
% reconstruction_qyy = reshape(reconstruction_qyy, [pf+1 pf+1 pf+1]);

ext_qxx = test.dpdx(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qyy = test.dpdy(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qzz = test.dpdz(rec.nodes.x,rec.nodes.y,rec.nodes.z);

%% errors

error_x = sqrt(sum(sum((ext_qxx-rec.u.ux).^2 .* rec.metric.invg .* rec.wwwf)));
error_y = sqrt(sum(sum((ext_qyy-rec.u.uy).^2 .* rec.metric.invg .* rec.wwwf)));
error_z = sqrt(sum(sum((ext_qzz-rec.u.uz).^2 .* rec.metric.invg .* rec.wwwf)));

%% velocity plots
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x reconstruction')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x exact')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx-reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x error')

% % ---
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y reconstruction')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y exact')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy-reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y error')
%
% % ---
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z reconstruction')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z exact')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz-reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z error')

%% construct incidence matrix

E32 = incidence_matrix.dim_3.discrete_divergence(p);

%% map local boundary face and global boundary face

% left face
for j = 1:p
    for k = 1:p
        local_dof = (k-1)*p + j;
        globl_dof_left(local_dof) = (k-1)*p*(p+1) + (j-1)*(p+1) + 1;
    end
end

% right face
for j = 1:p
    for k = 1:p
        local_dof = (k-1)*p + j;
        globl_dof_rght(local_dof) = (k-1)*p*(p+1) + (j-1)*(p+1) + p + 1;
    end
end

% bottom face

for i = 1:p
    for k = 1:p
        local_dof = (k-1)*p + i;
        globl_dof_bttm(local_dof) = p^2*(p+1) + (k-1)*p*(p+1) + i;
    end
end

% top face

for i = 1:p
    for k = 1:p
        local_dof = (k-1)*p + i;
        globl_dof_topp(local_dof) = p^2*(p+1) + (k-1)*p*(p+1) + i + p^2;
    end
end

% back face

for i = 1:p
    for j = 1:p
        local_dof = (j-1)*p+i;
        globl_dof_back(local_dof) = 2*p^2*(p+1) + local_dof;
    end
end

% front face

for i = 1:p
    for j = 1:p
        local_dof = (j-1)*p+i;
        globl_dof_frnt(local_dof) = 2*p^2*(p+1) + local_dof + p^3;
    end
end

%% local connectivity matrix

N.left = -1*ones(1,p^2);
N.rght = +1*ones(1,p^2);

N.bttm = -1*ones(1,p^2);
N.topp = +1*ones(1,p^2);

N.back = -1*ones(1,p^2);
N.frnt = +1*ones(1,p^2);

NN = [N.rght N.topp N.frnt N.left N.bttm N.back];

local_bndry_dof = [globl_dof_rght globl_dof_topp globl_dof_frnt globl_dof_left globl_dof_bttm globl_dof_back];

local_N = sparse(1:nr_local_lambda,local_bndry_dof,NN,nr_local_lambda,ttl_loc_dof);
local_N_3d = repmat(full(local_N),[1 1 ttl_nr_el]);
% figure
% spy(local_N)

%% assemble connectivity matrix 

gather_lambda = gather_matrix.dim_3.lambda_pressure(Kx,Ky,Kz,p);
gather_local_dof = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
gather_local_dof = gather_local_dof';

EN = AssembleMatrices2(gather_lambda, gather_local_dof, local_N_3d);

% figure
% spy(EN)

%% local systems

for eln = 1:ttl_nr_el
%     local_LHS(:,:,eln) = [M22(:,:,eln) E32'; E32 sparse(nr_volum, nr_volum)];
    local_LHS(eln) = {[M22(:,:,eln) E32'; E32 sparse(nr_volum,nr_volum)]};
end

%% inverse of local LHS

for eln = 1:ttl_nr_el
    inv_local_LHS(eln) = {inv(local_LHS{eln})};
end

%% RHS

RHS(nr_surfc+1:nr_surfc+nr_volum,:,:) = dof_f;

RHS2 = permute(RHS,[2 1 3]);
RHS2 = RHS2(:);

%% dirichlet boundary conditions

%% left face

basis_left_face = kron(dtize.basis.ez,dtize.basis.ey);

[nodes_left_face.xii,nodes_left_face.eta,nodes_left_face.zta] = meshgrid(-1,dtize.nodes.sy,dtize.nodes.sz);

nodes_left_face.xii = nodes_left_face.xii(:)';
nodes_left_face.eta = nodes_left_face.eta(:)';
nodes_left_face.zta = nodes_left_face.zta(:)';

% get new ref nodes
[nodes_left_face.xii2,nodes_left_face.eta2,nodes_left_face.zta2] = new_ref_nodes(1,1:Ky,1:Kz,nodes_left_face.xii,nodes_left_face.eta,nodes_left_face.zta,Ky,element.nodes);

[nodes_left_face.xxx,nodes_left_face.yyy,nodes_left_face.zzz] = domain.mapping(nodes_left_face.xii2,nodes_left_face.eta2,nodes_left_face.zta2);

phi_left_face = test.phi(nodes_left_face.xxx,nodes_left_face.yyy,nodes_left_face.zzz);

w88_left_face = kron(dtize.weights.wz,dtize.weights.wy);

nn_left = -1;

for eln = 1:Ky*Kz
    boundary_dof.left_face(:,eln) = nn_left * reduction(phi_left_face(:,:,eln), basis_left_face, w88_left_face, 1);
end

%% right face

basis_rght_face = kron(dtize.basis.ez,dtize.basis.ey);

[nodes_rght_face.xii,nodes_rght_face.eta,nodes_rght_face.zta] = meshgrid(+1,dtize.nodes.sy,dtize.nodes.sz);

nodes_rght_face.xii = nodes_rght_face.xii(:)';
nodes_rght_face.eta = nodes_rght_face.eta(:)';
nodes_rght_face.zta = nodes_rght_face.zta(:)';

% get new ref nodes
[nodes_rght_face.xii2,nodes_rght_face.eta2,nodes_rght_face.zta2] = new_ref_nodes(Kx,1:Ky,1:Kz,nodes_rght_face.xii,nodes_rght_face.eta,nodes_rght_face.zta,Ky,element.nodes);

[nodes_rght_face.xxx,nodes_rght_face.yyy,nodes_rght_face.zzz] = domain.mapping(nodes_rght_face.xii2,nodes_rght_face.eta2,nodes_rght_face.zta2);

phi_rght_face = test.phi(nodes_rght_face.xxx,nodes_rght_face.yyy,nodes_rght_face.zzz);

w88_rght_face = kron(dtize.weights.wz,dtize.weights.wy);

nn_rght = +1;

for eln = 1:Ky*Kz
    boundary_dof.rght_face(:,eln) = nn_rght * reduction(phi_rght_face(:,:,eln), basis_rght_face, w88_rght_face, 1); 
end

%% bottom face

basis_bttm_face = kron(dtize.basis.ez,dtize.basis.ex);
[nodes_bttm_face.xii,nodes_bttm_face.eta,nodes_bttm_face.zta] = meshgrid(dtize.nodes.sx,-1,dtize.nodes.sz);

nodes_bttm_face.xii = nodes_bttm_face.xii(:)';
nodes_bttm_face.eta = nodes_bttm_face.eta(:)';
nodes_bttm_face.zta = nodes_bttm_face.zta(:)';

% get new ref nodes
[nodes_bttm_face.xii2,nodes_bttm_face.eta2,nodes_bttm_face.zta2] = new_ref_nodes(1:Kx,1,1:Kz,nodes_bttm_face.xii,nodes_bttm_face.eta,nodes_bttm_face.zta,Kx,element.nodes);

for i = 1:Kx
    for k = 1:Kz
        local_eleid = (k-1)*Kx + i;
        [dtize.nodes_bttm_face.xii2(:,:,local_eleid),dtize.nodes_bttm_face.eta2(:,:,local_eleid),dtize.nodes_bttm_face.zta2(:,:,local_eleid)] = element.nodes(nodes_bttm_face.xii,nodes_bttm_face.eta,nodes_bttm_face.zta,i,1,k);
    end
end

w88_bttm_face = kron(dtize.weights.wz,dtize.weights.wx);

nn_bttm = -1;

boundary_dof.bttm_face = generate_boundary_dof(dtize.nodes_bttm_face, domain.mapping, test.phi, basis_bttm_face, w88_bttm_face, nn_bttm, Kx*Kz);

%% top face

basis_topp_face = kron(dtize.basis.ez,dtize.basis.ex);

[nodes_topp_face.xii,nodes_topp_face.eta,nodes_topp_face.zta] = meshgrid(dtize.nodes.sx,+1,dtize.nodes.sz);

nodes_topp_face.xii = nodes_topp_face.xii(:)';
nodes_topp_face.eta = nodes_topp_face.eta(:)';
nodes_topp_face.zta = nodes_topp_face.zta(:)';

% get new ref nodes
[nodes_rght_face.xii2,nodes_rght_face.eta2,nodes_rght_face.zta2] = new_ref_nodes(Kx,1:Ky,1:Kz,nodes_rght_face.xii,nodes_rght_face.eta,nodes_rght_face.zta,Ky,element.nodes);

for i = 1:Kx
    for k = 1:Kz
        local_eleid = (k-1)*Kx + i;
        [dtize.nodes_topp_face.xii2(:,:,local_eleid),dtize.nodes_topp_face.eta2(:,:,local_eleid),dtize.nodes_topp_face.zta2(:,:,local_eleid)] = element.nodes(nodes_topp_face.xii,nodes_topp_face.eta,nodes_topp_face.zta,i,Ky,k);
    end
end

w88_topp_face = kron(dtize.weights.wz,dtize.weights.wx);

nn_topp = +1;

boundary_dof.topp_face = generate_boundary_dof(dtize.nodes_topp_face, domain.mapping, test.phi, basis_topp_face, w88_topp_face, nn_topp, Kx*Kz);

%% back face

basis_back_face = kron(dtize.basis.ey,dtize.basis.ex);

[nodes_back_face.eta,nodes_back_face.xii] = meshgrid(dtize.nodes.sx,dtize.nodes.sy,-1);

nodes_back_face.xii = nodes_back_face.xii(:)';
nodes_back_face.eta = nodes_back_face.eta(:)';

[nodes_back_face.xxx,nodes_back_face.yyy,nodes_back_face.zzz] = domain.mapping(nodes_back_face.xii,nodes_back_face.eta,-1);

phi_back_face = test.phi(nodes_back_face.xxx,nodes_back_face.yyy,nodes_back_face.zzz);

w88_back_face = kron(dtize.weights.wy,dtize.weights.wx);

nn_back = -1;
dof_back_face = nn_back * reduction(phi_back_face, basis_back_face, w88_back_face, 1);

%% front face

basis_frnt_face = kron(dtize.basis.ey,dtize.basis.ex);

[nodes_frnt_face.eta,nodes_frnt_face.xii] = meshgrid(dtize.nodes.sx,dtize.nodes.sy);

nodes_frnt_face.xii = nodes_frnt_face.xii(:)';
nodes_frnt_face.eta = nodes_frnt_face.eta(:)';

[nodes_frnt_face.xxx,nodes_frnt_face.yyy,nodes_frnt_face.zzz] = domain.mapping(nodes_frnt_face.xii,nodes_frnt_face.eta,+1);

phi_frnt_face = test.phi(nodes_frnt_face.xxx,nodes_frnt_face.yyy,nodes_frnt_face.zzz);

w88_frnt_face = kron(dtize.weights.wy,dtize.weights.wx);

nn_frnt = +1;
dof_frnt_face = nn_frnt * reduction(phi_frnt_face, basis_frnt_face, w88_frnt_face, 1);

%% add Dirichlet BCs' 

% RHS(globl_dof_left) = dof_left_face;
% RHS(globl_dof_rght) = dof_rght_face;
% RHS(globl_dof_bttm) = dof_bttm_face;
% RHS(globl_dof_topp) = dof_topp_face;
% RHS(globl_dof_back) = dof_back_face;
% RHS(globl_dof_frnt) = dof_frnt_face;

% left 
for j = 1:Ky
    for k = 1:Kz
        local_eleid = (k-1)*Ky + j;
        globl_eleid = (k-1)*Kx*Ky + (j-1)*Kx + 1;
        RHS2(gather_local_dof(globl_dof_left,globl_eleid),1) = boundary_dof.left_face(:,local_eleid);
    end
end

% right
for j = 1:Ky
    for k = 1:Kz
        local_eleid = (k-1)*Ky + j;
        globl_eleid = (k-1)*Kx*Ky + (j-1)*Kx + Kx;
        RHS2(gather_local_dof(globl_dof_rght,globl_eleid),1) = boundary_dof.rght_face(:,local_eleid);
    end
end

% bottom 
for i = 1:Kx
    for k = 1:Kz
        local_eleid = (k-1)*Kx + i;
        globl_eleid = (k-1)*Kx*Ky + (1-1)*Kx + i;
        RHS2(gather_local_dof(globl_dof_bttm,globl_eleid),1) = boundary_dof.bttm_face(:,local_eleid);
    end
end

% top
for i = 1:Kx
    for k = 1:Kz
        local_eleid = (k-1)*Kx + i;
        globl_eleid = (k-1)*Kx*Ky + (Ky-1)*Kx + i;
        RHS2(gather_local_dof(globl_dof_topp,globl_eleid),1) = boundary_dof.topp_face(:,local_eleid);
    end
end

% back
% for i = 1:Kx
%     for j = 1:Ky
%         local_eleid = (j-1)*Ky + i;
%         globl_eleid = (1-1)*Kx*Ky + (j-1)*Kx + i;
%         RHS2(gather_local_dof(globl_dof_back,globl_eleid),1) = boundary_dof.back_face(:,local_eleid);
%     end
% end

% front
% for i = 1:Kx
%     for j = 1:Ky
%         local_eleid = (j-1)*Ky + i;
%         globl_eleid = (Kz-1)*Kx*Ky + (j-1)*Kx + i;
%         RHS2(gather_local_dof(globl_dof_frnt,globl_eleid),1) = boundary_dof.frnt_face(:,local_eleid);
%     end
% end

%% add Neumann BCs'

% Neumann_dof = 

BCn = sparse(nr_lambda,1);

%% remove Dirichlet boundary conditions from LHS and RHS

% define lambda dirichlet 

nr_int_lambda = (Kx-1)*Ky*Kz*p^2 + (Ky-1)*Kx*Kz*p^2 + (Kz-1)*Kx*Ky*p^2;

lambda_Dirichlet = nr_int_lambda+1 : nr_lambda;

% remove dirichlet lambda from EN-rows and BCn rows

EN(lambda_Dirichlet,:) = [];
BCn(lambda_Dirichlet,:) = [];

nr_boundary_lambda = 2*Ky*Kz*p^2 + 2*Kz*Kx*p^2 + 2*Kx*Ky*p^2;

%% solution

A11 = blkdiag(inv_local_LHS{:});

% figure
% spy(A11)

lambda = (EN * A11 * EN') \ (EN * A11 * RHS2 - BCn);

%% local solutions 

% add dirichlet boundaries back 

lambda = [lambda; zeros(nr_boundary_lambda,1)];

% add left boundaries
left_face = 3*p^2+1:4*p^2;

for j = 1:Ky
    for k = 1:Kz
        local_eleid = (k-1)*Ky + j;
        globl_eleid = (k-1)*Kx*Ky + (j-1)*Kx + 1;        
        lambda(gather_lambda(left_face,globl_eleid),1) = boundary_dof.left_face(:,local_eleid);
    end
end

% add right boundaries
rght_face = 1:p^2;

for j = 1:Ky
    for k = 1:Kz
        local_eleid = (k-1)*Ky + j;
        globl_eleid = (k-1)*Kx*Ky + (j-1)*Kx + Kx;
        lambda(gather_lambda(rght_face,globl_eleid),1) = -boundary_dof.rght_face(:,local_eleid);
    end
end

% add bottom boundaries
bttm_face = 4*p^2+1:5*p^2;

for i = 1:Kx
    for k = 1:Kz
        local_eleid = (k-1)*Kx + i;
        globl_eleid = (k-1)*Kx*Ky + (1-1)*Kx + i;
        lambda(gather_lambda(bttm_face,globl_eleid),1) = boundary_dof.bttm_face(:,local_eleid);
    end
end

% add topp boundaries
topp_face = p^2+1:2*p^2;

for i = 1:Kx
    for k = 1:Kz
        local_eleid = (k-1)*Kx + i;
        globl_eleid = (k-1)*Kx*Ky + (Ky-1)*Kx + i;
        lambda(gather_lambda(topp_face,globl_eleid),1) = -boundary_dof.topp_face(:,local_eleid);
    end
end

% add back face
back_face = 5*p^2+1:6*p^2;

% for i = 1:Kx
%     for j = 1:Ky
%         local_eleid = (j-1)*Kx + i;
%         globl_eleid = (1-1)*Kx*Ky + (j-1)*Kx + i;
%         lambda(gather_lambda(back_face,globl_eleid),1) = boundary_dof.back_face(:,local_eleid);
%     end
% end

% add front face
frnt_face = 2*p^2+1:3*p^2;

% for i = 1:Kx
%     for j = 1:Ky
%         local_eleid = (j-1)*Kx + i;
%         globl_eleid = (Kz-1)*Kx*Ky + (j-1)*Kx + i;
%         lambda(gather_lambda(frnt_face,globl_eleid),1) = boundary_dof.frnt_face(:,local_eleid);
%     end
% end

% sort lambda

for eln = 1:ttl_nr_el
    lambda_h(:,eln) = lambda(gather_lambda(:,eln));
end

% solve local system 

for eln = 1:ttl_nr_el
    X(:,eln) = inv_local_LHS{eln} * (RHS(:,:,eln) - local_N'*lambda_h(:,eln));
end

%% cochains

dof_p = X(nr_surfc+1:nr_surfc+nr_volum,:);

for eln = 1:ttl_nr_el
    dof_p(:,eln) = M333(:,:,eln) \ dof_p(:,eln);
end

%% reconstruction pressure

for eln = 1:ttl_nr_el
    rec.ppp(:,:,eln) = reconstruction.dim_3.volume(dof_p(:,eln),rec_basis_volume,pf,rec.metric.ggg(:,:,eln))';
end

ext_ppp = test.phi(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error_ppp = sqrt(sum(sum((ext_ppp-rec.ppp).^2 .* rec.metric.invg .* rec.wwwf)))

ppp = sum((ext_ppp-rec.ppp).^2 .* rec.metric.invg .* rec.wwwf);


% ext_ppp = reshape(ext_ppp, [pf+1 pf+1 pf+1]);
% rec.nodes.x = reshape(rec.nodes.x, [pf+1 pf+1 pf+1]);
% rec.nodes.y = reshape(rec.nodes.y, [pf+1 pf+1 pf+1]);
% rec.nodes.z = reshape(rec.nodes.z, [pf+1 pf+1 pf+1]);

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure reconstruction')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure exact')
%
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp-rec_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure error')

%% reconstruction flux

% [rec.u.ux, rec.u.uy, rec.u.uz] = reconstruction.dim_3.surface(X(1:nr_surfc,1),rec.jacobian,p, rec.metric.invg, rec.basis.surfac);

% errors

% error_x = sqrt(sum((ext_qxx-rec.u.ux).^2 .* rec.metric.invg .* rec.wwwf))
% error_y = sqrt(sum((ext_qyy-rec.u.uy).^2 .* rec.metric.invg .* rec.wwwf))
% error_z = sqrt(sum((ext_qzz-rec.u.uz).^2 .* rec.metric.invg .* rec.wwwf))
