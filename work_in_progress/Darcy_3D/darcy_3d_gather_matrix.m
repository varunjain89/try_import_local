%% script settings

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% test case

% test 1
test1.phi = @(x,y,z) (1-x.^2).*(1-y.^2).*(1-z.^2);

test1.dpdx = @(x,y,z) -2*x.*(1-y.^2).*(1-z.^2);
test1.dpdy = @(x,y,z) -2*y.*(1-x.^2).*(1-z.^2);
test1.dpdz = @(x,y,z) -2*z.*(1-x.^2).*(1-y.^2);

test1.d2pdx2 = @(x,y,z) -2*(1-y.^2).*(1-z.^2);
test1.d2pdy2 = @(x,y,z) -2*(1-x.^2).*(1-z.^2);
test1.d2pdz2 = @(x,y,z) -2*(1-x.^2).*(1-y.^2);

% test 2
% test2.phi = @(x,y,z) x.*(1-x).*(1-y.^2).*(1-z.^2);
% 
% test2.dpdx = @(x,y,z) (1-2*x).*(1-y.^2).*(1-z.^2);
% test2.dpdy = @(x,y,z) -2*y.*x.*(1-x).*(1-z.^2);
% test2.dpdz = @(x,y,z) -2*z.*x.*(1-x).*(1-y.^2);
% 
% test2.d2pdx2 = @(x,y,z) -2*(1-y.^2).*(1-z.^2);
% test2.d2pdy2 = @(x,y,z) -2*x.*(1-x).*(1-z.^2);
% test2.d2pdz2 = @(x,y,z) -2*x.*(1-x).*(1-y.^2);

test = test1;
test.fff = @(x,y,z) test.d2pdx2(x,y,z) + test.d2pdy2(x,y,z) + test.d2pdz2(x,y,z);

%% doamin definition and mapping 

xbound = [0.7 2.4];
ybound = [-0.3 1.83];
zbound = [-1.37 0];

% xbound = [-1 1];
% ybound = [-1 1];
% zbound = [-1 1];

c = 0.0;

%% discretization

Kx = 1;
Ky = 1;
Kz = 1;

p = 3;

el_bounds.x = linspace(-1,1,Kx+1);
el_bounds.y = linspace(-1,1,Ky+1);
el_bounds.z = linspace(-1,1,Kz+1);

domain.mapping = @(xii,eta,zta) mesh.dim_3.crazy_cube.mapping(xbound,ybound,zbound,c,xii,eta,zta);
domain.jacobian = @(xii,eta,zta) mesh.dim_3.crazy_cube.jacobian(xbound,ybound,zbound,c,xii,eta,zta);

element.mapping = @(xii,eta,zta,elx,ely,elz) mesh.dim_3.element.mapping(xii,eta,zta,elx,ely,elz,el_bounds,domain.mapping);
element.jacobian = @(xii,eta,zta,elx,ely,elz) mesh.dim_3.element.jacobian(xii,eta,zta,elx,ely,elz,el_bounds,domain.jacobian);

%% create discretization nodes & basis

[x,w] = GLLnodes(p);
[h,e] = MimeticpolyVal(x,p,1);

% h = speye(p+1);

dtize.basis.hx = h;
dtize.basis.hy = h;
dtize.basis.hz = h;

dtize.basis.ex = e;
dtize.basis.ey = e;
dtize.basis.ez = e;

dtize.weights.wx = w;
dtize.weights.wy = w;
dtize.weights.wz = w;

dtize.nodes.sx = x;
dtize.nodes.sy = x;
dtize.nodes.sz = x;

%% random variables

% nr_nodes = (p+1)^3;
% nr_edges = 2*p*(p+1)^2
nr_surfc = 3*p^2*(p+1);
nr_volum = p^3;

nr_lambda = (Kx+1)*Ky*Kz*p^2 + (Ky+1)*Kx*Kz*p^2 + (Kz+1)*Kx*Ky*p^2;

%% create discretization jacobian and nodes

[dtize.nodes.eta, dtize.nodes.xii, dtize.nodes.zta] = meshgrid(dtize.nodes.sx,dtize.nodes.sy,dtize.nodes.sz);

dtize.nodes.xii = dtize.nodes.xii(:)';
dtize.nodes.eta = dtize.nodes.eta(:)';
dtize.nodes.zta = dtize.nodes.zta(:)';

for i = 1:Kx
    for j = 1:Ky
        for k = 1:Kz
            eleid = (k-1)*p^2 + (j-1)*p + i;
            [dtize.nodes.x(:,:,eleid),dtize.nodes.y(:,:,eleid),dtize.nodes.z(:,:,eleid)] = element.mapping(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta,i,j,k);
            dtize.jacobian(eleid) = element.jacobian(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta,i,j,k);
        end
    end
end

dtize.www = kron(kron(dtize.weights.wz,dtize.weights.wy),dtize.weights.wx);

%% create discretization metric terms

dtize.metric.ggg = metric.dim_3.determinant_jacobian(dtize.jacobian);
dtize.metric.invg = 1 ./dtize.metric.ggg;

dtize.metric = metric.dim_3.poisson_metric(dtize.jacobian, dtize.metric.invg);
dtize.metric.ggg = metric.dim_3.determinant_jacobian(dtize.jacobian);
dtize.metric.invg = 1 ./dtize.metric.ggg;

%% create basis volume

basis_volume = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.ex);

%% reduction of f

M333 = construct_mass_matrix(basis_volume,basis_volume,dtize.www,dtize.metric.invg);

ff_int = test.fff(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
dof_f = M333 \ reduction(ff_int, basis_volume, dtize.www, 1);

%% reduction of p 

pp_int = test.phi(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
red_p  = M333 \ reduction(pp_int, basis_volume, dtize.www, 1);

%% reconstruction

pf = p+5;

%% create reconstruction nodes & basis

[xf,wf] = GLLnodes(pf);
[hf,ef] = MimeticpolyVal(xf,p,1);

rec.basis.hfx = hf;
rec.basis.hfy = hf;
rec.basis.hfz = hf;

rec.basis.efx = ef;
rec.basis.efy = ef;
rec.basis.efz = ef;

rec.weights.wfx = wf;
rec.weights.wfy = wf;
rec.weights.wfz = wf;

rec.nodes.sfx = xf;
rec.nodes.sfy = xf;
rec.nodes.sfz = xf;

%% create reconstruction jacobian and nodes

[rec.nodes.xii,rec.nodes.eta,rec.nodes.zta] = meshgrid(rec.nodes.sfx,rec.nodes.sfy,rec.nodes.sfz);

temp3 = rec.nodes.eta(:)';
temp4 = rec.nodes.xii(:)';

rec.nodes.xii = temp3;
rec.nodes.eta = temp4;
% rec.nodes.xii = rec.nodes.xii(:)';
% rec.nodes.eta = rec.nodes.eta(:)';

rec.nodes.zta = rec.nodes.zta(:)';

[rec.nodes.x,rec.nodes.y,rec.nodes.z] = domain.mapping(rec.nodes.xii,rec.nodes.eta,rec.nodes.zta);

rec.jacobian = domain.jacobian(rec.nodes.xii, rec.nodes.eta, rec.nodes.zta);

% determinant of jacobian 
rec.metric.ggg = metric.dim_3.determinant_jacobian(rec.jacobian);
rec.metric.invg = 1 ./rec.metric.ggg;

rec.wwwf = kron(kron(rec.weights.wfz,rec.weights.wfy),rec.weights.wfx);

%% create reconstruction basis and nodes - II

rec_basis_volume = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.efx);

%% reconstruction of f

rec.metric.ggg = metric.dim_3.determinant_jacobian(rec.jacobian);
rec.metric.invg = 1 ./rec.metric.ggg;

rec.fff = reconstruction.dim_3.volume(dof_f,rec_basis_volume,pf,rec.metric.ggg);

ext_fff = test.fff(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error_fff = sqrt(sum((ext_fff-rec.fff(:)').^2 .* rec.metric.invg .* rec.wwwf))

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec.fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff-rec.fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure error')

%% surface basis 

basis_surfac_yz = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.hx);
basis_surfac_zx = kron(kron(dtize.basis.ez,dtize.basis.hy),dtize.basis.ex);
basis_surfac_xy = kron(kron(dtize.basis.hz,dtize.basis.ey),dtize.basis.ex);

% basis_surfac = [basis_surfac_xy; basis_surfac_yz; basis_surfac_zx];

%% reduction of q^(n-1)

% qqx_int = test.dpdx(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
% qqy_int = test.dpdy(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
% qqz_int = test.dpdz(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);

% red.metric.x = qqx_int .* dtize.jacobian.dXdxii + qqy_int .* dtize.jacobian.dYdxii + qqz_int .* dtize.jacobian.dZdxii;
% red.metric.y = qqx_int .* dtize.jacobian.dXdeta + qqy_int .* dtize.jacobian.dYdeta + qqz_int .* dtize.jacobian.dZdeta;
% red.metric.z = qqx_int .* dtize.jacobian.dXdzta + qqy_int .* dtize.jacobian.dYdzta + qqz_int .* dtize.jacobian.dZdzta;

% dof_xx = reduction(red.metric.x, basis_surfac_yz, dtize.www, 1);
% dof_yy = reduction(red.metric.y, basis_surfac_zx, dtize.www, 1);
% dof_zz = reduction(red.metric.z, basis_surfac_xy, dtize.www, 1);

M222x1 = construct_mass_matrix(basis_surfac_yz,basis_surfac_yz,dtize.www,dtize.metric.g11);
M222x2 = construct_mass_matrix(basis_surfac_yz,basis_surfac_zx,dtize.www,dtize.metric.g12);
M222x3 = construct_mass_matrix(basis_surfac_yz,basis_surfac_xy,dtize.www,dtize.metric.g13);

M222y1 = construct_mass_matrix(basis_surfac_zx,basis_surfac_yz,dtize.www,dtize.metric.g21);
M222y2 = construct_mass_matrix(basis_surfac_zx,basis_surfac_zx,dtize.www,dtize.metric.g22);
M222y3 = construct_mass_matrix(basis_surfac_zx,basis_surfac_xy,dtize.www,dtize.metric.g23);

M222z1 = construct_mass_matrix(basis_surfac_xy,basis_surfac_yz,dtize.www,dtize.metric.g31);
M222z2 = construct_mass_matrix(basis_surfac_xy,basis_surfac_zx,dtize.www,dtize.metric.g32);
M222z3 = construct_mass_matrix(basis_surfac_xy,basis_surfac_xy,dtize.www,dtize.metric.g33);

% M222 = blkdiag(M222x1,M222y2,M222z3);

M222 = [M222x1 M222x2 M222x3; M222y1 M222y2 M222y3; M222z1 M222z2 M222z3];

% dof_q = [dof_xx; dof_yy; dof_zz];

% dof_q = M222 \ dof_q;

% figure
% spy(M222)

%% reconstruction of q^(n-1)

% dof_xx = dof_q(1:p^2*(p+1));
% dof_yy = dof_q(p^2*(p+1)+1:2*p^2*(p+1));
% dof_zz = dof_q(2*p^2*(p+1)+1:3*p^2*(p+1));

rec.basis.surfac.xy = kron(kron(rec.basis.hfz,rec.basis.efy),rec.basis.efx);
rec.basis.surfac.yz = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.hfx);
rec.basis.surfac.zx = kron(kron(rec.basis.efz,rec.basis.hfy),rec.basis.efx);

% [rec.u.ux, rec.u.uy, rec.u.uz] = reconstruction.dim_3.surface(dof_q,rec.jacobian,p, rec.metric.invg, rec.basis.surfac);

% reconstruction_qzz = rec_basis_surfac_xy' * dof_zz;
% reconstruction_qxx = rec_basis_surfac_yz' * dof_xx;
% reconstruction_qyy = rec_basis_surfac_zx' * dof_yy;
% 
% reconstruction_qzz = reshape(reconstruction_qzz, [pf+1 pf+1 pf+1]);
% reconstruction_qxx = reshape(reconstruction_qxx, [pf+1 pf+1 pf+1]);
% reconstruction_qyy = reshape(reconstruction_qyy, [pf+1 pf+1 pf+1]);

ext_qxx = test.dpdx(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qyy = test.dpdy(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qzz = test.dpdz(rec.nodes.x,rec.nodes.y,rec.nodes.z);

%% errors 

% error_x = sqrt(sum((ext_qxx-rec.u.ux).^2 .* rec.metric.invg .* rec.wwwf))
% error_y = sqrt(sum((ext_qyy-rec.u.uy).^2 .* rec.metric.invg .* rec.wwwf))
% error_z = sqrt(sum((ext_qzz-rec.u.uz).^2 .* rec.metric.invg .* rec.wwwf))

%% velocity plots 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx-reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x error')

% % --- 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy-reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y error')
% 
% % --- 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz-reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z error')

%% construct incidence matrix

E32 = incidence_matrix.dim_3.discrete_divergence(p);

%% LHS

LHS = [M222 E32'; E32 sparse(nr_volum,nr_volum)];

% figure
% spy(LHS)

RHS(nr_surfc+1:nr_surfc+nr_volum,1) = dof_f;

% RHS=RHS(:);

%% dirichlet boundary conditions 

%% left face 

basis_left_face = kron(dtize.basis.ez,dtize.basis.ey);

[nodes_left_face.zta,nodes_left_face.eta] = meshgrid(dtize.nodes.sy,dtize.nodes.sz);

nodes_left_face.eta = nodes_left_face.eta(:)';
nodes_left_face.zta = nodes_left_face.zta(:)';

[nodes_left_face.xxx,nodes_left_face.yyy,nodes_left_face.zzz] = domain.mapping(-1,nodes_left_face.eta,nodes_left_face.zta);

phi_left_face = test.phi(nodes_left_face.xxx,nodes_left_face.yyy,nodes_left_face.zzz);

w88_left_face = kron(dtize.weights.wz,dtize.weights.wy);

nn_left = -1;
dof_left_face = nn_left * reduction(phi_left_face, basis_left_face, w88_left_face, 1);

%% right face 

basis_rght_face = kron(dtize.basis.ez,dtize.basis.ey);

[nodes_rght_face.zta,nodes_rght_face.eta] = meshgrid(dtize.nodes.sy,dtize.nodes.sz);

nodes_rght_face.eta = nodes_rght_face.eta(:)';
nodes_rght_face.zta = nodes_rght_face.zta(:)';

[nodes_rght_face.xxx,nodes_rght_face.yyy,nodes_rght_face.zzz] = domain.mapping(+1,nodes_rght_face.eta,nodes_rght_face.zta);

phi_rght_face = test.phi(nodes_rght_face.xxx,nodes_rght_face.yyy,nodes_rght_face.zzz);

w88_rght_face = kron(dtize.weights.wz,dtize.weights.wy);

nn_rght = +1;
dof_rght_face = nn_rght * reduction(phi_rght_face, basis_rght_face, w88_rght_face, 1);

%% bottom face

basis_bttm_face = kron(dtize.basis.ez,dtize.basis.ex);

[nodes_bttm_face.zta,nodes_bttm_face.xii] = meshgrid(dtize.nodes.sx,dtize.nodes.sz);

nodes_bttm_face.xii = nodes_bttm_face.xii(:)';
nodes_bttm_face.zta = nodes_bttm_face.zta(:)';

[nodes_bttm_face.xxx,nodes_bttm_face.yyy,nodes_bttm_face.zzz] = domain.mapping(nodes_bttm_face.xii,-1,nodes_bttm_face.zta);

phi_bttm_face = test.phi(nodes_bttm_face.xxx,nodes_bttm_face.yyy,nodes_bttm_face.zzz);

w88_bttm_face = kron(dtize.weights.wz,dtize.weights.wx);

nn_bttm = -1;
dof_bttm_face = nn_bttm * reduction(phi_bttm_face, basis_bttm_face, w88_bttm_face, 1);

%% top face

basis_topp_face = kron(dtize.basis.ez,dtize.basis.ex);

[nodes_topp_face.zta,nodes_topp_face.xii] = meshgrid(dtize.nodes.sx,dtize.nodes.sz);

nodes_topp_face.xii = nodes_topp_face.xii(:)';
nodes_topp_face.zta = nodes_topp_face.zta(:)';

[nodes_topp_face.xxx,nodes_topp_face.yyy,nodes_topp_face.zzz] = domain.mapping(nodes_topp_face.xii,+1,nodes_topp_face.zta);

phi_topp_face = test.phi(nodes_topp_face.xxx,nodes_topp_face.yyy,nodes_topp_face.zzz);

w88_topp_face = kron(dtize.weights.wz,dtize.weights.wx);

nn_topp = +1;
dof_topp_face = nn_topp * reduction(phi_topp_face, basis_topp_face, w88_topp_face, 1);

%% back face

basis_back_face = kron(dtize.basis.ey,dtize.basis.ex);

[nodes_back_face.eta,nodes_back_face.xii] = meshgrid(dtize.nodes.sx,dtize.nodes.sy);

nodes_back_face.xii = nodes_back_face.xii(:)';
nodes_back_face.eta = nodes_back_face.eta(:)';

[nodes_back_face.xxx,nodes_back_face.yyy,nodes_back_face.zzz] = domain.mapping(nodes_back_face.xii,nodes_back_face.eta,-1);

phi_back_face = test.phi(nodes_back_face.xxx,nodes_back_face.yyy,nodes_back_face.zzz);

w88_back_face = kron(dtize.weights.wy,dtize.weights.wx);

nn_back = -1;
dof_back_face = nn_back * reduction(phi_back_face, basis_back_face, w88_back_face, 1);

%% front face

basis_frnt_face = kron(dtize.basis.ey,dtize.basis.ex);

[nodes_frnt_face.eta,nodes_frnt_face.xii] = meshgrid(dtize.nodes.sx,dtize.nodes.sy);

nodes_frnt_face.xii = nodes_frnt_face.xii(:)';
nodes_frnt_face.eta = nodes_frnt_face.eta(:)';

[nodes_frnt_face.xxx,nodes_frnt_face.yyy,nodes_frnt_face.zzz] = domain.mapping(nodes_frnt_face.xii,nodes_frnt_face.eta,+1);

phi_frnt_face = test.phi(nodes_frnt_face.xxx,nodes_frnt_face.yyy,nodes_frnt_face.zzz);

w88_frnt_face = kron(dtize.weights.wy,dtize.weights.wx);

nn_frnt = +1;
dof_frnt_face = nn_frnt * reduction(phi_frnt_face, basis_frnt_face, w88_frnt_face, 1);

%% add Dirichlet BC's 

% left face
for j = 1:p
    for k = 1:p
        local_dof = (k-1)*p + j;
        globl_dof_left(local_dof) = (k-1)*p*(p+1) + (j-1)*(p+1) + 1;
    end
end

RHS(globl_dof_left) = dof_left_face;

% right face 
for j = 1:p
    for k = 1:p
        local_dof = (k-1)*p + j;
        globl_dof_rght(local_dof) = (k-1)*p*(p+1) + (j-1)*(p+1) + p + 1;
    end
end

RHS(globl_dof_rght) = dof_rght_face;

% bottom face

for i = 1:p
    for k = 1:p
        local_dof = (k-1)*p + i;
        globl_dof_bttm(local_dof) = p^2*(p+1) + (k-1)*p*(p+1) + i;
    end
end

RHS(globl_dof_bttm) = dof_bttm_face;

% top face 

for i = 1:p
    for k = 1:p
        local_dof = (k-1)*p + i;
        globl_dof_topp(local_dof) = p^2*(p+1) + (k-1)*p*(p+1) + i + p^2;
    end
end

RHS(globl_dof_topp) = dof_topp_face;

% back face 

for i = 1:p
    for j = 1:p
        local_dof = (j-1)*p+i;
        globl_dof_back(local_dof) = 2*p^2*(p+1) + local_dof;
    end
end

RHS(globl_dof_back) = dof_back_face;

% front face

for i = 1:p
    for j = 1:p
        local_dof = (j-1)*p+i;
        globl_dof_frnt(local_dof) = 2*p^2*(p+1) + local_dof + p^3;
    end
end

RHS(globl_dof_frnt) = dof_frnt_face;

%% solution 

X = LHS \ RHS;

%% cochains 

dof_p = X(nr_surfc+1:nr_surfc+nr_volum);

dof_p = M333 \ dof_p;

%% reconstruction pressure 

rec_ppp = reconstruction.dim_3.volume(dof_p,rec_basis_volume,pf,rec.metric.ggg);
% rec_ppp = reconstruction.dim_3.volume(red_p,rec_basis_volume,pf,rec.metric.ggg)

ext_ppp = test.phi(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error_ppp = sqrt(sum((ext_ppp-rec_ppp(:)').^2 .* rec.metric.invg .* rec.wwwf))

% ext_ppp = reshape(ext_ppp, [pf+1 pf+1 pf+1]);
% rec.nodes.x = reshape(rec.nodes.x, [pf+1 pf+1 pf+1]);
% rec.nodes.y = reshape(rec.nodes.y, [pf+1 pf+1 pf+1]);
% rec.nodes.z = reshape(rec.nodes.z, [pf+1 pf+1 pf+1]);

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp-rec_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure error')

%% reconstruction flux

[rec.u.ux, rec.u.uy, rec.u.uz] = reconstruction.dim_3.surface(X(1:nr_surfc,1),rec.jacobian,p, rec.metric.invg, rec.basis.surfac);

% errors 

error_x = sqrt(sum((ext_qxx-rec.u.ux).^2 .* rec.metric.invg .* rec.wwwf))
error_y = sqrt(sum((ext_qyy-rec.u.uy).^2 .* rec.metric.invg .* rec.wwwf))
error_z = sqrt(sum((ext_qzz-rec.u.uz).^2 .* rec.metric.invg .* rec.wwwf))

%% gather matrix

Kx = 3;
Ky = 3;
Kz = 3; 

% right faces 

count1 = 1;

for k = 1:Kz
    for j = 1:Ky
        for i = 1:Kx-1
            eleeid = (k-1)*p^2 + (j-1)*p + i;
            faceid = 1:p^2;
            GM(eleeid,faceid) = (count1-1)*p^2 + 1 : (count1-1)*p^2 + p^2;
            count1 = count1+1;
        end
    end
end

% left faces

for i = 2:Kx
    for j = 1:Ky
        for k = 1:Kz
            eleeid = (k-1)*p^2 + (j-1)*p + i;
            faceid = 3*p^2+1:4*p^2;
            facert = 1:p^2;
            GM(eleeid,faceid) = GM(eleeid-1,facert);
        end
    end
end

% top faces

count2 = count1;

for k = 1:Kz
    for j = 1:Ky-1
        for i = 1:Kx
            eleeid = (k-1)*p^2 + (j-1)*p + i;
            faceid = p^2+1:2*p^2;
            GM(eleeid,faceid) = (count2-1)*p^2 + 1 : (count2-1)*p^2 + p^2;
            count2 = count2+1;
        end
    end
end

% bottom faces

for i = 1:Kx
    for j = 2:Ky
        for k = 1:Kz
            eleeid = (k-1)*p^2 + (j-1)*p + i;
            faceid = 4*p^2+1:5*p^2;
            facetp = p^2+1:2*p^2;
            GM(eleeid,faceid) = GM(eleeid-Kx,facetp);
        end
    end
end

count3 = count2;

% front faces

for k = 1:Kz-1
    for j = 1:Ky
        for i = 1:Kx
            eleeid = (k-1)*p^2 + (j-1)*p + i;
            faceid = 2*p^2+1:3*p^2;
            GM(eleeid,faceid) = (count3-1)*p^2 + 1 : (count3-1)*p^2 + p^2;
            count3 = count3+1;
        end
    end
end

% back faces

for i = 1:Kx
    for j = 1:Ky
        for k = 2:Kz
            eleeid = (k-1)*p^2 + (j-1)*p + i;
            faceid = 5*p^2+1:6*p^2;
            faceft = 2*p^2+1:3*p^2;
            GM(eleeid,faceid) = GM(eleeid-Kx*Ky,faceft);
        end
    end
end

% Right domain faces

count4 = count3;

for k=1:Kz
    for j=1:Ky
        eleeid = (k-1)*p^2 + (j-1)*p + Kx;
        faceid = 1:p^2;
        GM(eleeid,faceid) = (count4-1)*p^2 + 1 : (count4-1)*p^2 + p^2;
        count4 = count4 + 1;
    end
end

% Left domain faces

count5 = count4;

for k=1:Kz
    for j=1:Ky        
        eleeid = (k-1)*p^2 + (j-1)*p + 1;
        faceid = 3*p^2+1:4*p^2;
        GM(eleeid,faceid) = (count5-1)*p^2 + 1 : (count5-1)*p^2 + p^2;
        count5 = count5 + 1;
    end
end

% Top domain faces

count6 = count5;

for k=1:Kz
    for i=1:Kx
        eleeid = (k-1)*p^2 + (Ky-1)*p + i;
        faceid = p^2+1:2*p^2;
        GM(eleeid,faceid) = (count6-1)*p^2 + 1 : (count6-1)*p^2 + p^2;
        count6 = count6 + 1;
    end
end

% Bottom domain faces

count7 = count6;

for k=1:Kz
    for i=1:Kx
        eleeid = (k-1)*p^2 + (1-1)*p + i;
        faceid = 4*p^2+1:5*p^2;
        GM(eleeid,faceid) = (count7-1)*p^2 + 1 : (count7-1)*p^2 + p^2;
        count7 = count7 + 1;
    end
end

% Front domain faces

count8 = count7;

for j=1:Ky
    for i=1:Kx
        eleeid = (Kz-1)*p^2 + (j-1)*p + i;
        faceid = 2*p^2+1:3*p^2;
        GM(eleeid,faceid) = (count8-1)*p^2 + 1 : (count8-1)*p^2 + p^2;
        count8 = count8 + 1;
    end
end

% Back domain faces

count9 = count8;

for j=1:Ky
    for i=1:Kx
        eleeid = (1-1)*p^2 + (j-1)*p + i;
        faceid = 5*p^2+1:6*p^2;
        GM(eleeid,faceid) = (count9-1)*p^2 + 1 : (count9-1)*p^2 + p^2;
        count9 = count9 + 1;
    end
end