%% script settings

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% test case

test.phi = @(x,y,z) (1-x.^2).*(1-y.^2).*(1-z.^2);

test.dpdx = @(x,y,z) -2*x.*(1-y.^2).*(1-z.^2);
test.dpdy = @(x,y,z) -2*y.*(1-x.^2).*(1-z.^2);
test.dpdz = @(x,y,z) -2*z.*(1-x.^2).*(1-y.^2);

test.d2pdx2 = @(x,y,z) -2*(1-y.^2).*(1-z.^2);
test.d2pdy2 = @(x,y,z) -2*(1-x.^2).*(1-z.^2);
test.d2pdz2 = @(x,y,z) -2*(1-x.^2).*(1-y.^2);

test.fff = @(x,y,z) test.d2pdx2(x,y,z) + test.d2pdy2(x,y,z) + test.d2pdz2(x,y,z);

%% mappings

xbound = [0 1];
ybound = [0 1];
zbound = [0 1];

c = 0.1;

domain.mapping = @(xii,eta,zta) mesh.dim_3.crazy_cube.mapping(xbound,ybound,zbound,c,xii,eta,zta);
domain.jacobian = @(xii,eta,zta) mesh.dim_3.crazy_cube.jacobian(xbound,ybound,zbound,c,xii,eta,zta);

%% discretization

p = 10;

%% create discretization basis

[x,w] = GLLnodes(p);
[h,e] = MimeticpolyVal(x,p,1);

dtize.basis.hx = h;
dtize.basis.hy = h;
dtize.basis.hz = h;

dtize.basis.ex = e;
dtize.basis.ey = e;
dtize.basis.ez = e;

dtize.weights.wx = w;
dtize.weights.wy = w;
dtize.weights.wz = w;

dtize.nodes.sx = x;
dtize.nodes.sy = x;
dtize.nodes.sz = x;

%% random variables

% nr_nodes = (p+1)^3;
% nr_edges = 2*p*(p+1)^2
nr_surfc = 3*p^2*(p+1);
nr_volum = p^3;

%% create discretization nodes

[dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta] = meshgrid(dtize.nodes.sx,dtize.nodes.sy,dtize.nodes.sz);

temp1 = dtize.nodes.eta(:)';
temp2 = dtize.nodes.xii(:)';

dtize.nodes.xii = temp1;
dtize.nodes.eta = temp2;
% dtize.nodes.xii = dtize.nodes.xii(:)';
% dtize.nodes.eta = dtize.nodes.eta(:)';

dtize.nodes.zta = dtize.nodes.zta(:)';

[dtize.nodes.x,dtize.nodes.y,dtize.nodes.z] = domain.mapping(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta);

dtize.jacobian = domain.jacobian(dtize.nodes.xii, dtize.nodes.eta, dtize.nodes.zta);

dtize.www = kron(kron(dtize.weights.wz,dtize.weights.wy),dtize.weights.wx);

%% create discretization metric terms

dtize.metric.ggg = metric.dim_3.determinant_jacobian(dtize.jacobian);
dtize.metric.invg = 1 ./dtize.metric.ggg;

% dtize.metric = metric.dim_3.g11(dtize.jacobian, dtize.metric.invg);

dtize.metric = metric.dim_3.poisson_metric(dtize.jacobian, dtize.metric.invg);
dtize.metric.ggg = metric.dim_3.determinant_jacobian(dtize.jacobian);
dtize.metric.invg = 1 ./dtize.metric.ggg;

%% create basis volume

basis_volume = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.ex);

%% reduction of f

M333 = construct_mass_matrix(basis_volume,basis_volume,dtize.www,dtize.metric.invg);

ff_int = test.fff(dtize.nodes.x(:)',dtize.nodes.y(:)',dtize.nodes.z(:)');

dual_basis = M333 \ basis_volume;

dof_f = reduction(ff_int, dual_basis, dtize.www, 1);

%% reconstruction

pf = p+5;

%% create reconstruction basis

[xf,wf] = GLLnodes(pf);
[hf,ef] = MimeticpolyVal(xf,p,1);

rec.basis.hfx = hf;
rec.basis.hfy = hf;
rec.basis.hfz = hf;

rec.basis.efx = ef;
rec.basis.efy = ef;
rec.basis.efz = ef;

rec.weights.wfx = wf;
rec.weights.wfy = wf;
rec.weights.wfz = wf;

rec.nodes.sfx = xf;
rec.nodes.sfy = xf;
rec.nodes.sfz = xf;

%% create reconstruction nodes

[rec.nodes.xii,rec.nodes.eta,rec.nodes.zta] = meshgrid(rec.nodes.sfx,rec.nodes.sfy,rec.nodes.sfz);

temp3 = rec.nodes.eta(:)';
temp4 = rec.nodes.xii(:)';

rec.nodes.xii = temp3;
rec.nodes.eta = temp4;
% rec.nodes.xii = rec.nodes.xii(:)';
% rec.nodes.eta = rec.nodes.eta(:)';

rec.nodes.zta = rec.nodes.zta(:)';

[rec.nodes.x,rec.nodes.y,rec.nodes.z] = domain.mapping(rec.nodes.xii,rec.nodes.eta,rec.nodes.zta);

rec.jacobian = domain.jacobian(rec.nodes.xii, rec.nodes.eta, rec.nodes.zta);

% determinant of jacobian 
rec.metric.ggg = metric.dim_3.determinant_jacobian(rec.jacobian);
rec.metric.invg = 1 ./rec.metric.ggg;

rec.wwwf = kron(kron(rec.weights.wfz,rec.weights.wfy),rec.weights.wfx);

%% create reconstruction basis and nodes - II

rec_basis_volume = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.efx);

%% reconstruction of f

rec.metric.ggg = metric.dim_3.determinant_jacobian(rec.jacobian);
rec.metric.invg = 1 ./rec.metric.ggg;

rec.fff = reconstruction.dim_3.volume(dof_f,rec_basis_volume,pf,rec.metric.ggg);

ext_fff = test.fff(rec.nodes.x,rec.nodes.y,rec.nodes.z);

error = sqrt(sum((ext_fff-rec.fff(:)').^2 .* rec.wwwf))

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec.fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_fff-rec.fff,[-0.5 0.5],0,0)
% colorbar
% title('pressure error')

%% surface basis 

basis_surfac_yz = kron(kron(dtize.basis.ez,dtize.basis.ey),dtize.basis.hx);
basis_surfac_zx = kron(kron(dtize.basis.ez,dtize.basis.hy),dtize.basis.ex);
basis_surfac_xy = kron(kron(dtize.basis.hz,dtize.basis.ey),dtize.basis.ex);

% basis_surfac = [basis_surfac_xy; basis_surfac_yz; basis_surfac_zx];

%% reduction of q^(n-1)

qqx_int = test.dpdx(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
qqy_int = test.dpdy(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);
qqz_int = test.dpdz(dtize.nodes.x,dtize.nodes.y,dtize.nodes.z);

red.metric.x = qqx_int .* dtize.jacobian.dXdxii + qqy_int .* dtize.jacobian.dYdxii + qqz_int .* dtize.jacobian.dZdxii;
red.metric.y = qqx_int .* dtize.jacobian.dXdeta + qqy_int .* dtize.jacobian.dYdeta + qqz_int .* dtize.jacobian.dZdeta;
red.metric.z = qqx_int .* dtize.jacobian.dXdzta + qqy_int .* dtize.jacobian.dYdzta + qqz_int .* dtize.jacobian.dZdzta;

dof_xx = reduction(red.metric.x, basis_surfac_yz, dtize.www, 1);
dof_yy = reduction(red.metric.y, basis_surfac_zx, dtize.www, 1);
dof_zz = reduction(red.metric.z, basis_surfac_xy, dtize.www, 1);

M222x1 = construct_mass_matrix(basis_surfac_yz,basis_surfac_yz,dtize.www,dtize.metric.g11);
M222x2 = construct_mass_matrix(basis_surfac_yz,basis_surfac_zx,dtize.www,dtize.metric.g12);
M222x3 = construct_mass_matrix(basis_surfac_yz,basis_surfac_xy,dtize.www,dtize.metric.g13);

M222y1 = construct_mass_matrix(basis_surfac_zx,basis_surfac_yz,dtize.www,dtize.metric.g21);
M222y2 = construct_mass_matrix(basis_surfac_zx,basis_surfac_zx,dtize.www,dtize.metric.g22);
M222y3 = construct_mass_matrix(basis_surfac_zx,basis_surfac_xy,dtize.www,dtize.metric.g23);

M222z1 = construct_mass_matrix(basis_surfac_xy,basis_surfac_yz,dtize.www,dtize.metric.g31);
M222z2 = construct_mass_matrix(basis_surfac_xy,basis_surfac_zx,dtize.www,dtize.metric.g32);
M222z3 = construct_mass_matrix(basis_surfac_xy,basis_surfac_xy,dtize.www,dtize.metric.g33);

% M222 = blkdiag(M222x1,M222y2,M222z3);

M222 = [M222x1 M222x2 M222x3; M222y1 M222y2 M222y3; M222z1 M222z2 M222z3];

dof = [dof_xx; dof_yy; dof_zz];

dof = M222 \ dof;

% figure
% spy(M222)

%% reconstruction of q^(n-1)

dof_xx = dof(1:p^2*(p+1));
dof_yy = dof(p^2*(p+1)+1:2*p^2*(p+1));
dof_zz = dof(2*p^2*(p+1)+1:3*p^2*(p+1));

rec.basis.surfac.xy = kron(kron(rec.basis.hfz,rec.basis.efy),rec.basis.efx);
rec.basis.surfac.yz = kron(kron(rec.basis.efz,rec.basis.efy),rec.basis.hfx);
rec.basis.surfac.zx = kron(kron(rec.basis.efz,rec.basis.hfy),rec.basis.efx);

[rec.u.ux, rec.u.uy, rec.u.uz] = reconstruction.dim_3.surface(dof,rec.jacobian,p, rec.metric.invg, rec.basis.surfac);

% reconstruction_qzz = rec_basis_surfac_xy' * dof_zz;
% reconstruction_qxx = rec_basis_surfac_yz' * dof_xx;
% reconstruction_qyy = rec_basis_surfac_zx' * dof_yy;
% 
% reconstruction_qzz = reshape(reconstruction_qzz, [pf+1 pf+1 pf+1]);
% reconstruction_qxx = reshape(reconstruction_qxx, [pf+1 pf+1 pf+1]);
% reconstruction_qyy = reshape(reconstruction_qyy, [pf+1 pf+1 pf+1]);

ext_qxx = test.dpdx(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qyy = test.dpdy(rec.nodes.x,rec.nodes.y,rec.nodes.z);
ext_qzz = test.dpdz(rec.nodes.x,rec.nodes.y,rec.nodes.z);

%% errors 

error_x = sqrt(sum((ext_qxx-rec.u.ux).^2 .* rec.wwwf))
error_y = sqrt(sum((ext_qyy-rec.u.uy).^2 .* rec.wwwf))
error_z = sqrt(sum((ext_qzz-rec.u.uz).^2 .* rec.wwwf))

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qxx-reconstruction_qxx,[0 0.5],0,0)
% colorbar
% title('velocity x error')

% % --- 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qyy-reconstruction_qyy,[0 0.5],0,0)
% colorbar
% title('velocity y error')
% 
% % --- 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_qzz-reconstruction_qzz,[0 0.5],0,0)
% colorbar
% title('velocity z error')

% %% construct incidence matrix
% 
% E32 = incidence_matrix.dim_3.discrete_divergence(p);
% 
% %% LHS
% 
% LHS = [M222 E32'; E32 sparse(nr_volum,nr_volum)];
% 
% figure
% spy(LHS)
% 
% RHS(nr_surfc+1:nr_surfc+nr_volum) = dof_f;
% 
% RHS=RHS(:);
% 
% %% solution 
% 
% X = LHS \ RHS;
% 
% %% cochains 
% 
% dof_p = X(nr_surfc+1:nr_surfc+nr_volum);
% 
% dof_p = M333 \ dof_p;
% 
% %% reconstruction pressure 
% 
% rec_ppp = reconstruction.dim_3.volume(dof_p,rec_basis_volume,pf,rec.metric.ggg);
% 
% ext_ppp = test.phi(rec.nodes.x,rec.nodes.y,rec.nodes.z);
% 
% error = sqrt(sum((ext_ppp(:)-rec_ppp(:)).^2 .* rec.wwwf'))

% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,rec_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure reconstruction')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure exact')
% 
% figure
% slice(rec.nodes.x,rec.nodes.y,rec.nodes.z,ext_ppp-rec_ppp,[-0.5 0.5],0,0)
% colorbar
% title('pressure error')
% 
% %% reconstruction flux

