clc
close all
clear variables

p = 4;

x = sym('x',[1 p]);
y = sym('y',[1 p]);
z = sym('z',[1 p]);

[X,Y,Z] = meshgrid(x,y,z);

% [a(:) b(:) c(:)]
% 
% reshape (a, [27 1])
% 
% x1 = permute(a,[2 1 3])
% out=x1(:)
% 
% y1 = permute(b,[2 1 3])
% out=y1(:)

X2 = permute(X,[2 1 3]);
X2 = X2(:);

X3 = reshape(X2, [p p p]);
X3 = permute(X3,[2 1 3]);