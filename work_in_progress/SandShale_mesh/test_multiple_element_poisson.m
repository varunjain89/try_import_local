%% created on 26 Sept.

clc
% close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% analytical solution 

phi_an = @(x,y) sin(2*pi*x).*sin(2*pi*y);
u_an   = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
v_an   = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
f_an   = @(x,y) -8*pi*pi*sin(2*pi*x).*sin(2*pi*y);

%% domain

xbound = [-1 1];
ybound = [-1 1];
c = 0.25;

%% number of elements 

K = 20;
p = 3;

element_bounds_x = linspace(-1,1,K+1);
element_bounds_y = linspace(-1,1,K+1);

ttl_nr_el = K^2;
ttl_nr_pp = K^2*p^2;
ttl_nr_ed = 2*K*p*(K*p + 1);

local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% mesh mapping and derivatives

domain_mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

el_mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain_mapping, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain_dX_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain_dX_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain_dY_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain_dY_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);

%% plot mesh

[xp, wp] = GLLnodes(9);

[xi, eta] = meshgrid(xp);

% figure(1)
% hold on
% 
% for elx = 1:K
%     for ely = 1:K
%         [x_plot, y_plot] = el_mapping(xi, eta, elx, ely);
% %         fill(x_plot,y_plot,'r')
%         plot(x_plot,y_plot)
%     end
% end

% figure(2)
% hold on 
% 
% for elx = 1:K
%     for ely = 1:K
%         % bottom
%         [x_plot, y_plot] = el_mapping(xp, -1, elx, ely);
%         plot(x_plot,y_plot,'k')
% %         fill(x_plot,y_plot,'r')
%         % top
%         [x_plot, y_plot] = el_mapping(xp, 1, elx, ely);
%         plot(x_plot,y_plot,'k')
%         
%         % left
%         [x_plot, y_plot] = el_mapping(-1, xp, elx, ely);
%         plot(x_plot,y_plot,'k')
%         
%         % right
%         [x_plot, y_plot] = el_mapping(1, xp, elx, ely);
%         plot(x_plot,y_plot,'k')
%     end
% end

%%

% rng(0,'twister');
% r = randi([1 K^2],1,80);
load('r2')

%%

figure(3)
hold on 
for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K+ely;
        if find(r == el)
            % bottom
            [x_plot_bot, y_plot_bot] = el_mapping(xp, -1, elx, ely);
            
            % top
            [x_plot_top, y_plot_top] = el_mapping(xp, 1, elx, ely);
            
            % left
            [x_plot_left, y_plot_left] = el_mapping(-1, xp, elx, ely);
            
            % right
            [x_plot_right, y_plot_right] = el_mapping(1, xp, elx, ely);
            
            x_plot = [x_plot_bot x_plot_right];
            y_plot = [y_plot_bot y_plot_right];
            fill(x_plot,y_plot,'k')
            x_plot = [x_plot_top x_plot_left];
            y_plot = [y_plot_top y_plot_left];
            fill(x_plot,y_plot,'k')
        end
    end
end

%%
        
    



