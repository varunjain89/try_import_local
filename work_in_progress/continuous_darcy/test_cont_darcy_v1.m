clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% analytical solution

%% case 1

% phi_an   = @(x,y) (x.^2 -8*x + 15).*(y.^2 -11*y + 28);
% dp_dx    = @(x,y) (2*x-8) .*(y.^2 -11*y + 28);
% dp_dy    = @(x,y) (2*y-11).*(x.^2 -8*x + 15);
% d2p_dxdy = @(x,y) (2*x-8).*(2*y-11);
% d2p_dx2  = @(x,y) 2*(y.^2 -11*y + 28);
% d2p_dy2  = @(x,y) 2*(x.^2 -8*x + 15);

% k11_an  = @(x,y) 4*ones(size(x));
% k12_an  = @(x,y) 3*ones(size(x));
% k21_an  = @(x,y) 3*ones(size(x));
% k22_an  = @(x,y) 5*ones(size(x));
% dk11_dx = @(x,y) 0*ones(size(x));
% dk12_dx = @(x,y) 0*ones(size(x));
% dk12_dy = @(x,y) 0*ones(size(x));
% dk22_dy = @(x,y) 0*ones(size(x));

%% case 2

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

a = 0.1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%%

xbound = [0 1];
ybound = [0 1];
c = 0.3;

%% discretization

K = 3;
p = 6;

ttl_nr_el = K^2;

local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% domain / mesh mapping and derivatives

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

mesh1.eval_p_der();

%% LHS systems

mesh1.eval_dom_xy();

eval.K.k11 = k11_an(mesh1.eval.x, mesh1.eval.y);
eval.K.k12 = k12_an(mesh1.eval.x, mesh1.eval.y);
eval.K.k22 = k22_an(mesh1.eval.x, mesh1.eval.y);
eval_detK = eval.K.k11.*eval.K.k22 - eval.K.k12.^2;

eval_p_ggg = metric.ggg_v2(mesh1.eval.p);
eval.p.g11 = flow.darcy.g11K_v2(mesh1.eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g12 = flow.darcy.g12K_v2(mesh1.eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g22 = flow.darcy.g22K_v2(mesh1.eval.p, eval_p_ggg, eval.K, eval_detK);

M11 = zeros(local_nr_ed, local_nr_ed, ttl_nr_el);
M22 = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);

E211 = incidence_matrix.E21(p);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v5(p, eval.p, eln, mesh1.basis, mesh1.wp);
end

E211_2  = repmat(full(E211),[1 1 K^2]);

M11_2 = M11;

%%

ttl_loc_dof2 = 2*p*(p-1)+p^2;

GM_local_dof = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof2);
GM_local_dof_p = GM_local_dof(:,2*p*(p-1)+1:2*p*(p-1)+p^2);

% GM1 = GM_continuous_1_form_v2(K,p);

% assM11 = AssembleMatrices2(gcf, gcf, M11);
% assE21 = AssembleMatrices2(GM_local_dof_p', gcf, E211_2);

% figure
% spy(assM11)
% 
% figure
% spy(assE21)

gcf2 = GM_continuous_1_form_v2(K,p);

assM11 = AssembleMatrices2_dd(gcf2, gcf2, M11);
assE21 = AssembleMatrices2_dd(GM_local_dof_p', gcf2, E211_2);

i1 = assM11.rr;
j1 = assM11.cc;
s1 = assM11.data;

i2 = assE21.rr;
j2 = assE21.cc;
s2 = assE21.data;

i3 = j2;
j3 = i2;
s3 = s2;

ii = [i1;i2;i3];
jj = [j1;j2;j3];
ss = [s1;s2;s3];

% size_LHS = ttl_loc_dof*ttl_nr_el + 2*K*p*(K+1);
LHS = sparse(ii, jj, ss);

figure
spy(LHS)

set(gca,'TickLabelInterpreter','latex','FontSize',20);

% export_fig('spy_LHS_cont.pdf','-pdf','-r864','-painters','-transparent');

ttl_loc_dof = 2*p*(p+1)+p^2;

GM_local_dof = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);


E211_2  = repmat(full(E211),[1 1 K^2]);

M11    = AssembleMatrices2_dd(GM_local_dof_q', GM_local_dof_q', M11);
E211_2 = AssembleMatrices2_dd(GM_local_dof_p', GM_local_dof_q', E211_2);

i1 = M11.rr;
j1 = M11.cc;
s1 = M11.data;

i2 = E211_2.rr;
j2 = E211_2.cc;
s2 = E211_2.data;

% this is transpose of assembled E21. I have simply swapped the x and y
% axis.

i3 = j2;
j3 = i2;
s3 = s2;

N = full(incidence_matrix.OutwardNormalMatrix(p));
N_3D = repmat(N,[1 1 K^2]);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
ass_conn = AssembleMatrices2_dd(GM_lambda, GM_local_dof_q', N_3D);

% [i4, j4, s4] = find(ass_conn);
i4 = ass_conn.rr;
j4 = ass_conn.cc;
s4 = ass_conn.data;

N_spy = sparse(i4,j4,s4,2*K*p*(K-1)+4*K*p,K^2*(p^2 + 2*p*(p+1)));
N_spy = N_spy(1:2*K*p*(K-1),:);

figure
spy(N_spy)

set(gca,'TickLabelInterpreter','latex','FontSize',20);

% export_fig('spy_EN.pdf','-pdf','-r864','-painters','-transparent');

i4 = ttl_loc_dof*ttl_nr_el + i4;

% this is transpose of connectivity matrix. I have simply swapped the x
% and y coordinates. 

i5 = j4;
j5 = i4;
s5 = s4;

ii = [i1;i2;i3;i4;i5];
jj = [j1;j2;j3;j4;j5];
ss = [s1;s2;s3;s4;s5];

size_LHS = ttl_loc_dof*ttl_nr_el + 2*K*p*(K+1);
LHS = sparse(ii, jj, ss, size_LHS, size_LHS);

%% RHS

ff = twoForm();
ff.K = K;
ff.p = p;
ff.domain = mesh1.domain;

ff.el_bounds_x = mesh1.el.bounds.x;
ff.el_bounds_y = mesh1.el.bounds.y;

F_3D = ff.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + 2*p*(p+1);
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

RHS_con = zeros(2*K*p*(K+1),1);

RHS2 = [RHS; RHS_con];

%% implementing Dirichlet BC's

LHS = LHS(1:end-4*K*p,1:end-4*K*p);
RHS3 = RHS2(1:end-4*K*p,1);

% cond(LHS)
condest(LHS)
%% solution
spy(LHS)
% export_fig('spy_hybrid_K3_N6_c03.pdf','-pdf','-r864','-painters','-transparent');
tic
X_h = LHS\RHS3;
toc

%% separating cochains

loc_nr_ed = 2*p*(p+1);
loc_nr_pp = p^2;
loc_nr_lm = 4*p;

q_h = zeros(loc_nr_ed, ttl_nr_el);
p_h = zeros(loc_nr_pp, ttl_nr_el);
lm_h = zeros(loc_nr_lm, ttl_nr_el);

GM_lambda2 = GM_lambda + ttl_loc_dof*ttl_nr_el;
GM_lambda2 = GM_lambda2';
X_h2 = [X_h; zeros(4*K*p,1)];


for eln = 1:ttl_nr_el
    q_h(1:loc_nr_ed,eln) = X_h(GM_local_dof_q(eln,:));
    p_h(1:loc_nr_pp,eln) = X_h(GM_local_dof_p(eln,:));
    lm_h(1:loc_nr_lm,eln) = X_h2(GM_lambda2(eln,:));
end

M22s = zeros(p^2,p^2,ttl_nr_el);

for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v3(p, eval_p_ggg(:,:,eln), mesh1.basis, mesh1.wp);
end

p_h_2 = zeros(size(p_h));

for eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

%% post processing 

mesh1.eval_pf_der();

pf = p+5;

gf = zeros(pf+1,pf+1,ttl_nr_el);

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

eval.pf.dx_dxii = mesh1.eval_pf_dx_dxii;
eval.pf.dx_deta = mesh1.eval_pf_dx_deta;
eval.pf.dy_dxii = mesh1.eval_pf_dy_dxii;
eval.pf.dy_deta = mesh1.eval_pf_dy_deta;

eval.pf.ggg = metric.ggg_v2(eval.pf);

%%
pp_h = twoForm();

pp_h.domain = mesh1.domain;
pp_h.K = K;
pp_h.p = p;

pp_h.el_bounds_x = mesh1.el.bounds.x;
pp_h.el_bounds_y = mesh1.el.bounds.y;

pp_h.cochain    = p_h_2;
pp_h.pf         = pf;
pp_h.el_mapping   = mesh1.el.mapping;
pp_h.eval_pf_ggg  = eval.pf.ggg;

[xf_3D, yf_3D, pf_h] = pp_h.reconstruction(mesh1.basis);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln)')
% end
% colorbar

%% reconstruction of 1-form 

qq = oneForm;
qq.K = K;
qq.p = p;
qq.pf = pf;

qq.reconstruction(q_h, ttl_nr_el, mesh1);

rec_qx = qq.rec_qx;
rec_qy = qq.rec_qy;

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qx(:,:,eln))
% end
% colorbar

%% exact values

qx_ex = q_x_an(xf_3D, yf_3D);
qy_ex = q_y_an(xf_3D, yf_3D);
pp_ex = phi_an(xf_3D, yf_3D);

pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

error.pp = error_processor(pp_ex, pf_h, wfxy2, eval.pf.ggg);
error.qx = error_processor(qx_ex, -rec_qx, wfxy2, eval.pf.ggg);
error.qy = error_processor(qy_ex, -rec_qy, wfxy2, eval.pf.ggg);

error;

%% Hdiv error

[pp_sqrt, pp_sqre] = error_processor_v2(pp_ex, pf_h, wfxy2, eval.pf.ggg);
[qx_sqrt, qx_sqre] = error_processor_v2(qx_ex, -rec_qx, wfxy2, eval.pf.ggg);
[qy_sqrt, qy_sqre] = error_processor_v2(qy_ex, -rec_qy, wfxy2, eval.pf.ggg);

% evaluate discrete divQ

for el = 1:K^2
    divQ(:,el) = E211 * q_h(:,el);
end

% reconstruct divQ using 2-form reconstruction

divQ_h = twoForm();

divQ_h.domain = mesh1.domain;
divQ_h.K = K;
divQ_h.p = p;

divQ_h.el_bounds_x = mesh1.el.bounds.x;
divQ_h.el_bounds_y = mesh1.el.bounds.y;

divQ_h.cochain    = divQ;
divQ_h.pf         = pf;
divQ_h.el_mapping   = mesh1.el.mapping;
divQ_h.eval_pf_ggg  = eval.pf.ggg;

[xf_3D, yf_3D, divQf_h] = divQ_h.reconstruction(mesh1.basis);

% for el = 1:K^2
%     
% end

% determine error

ff_ex = f_an(xf_3D, yf_3D);

divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

[ff_sqrt, ff_sqre] = error_processor_v2(ff_ex, divQf_h, wfxy2, eval.pf.ggg);

err_divQ = qx_sqre + qy_sqre + ff_sqre;
err_divQ = sqrt(err_divQ);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), ff_ex(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h(:,:,eln))
% end
% colorbar

%% H10 error 

% evaluate discrete grad p

for el = 1:K^2
    u_h(:,el) = E211' * p_h(:,el) + N' * lm_h(:,el);
%     u_h(:,el) = E211' * p_h(:,el);
end

for el = 1:K^2
%     p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
    u_h_2(:,el) = M11_2(:,:,el)\u_h(:,el);
end
   
% reconstruction of 1-form 

gradp = oneForm;
gradp.K = K;
gradp.p = p;
gradp.pf = pf;

gradp.reconstruction(u_h_2, ttl_nr_el, mesh1);

[error.gradp_x, gradp_x_sqre] = error_processor_v2(qx_ex, gradp.rec_qx, wfxy2, eval.pf.ggg);
[error.gradp_y, gradp_y_sqre] = error_processor_v2(qy_ex, gradp.rec_qy, wfxy2, eval.pf.ggg);

err_H1p2 = err_H1(pp_sqre, gradp_x_sqre, gradp_y_sqre)

% reconstruct grad p using 1-form reconstruction

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_x(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), qx_ex(:,:,eln))
% end
% colorbar

% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_y(:,:,eln))
% end
% colorbar

%% error in mass conservation 

Err_mass = divQ - F_3D;

mass_h = twoForm();

mass_h.domain = mesh1.domain;
mass_h.K = K;
mass_h.p = p;

mass_h.el_bounds_x = mesh1.el.bounds.x;
mass_h.el_bounds_y = mesh1.el.bounds.y;

mass_h.cochain      = Err_mass;
mass_h.pf           = pf;
mass_h.el_mapping   = mesh1.el.mapping;
mass_h.eval_pf_ggg  = eval.pf.ggg;

[xf_3D, yf_3D, massf_h] = mass_h.reconstruction(mesh1.basis);

mass_ex = zeros(size(xf_3D));

massf_h = permute(massf_h, [2 1 3]); % transpose along dim = 3.

[mass_sqrt, mass_sqre] = error_processor_v2(mass_ex, massf_h, wfxy2, eval.pf.ggg);