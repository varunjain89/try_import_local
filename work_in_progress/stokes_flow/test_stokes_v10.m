%% Created on 29th Sep, 2018 
%% as beautiful as marilyn monroe 

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../finished_programs/elasticity_eq')

%% analytical cases 

mu = 1;

%--------- Case 0 

u_an     = @(x,y) 0*ones(size(x));
v_an     = @(x,y) 0*ones(size(x));
p_an     = @(x,y) (1-x.^2).*(1-y.^2);

dp_dx_an = @(x,y) -2*x.*(1-y.^2);
dp_dy_an = @(x,y) -2*y.*(1-x.^2);

du_dx_an = @(x,y) 0*ones(size(x));
du_dy_an = @(x,y) 0*ones(size(x));
dv_dx_an = @(x,y) 0*ones(size(x));
dv_dy_an = @(x,y) 0*ones(size(x));

d2u_dx2  = @(x,y) 0*ones(size(x));
d2u_dy2  = @(x,y) 0*ones(size(x));
d2v_dx2  = @(x,y) 0*ones(size(x));
d2v_dy2  = @(x,y) 0*ones(size(x));

%--------- Case I 

% u_an     = @(x,y) 0*ones(size(x));
% v_an     = @(x,y) 0*ones(size(x));
% p_an     = @(x,y) sin(pi*x).*sin(pi*y);
% 
% dp_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
% dp_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
% 
% du_dx_an = @(x,y) 0*ones(size(x));
% du_dy_an = @(x,y) 0*ones(size(x));
% dv_dx_an = @(x,y) 0*ones(size(x));
% dv_dy_an = @(x,y) 0*ones(size(x));
% 
% d2u_dx2  = @(x,y) 0*ones(size(x));
% d2u_dy2  = @(x,y) 0*ones(size(x));
% d2v_dx2  = @(x,y) 0*ones(size(x));
% d2v_dy2  = @(x,y) 0*ones(size(x));

%--------- Case II

% u_an     = @(x,y) -sin(pi*x).*cos(pi*y);
% v_an     = @(x,y) cos(pi*x).*sin(pi*y);
% p_an     = @(x,y) sin(pi*x).*sin(pi*y);
% 
% dp_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
% dp_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
% 
% du_dx_an = @(x,y) -pi*cos(pi*x).*cos(pi*y);
% du_dy_an = @(x,y) pi*sin(pi*x).*sin(pi*y);
% dv_dx_an = @(x,y) -pi*sin(pi*x).*sin(pi*y);
% dv_dy_an = @(x,y) pi*cos(pi*x).*cos(pi*y);
% 
% d2u_dx2  = @(x,y) pi^2*sin(pi*x).*cos(pi*y);
% d2u_dy2  = @(x,y) pi^2*sin(pi*x).*cos(pi*y);
% d2v_dx2  = @(x,y) -pi^2*cos(pi*x).*sin(pi*y);
% d2v_dy2  = @(x,y) -pi^2*cos(pi*x).*sin(pi*y);

%--------- 

fx_an      = @(x,y) (dp_dx_an(x,y) - mu*(d2u_dx2(x,y) + d2u_dy2(x,y) ));
fy_an      = @(x,y) (dp_dy_an(x,y) - mu*(d2v_dx2(x,y) + d2v_dy2(x,y) ));
tq_an      = @(x,y) -y.*fx_an(x,y) + x.*fy_an(x,y);

e_xx_an = @(x,y) du_dx_an(x,y);
e_yy_an = @(x,y) dv_dy_an(x,y);
e_xy_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));
e_yx_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));

tau_xx_an  = @(x,y) mu*e_xx_an(x,y) - p_an(x,y);
tau_yy_an  = @(x,y) mu*e_yy_an(x,y) - p_an(x,y);
tau_xy_an  = @(x,y) mu*e_xy_an(x,y);
tau_yx_an  = @(x,y) mu*e_xy_an(x,y);

q_x_an     = @(x,y) u_an(x,y);
q_y_an     = @(x,y) v_an(x,y);
w_an       = @(x,y) 0.5*(dv_dx_an(x,y) - du_dy_an(x,y));

lambdaX_an  = @(x,y) u_an(x,y) + y.*w_an(x,y);
lambdaY_an  = @(x,y) v_an(x,y) - x.*w_an(x,y);
% lambdaP_an = @(x,y) p_an(x,y);

bc_u_b = @(x,y) u_an(x,y);
bc_u_t = @(x,y) u_an(x,y);
bc_u_l = @(x,y) u_an(x,y);
bc_u_r = @(x,y) u_an(x,y);

bc_v_b = @(x,y) v_an(x,y);
bc_v_t = @(x,y) v_an(x,y);
bc_v_l = @(x,y) v_an(x,y);
bc_v_r = @(x,y) v_an(x,y);

%%
K = 1;
p = 4;

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

[LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = elasticity_non_staggered.local_numbering(p);

%% constitutive mass matrix

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

domain_mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

[element_bounds_x, ~] = GLLnodes(K);
element_bounds_y = element_bounds_x;

el_mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain_mapping, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain_dX_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain_dX_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain_dY_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain_dY_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);

[xp, ~] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_p_dx_dxii(:,:,el) = el_dX_dxii(xip,etap,elx,ely);
        eval_p_dx_deta(:,:,el) = el_dX_deta(xip,etap,elx,ely);
        eval_p_dy_dxii(:,:,el) = el_dY_dxii(xip,etap,elx,ely);
        eval_p_dy_deta(:,:,el) = el_dY_deta(xip,etap,elx,ely);
    end
end

[g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44] = new_methods.metric_terms_Stokes_v3(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);

MS = elasticity_non_staggered.full_mass_matrix_g_v2_vectorize(p, g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44);

%% mass matrix q 

eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);
eval_p_g11 = metric.g11(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g12 = metric.g12(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g22 = metric.g22(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);

M11 = mass_matrix.M1_3(p, eval_p_g11(:,:,el), eval_p_g12(:,:,el), eval_p_g22(:,:,el));

%% new mass matrix for 2nd constittutive law 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

% p_int = p+1;
% [quad_int_xi, wp_int] = GLLnodes(p_int);

[x_int,y_int] = meshgrid(xp);
x_int = x_int';
y_int = y_int';

% plot(x_int,y_int,'+r')

MAM = zeros(2*p*(p+1),4*p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        
%         xi_i1 = xp(i);
%         xi_i2 = xp(i+1);
%         eta_j = xp(j);
        
        % points on physical domain
        %         [x_int,y_int] = map_local_edge(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         [x_int,y_int] = el_mapping(quad_int_xi,quad_int_xi,1,1);
        
        %------ with Txx 
%         for k = 1:p+1
%             for l = 1:p
%                 dofkl = LN_tau_xx(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - 0*x_int(xq,yq)*nu*ep(i,xq)*hp(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Tyx
        for k = 1:p
            for l = 1:p+1
                dofkl = p*(p+1) + LN_tau_yx(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - y_int(xq,yq)*ep(i,xq)*hp(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Txy
%         for k = 1:p+1
%             for l = 1:p
%                 dofkl = 2*p*(p+1) + LN_tau_xy(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + 0*ep(i,xq)*hp(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Tyy
        for k = 1:p
            for l =1:p+1
                dofkl = 3*p*(p+1) + LN_tau_yy(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + x_int(xq,yq)*ep(i,xq)*hp(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
    end
end

for i = 1:p+1
    for j =1:p
        edgeij = p*(p+1) + (i-1)*p +j;
        
%         xi_i  = xp(i);
%         eta_j1 = xp(j);
%         eta_j2 = xp(j+1);
        
        % points on physical domain
%         [x_int,y_int] = map_local_y_edge(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
        %------ with Txx 
        for k = 1:p+1
            for l = 1:p
                dofkl = LN_tau_xx(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - y_int(xq,yq)*hp(i,xq)*ep(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Tyx
%         for k = 1:p
%             for l = 1:p+1
%                 dofkl = p*(p+1) + LN_tau_yx(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - *hp(i,xq)*ep(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Txy
        for k = 1:p+1
            for l = 1:p
                dofkl = 2*p*(p+1) + LN_tau_xy(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + x_int(xq,yq)*hp(i,xq)*ep(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Tyy
%         for k = 1:p
%             for l =1:p+1
%                 dofkl = 3*p*(p+1) + LN_tau_yy(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + y_int(xq,yq)*nu*hp(i,xq)*ep(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end

    end
end

% spy(MAM)

%% conservation of linear momentum

Ex = elasticity_non_staggered.incidence_matrix.linear_momentum_x(p, LN_tau_xx, LN_tau_yx);
Ey = elasticity_non_staggered.incidence_matrix.linear_momentum_y(p, LN_tau_yy, LN_tau_xy);

Exy = [Ex; Ey];

%% conservation of angular momentum 

am_E21 = elasticity_non_staggered.incidence_matrix.AM_incidence_matrix(p);

%% conservation of mass 

E21q = incidence_matrix.E21(p);

%% LHS system 

% LHS = zeros(9*p*(p+1) + 4*p^2);

temp1 = p*(p+1);
temp2 = p^2;

%% E10p

% for fx

for i = 1:p
    for j = 1:p
        volij = (i-1)*p+j;
        
        p_left  = p*(p+1) + (i-1)*p + j;
        p_right = p*(p+1) + i*p + j;
        
        E10p(volij, p_left)  = -1;
        E10p(volij, p_right) = +1;
    end
end

% for fy

for i = 1:p
    for j = 1:p
        volij = p^2 + (i-1)*p+j;
        
        p_top = j*p + i;
        p_bot = (j-1)*p + i;
        
        E10p(volij, p_bot) = -1;
        E10p(volij, p_top) = +1;
    end
end

xxx = E10p;

yyy = eye(2*temp1);
zzz = E21q';

%% dp_dx and dp_dy 

divP = zeros(2*p^2,2*p*(p+1));

for i = 1:p
    for j = 1:p
        volij = (i-1)*p + j;
        
        p_left  = p*(p+1) + (i-1)*p + j;
        p_right = p*(p+1) + i*p + j;
        
        divP(volij, p_left)  = -1;
        divP(volij, p_right) = +1;
        
        p_bot = (j-1)*p + i;
        p_top = j*p + i;

        divP(p^2 + volij, p_bot) = -1;
        divP(p^2 + volij, p_top) = +1;
    end
end

%% stress Torque calculation

TQ_shear = elasticity_non_staggered.torque.torque_shear_v3(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);
TQ_norm  = elasticity_non_staggered.torque.torque_normal_v3(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);

TQ = (TQ_shear + TQ_norm);
AM4    = am_E21*TQ;

%% pressure torque calculation

TQ_press  = new_methods.torque_normal_pressure_v1(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);

AMP = am_E21 * TQ_press;

% spy(TQ_press)

%% LHS system 

TZ1 = zeros(2*temp1, 4*temp1);
TZ2 = zeros(4*temp1);
TZ3 = zeros(p^2, 4*temp1);
TZ4 = zeros(2*p^2, 4*temp1);
TZ5 = zeros(p^2, 2*temp1);
TZ6 = zeros(2*p^2, 2*temp1);
TZ7 = zeros(2*temp1);
TZ8 = zeros(2*p^2,p^2);
TZ9 = zeros(p^2);
TZ0 = zeros(2*p^2);

LHS = [MS   TZ1'    Exy'    AM4';
       TZ1  M11     -divP'  -AMP';
       Exy  -divP   TZ0     TZ8 ;
       AM4  -AMP    TZ8'    TZ9];

det(LHS)
cond(LHS)
% spy(LHS)

%% RHS system

RHS = zeros(6*p*(p+1) + 3*p^2, 1);

Fx_h = reduction.of2form_multi_element_5(fx_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);
Fy_h = reduction.of2form_multi_element_5(fy_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);
TQ_h = reduction.of2form_multi_element_5(tq_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);

RHS(6*p*(p+1) + 1 : 6*p*(p+1) + p^2)         = -Fx_h;
RHS(6*p*(p+1) + p^2 + 1 : 6*p*(p+1) + 2*p^2) = -Fy_h;
RHS(6*p*(p+1) + 2*p^2 + 1:6*p*(p+1) + 3*p^2) = -TQ_h;

%% boundary conditions

%% direct solver 

X_h = LHS\RHS;

%% separating cochains

temp = p*(p+1);

TXX_h = X_h(1:temp);
TYX_h = X_h(temp+1:2*temp);
TXY_h = X_h(2*temp+1:3*temp);
TYY_h = X_h(3*temp+1:4*temp);
Px_h = X_h(4*temp+1:5*temp);
Py_h = X_h(5*temp+1:6*temp);

U_h   = X_h(6*temp+1:6*temp+p^2);
V_h   = X_h(6*temp+p^2+1:6*temp+2*p^2);
W_h   = X_h(6*temp+2*p^2+1:6*temp+3*p^2);

%% reconstruction

ttl_nr_el = K^2;

M22s = zeros(p^2, p^2, ttl_nr_el);

for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, eval_p_ggg(:,:,el));
end

U_h_2 = M22s\U_h;
V_h_2 = M22s\V_h;
W_h_2 = M22s\W_h;

pf = 30;

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);

eval_pf_dx_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dx_deta = zeros(pf+1,pf+1,K^2);
eval_pf_dy_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dy_deta = zeros(pf+1,pf+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_pf_dx_dxii(:,:,el) = el_dX_dxii(xif,etaf,elx,ely);
        eval_pf_dx_deta(:,:,el) = el_dX_deta(xif,etaf,elx,ely);
        eval_pf_dy_dxii(:,:,el) = el_dY_dxii(xif,etaf,elx,ely);
        eval_pf_dy_deta(:,:,el) = el_dY_deta(xif,etaf,elx,ely);
    end
end

eval_pf_ggg = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);

%% reconstruction of lambdaX

[xf_3D, yf_3D, uf_h] = reconstruction.of2form_multi_element_v4(U_h_2, p, pf, el_mapping, eval_pf_ggg, K);

set(0,'defaultfigureposition',[0 0 1500 800])
figure
subplot(4,4,13)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), uf_h(:,:,el)')
end
title('computed U');
colorbar

u_plot = lambdaX_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,9)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el))
% contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el),'edgecolor','none')
title('analytical U');
colorbar

%% reconstruction of lambdaY

[xf_3D, yf_3D, vf_h] = reconstruction.of2form_multi_element_v4(V_h_2, p, pf, el_mapping, eval_pf_ggg, K);


subplot(4,4,14)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), vf_h(:,:,el)')
end
title('computed V');
colorbar

v_plot = lambdaY_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,10)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), v_plot(:,:,el))
title('analytical V');
colorbar

%% reconstruction of W - omega / rotation

[xf_3D, yf_3D, wf_h] = reconstruction.of2form_multi_element_v4(W_h_2, p, pf, el_mapping, eval_pf_ggg, K);

subplot(4,4,15)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), wf_h(:,:,el)')
end
title('computed W');
colorbar

w_plot = w_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,11)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), w_plot(:,:,el))
title('analytical W');
colorbar

%% reconstruction of txx

tau_xx_f_h = reconstruction.reconstruct1yform_2(TYY_h(:,el), TXX_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));

subplot(4,4,5)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xx_f_h(:,:,el)')
title('computed tau_xx');
colorbar

txx_plot = tau_xx_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txx_plot(:,:,el))
title('analytical tau_xx');
colorbar

%% reconstruction of tyy

tau_yy_f_h = reconstruction.reconstruct1xform_2(TYY_h(:,el), TXX_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));

subplot(4,4,6)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yy_f_h(:,:,el)')
title('computed tau_yy');
colorbar

tyy_plot = tau_yy_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(4,4,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyy_plot(:,:,el))
title('analytical tau_yy');
colorbar

%% reconstruction of txy

tau_xy_f_h = reconstruction.reconstruct1yform_2(TYX_h(:,el), TXY_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));

subplot(4,4,7)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xy_f_h(:,:,el)')
title('computed tau_xy');
colorbar

txy_plot = tau_xy_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(4,4,3)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txy_plot(:,:,el))
title('analytical tau_xy');
colorbar

%% reconstruction of tyx

tau_yx_f_h = reconstruction.reconstruct1xform_2(TYX_h(:,el), TXY_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));

subplot(4,4,8)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yx_f_h(:,:,el)')
title('computed tau_yx');
colorbar

tyx_plot = tau_yx_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(4,4,4)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyx_plot(:,:,el))
title('analytical tau_yx');
colorbar