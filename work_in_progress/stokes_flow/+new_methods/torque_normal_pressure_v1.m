function [TQ_norm] = torque_normal_pressure_v1(p, xbound, ybound, el_dX_dxii, el_dX_deta, el_dY_dxii, el_dY_deta)
%ANGULAR_MOMENTUM Summary of this function goes here
%   Detailed explanation goes here

% Created on: 18 May, 2018

% xbound = [-1 1];
% ybound = [-1 1];
c = 0.0;

[xp,~] = GLLnodes(p);

%--- number of integration points to integrate the edge ----%

p_int = p+5;
[quad_int_xi, wp_int] = GLLnodes(p_int);

%---- initializing array for normal stress \sigma _yy
TQx = zeros(p*(p+1),2*p*(p+1));

% figure
% hold on

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        
        xi_i1 = xp(i);
        xi_i2 = xp(i+1);
        eta_j = xp(j);
        
        % points on physical domain
        [x_int,~] = map_local_edge(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         plot(x_int, y_int,'o')
        
        % local pull back of the edge / pull back at each integration point
        dx_dxi_int  = local_dx_dxi(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         dy_dxi_int  = local_dy_dxi(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         dx_deta_int = local_dx_deta(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         dy_deta_int = local_dy_deta(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         g_int = metric.ggg(dx_dxi_int, dx_deta_int, dy_dxi_int, dy_deta_int);
        % (x sigma_yy - y sigma_yx)
        
        % this should not be local but global 
%         dy_deta_g = dy_deta_int./ g_int;
%         dy_deta_g = magic_x;
        
        %this function maps local edge xi to global xi 
        xif  = 0.5*(xi_i1 + xi_i2) + 0.5*(xi_i2 - xi_i1).* quad_int_xi;
        etaf = eta_j;
        
        eval_pf_dx_dxii(:,:) = el_dX_dxii(xif,etaf);
        eval_pf_dx_deta(:,:) = el_dX_deta(xif,etaf);
        eval_pf_dy_dxii(:,:) = el_dY_dxii(xif,etaf);
        eval_pf_dy_deta(:,:) = el_dY_deta(xif,etaf);
        eval_pf_g = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
        
        eval_dy_deta_g = eval_pf_dy_deta./eval_pf_g;
        
        % the edge functions a integration point
        [~,ep_int] = MimeticpolyVal(xif,p,1);
        
        % the integral
        for k = 1:p
            dof_Tyy = (j-1)*p + k;
            for x = 1:p_int+1
%                 %%%% - old reference formulation - %%%
%                 TQx(edgeij, dof_Tyy) = TQx(edgeij, dof_Tyy) + x_int(x)*dx_dxi_int(x)*ep_int(k,x)*wp_int(x);
                %%% - new formulation with mapping - %%%
                TQx(edgeij, dof_Tyy) = TQx(edgeij, dof_Tyy) + x_int(x)*eval_dy_deta_g(x)*dx_dxi_int(x)*ep_int(k,x)*wp_int(x);
            end
        end
        
    end
end

% spy(TQx)

TQy = zeros(p*(p+1),2*p*(p+1));

for i = 1:p+1
    for j = 1:p
        edgeij = (i-1)*p + j;
        
        xi_i  = xp(i);
        eta_j1 = xp(j);
        eta_j2 = xp(j+1);
        
        % points on physical domain
        [~,y_int] = map_local_y_edge(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
        dy_deta_int = local_dy_deta(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         dx_deta_int = local_dx_deta(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         dy_dxii_int = local_dy_dxi(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         dx_dxii_int = local_dx_dxi(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
%         g_int = metric.ggg(dx_dxii_int, dx_deta_int, dy_dxii_int, dy_deta_int);
%         dx_dxii_g = dx_dxii_int./ g_int;
%         dx_dxii_g = magic_y;
        % (y sigma xx - x sigma_xy)
        
        %this function maps local edge xi to global xi 
        xif  = xi_i;
        etaf = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* quad_int_xi;
        
        eval_pf_dx_dxii(:,:) = el_dX_dxii(xif,etaf);
        eval_pf_dx_deta(:,:) = el_dX_deta(xif,etaf);
        eval_pf_dy_dxii(:,:) = el_dY_dxii(xif,etaf);
        eval_pf_dy_deta(:,:) = el_dY_deta(xif,etaf);
        eval_pf_g = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
        
        eval_dx_dxii_g = eval_pf_dx_dxii./eval_pf_g;
        
        [~,ep_int] = MimeticpolyVal(etaf,p,1);
        
        for l = 1:p
            dof_Txx = p*(p+1) + (i-1)*p + l;
            for y = 1:p_int+1
%                 TQy(edgeij, dof_Txx) = TQy(edgeij, dof_Txx) + y_int(y)*dy_deta_int(y)*ep_int(l,y)*wp_int(y);
%                 TQy(edgeij, dof_Txx) = TQy(edgeij, dof_Txx) - y_int(y)*dy_deta_int(y)*ep_int(l,y)*wp_int(y);
                TQy(edgeij, dof_Txx) = TQy(edgeij, dof_Txx) - y_int(y)*eval_dx_dxii_g(y)*dy_deta_int(y)*ep_int(l,y)*wp_int(y);
            end
        end
        
    end
end

% spy(TQy)

TQ_norm = [TQx; TQy];

end

