function [g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44] = metric_terms_Stokes_v3(dx_dxi, dx_deta, dy_dxi, dy_deta)
%METRIC_TERMS_YI Summary of this function goes here
%   Detailed explanation goes here

% Created On - 25 June, 2018

nu = 0.3;

J11  = dx_dxi;
J12  = dx_deta;
J21  = dy_dxi;
J22  = dy_deta;
detJ = dx_dxi.*dy_deta - dx_deta.*dy_dxi;

a = 1;
b = 1;
c = 1;
d = 1;
m = 0;
n = 0;

g11 = +(a .* J11 .* J11 + b .* J21 .* J21) ./ detJ;
g12 = -(a .* J12 .* J11 + b .* J21 .* J22) ./ detJ;
g13 = -m .* J21 .* J11 ./ detJ;
g14 = +m .* J22 .* J11 ./ detJ;

g21 = -(a .* J12 .* J11 + b .* J21 .* J22) ./ detJ;
g22 = +(a .* J12 .* J12 + b .* J22 .* J22) ./ detJ;
g23 = +m .* J21 .* J12 ./ detJ;
g24 = -m .* J22 .* J12 ./ detJ;

g31 = -n .* J11 .* J21 ./ detJ;
g32 = +n .* J12 .* J21 ./ detJ;
g33 = +(c .* J11 .* J11 + d .* J21 .* J21) ./ detJ;
g34 = -(c .* J12 .* J11 + d .* J21 .* J22) ./ detJ;

g41 = +n .* J11 .* J22 ./ detJ;
g42 = -n .* J12 .* J22 ./ detJ;
g43 = -(c .* J12 .* J11 + d .* J21 .* J22) ./ detJ;
g44 = +(c .* J12 .* J12 + d .* J22 .* J22) ./ detJ;

end

