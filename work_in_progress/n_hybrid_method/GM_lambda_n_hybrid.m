function [ gcf ] = GM_lambda_n_hybrid( K, p )
%GM_CONTINUOUS_1_FORM Summary of this function goes here
%   Detailed explanation goes here

% Created on  : 7th Feb, 2018
% Modified on : 12 Nov, 2018
% Modified on : 9 April, 2018
% Description : 

ttl_nr_el = K^2;
ttl_loc_dof = 2*p*(p-1)+p^2;

GM_local_dof = gather_matrix.GM_total_local_dof_v3(K^2, ttl_loc_dof);

GM_local_dof_q = GM_local_dof(:,1:2*p*(p-1));
% GM_local_dof_p = GM_local_dof(:,2*p*(p-1)+1:2*p*(p-1)+p^2);

ggv = gather_matrix.GM_boundary_LM_nodes(K,p);
ggv2 = ggv + ttl_nr_el * ttl_loc_dof;

% bottom boundary
gcf(1:p,:) = ggv2(1:p,:);
% top boundary
gcf(p^2+1:p^2+p,:) = ggv2(3*p+1:4*p,:);
% left boundary
gcf(p*(p+1) + 1: p*(p+1) + p,:) = ggv2(2*p+1:3*p,:);
% right boundary
gcf(p*(p+1) + p^2 +1: 2*p*(p+1),:) = ggv2(p+1:2*p,:);

gcf(p+1:p^2,:) = GM_local_dof_q(:,1:p*(p-1))';
gcf(p*(p+1)+p+1:p*(p+1)+p^2,:) = GM_local_dof_q(:,p*(p-1)+1:2*p*(p-1))';

end

