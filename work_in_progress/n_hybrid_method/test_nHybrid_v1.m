%% 8th April, 2019
%% i'm trying n - level hybridmethod to further reduce the problem size

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% analytical solution

% case 2

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

a = 1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% domain

xbound = [0 1];
ybound = [0 1];
c = 0.3;

%% discretization

K = 27;

Kx = K;
Ky = K;

p = 3;

levels = 3;

refine = zeros(levels,2);

r1x = 3;
riy = 3;

r2x = 3;
r2y = 3;

r3x = 3;
r3y = 3;

if (Kx ~= r1x * r2x * r3x)
    disp(['Number of elements DONT match !!! '])
else 
    disp(['Okay, you can continue !!!'])
end

%% 

GM_lambda_1 = gather_matrix.GM_boundary_LM_nodes(r3x,p);
nr_int_lambda_dof = max(max(GM_lambda_1))
GM_lambda_2 = gather_matrix.GM_lambda_n_hybrid_v2(r,c,p, nr_int_lambda_dof);

%%

ttl_nr_el = K^2;
local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% LHS systems

% evaluate jacobian at GLL points
% mesh1.eval_p_der();
mesh1.eval_p_der_v2();

% evaluate material properties at GLL points
mesh1.eval_dom_xy(); 
eval.K = flow.darcy.material(k11_an, k12_an, k22_an, mesh1.eval.xy);

% evaluate metric terms for M11
eval.p = flow.darcy.metric(mesh1.eval.p, eval.K);

mesh1.eval.metric = eval.p;

M11 = zeros(2*p*(p+1),2*p*(p+1),ttl_nr_el);

for eln = 1:ttl_nr_el
    % this part is already implicitly multi threaded (it seems) and has 
    % to go to different clusters for further improvement in efficiency
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% 

E211    = incidence_matrix.E21(p);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 K^2]);

sys_3D = cell(1,ttl_nr_el);

tic
for eln = 1:ttl_nr_el
    sys_3D(eln) = {[M11(:,:,eln) E211'; E211 sparse(p^2,p^2)]};
end
toc

A = blkdiag(sys_3D{:});

sys_3D_inv = cell(1,ttl_nr_el);

tic
parfor eln = 1:ttl_nr_el
    sys_3D_inv(eln) = {inv([M11(:,:,eln) E211'; E211 sparse(p^2,p^2)])};
end
toc

tic
A_inv = blkdiag(sys_3D_inv{:});
toc

%% assembly of system

ttl_loc_dof = 2*p*(p+1)+p^2;
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);

% figure
% spy(ass_conn2)

%%

r = 9;
cc = 3;

temp_GM_lam2 = gather_matrix.GM_lambda_n_hybrid(r,cc,p);

% count = 0;

ccx = cc;
ccy = cc;

rx = r;
ry = r;

B = 1:rx*ry;
B = reshape(B,[rx,ry]); % check this if diffferent rx and ry

for el = 1:ccy
    B2((el-1)*r+1:el*r,:) = (el-1)*(rx*ry) + B; %check this
end

B2 = B2(:);
% B2(28:54) = 27 +B2(1:27);
% B2(55:81) = 54 +B2(1:27);

for el = 1:ccx
    B3((el-1)*ccy*(rx*ry)+1:el*ccy*(rx*ry)) = (el-1)*ccy*(rx*ry) + B2;
end

for i = 1:ccx*rx*ccy*ry
    temp_GM_lam3(:,i) = temp_GM_lam2(:,B3(i));
end

% spy(temp_GM_lam3)

AA = 1:p;

for i = 1:K
    eleid_bot = (i-1)*K + 1;
    temp_GM_lam3(1:p, eleid_bot) = AA + 2*K*(K-1)*p + (i-1)*p;
    
    eleid_right = K*(K-1) + i;
    temp_GM_lam3(p+1:2*p, eleid_right) = AA + 2*K*(K-1)*p + K*p + (i-1)*p;
    
    eleid_left = i;
    temp_GM_lam3(2*p+1:3*p, eleid_left) = AA + 2*K*(K-1)*p + 2*K*p + (i-1)*p;
    
    eleid_top = K + (i-1)*K;
    temp_GM_lam3(3*p+1:4*p, eleid_top) = AA + 2*K*(K-1)*p + 3*K*p + (i-1)*p;
end

% spy(temp_GM_lam3)

ass_conn22 = AssembleMatrices2(temp_GM_lam3, GM_local_dof_q', N_3D);

figure
spy(ass_conn22)

ass_conn2 = ass_conn22;

%% RHS

volForm = twoForm_v2(mesh1);

F_3D = volForm.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + 2*p*(p+1);
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

%% calculate lambda 

% ii2 = [i1;i2;i3];
% jj2 = [j1;j2;j3];
% ss2 = [s1;s2;s3];
% 
% size_LHS2 = ttl_loc_dof*ttl_nr_el;
% A = sparse(ii2, jj2, ss2, size_LHS2, size_LHS2);

% spy(A)
% implementing dirichlet BCs
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];
ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);

%%

temp2 = full((ass_conn3 * A_inv*ass_conn3'));
spy(temp2)
this1 = det(temp2)
this2 = cond(temp2)

lambda = (ass_conn3 * A_inv*ass_conn3')\(ass_conn3*A_inv*RHS);

