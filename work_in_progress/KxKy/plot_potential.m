function [ ] = plot_potential(pp_ex,rec_p,rec_x,rec_y,ttl_nr_el)

figure
hold on

for eln = 1:ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'LineColor','none')
end

colorbar
title('exact solution p')
set(gcf, 'Position',  [0100, 500, 550, 400])
% caxis([-1 1])

figure
hold on

for eln = 1:ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'LineColor','none')
end
colorbar
title('reconstructed solution p')
set(gcf, 'Position',  [700, 500, 550, 400])
% caxis([-1 1])

figure
hold on

for eln = 1:ttl_nr_el
    surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln))
end
colorbar
title('difference p')
set(gcf, 'Position',  [1300, 500, 550, 400])

end

