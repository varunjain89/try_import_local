%% script settings

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% test case

test.phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
test.dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
test.dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
test.d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
test.d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
test.d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

% if Darcy 

% a = 0.1;
% 
% test.k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
% test.k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
% test.k21_an  = @(x,y) k12(x,y);
% test.k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
% test.dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
% test.dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
% test.dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
% test.dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

test.bc_left = @(x,y) test.phi_an(x,y);
test.bc_rght = @(x,y) test.phi_an(x,y);
test.bc_topp = @(x,y) test.phi_an(x,y);
test.bc_bott = @(x,y) test.phi_an(x,y);

%% analytical solution 

% if Poisson

analytic.ux = @(x,y) test.dp_dx(x,y);
analytic.uy = @(x,y) test.dp_dy(x,y);
analytic.ff = @(x,y) test.d2p_dx2(x,y) + test.d2p_dy2(x,y);

% if Darcy

% analytic.qx = @(x,y) -test.k22_an(x,y).*test.dp_dy(x,y) - test.k12_an(x,y).*test.dp_dx(x,y);
% analytic.qy = @(x,y)  test.k11_an(x,y).*test.dp_dx(x,y) + test.k12_an(x,y).*test.dp_dy(x,y);
% analytic.ff   = @(x,y) test.dk11_dx(x,y).*test.dp_dx(x,y) + test.k11_an(x,y).*test.d2p_dx2(x,y)...
%               + test.dk12_dx(x,y).*test.dp_dy(x,y) + test.k12_an(x,y).*test.d2p_dxdy(x,y)...
%               + test.dk12_dy(x,y).*test.dp_dx(x,y) + test.k12_an(x,y).*test.d2p_dxdy(x,y)...
%               + test.dk22_dy(x,y).*test.dp_dy(x,y) + test.k22_an(x,y).*test.d2p_dy2(x,y);

%% domain definition

xbound = [2.1 3.4];
ybound = [-1.7 0.1];
c = 0.1;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% mesh object

mesh00 = n_hybrid_meshv2(domain);

% check domain mapping
% mesh00.check_domain_mapping();

%% discretize the doamin 

Kx = 3;
Ky = 3;

mesh00.discretize(Kx, Ky);

% check element mappign
% mesh00.check_element_mapping();

p = 10;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;
ttl_nr_lambda = Kx*p*(Ky+1) + Ky*p*(Kx+1);
nr_int_lambda = Kx*p*(Ky-1) + Ky*p*(Kx-1);

%% check reduction/reconstruction for mappings 

% check.red_rec_2formv2(analytic.ff, mesh00, p);
% check.red_rec_1form_inner(analytic.ux,analytic.uy,mesh00,p);

%% M11

% ------- calculate metric terms -------

[dtize.xp, dtize.wp] = GLLnodes(p);
[dtize.hp, dtize.ep] = MimeticpolyVal(dtize.xp,p,1);

[dtize.xii, dtize.eta] = meshgrid(dtize.xp);
dtize.jac = mesh00.evaluate_jacobian_all(dtize.xii, dtize.eta);
dtize.metric = flow.poisson.eval_metric(dtize.jac);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, dtize.metric.g11(:,:,eln), dtize.metric.g12(:,:,eln), dtize.metric.g22(:,:,eln), dtize);
end

%% E21

E211    = incidence_matrix.E21(p);

%% EN1 (and some boundary conditions)

GM_lambda = gather_matrix.GM_boundary_LM_nodes_KxKy(Kx,Ky,p); % needs to be updated for Kx and Ky

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh00.ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
% GM_local_dof_p = GM_local_dof(:,loc_dof_u+1:loc_dof_u+loc_dof_p);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 mesh00.ttl_nr_el]);

EN1 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
EN1 = [EN1 sparse(ttl_nr_lambda,loc_dof_p)];

% get indices of lambda with Dirichlet BCs
idx_bott = nr_int_lambda + 1 : nr_int_lambda + Kx * p;
idx_left = idx_bott(end) + 1 : idx_bott(end) + Ky * p;
idx_rght = idx_left(end) + 1 : idx_left(end) + Ky * p;
idx_topp = idx_rght(end) + 1 : idx_rght(end) + Kx * p;

idx_dirichlet = [idx_bott idx_left idx_rght idx_topp];

% remove rows with Dirichlet BCs

EN1(idx_dirichlet,:) = [];

%% RHS term

dof_f = zeros(loc_dof_p, mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    M22 = mass_matrix.M2_6(dtize, dtize.metric.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(analytic.ff, mesh00.el(eln).mapping, dtize, M22);
end

F0(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

F0 = F0(:);

%% Dirichlet boundary conditions

% left

BC_Dirichlet_left = zeros(p,Ky);
nn_left = -1;

for eln = 1:Ky
    map_left = @(tt) mesh00.el(eln).mapping(-1,tt);
    BC_Dirichlet_left(:,eln) = dirichlet_bc(map_left,dtize,test.bc_left,p,nn_left);
end

% right

BC_Dirichlet_rght = zeros(p,Ky);
nn_rght = 1;

for eln = 1:Ky
    ele_id = (Kx-1)*Ky+eln;
    map_rght = @(tt) mesh00.el(ele_id).mapping(+1,tt);
    BC_Dirichlet_rght(:,eln) = dirichlet_bc(map_rght,dtize,test.bc_rght,p,nn_rght);
end

% bottom

BC_Dirichlet_bott = zeros(p,Kx);
nn_bott = -1;

for eln = 1:Kx
    ele_id = (eln-1)*Ky+1;
    map_bott = @(tt) mesh00.el(ele_id).mapping(tt,-1);
    BC_Dirichlet_bott(:,eln) = dirichlet_bc(map_bott,dtize,test.bc_bott,p,nn_bott);
end

% top

BC_Dirichlet_topp = zeros(p,Kx);
nn_topp = +1;

for eln = 1:Kx
    ele_id = eln*Ky;
    map_topp = @(tt) mesh00.el(ele_id).mapping(tt,+1);
    BC_Dirichlet_topp(:,eln) = dirichlet_bc(map_topp,dtize,test.bc_topp,p,nn_topp);
end

% for bottom edge 

loc_dof_u_bott = 1:p;
loc_dof_K_bott = 1:Ky:(Kx-1)*Ky+1;
idx_bc_bott = GM_local_dof_q(loc_dof_K_bott,loc_dof_u_bott)';

%substituting BC in RHS
F0(idx_bc_bott) = BC_Dirichlet_bott;

% for right edge 

loc_dof_u_rght = loc_dof_u - p + 1 : loc_dof_u;
loc_dof_K_rght = (Kx-1)*Ky+1 : Kx*Ky;
idx_bc_rght = GM_local_dof_q(loc_dof_K_rght,loc_dof_u_rght)';

%substituting BC in RHS
F0(idx_bc_rght) = BC_Dirichlet_rght;

% for left edge 

loc_dof_u_left = loc_dof_u/2 + 1 : loc_dof_u/2 + p;
loc_dof_K_left = 1:Ky;
idx_bc_left = GM_local_dof_q(loc_dof_K_left,loc_dof_u_left)';

%substituting BC in RHS
F0(idx_bc_left) = BC_Dirichlet_left;

% for top edge 

loc_dof_u_topp = p^2+1 : p^2+p;
loc_dof_K_topp = Ky:Ky:Ky*Kx;
idx_bc_topp = GM_local_dof_q(loc_dof_K_topp,loc_dof_u_topp)';

%substituting BC in RHS
F0(idx_bc_topp) = BC_Dirichlet_topp;

%% Neumann boundary conditions

% --- to be done ---

%% evaluate lambda

% calculate inverse of A

sys_3D = cell(1,mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    sys_3D(eln) = {[M11(:,:,eln) E211'; E211 sparse(p^2,p^2)]};
end

A = blkdiag(sys_3D{:});

sys_3D_inv = cell(1,mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    sys_3D_inv(eln) = {inv(sys_3D{eln})};
end

A11 = blkdiag(sys_3D_inv{:});

% --- to be done --- add Neumann boundary conditions 
lambda = (EN1 * A11 * EN1') \ (EN1 * A11 * F0);

% add the rows with Dirichlet BCs again
lambda2 = [lambda(1:nr_int_lambda); zeros(size(idx_bott))'; zeros(size(idx_left))'; zeros(size(idx_rght))'; zeros(size(idx_topp))'];

% sort lambda
lambda_h = zeros(4*p,mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    lambda_h(:,eln) = lambda2(GM_lambda(:,eln));
end

%% evaluate element-wise X_i

X_h3 = cell(1,mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    X_h3(eln) = {sys_3D_inv{eln} * ([zeros(2*p*(p+1),1); dof_f(:,eln)] - [N'*lambda_h(:,eln); zeros(p^2,1)])};
end

%% save file 

%% separating cochains 

q_h = zeros(loc_dof_u, mesh00.ttl_nr_el);
p_h = zeros(loc_dof_p, mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:2*p*(p+1));
    p_h(:,eln) = X_h3{eln}(2*p*(p+1) + 1:2*p*(p+1)+ p^2);
end

%% 

pf = p+5;

[s,w] = GLLnodes(pf);

[wfx, wfy] = meshgrid(w);
wfxy  = wfx .* wfy;
wxwy = repmat(wfxy, 1, 1, mesh00.ttl_nr_el);

[basis_rec.hf, basis_rec.ef] = MimeticpolyVal(s,p,1);

[rec_xii, rec_eta] = meshgrid(s, s);
jac_rec = mesh00.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

%% post process flux

rec_x = zeros(size(rec_xii,1),size(rec_xii,2),mesh00.ttl_nr_el);
rec_y = zeros(size(rec_x));

for eln = 1:mesh00.ttl_nr_el
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh00.el(eln).mapping(rec_xii, rec_eta);
end

% exact solution
ux_ex = analytic.ux(rec_x,rec_y);
uy_ex = analytic.uy(rec_x,rec_y);

rec_normal_ux = zeros(size(rec_x));
rec_normal_uy = zeros(size(rec_x));

% reconstructed solution
for eln = 1:mesh00.ttl_nr_el
    rec_normal_ux(:,:,eln) = reconstruction.reconstruct1xform_v9(q_h(1:p*(p+1),eln), q_h(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln,basis_rec);
    rec_normal_uy(:,:,eln) = reconstruction.reconstruct1yform_v9(q_h(1:p*(p+1),eln), q_h(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln,basis_rec);
end

% l2-error
rec_normal_ux = permute(rec_normal_ux, [2 1 3]); % transpose along dim = 3.
rec_normal_uy = permute(rec_normal_uy, [2 1 3]); % transpose along dim = 3.

[error.qx] = error_processor_v3(ux_ex, rec_normal_ux, wxwy, metric_rec.ggg);
[error.qy] = error_processor_v3(uy_ex, rec_normal_uy, wxwy, metric_rec.ggg);

% velocity plots
% rec_normal_ux = permute(rec_normal_ux, [2 1 3]); % transpose along dim = 3.
% rec_normal_uy = permute(rec_normal_uy, [2 1 3]); % transpose along dim = 3.
% plot_normal_velocity(ux_ex,rec_normal_ux,uy_ex,rec_normal_uy,rec_x,rec_y,mesh00.ttl_nr_el);

%% post process potential

sx = GLLnodes(pf);
sy = sx;

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh00.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

for eln = 1:mesh00.ttl_nr_el
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh00.el(eln).mapping(rec_xii, rec_eta);
end

% exact solution 
pp_ex = test.phi_an(rec_x,rec_y);

% post processing dual to primal
p_h_2 = zeros(size(p_h));

for eln = 1:mesh00.ttl_nr_el
    M22 = mass_matrix.M2_6(dtize, dtize.metric.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

rec_pp = zeros(size(metric_rec.ggg));

% reconstruction pressure
for eln = 1:mesh00.ttl_nr_el
    rec_pp(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
end

% l2-error
[error.pp] = error_processor_v3(pp_ex, rec_pp, wxwy, metric_rec.ggg);

gradp = zeros(loc_dof_u,mesh00.ttl_nr_el);

% H1-error
for eln = 1:mesh00.ttl_nr_el
    gradp(:,eln) = - E211' * p_h(:,eln) - N' * lambda_h(:,eln);
end

% post processing dual to primal
gradp_2 = zeros(size(gradp));

for eln = 1:mesh00.ttl_nr_el
    gradp_2(:,eln) = M11(:,:,eln)\gradp(:,eln);
end

rec_tang_ux = zeros(size(rec_x));
rec_tang_uy = zeros(size(rec_x));

% reconstructed solution
for eln = 1:mesh00.ttl_nr_el
    rec_tang_ux(:,:,eln) = reconstruction.reconstruct1xform_v9(gradp_2(1:p*(p+1),eln), gradp_2(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln,basis_rec);
    rec_tang_uy(:,:,eln) = reconstruction.reconstruct1yform_v9(gradp_2(1:p*(p+1),eln), gradp_2(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln,basis_rec);
end

% l2-error
rec_tang_ux = permute(rec_tang_ux, [2 1 3]); % transpose along dim = 3.
rec_tang_uy = permute(rec_tang_uy, [2 1 3]); % transpose along dim = 3.

[error.tang_ux] = error_processor_v3(ux_ex, rec_tang_ux, wxwy, metric_rec.ggg);
[error.tang_uy] = error_processor_v3(uy_ex, rec_tang_uy, wxwy, metric_rec.ggg);

% velocity plots
% plot_normal_velocity(ux_ex,rec_tang_ux,uy_ex,rec_tang_uy,rec_x,rec_y,mesh00.ttl_nr_el);

error.pp_H1.sqrt = sqrt(error.pp.sqre + error.tang_ux.sqre + error.tang_uy.sqre);

% pressure plots
plot_potential(pp_ex,rec_pp,rec_x,rec_y,mesh00.ttl_nr_el)

%% post process lambda at all edges

% create 1-D M11
M1_1D = mass_matrix.dim_1.M1(p,dtize.ep,dtize.wp);

% get lambda dof of the corresponding edge
lam_dof = lambda_h(3*p+1:4*p,1);

%change from dual_dof to primal_dof
primal_lambda_dof = M1_1D \ lam_dof;

% reconstruction of lambda 
lambda_rec = reconstruction.dim_1.edge(primal_lambda_dof,basis_rec.ef);

% mapping of corrresponding edge
[x_rec_pt,y_rec_pt] = mesh00.el(1).mapping(s,1);

% exact lambda @ x,y points
lambda_ex = test.phi_an(x_rec_pt,y_rec_pt);

% l2-error
g = 1;              %%%% check this term with Marc

l2_err_lambda = (lambda_ex-lambda_rec) .^ 2 .* w * g;
l2_err_lambda = sum(l2_err_lambda);
l2_err_lambda = sqrt(l2_err_lambda);

% plot results
figure
plot(x_rec_pt,lambda_ex,'-o')
hold on
plot(x_rec_pt,lambda_rec,'-+')

%% conservation of mass

divQ = zeros(loc_dof_p,mesh00.ttl_nr_el);

for el = 1:mesh00.ttl_nr_el
    divQ(:,el) = E211 * q_h(:,el);
end

cont_const = divQ - dof_f;

rec_cont_const = zeros(size(metric_rec.ggg));

% reconstruction pressure
for eln = 1:mesh00.ttl_nr_el
    rec_cont_const(:,:,eln) = reconstruction.reconstruct2form_v6(cont_const(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
end

% l2-error
[error.cont_const] = error_processor_v3(zeros(size(rec_cont_const)), rec_cont_const, wxwy, metric_rec.ggg);

% pressure plots
% plot_potential(rand(size(rec_cont_const))*1e-20,rec_cont_const,rec_x,rec_y,mesh00.ttl_nr_el)

% Hdiv error
error.q_Hdiv.sqrt = sqrt(error.qx.sqre + error.qy.sqre + error.cont_const.sqre);
