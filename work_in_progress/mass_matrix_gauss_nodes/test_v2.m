clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));

dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);
          
xbound = [-1 1];
ybound = [-1 1];
c = 0.00;

K = 1;
p = 3;

ttl_nr_el = K^2;

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

% evaluate jacobian derivatives at gauss nodes
[x,w] = Gnodes2(p+1);
[xiip, etap] = meshgrid(x);
mesh1.eval_jac_der(xiip, etap);

% evaluate metric terms at gauss nodes
mesh1.eval_dom_xy_gauss();

figure
plot(mesh1.eval.xy.x, mesh1.eval.xy.y, '+')

eval.K = flow.darcy.material(k11_an, k12_an, k22_an, mesh1.eval.xy);
mesh1.eval.metric = flow.darcy.metric(mesh1.eval.p, eval.K);

M11 = zeros(2*p*(p+1),2*p*(p+1),ttl_nr_el);
% M11 = mass_matrix.M1_v7(mesh1, 1);
M11 = mass_matrix.M1_v7_gauss(mesh1, 1);

figure
spy(M11)

E211    = incidence_matrix.E21(p);

volForm = twoForm_v2(mesh1);

F_3D = volForm.reduction(f_an);

LHS = [M11 E211'; E211 sparse(p^2,p^2)];
RHS = [sparse(2*p*(p+1),1); F_3D];

X = LHS\RHS;

M22s = zeros(p^2,p^2,ttl_nr_el);

for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end

pf = 30;

mesh1.eval_pf_der(pf);

p_h = X(2*p*(p+1)+1:2*p*(p+1)+p^2);

p_h_2 = zeros(size(p_h));

for eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

xiif2 = linspace(-1,1,30);

[~,e] = MimeticpolyVal(xiif2,p,1);
basiis = kron(e,e);

[xiif, etaf] = meshgrid(xiif2);
jac2 = mesh1.eval_jac_der2(xiif, etaf);

[pf] = reconstruction.reconstruct2form_v4(p_h_2, basiis, jac2.ggg);

figure
contourf(xiif, etaf,pf)

% [xf_3D, yf_3D, pf_h] = volForm.reconstruction(p_h_2);
% pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln))
% end
% colorbar

% [~, wf] = GLLnodes(pf);
% [wfx, wfy] = meshgrid(wf);
% 
% wfxy  = wfx .* wfy;
% wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

% pp_ex = phi_an(xf_3D, yf_3D);
% [error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

% error.pp.sqrt