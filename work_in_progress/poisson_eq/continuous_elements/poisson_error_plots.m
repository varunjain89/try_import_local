clc
close all
clear variables

addpath('../../..')
addpath('../../../MSEM')

%% h - convergence

nh = 10;

for K = 1:nh
    err_h(1,K) = poisson_error(K,1);
    err_h(2,K) = poisson_error(K,4);
    err_h(3,K) = poisson_error(K,7);
%     err_h(2,K) = poisson_error(K,p);
%     err_h(3,K) = poisson_error(K,p);
    K
end

%%

h = 1 ./(1:nh);

slope1 = (log10(err_h(1,nh-5))-log10(err_h(1,nh)))/(log10(h(nh-5))-log10(h(nh)))
slope2 = (log10(err_h(2,nh-5))-log10(err_h(2,nh)))/(log10(h(nh-5))-log10(h(nh)))
slope3 = (log10(err_h(3,nh-5))-log10(err_h(3,nh)))/(log10(h(nh-5))-log10(h(nh)))

figure
loglog(h(5:end),err_h(1,5:end),'-o')
hold on
loglog(h(5:end),err_h(2,5:end),'-o')
loglog(h(5:end),err_h(3,5:end),'-o')

%% p - convergence

% np = 10;
% 
% for p = 1:np
%     err_p(1,p) = poisson_error(1,p);
% end
% 
% %%
% 
% N = 1:np;
% 
% figure
% semilogy(N,err_p,'-o')