clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
% addpath('../../export_fig')

%% analytical solution

% case 2

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

a = 0.1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% domain

xbound = [0 1];
ybound = [0 1];

% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%%

mesh2 = n_hybrid_mesh(domain);

mesh2.check_domain_mapping();

%% define discretization 

K = 3;
p = 2;

mesh2.discretize(K,K);

mesh2.check_element_mapping();

el_bounds.x = linspace(-1,1,K+1);
el_bounds.y = linspace(-1,1,K+1);

[Ex, Ey] = meshgrid(el_bounds.x, el_bounds.y);

h = (el_bounds.x(2) - el_bounds.x(1))/2

rng('default');

Ex(2:end-1,2:end-1) = Ex(2:end-1,2:end-1) + h*rand(K-1);
Ey(2:end-1,2:end-1) = Ey(2:end-1,2:end-1) + h*rand(K-1);

[Ex, Ey] = domain.mapping(Ex,Ey);

figure
plot(Ex,Ey,'k')
hold on
plot(Ex',Ey','k')

% mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y)
% figure
% hold on

xii = linspace(-1,1,11);
[s,t] = meshgrid(xii);

lin_map = @(p,q,s) p.*(1-s) + q*s;
d_lin_map = @(p,q,s) q-p;

for i = 1:K
    for j = 1:K
        
        eleid = (i-1)*K + j
        
        x1 = Ex(j,i);
        x2 = Ex(j,i+1);
        x3 = Ex(j+1,i+1);
        x4 = Ex(j+1,i);
        
        y1 = Ey(j,i);
        y2 = Ey(j,i+1);
        y3 = Ey(j+1,i+1);
        y4 = Ey(j+1,i);
        
        F_L_x = @(s,t) lin_map(x1,x4,t);
        F_L_y = @(s,t) lin_map(y1,y4,t);
        
        F_R_x = @(s,t) lin_map(x2,x3,t);
        F_R_y = @(s,t) lin_map(y2,y3,t);
        
        F_B_x = @(s,t) lin_map(x1,x2,s);
        F_B_y = @(s,t) lin_map(y1,y2,s);
        
        F_T_x = @(s,t) lin_map(x4,x3,s);
        F_T_y = @(s,t) lin_map(y4,y3,s);
        
        dFLx_dt = @(s,t) d_lin_map(x1,x4);
        dFLy_dt = @(s,t) d_lin_map(y1,y4);
        
        dFRx_dt = @(s,t) d_lin_map(x2,x3);
        dFRy_dt = @(s,t) d_lin_map(y2,y3);
        
        dFBx_ds = @(s,t) d_lin_map(x1,x2);
        dFBy_ds = @(s,t) d_lin_map(y1,y2);
        
        dFTx_ds = @(s,t) d_lin_map(x4,x3);
        dFTy_ds = @(s,t) d_lin_map(y4,y3);
        
        [x,y] = mesh.transfinite_mesh.mapping(s,t,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
        plot(x,y,'+')
        
        el(eleid).mapping = mesh.transfinite_mesh.mapping(s,t,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
        el(eleid).dX_dxii = mesh.transfinite_mesh.dX_dxii(s,t,F_L_x,F_R_x,dFBx_ds,dFTx_ds);
        el(eleid).dX_deta = mesh.transfinite_mesh.dX_deta(s,t,F_B_x,F_T_x,dFLx_dt,dFRx_dt);
        el(eleid).dY_dxii = mesh.transfinite_mesh.dY_dxii(s,t,F_L_y,F_R_y,dFBy_ds,dFTy_ds);
        el(eleid).dY_deta = mesh.transfinite_mesh.dY_deta(s,t,F_B_y,F_T_y,dFLy_dt,dFRy_dt);
    end
end

for i = 1:K
    for j = 1:K
        [x,y] = mesh.transfinite_mesh.mapping(s,t,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
        plot(x,y,'+')
    end
end

