%% Created on 18th May, 2018 

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

f_an   = @(x,y) -8*pi*pi*sin(2*pi*x).*sin(2*pi*y);

xbound = [-1 1];
ybound = [-1 1];

c = 0.0;

K = 1;
p = 3;

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

mesh1.eval_p_der();

M22 = mass_matrix.M2_2(p, mesh1.eval.p.ggg(:,:,1));

[xp, wp] = GLLnodes(p);
[~, ep] = MimeticpolyVal(xp,p,1);

[xii, eta] = meshgrid(xp);
xii = xii';
eta = eta';

ff = f_an(xii,eta);

C2 = zeros(p^2,1);

for i = 1:p
    for j = 1:p
        volij = (i-1)*p +j;
        for px = 1:p+1
            for py = 1:p+1
                C2(volij) = C2(volij) + ep(i,px)*ep(j,py)*ff(px,py)*wp(px)*wp(py);
            end
        end
    end
end

C22 = M22\C2;

volForm = twoForm_v2(mesh1);
Fx_h = volForm.reduction(f_an);
