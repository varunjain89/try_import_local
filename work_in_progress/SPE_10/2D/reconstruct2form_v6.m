function [ rec2D ] = reconstruct2form_v6(cochain, basis1D, gf)

% modified on 13 July, 2019
% modified on 26 July, 2019

pf = size(basis1D.efx,2);
shape = [pf pf];

gf = gf(:)';
gf = 1 ./gf;

efx = basis1D.efx;
efy = basis1D.efy;

basis  = kron(efx,efy);

metric = bsxfun(@times,basis,gf);

rec = bsxfun(@times,metric,cochain);
rec = sum(rec,1);

rec2D = reshape(rec, shape);

end

