% clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
% addpath('../../export_fig')

%% define mesh

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

K = 1;
p = 13;

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

pf = 40;

mesh1.eval_pf_der(pf);

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, K^2);

%% 0-form

% redcution 0-form

pp_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);

PP_h = reduction.zero_form_dual_dof(pp_an, mesh1);

% reconstruction 0 - form

% interpolation error 

%% 1-form

% reduction 1-form

% reconstruction 1-form

% interpolation error

%% 2 - form
          
% reduction 2-form

ff_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);

volForm = twoForm_v2(mesh1);

FF_h  = volForm.reduction(ff_an);
FF_h2 = reduction.two_form_dual_dof(ff_an, mesh1);

mesh1.eval_p_der_v2();

for eln = 1:K^2
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end

% reconstruction 2-form

[xf_3D, yf_3D, ff_hh] = volForm.reconstruction(M22s\FF_h2);
% [xf_3D, yf_3D, ff_hh] = volForm.reconstruction(FF_h);

% interpolation error = ff_ex - ff_hh(exact)

ff_ex = ff_an(xf_3D, yf_3D);

[error.ff] = error_processor_v3(ff_ex, ff_hh, wfxy2, mesh1.eval.pf.ggg);
error.ff.sqrt

% plot 

figure
hold on
for eln = 1:K^2
    contourf(xf_3D(:,:,eln),yf_3D(:,:,eln),ff_hh(:,:,eln)')
end
colorbar
