function [g] = metric_terms_Yi_v2(p)
%METRIC_TERMS_YI Summary of this function goes here
%   Detailed explanation goes here

% Created On - 25 June, 2018
% modified on 15 November, 2018

nu = 0.3;

J11  = p.dx_dxii;
J12  = p.dx_deta;
J21  = p.dy_dxii;
J22  = p.dy_deta;
detJ = p.dx_dxii.*p.dy_deta - p.dx_deta.*p.dy_dxii;

a = 1;
b = 1 + nu;
c = 1 + nu;
d = 1;
m = -nu;
n = -nu;

g.g11 = +(a .* J11 .* J11 + b .* J21 .* J21) ./ detJ;
g.g12 = -(a .* J12 .* J11 + b .* J21 .* J22) ./ detJ;
g.g13 = -m .* J21 .* J11 ./ detJ;
g.g14 = +m .* J22 .* J11 ./ detJ;

g.g21 = -(a .* J12 .* J11 + b .* J21 .* J22) ./ detJ;
g.g22 = +(a .* J12 .* J12 + b .* J22 .* J22) ./ detJ;
g.g23 = +m .* J21 .* J12 ./ detJ;
g.g24 = -m .* J22 .* J12 ./ detJ;

g.g31 = -n .* J11 .* J21 ./ detJ;
g.g32 = +n .* J12 .* J21 ./ detJ;
g.g33 = +(c .* J11 .* J11 + d .* J21 .* J21) ./ detJ;
g.g34 = -(c .* J12 .* J11 + d .* J21 .* J22) ./ detJ;

g.g41 = +n .* J11 .* J22 ./ detJ;
g.g42 = -n .* J12 .* J22 ./ detJ;
g.g43 = -(c .* J12 .* J11 + d .* J21 .* J22) ./ detJ;
g.g44 = +(c .* J12 .* J12 + d .* J22 .* J22) ./ detJ;

end

