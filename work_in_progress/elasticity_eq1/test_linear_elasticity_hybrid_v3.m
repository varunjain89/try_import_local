%% Created on 10th July, 2018 

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../finished_programs/elasticity_eq')

%% analytical solution

E = 1;
nu = 0.3;

% u_an     = @(x,y) -3*y;
% v_an     = @(x,y) 3*x;
% du_dx_an = @(x,y) 0*ones(size(x));
% du_dy_an = @(x,y) -3*ones(size(x));
% dv_dx_an = @(x,y) +3*ones(size(x));
% dv_dy_an = @(x,y) 0*ones(size(x));
% d2u_dx2  = @(x,y) 0*ones(size(x));
% d2u_dy2  = @(x,y) 0*ones(size(x));
% d2v_dxdy = @(x,y) 0*ones(size(x));
% d2v_dy2  = @(x,y) 0*ones(size(x));
% d2v_dx2  = @(x,y) 0*ones(size(x));
% d2u_dxdy = @(x,y) 0*ones(size(x));

% u_an     = @(x,y) x.*(1-x).*y.*(1-y);
% v_an     = @(x,y) x.*(1-x).*y.*(1-y);
% du_dx_an = @(x,y) (1-2.*x).*y.*(1-y);
% du_dy_an = @(x,y) x.*(1-x).*(1-2*y);
% dv_dx_an = @(x,y) (1-2*x).*y.*(1-y);
% dv_dy_an = @(x,y) x.*(1-x).*(1-2*y);
% d2u_dx2  = @(x,y) -2*y.*(1-y);
% d2u_dy2  = @(x,y) -2*x.*(1-x);
% d2v_dxdy = @(x,y) (1-2*x).*(1-2*y);
% d2v_dy2  = @(x,y) -2*x.*(1-x);
% d2v_dx2  = @(x,y) -2*y.*(1-y);
% d2u_dxdy = @(x,y) (1-2*x).*(1-2*y);

% u_an     = @(x,y) (1-x.^2).*(1-y.^2);
% v_an     = @(x,y) (1-x.^2).*(1-y.^2);
% du_dx_an = @(x,y) -2*x.*(1-y.^2);
% du_dy_an = @(x,y) -2*y.*(1-x.^2);
% dv_dx_an = @(x,y) -2*x.*(1-y.^2);
% dv_dy_an = @(x,y) -2*y.*(1-x.^2);
% d2u_dx2  = @(x,y) -2*(1-y.^2);
% d2u_dy2  = @(x,y) -2*(1-x.^2);
% d2v_dxdy = @(x,y) 4*x.*y;
% d2v_dy2  = @(x,y) -2*(1-x.^2);
% d2v_dx2  = @(x,y) -2*(1-y.^2);
% d2u_dxdy = @(x,y) 4*x.*y;

u_an     = @(x,y) sin(pi*x).*sin(pi*y);
v_an     = @(x,y) sin(pi*x).*sin(pi*y);
du_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
du_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
dv_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
dv_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2u_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2v_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);

% u_an     = @(x,y) sin(pi*(x-1)).*sin(pi*(y-1));
% v_an     = @(x,y) sin(pi*(x-1)).*sin(pi*(y-1));
% du_dx_an = @(x,y) pi*cos(pi*(x-1)).*sin(pi*(y-1));
% du_dy_an = @(x,y) pi*sin(pi*(x-1)).*cos(pi*(y-1));
% dv_dx_an = @(x,y) pi*cos(pi*(x-1)).*sin(pi*(y-1));
% dv_dy_an = @(x,y) pi*sin(pi*(x-1)).*cos(pi*(y-1));
% d2u_dx2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2u_dy2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2v_dxdy = @(x,y) pi^2*cos(pi*(x-1)).*cos(pi*(y-1));
% d2v_dy2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2v_dx2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2u_dxdy = @(x,y) pi^2*cos(pi*(x-1)).*cos(pi*(y-1));

% u_an     = @(x,y) sin(2*pi*x).*cos(2*pi*y);
% v_an     = @(x,y) cos(2*pi*x).*sin(2*pi*y);
% du_dx_an = @(x,y) 2*pi*cos(2*pi*x).*cos(2*pi*y);
% du_dy_an = @(x,y) -2*pi*sin(2*pi*x).*sin(2*pi*y);
% dv_dx_an = @(x,y) -2*pi*sin(2*pi*x).*sin(2*pi*y);
% dv_dy_an = @(x,y) 2*pi*cos(2*pi*x).*cos(2*pi*y);
% d2u_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*cos(2*pi*y);
% d2u_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*cos(2*pi*y);
% d2v_dxdy = @(x,y) -4*pi^2*sin(2*pi*x).*cos(2*pi*y);
% d2v_dy2  = @(x,y) -4*pi^2*cos(2*pi*x).*sin(2*pi*y);
% d2v_dx2  = @(x,y) -4*pi^2*cos(2*pi*x).*sin(2*pi*y);
% d2u_dxdy = @(x,y) -4*pi^2*cos(2*pi*x).*sin(2*pi*y);

e_xx_an = @(x,y) du_dx_an(x,y);
e_yy_an = @(x,y) dv_dy_an(x,y);
e_xy_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));
e_yx_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));

w_an      = @(x,y) 0.5*(dv_dx_an(x,y) - du_dy_an(x,y));
tau_xx_an = @(x,y) E/(1-nu^2)*(e_xx_an(x,y) + nu*e_yy_an(x,y));
tau_yy_an = @(x,y) E/(1-nu^2)*(e_yy_an(x,y) + nu*e_xx_an(x,y));
tau_xy_an = @(x,y) E/(1+nu)*e_xy_an(x,y);
tau_yx_an = @(x,y) E/(1+nu)*e_yx_an(x,y);
fx_an     = @(x,y) -E/(1-nu^2) * ( d2u_dx2(x,y) + 0.5*(1-nu)*d2u_dy2(x,y) + 0.5*(1+nu)*d2v_dxdy(x,y) );
fy_an     = @(x,y) -E/(1-nu^2) * ( d2v_dy2(x,y) + 0.5*(1-nu)*d2v_dx2(x,y) + 0.5*(1+nu)*d2u_dxdy(x,y) );
tq_an     = @(x,y) -y.*fx_an(x,y) + x.*fy_an(x,y);

lambdaX_an  = @(x,y) u_an(x,y) + y.*w_an(x,y);
lambdaY_an  = @(x,y) v_an(x,y) - x.*w_an(x,y);

%%

K = 10;
p = 10;

c = 0.0;

[LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = elasticity_non_staggered.local_numbering(p);

%% mapping functions

xbound = [-1 1];
ybound = [-1 1];

domain_mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

% [element_bounds_x, ~] = GLLnodes(K);
% element_bounds_y = element_bounds_x;

element_bounds_x = linspace(-1,1,K+1);
element_bounds_y = linspace(-1,1,K+1);

el_mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain_mapping, element_bounds_x, element_bounds_y, elx, ely, xi, eta);

for elx = 1:K
    for ely = 1:K
        el = (elx-1) * K + ely;
        el_xbound = [element_bounds_x(elx) element_bounds_x(elx+1)];
        el_ybound = [element_bounds_y(ely) element_bounds_y(ely+1)];
        
        el_domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(el_xbound, c, xi, eta);
        el_domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(el_xbound, c, xi, eta);
        el_domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(el_ybound, c, xi, eta);
        el_domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(el_ybound, c, xi, eta);
        
        LHS_3D(:,:,el) = elasticity_non_staggered.func_linear_elasticity_v4_LHS_vectorize(p, el_xbound, el_ybound, c, el_domain_dX_dxii, el_domain_dX_deta, el_domain_dY_dxii, el_domain_dY_deta);
    end
end

q = num2cell(LHS_3D,[1,2]);
LHS_2D = blkdiag(q{:});

%% connectivity matrix

GM_local_dof = elasticity_non_staggered.gather_matrix_total_local_dof_v2(K,p);
GM_VV = elasticity_non_staggered.gather_matrix_VV_v2(K,p);

%%

total_local_dof = 4*p*(p+1);

N_lambda_x = zeros(4*p, total_local_dof);
N_lambda_y = zeros(4*p, total_local_dof);

for i = 1:p
    % lambda-x
    
    % bottom 
    local_vvv_bot_lx = i;
    local_dof_bot = p*(p+1) + LN_tau_yx(i,1);
    
    % top
    local_vvv_top_lx = p + i;
    local_dof_top = p*(p+1) + LN_tau_yx(i,p+1);
    
    % left
    local_vvv_lef_lx = 2*p + i;
    local_dof_lef = LN_tau_xx(1,i);
    
    % right
    local_vvv_rig_lx = 3*p + i;
    local_dof_rig = LN_tau_xx(p+1,i);
    
    N_lambda_x(local_vvv_bot_lx, local_dof_bot) = -1;
    N_lambda_x(local_vvv_top_lx, local_dof_top) = +1;
    N_lambda_x(local_vvv_lef_lx, local_dof_lef) = -1;
    N_lambda_x(local_vvv_rig_lx, local_dof_rig) = +1;
    
    % lambda-y
    
    % bottom 
    local_vvv_bot_ly = i;
    local_dof_bot = 3*p*(p+1) + LN_tau_yy(i,1);
    
    % top
    local_vvv_top_ly = p + i;
    local_dof_top = 3*p*(p+1) + LN_tau_yy(i,p+1);
    
    % left
    local_vvv_lef_ly = 2*p + i;
    local_dof_lef = 2*p*(p+1) + LN_tau_xy(1,i);
    
    % right
    local_vvv_rig_ly = 3*p + i;
    local_dof_rig = 2*p*(p+1) + LN_tau_xy(p+1,i);
    
    N_lambda_y(local_vvv_bot_ly, local_dof_bot) = -1;
    N_lambda_y(local_vvv_top_ly, local_dof_top) = +1;
    N_lambda_y(local_vvv_lef_ly, local_dof_lef) = -1;
    N_lambda_y(local_vvv_rig_ly, local_dof_rig) = +1;
end

N = [N_lambda_x; N_lambda_y];

%%

N_3D = repmat(N,[1 1 K^2]);

GM_local_dof_2 = GM_local_dof(:,1:4*p*(p+1));

assVV   = AssembleMatrices2(GM_VV', GM_local_dof_2', N_3D);
assVV2  = [assVV zeros(2*K*(K+1)*2*p, 3*p^2)];

%%

tz = zeros(K*p*(K+1)*4);

hybrid_LHS = [LHS_2D assVV2'; assVV2 tz];

temp1 = (4*p*(p+1)+3*p^2) * K^2 + K*p*(K-1)*4;
hybrid_LHS = hybrid_LHS(1:temp1,1:temp1);

% spy(hybrid_LHS)

%% RHS

% f_h = reduction.of2form_multi_element_5(f_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);

Fx_h = reduction.of2form_multi_element_5(fx_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);
Fy_h = reduction.of2form_multi_element_5(fy_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);
TQ_h = reduction.of2form_multi_element_5(tq_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);

for el = 1:K^2
    RHS((el-1)*(4*p*(p+1)+3*p^2) + 4*p*(p+1)+1 : (el-1)*(4*p*(p+1)+3*p^2) + 4*p*(p+1)+ p^2,1) = -Fx_h(:,el);
    RHS((el-1)*(4*p*(p+1)+3*p^2) + 4*p*(p+1)+p^2+1 : (el-1)*(4*p*(p+1)+3*p^2) + 4*p*(p+1)+ 2*p^2,1) = -Fy_h(:,el);
    RHS((el-1)*(4*p*(p+1)+3*p^2) + 4*p*(p+1)+2*p^2+1 : (el-1)*(4*p*(p+1)+3*p^2) + 4*p*(p+1)+ 3*p^2,1) = -TQ_h(:,el);
end

RHS_con = zeros(2*K*(K-1)*2*p,1);

RHS2 = [RHS; RHS_con];

dof_LHS = 4*p*(p+1) + 3*p^2;
dof_con = 2*K*(K-1)*2*p;

%% solution 

X_h = hybrid_LHS\RHS2;

%% seperating cochains

tdf2  = 4*p*(p+1) + 3*p^2;
temp2 = p*(p+1);

for el = 1:K^2
    TXX_h(:,el) = X_h((el-1)*tdf2+0*temp2+1:(el-1)*tdf2+temp2);
    TYX_h(:,el) = X_h((el-1)*tdf2+1*temp2+1:(el-1)*tdf2+2*temp2);
    TXY_h(:,el) = X_h((el-1)*tdf2+2*temp2+1:(el-1)*tdf2+3*temp2);
    TYY_h(:,el) = X_h((el-1)*tdf2+3*temp2+1:(el-1)*tdf2+4*temp2);
    U_h(:,el)   = X_h((el-1)*tdf2+4*temp2+1:(el-1)*tdf2+4*temp2+p^2);
    V_h(:,el)   = X_h((el-1)*tdf2+4*temp2+p^2+1:(el-1)*tdf2+4*temp2+2*p^2);
    W_h(:,el)   = X_h((el-1)*tdf2+4*temp2+2*p^2+1:(el-1)*tdf2+4*temp2+3*p^2);
end

%% reconstruction

[xp, wp] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx-1) * K + ely;
        el_xbound = [element_bounds_x(elx) element_bounds_x(elx+1)];
        el_ybound = [element_bounds_y(ely) element_bounds_y(ely+1)];
        
        eval_p_dx_dxii(:,:,el) = mesh.crazy_mesh.dx_dxii(el_xbound, c, xip, etap);
        eval_p_dx_deta(:,:,el) = mesh.crazy_mesh.dx_deta(el_xbound, c, xip, etap);
        eval_p_dy_dxii(:,:,el) = mesh.crazy_mesh.dy_dxii(el_ybound, c, xip, etap);
        eval_p_dy_deta(:,:,el) = mesh.crazy_mesh.dy_deta(el_ybound, c, xip, etap);
    end
end

eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);

local_nr_pp = p^2;
ttl_nr_el   = K^2;
M22s = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);

for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, eval_p_ggg(:,:,el));
end

for el = 1:K^2
    U_h_2(:,el) = M22s(:,:,el)\U_h(:,el);
    V_h_2(:,el) = M22s(:,:,el)\V_h(:,el);
    W_h_2(:,el) = M22s(:,:,el)\W_h(:,el);
end

%% reconstruction

pf = 30;

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);

eval_pf_dx_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dx_deta = zeros(pf+1,pf+1,K^2);
eval_pf_dy_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dy_deta = zeros(pf+1,pf+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        el_xbound = [element_bounds_x(elx) element_bounds_x(elx+1)];
        el_ybound = [element_bounds_y(ely) element_bounds_y(ely+1)];
        
        eval_pf_dx_dxii(:,:,el) = mesh.crazy_mesh.dx_dxii(el_xbound, c, xif, etaf);
        eval_pf_dx_deta(:,:,el) = mesh.crazy_mesh.dx_deta(el_xbound, c, xif, etaf);
        eval_pf_dy_dxii(:,:,el) = mesh.crazy_mesh.dy_dxii(el_ybound, c, xif, etaf);
        eval_pf_dy_deta(:,:,el) = mesh.crazy_mesh.dy_deta(el_ybound, c, xif, etaf);
    end
end

eval_pf_ggg = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);

%% reconstruction of lambdaX

[xf_3D, yf_3D, uf_h] = reconstruction.of2form_multi_element_v4(U_h_2, p, pf, el_mapping, eval_pf_ggg, K);

figure
subplot(4,4,13)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), uf_h(:,:,el)')
end
title('computed lambda_X');
colorbar

subplot(4,4,9)
hold on
for el = 1:K^2
    u_plot(:,:,el) = lambdaX_an(xf_3D(:,:,el),yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el))
end
title('analytical lambda_X');
colorbar

%% reconstruction of lambdaY

[xf_3D, yf_3D, vf_h] = reconstruction.of2form_multi_element_v4(V_h_2, p, pf, el_mapping, eval_pf_ggg, K);

subplot(4,4,14)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), vf_h(:,:,el)')
end
title('computed lambda_Y');
colorbar

subplot(4,4,10)
hold on
for el = 1:K^2
    v_plot(:,:,el) = lambdaY_an(xf_3D(:,:,el),yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), v_plot(:,:,el))
end
title('analytical lambda_Y');
colorbar

%% reconstruction of W - omega / rotation

[xf_3D, yf_3D, wf_h] = reconstruction.of2form_multi_element_v4(W_h_2, p, pf, el_mapping, eval_pf_ggg, K);

subplot(4,4,15)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), wf_h(:,:,el)')
end
title('computed W');
colorbar

subplot(4,4,11)
hold on
for el = 1:K^2
    w_plot(:,:,el) = w_an(xf_3D(:,:,el),yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), w_plot(:,:,el))
end
title('analytical W');
colorbar

%% reconstruction of txx

% tau_xx_f_h = reconstruct1yform_2(TYX_h(:,el), TXX_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;
        tau_xx_f_h(:,:,el) = reconstruction.reconstruct1yform_2(TYX_h(:,el), TXX_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));
    end
end

subplot(4,4,5)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xx_f_h(:,:,el)')
end
title('computed tau_xx');
colorbar

subplot(4,4,1)
hold on
for el = 1:K^2
    txx_plot(:,:,el) = tau_xx_an(xf_3D(:,:,el), yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), txx_plot(:,:,el))
end
title('analytical tau_xx');
colorbar

%% reconstruction of tyy

% tau_yy_f_h = reconstruct1xform_2(TYY_h(:,el), TXY_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;
        tau_yy_f_h(:,:,el) = reconstruction.reconstruct1xform_2(TYY_h(:,el), TXY_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));
    end
end

subplot(4,4,6)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yy_f_h(:,:,el)')
end
title('computed tau_yy');
colorbar

subplot(4,4,2)
hold on
for el = 1:K^2
    tyy_plot(:,:,el) = tau_yy_an(xf_3D(:,:,el), yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyy_plot(:,:,el))
end

title('analytical tau_yy');
colorbar

%% reconstruction of txy

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;
        tau_xy_f_h(:,:,el) = reconstruction.reconstruct1yform_2(TYY_h(:,el), TXY_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));
    end
end

subplot(4,4,7)
hold on 
for el = 1:K^2
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xy_f_h(:,:,el)')
end
title('computed tau_xy');
colorbar

subplot(4,4,3)
hold on
for el = 1:K^2
    txy_plot(:,:,el) = tau_xy_an(xf_3D(:,:,el), yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), txy_plot(:,:,el))
end
title('analytical tau_xy');
colorbar

%% reconstruction of tyx

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;
        tau_yx_f_h(:,:,el) = reconstruction.reconstruct1xform_2(TYX_h(:,el), TXX_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));
    end
end

subplot(4,4,8)
hold on
for el = 1:K^2
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yx_f_h(:,:,el)')
end
title('computed tau_yx');
colorbar

subplot(4,4,4)
hold on 
for el = 1:K^2
    tyx_plot(:,:,el) = tau_yx_an(xf_3D(:,:,el), yf_3D(:,:,el));
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyx_plot(:,:,el))
end
title('analytical tau_yx');
colorbar

