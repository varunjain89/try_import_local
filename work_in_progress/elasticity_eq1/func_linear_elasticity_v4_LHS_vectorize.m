function [LHS] = func_linear_elasticity_v4_LHS_vectorize(p, xbound, ybound, c, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta)
%FUNC_LINEAR_ELASTICITY_V4_LHS Summary of this function goes here
%   Detailed explanation goes here

%% Created on 10th July, 2018 

%% 

[xp, ~] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii(:,:) = domain_dX_dxii(xip,etap);
eval_p_dx_deta(:,:) = domain_dX_deta(xip,etap);
eval_p_dy_dxii(:,:) = domain_dY_dxii(xip,etap);
eval_p_dy_deta(:,:) = domain_dY_deta(xip,etap);

[g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44] = elasticity_non_staggered.metric_terms_Yi(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);
MS = elasticity_non_staggered.full_mass_matrix_g_v2_vectorize(p, g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44);


%% torque

TQ2     = total_torque_vec_v2(p, xbound, ybound, c, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);
am_E21  = elasticity_non_staggered.incidence_matrix.AM_incidence_matrix(p);

AM4     = am_E21*TQ2;
W       = AM4;

%% conservation of linear momentum 

[LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = elasticity_non_staggered.local_numbering(p);

Ex = elasticity_non_staggered.incidence_matrix.linear_momentum_x(p, LN_tau_xx, LN_tau_yx);
Ey = elasticity_non_staggered.incidence_matrix.linear_momentum_y(p, LN_tau_yy, LN_tau_xy);

%% LHS system 

LHS = [MS Ex' Ey' W';
    Ex zeros(p^2, p^2) zeros(p^2, p^2) zeros(p^2, p^2);
    Ey zeros(p^2, p^2) zeros(p^2, p^2) zeros(p^2, p^2);
    W zeros(p^2, p^2) zeros(p^2, p^2) zeros(p^2, p^2)];
end

