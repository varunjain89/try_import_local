function [ TQ ] = total_torque_vec_v2(p, xbound, ybound, c, el_dX_dxii, el_dX_deta, el_dY_dxii, el_dY_deta)
%TOTAL_TORQUE Summary of this function goes here
%   Detailed explanation goes here


% created on 6th july, 2018 bologna conference
% modified on 7th November, 2018

[xp,~] = GLLnodes(p);
[hp,ep] = MimeticpolyVal(xp,p,1);

%--- number of integration points for reduction ----%

p_int = p+5;
[quad_int_xi, wp_int] = GLLnodes(p_int);


%---- initializing array Torque in x
TQx = zeros(p*(p+1),4*p*(p+1));

% figure
% hold on

% for i = 1:p
% %     for j = 1:p+1
% %         edgeij = (j-1)*p + i;
%         
%         xi_i1 = xp(i);
%         xi_i2 = xp(i+1);
% %         eta_j = xp(j);
%         
%         xif2 = 0.5*(xi_i1 + xi_i2) + 0.5*(xi_i2 - xi_i1).* quad_int_xi;
%         
%         if i == 1
%             xif3 = xif2;
%         else
%             xif3 = [xif3 xif2];
%         end
%         
% %         etaf2(:,edgeij) = eta_j;
%         
% %         plot(xif2,xp(1),'o')
% %         plot(xif2(:,edgeij), etaf2(:,edgeij),'o')
%         
% %     end
% end
% 
% plot(xif3, xp(1),'o')
% 
% [hp_int3,ep_int3] = MimeticpolyVal(xif3,p,1);
% ep_int4 = reshape(ep_int3,[p,p_int+1,p]);
% [hp_int4,ep_int4] = MimeticpolyVal(xp,p,1);

for i = 1:p
    % points on local edge of ref domain
    xi_i1 = xp(i);
    xi_i2 = xp(i+1);
    
    % fine points on local edge
    xif  = 0.5*(xi_i1 + xi_i2) + 0.5*(xi_i2 - xi_i1).* quad_int_xi;
    
    % basis functions on fine points of local edge
    [hp_int1,ep_int1] = MimeticpolyVal(xif,p,1);
    
    for j = 1:p+1
        edgeij = (j-1)*p + i;
                
        eta_j = xp(j);
        
        % points on physical domain
        [x_int,y_int] = map_local_edge(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         plot(x_int, y_int,'o')
        
        % local pull back of the edge / scaling of the edge / pull back at each integration point
        dx_dxi_int  = local_dx_dxi(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
        dy_dxi_int  = local_dy_dxi(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         dx_deta_int = local_dx_deta(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         dy_deta_int = local_dy_deta(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         g_int = metric.ggg(dx_dxi_int, dx_deta_int, dy_dxi_int, dy_deta_int);
        % (x sigma_yy - y sigma_yx)
%         
        % this should not be local but global - because this is for
        % reconstruction
%         dy_deta_g = dy_deta_int./ g_int;

        %% calculate reconstruction mapping 
        
        % substitute global xif and etaf, also change mapping to domain
        % mapping to make it easier and cleaner
        
        %this function maps local edge xi to global xi 
        
        etaf = eta_j;
%         plot(xif, etaf,'o')
        
        eval_pf_dx_dxii(:,:) = el_dX_dxii(xif,etaf);
        eval_pf_dx_deta(:,:) = el_dX_deta(xif,etaf);
        eval_pf_dy_dxii(:,:) = el_dY_dxii(xif,etaf);
        eval_pf_dy_deta(:,:) = el_dY_deta(xif,etaf);
        eval_pf_g = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
        
        eval_dy_deta_g = eval_pf_dy_deta./eval_pf_g;
        eval_dx_deta_g = eval_pf_dx_deta./eval_pf_g;
        eval_dx_dxii_g = eval_pf_dx_dxii./eval_pf_g;
        eval_dy_dxii_g = eval_pf_dy_dxii./eval_pf_g;
        
        % the edge functions at integration point
        
%         [hp_int2,ep_int2] = MimeticpolyVal(etaf,p,1);
        hp_int2 = hp(:,j);
        ep_int2 = ep(:,j);
        
        % the integral
        %%%%%%%%%%%%% reconstruction uses 2-D integration here we only have
        %%%%%%%%%%%%% 1-D integration check this
        
        for x = 1:p_int+1
            
            for k = 1:p
                for l = 1:p+1
                    dof_Tyy = 3*p*(p+1) + (l-1)*p+k;
                    dof_Tyx = p*(p+1) + (l-1)*p + k;
                    
                    TQx(edgeij, dof_Tyy) = TQx(edgeij, dof_Tyy) + x_int(x)*(dx_dxi_int(x)*eval_dy_deta_g(x) - dy_dxi_int(x)*eval_dx_deta_g(x))*ep_int1(k,x)*hp_int2(l,1)*wp_int(x);
                    TQx(edgeij, dof_Tyx) = TQx(edgeij, dof_Tyx) + y_int(x)*(dy_dxi_int(x)*eval_dx_deta_g(x) - dx_dxi_int(x)*eval_dy_deta_g(x))*ep_int1(k,x)*hp_int2(l,1)*wp_int(x);
                end
            end
            
            for k = 1:p+1
                for l =1:p
                    dof_Txy = 2*p*(p+1) + (k-1)*p + l;
                    dof_Txx = (k-1)*p + l;
                    
                    TQx(edgeij, dof_Txy) = TQx(edgeij, dof_Txy) + x_int(x)*(dy_dxi_int(x)*eval_dx_dxii_g(x) - dx_dxi_int(x)*eval_dy_dxii_g(x))*hp_int1(k,x)*ep_int2(l,1)*wp_int(x);
                    TQx(edgeij, dof_Txx) = TQx(edgeij, dof_Txx) + y_int(x)*(dx_dxi_int(x)*eval_dy_dxii_g(x) - dy_dxi_int(x)*eval_dx_dxii_g(x))*hp_int1(k,x)*ep_int2(l,1)*wp_int(x);
                end
            end
        end
        
    end
end

%%%% this part is exactly the same as above maybe somethings can be
%%%% converted to functions to remove redundancy in the program

%---- initializing array Torque in y
TQy = zeros(p*(p+1),4*p*(p+1));

% figure
% hold on

for j = 1:p
    eta_j1 = xp(j);
    eta_j2 = xp(j+1);
    
    etaf = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* quad_int_xi;
    [hp_int2,ep_int2] = MimeticpolyVal(etaf,p,1);
    
    for i = 1:p+1
        edgeij = (i-1)*p + j;
        
        xi_i   = xp(i);
        
        
        
        % points on physical domain
        [x_int,y_int] = map_local_y_edge(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
        % local pull back of the edge / pull back at each integration point
        % - contributing to reduction part of torque
        dy_deta_int = local_dy_deta(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        dx_deta_int = local_dx_deta(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         dy_dxii_int = local_dy_dxi(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         dx_dxii_int = local_dx_dxi(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        

        %% calculate reconstruction mapping 
        
        % substitute global xif and etaf, also change mapping to domain
        % mapping to make it easier and cleaner
        
        %this function maps local edge xi to global xi
        
        xif  = xi_i;
        
        
        % global pullback contributing to reconstruction of stress DOF
        
        eval_pf_dx_dxii(:,:) = el_dX_dxii(xif,etaf);
        eval_pf_dx_deta(:,:) = el_dX_deta(xif,etaf);
        eval_pf_dy_dxii(:,:) = el_dY_dxii(xif,etaf);
        eval_pf_dy_deta(:,:) = el_dY_deta(xif,etaf);
        eval_pf_g = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
        
        eval_dy_deta_g = eval_pf_dy_deta./eval_pf_g;
        eval_dx_deta_g = eval_pf_dx_deta./eval_pf_g;
        eval_dx_dxii_g = eval_pf_dx_dxii./eval_pf_g;
        eval_dy_dxii_g = eval_pf_dy_dxii./eval_pf_g;
        
        % the edge functions at integration point
%         [hp_int1,ep_int1] = MimeticpolyVal(xif,p,1);
        hp_int1 = hp(:,i);
        ep_int1 = ep(:,i);
        
        % the integral
        %%%%%%%%%%%%% reconstruction uses 2-D integration here we only have
        %%%%%%%%%%%%% 1-D integration check this
        
        for y = 1:p_int+1
            
            for k = 1:p
                for l = 1:p+1
                    dof_Tyy = 3*p*(p+1) + (l-1)*p+k;
                    dof_Tyx = p*(p+1) + (l-1)*p + k;
                    
                    TQy(edgeij, dof_Tyy) = TQy(edgeij, dof_Tyy) + x_int(y)*(dx_deta_int(y)*eval_dy_deta_g(y) - dy_deta_int(y)*eval_dx_deta_g(y))*ep_int1(k,1)*hp_int2(l,y)*wp_int(y);
                    TQy(edgeij, dof_Tyx) = TQy(edgeij, dof_Tyx) + y_int(y)*(dy_deta_int(y)*eval_dx_deta_g(y) - dx_deta_int(y)*eval_dy_deta_g(y))*ep_int1(k,1)*hp_int2(l,y)*wp_int(y);
                end
            end
            
            for k = 1:p+1
                for l =1:p
                    dof_Txy = 2*p*(p+1) + (k-1)*p + l;
                    dof_Txx = (k-1)*p + l;
                    
                    TQy(edgeij, dof_Txy) = TQy(edgeij, dof_Txy) + x_int(y)*(dy_deta_int(y)*eval_dx_dxii_g(y) - dx_deta_int(y)*eval_dy_dxii_g(y))*hp_int1(k,1)*ep_int2(l,y)*wp_int(y);
                    TQy(edgeij, dof_Txx) = TQy(edgeij, dof_Txx) + y_int(y)*(dx_deta_int(y)*eval_dy_dxii_g(y) - dy_deta_int(y)*eval_dx_dxii_g(y))*hp_int1(k,1)*ep_int2(l,y)*wp_int(y);
                end
            end
        end
        
    end
end

TQ = [TQx; TQy];

end

