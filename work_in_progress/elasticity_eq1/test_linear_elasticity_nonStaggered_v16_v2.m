%% Created on 18th May, 2018 

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../finished_programs/elasticity_eq')

%% analytical solution

E = 1;
nu = 0.3;

u_an     = @(x,y) sin(pi*x).*sin(pi*y);
v_an     = @(x,y) sin(pi*x).*sin(pi*y);
du_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
du_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
dv_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
dv_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2u_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2v_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);

e_xx_an = @(x,y) du_dx_an(x,y);
e_yy_an = @(x,y) dv_dy_an(x,y);
e_xy_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));
e_yx_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));

w_an      = @(x,y) 0.5*(dv_dx_an(x,y) - du_dy_an(x,y));
tau_xx_an = @(x,y) E/(1-nu^2)*(e_xx_an(x,y) + nu*e_yy_an(x,y));
tau_yy_an = @(x,y) E/(1-nu^2)*(e_yy_an(x,y) + nu*e_xx_an(x,y));
tau_xy_an = @(x,y) E/(1+nu)*e_xy_an(x,y);
tau_yx_an = @(x,y) E/(1+nu)*e_yx_an(x,y);
fx_an     = @(x,y) -E/(1-nu^2) * ( d2u_dx2(x,y) + 0.5*(1-nu)*d2u_dy2(x,y) + 0.5*(1+nu)*d2v_dxdy(x,y) );
fy_an     = @(x,y) -E/(1-nu^2) * ( d2v_dy2(x,y) + 0.5*(1-nu)*d2v_dx2(x,y) + 0.5*(1+nu)*d2u_dxdy(x,y) );
tq_an     = @(x,y) -y.*fx_an(x,y) + x.*fy_an(x,y);

lambdaX_an  = @(x,y) u_an(x,y) + y.*w_an(x,y);
lambdaY_an  = @(x,y) v_an(x,y) - x.*w_an(x,y);

%% domain

xbound = [3.3 4.9];
ybound = [2.1 5.3];

xbound = [-1 1];
ybound = [-1 1];

c = 0.0;

%% number of elements

K = 1;
p = 1;

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%%

ttl_nr_el = K^2;
local_nr_pp = p^2;

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

total_local_dof = local_nr_tau_xx + local_nr_tau_xy + local_nr_tau_yx + local_nr_tau_yy;

%% calculate jacobian terms terms for mass matrix

mesh1.eval_p_der();

%% calculation of metric terms

[gg] = metric_terms_Yi_v2(mesh1.eval.p);

%% local numbering

[LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = elasticity_non_staggered.local_numbering(p);

%% constitutive mass matrix 

MS = full_mass_matrix_g_v2_vectorize_v2(p, gg);

%% conservation of angular momentum 

TQ_shear = torque_shear_v3(p, xbound, ybound, mesh1.domain.dX_dxii, mesh1.domain.dX_deta, mesh1.domain.dY_dxii, mesh1.domain.dY_deta);
TQ_norm  = torque_normal_v3(p, xbound, ybound, mesh1.domain.dX_dxii, mesh1.domain.dX_deta, mesh1.domain.dY_dxii, mesh1.domain.dY_deta);

TQ = (TQ_shear + TQ_norm);

am_E21 = elasticity_non_staggered.incidence_matrix.AM_incidence_matrix(p);
W    = am_E21*TQ;

%% conservation of linear momentum 

Ex = elasticity_non_staggered.incidence_matrix.linear_momentum_x(p, LN_tau_xx, LN_tau_yx);
Ey = elasticity_non_staggered.incidence_matrix.linear_momentum_y(p, LN_tau_yy, LN_tau_xy);

%% LHS system 

LHS = [MS Ex' Ey' W';
    Ex zeros(p^2, p^2) zeros(p^2, p^2) zeros(p^2, p^2);
    Ey zeros(p^2, p^2) zeros(p^2, p^2) zeros(p^2, p^2);
    W zeros(p^2, p^2) zeros(p^2, p^2) zeros(p^2, p^2)];

% figure
% spy(LHS)

%% RHS

RHS = zeros(total_local_dof + 3*p^2, 1);

volForm = twoForm_v2(mesh1);

Fx_h = volForm.reduction(fx_an);
Fy_h = volForm.reduction(fy_an);
TQ_h = volForm.reduction(tq_an);

RHS(total_local_dof + 1 : total_local_dof + p^2)         = -Fx_h;
RHS(total_local_dof + p^2 + 1 : total_local_dof + 2*p^2) = -Fy_h;
RHS(total_local_dof + 2*p^2 + 1:total_local_dof + 3*p^2) = -TQ_h;

%% TQ from projected f !!! 

[xp, wp] = GLLnodes(p);
[~, ep] = MimeticpolyVal(xp,p,1);

% p_int = p+1;
% [quad_int_xi, wp_int] = GLLnodes(p_int);

[x_int,y_int] = meshgrid(xp);
x_int = x_int';
y_int = y_int';

M22s = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);
for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, mesh1.eval.p.ggg(:,:,el));
end

TQfx = zeros(p^2);
TQfy = zeros(p^2);

for i = 1:p
    for j = 1:p
        edgeij = (i-1)*p +j;
        for k = 1:p
            for l =1:p
                edgekl = (k-1)*p +l;
                for px = 1:p+1
                    for py = 1:p+1
                        TQfx(edgeij, edgekl) = TQfx(edgeij, edgekl) + ep(i,px)*ep(j,py)*x_int(px,py)*ep(k,px)*ep(l,py)*wp(px)*wp(py);
                    end
                end
            end
        end
    end
end

for i = 1:p
    for j = 1:p
        edgeij = (i-1)*p +j;
        for k = 1:p
            for l =1:p
                edgekl = (k-1)*p +l;
                for px = 1:p+1
                    for py = 1:p+1
                        TQfy(edgeij, edgekl) = TQfy(edgeij, edgekl) + ep(i,px)*ep(j,py)*y_int(px,py)*ep(k,px)*ep(l,py)*wp(px)*wp(py);
                    end
                end
            end
        end
    end
end

TQQ = TQfx * Fy_h - TQfy * Fx_h;
TQQ2 = M22s\TQQ;

% RHS(total_local_dof + 2*p^2 + 1:total_local_dof + 3*p^2) = -TQQ2;

%% boudnary condition

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

% bottom boundary

for i = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,1);
    for xqd = 1:p+1
        %here i have used minus sign because unit normal is outward acting
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),-1,1,1);
        RHS(edgeij) = RHS(edgeij) - ep(i,xqd)*v_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),-1)*wp(xqd);
    end
end

for i = 1:p
    edgeij = local_nr_tau_xx + LN_tau_yx(i,1);
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),-1,1,1);
        RHS(edgeij) = RHS(edgeij) - ep(i,xqd)*u_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(xp(xqd),-1)*wp(xqd);
    end
end

% top boundary

for i = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,p+1);
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),1,1,1);
        %here i have used minus sign because unit normal is outward acting
        RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),1)*wp(xqd);
    end
end

for i = 1:p
    edgeij = local_nr_tau_xx + LN_tau_yx(i,p+1);
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),1,1,1);
        RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(xp(xqd),1)*wp(xqd);
    end
end

% left boundary

for j = 1:p
    edgeij = LN_tau_xx(1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(-1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) - ep(j,yqd)*u_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(-1,xp(yqd))*wp(yqd);
    end
end

for j = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(-1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) - ep(j,yqd)*v_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(-1,xp(yqd))*wp(yqd);
    end
end

% right boundary

for j = 1:p
    edgeij = LN_tau_xx(p+1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(1,xp(yqd))*wp(yqd);
    end
end

for j = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(p+1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(1,xp(yqd))*wp(yqd);
    end
end

% RHS(1:total_local_dof) = 1/factor*RHS(1:total_local_dof);

%% direct solver

X_h = LHS\RHS;

%% separating cochains

temp = p*(p+1);

TXX_h = X_h(1:temp);
TYX_h = X_h(temp+1:2*temp);
TXY_h = X_h(2*temp+1:3*temp);
TYY_h = X_h(3*temp+1:4*temp);
U_h   = X_h(4*temp+1:4*temp+p^2);
V_h   = X_h(4*temp+p^2+1:4*temp+2*p^2);
W_h   = X_h(4*temp+2*p^2+1:4*temp+3*p^2);

%% reconstruction

M22s = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);
for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, mesh1.eval.p.ggg(:,:,el));
end

U_h_2 = M22s\U_h;
V_h_2 = M22s\V_h;
W_h_2 = M22s\W_h;

%% reconstruction

pf = 20;
mesh1.eval_pf_der(pf);

%% reconstruction of lambdaX

[xf_3D, yf_3D, uf_h] = volForm.reconstruction(U_h_2);

% set(0,'defaultfigureposition',[0 0 1500 800])
figure
subplot(4,4,13)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), uf_h(:,:,el)')
end
title('computed U');
colorbar

u_plot = lambdaX_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,9)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el))

title('analytical U');
colorbar

%% reconstruction of lambdaY

[xf_3D, yf_3D, vf_h] = volForm.reconstruction(V_h_2);

subplot(4,4,14)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), vf_h(:,:,el)')
end
title('computed V');
colorbar

v_plot = lambdaY_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,10)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), v_plot(:,:,el))
title('analytical V');
colorbar

%% reconstruction of W - omega / rotation

[xf_3D, yf_3D, wf_h] = volForm.reconstruction(W_h_2);

subplot(4,4,15)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), wf_h(:,:,el)')
end
title('computed W');
colorbar

w_plot = w_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,11)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), w_plot(:,:,el))
title('analytical W');
colorbar

%% reconstruction of txx

surForm = oneForm_v2(mesh1);

SIG_X = [TYX_h; TXX_h];
SIG_Y = [TYY_h; TXY_h];

recX = surForm.reconstruction(SIG_X);
recY = surForm.reconstruction(SIG_Y);

tau_yx_f_h = recX.qx;
tau_xx_f_h = recX.qy;

tau_yy_f_h = recY.qx;
tau_xy_f_h = recY.qy;

subplot(4,4,5)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xx_f_h(:,:,el))
title('computed tau_xx');
colorbar

txx_plot = tau_xx_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txx_plot(:,:,el))
title('analytical tau_xx');
colorbar

%% reconstruction of tyy

subplot(4,4,6)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yy_f_h(:,:,el))
title('computed tau_yy');
colorbar

tyy_plot = tau_yy_an(xf_3D, yf_3D)  + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyy_plot(:,:,el))
title('analytical tau_yy');
colorbar

%% reconstruction of txy

subplot(4,4,7)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), -tau_xy_f_h(:,:,el))
title('computed tau_xy');
colorbar

txy_plot = tau_xy_an(xf_3D, yf_3D)  + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,3)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txy_plot(:,:,el))
title('analytical tau_xy');
colorbar

%% reconstruction of tyx

subplot(4,4,8)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yx_f_h(:,:,el))
title('computed tau_yx');
colorbar

tyx_plot = tau_yx_an(xf_3D, yf_3D)  + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,4)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyx_plot(:,:,el))
title('analytical tau_yx');
colorbar

%% reconstruction linear momentum in X

TR = [TXX_h; TYX_h; TXY_h; TYY_h];

LM_X_h = Ex * TR + Fx_h;

[xf_3D, yf_3D, lm_x_h] = volForm.reconstruction(LM_X_h);

figure
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), lm_x_h(:,:,el)')
end
title('linear momentum in X');
colorbar

%% reconstruction linear momentum in Y

LM_Y_h = Ey * TR + Fy_h;

[xf_3D, yf_3D, lm_y_h] = volForm.reconstruction(LM_Y_h);

figure
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), lm_y_h(:,:,el)')
end
title('linear momentum in Y');
colorbar

%% reconstruction angular momentum

Err_AM_h = W * TR + TQ_h;

[xf_3D, yf_3D, err_am_h] = volForm.reconstruction(Err_AM_h);


%% figure

figure
contourf(xf_3D(:,:,el), yf_3D(:,:,el), (tau_yx_f_h(:,:,el)+-tau_xy_f_h(:,:,el))*0.5)
colorbar

%% error estimates

[~, wf] = GLLnodes(pf);
wff = kron(wf,wf');

% tau_xx 
err_tauXX = txx_plot - tau_xx_f_h';
l2_err_tauXX = elasticity_non_staggered.l2_error(err_tauXX, wff);

% tau_yy
err_tauYY = tyy_plot - tau_yy_f_h';
l2_err_tauYY = elasticity_non_staggered.l2_error(err_tauYY, wff);

% tau_xy 
err_tauXY = txy_plot - tau_xy_f_h';
l2_err_tauXY = elasticity_non_staggered.l2_error(err_tauXY, wff);

% tau_yx
err_tauYX = tyx_plot - tau_yx_f_h';
l2_err_tauYX = elasticity_non_staggered.l2_error(err_tauYX, wff);

% lambda - X
err_lambdaX = u_plot - uf_h';
l2_err_lambdaX = elasticity_non_staggered.l2_error(err_lambdaX, wff);

% lambda - Y
err_lambdaY = v_plot - vf_h';
l2_err_lambdaY = elasticity_non_staggered.l2_error(err_lambdaY, wff);

% lambda - w 
err_lambdaW = w_plot - wf_h';
l2_err_lambdaW = elasticity_non_staggered.l2_error(err_lambdaW, wff);

% symmetry 
err_symm = tau_xy_f_h' - tau_yx_f_h';
l2_err_symm = elasticity_non_staggered.l2_error(err_symm, wff);

% linear momentum in X
l2_err_lmX = elasticity_non_staggered.l2_error(lm_x_h, wff);

% linear momentum in Y
l2_err_lmY = elasticity_non_staggered.l2_error(lm_y_h, wff);

% angular momentum 
l2_err_am = elasticity_non_staggered.l2_error(err_am_h, wff);