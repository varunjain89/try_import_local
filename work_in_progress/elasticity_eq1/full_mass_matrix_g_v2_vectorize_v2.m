function [ MS ] = full_mass_matrix_g_v2_vectorize_v2(p, gg)
%FULL_MASS_MATRIX Summary of this function goes here
%   Detailed explanation goes here

% Created on: 16th July, 2018
% modified on 15 nove, 2018

g111 = gg.g11';
g112 = gg.g12';
g113 = gg.g13';
g114 = gg.g14';
g121 = gg.g21';
g122 = gg.g22';
g123 = gg.g23';
g124 = gg.g24';
g131 = gg.g31';
g132 = gg.g32';
g133 = gg.g33';
g134 = gg.g34';
g141 = gg.g41';
g142 = gg.g42';
g143 = gg.g43';
g144 = gg.g44';

g111 = g111(:)';
g112 = g112(:)';
g113 = g113(:)';
g114 = g114(:)';
g121 = g121(:)';
g122 = g122(:)';
g123 = g123(:)';
g124 = g124(:)';
g131 = g131(:)';
g132 = g132(:)';
g133 = g133(:)';
g134 = g134(:)';
g141 = g141(:)';
g142 = g142(:)';
g143 = g143(:)';
g144 = g144(:)';


[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

for i = 1:p+1
   O(i) = (i-1)*(p+1) + 1;
end

O1 = O;

for i = 1:p
    O1 = horzcat(O1, O+i);
end

% A12 = [1 5 9 13 2 6 10 14 3 7 11 15 4 8 12 16];

hex  = kron(hp,ep);   % basis functions in x-direction with quad pts. numbered horizontally
                    % these are also the basis functions in y-direction,
                    % but with quad pts numbered vertically. We want the
                    % quad pts to be numbered horizontally, therefore we
                    % will resructure their numbering.
hey  = hex(:,O1);

%% multiplying with metric terms and weights

ww = kron(wp,wp);

%% M11

hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey22, g111); % multiplying test function with metric terms
M1_22 = Slice3DMult2D(hey22, hey'); % multiplying with dof basis

M11 = M1_22;

%% M12

hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey21, g112); % multiplying test function with metric terms
M1_21 = Slice3DMult2D(hey21, hex'); % multiplying with dof basis

M12 = M1_21;

%% M13

hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey22, g113); % multiplying test function with metric terms
M1_22 = Slice3DMult2D(hey22, hey'); % multiplying with dof basis

M13 = M1_22;

%% M14 

hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey21, g114); % multiplying test function with metric terms
M1_21 = Slice3DMult2D(hey21, hex'); % multiplying with dof basis

M14 = M1_21;

%% M21

hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex12, g121); % multiplying test function with metric terms
M1_12 = Slice3DMult2D(hex12, hey'); % multiplying with dof basis

M21 = M1_12;

%% M22

hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex11 = bsxfun(@times, hex11, g122); % multiplying test function with metric terms
M1_11 = Slice3DMult2D(hex11, hex'); % multiplying with dof basis

M22 = M1_11;

%% M23

hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex12, g123); % multiplying test function with metric terms
M1_12 = Slice3DMult2D(hex12, hey'); % multiplying with dof basis

M23 = M1_12;

%% M24

hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex11 = bsxfun(@times, hex11, g124); % multiplying test function with metric terms
M1_11 = Slice3DMult2D(hex11, hex'); % multiplying with dof basis

M24 = M1_11;

%% M31

hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey22, g131); % multiplying test function with metric terms
M1_22 = Slice3DMult2D(hey22, hey'); % multiplying with dof basis

M31 = M1_22;

%% M32

hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey21, g132); % multiplying test function with metric terms
M1_21 = Slice3DMult2D(hey21, hex'); % multiplying with dof basis

M32 = M1_21;

%% M33

hey22 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey22 = bsxfun(@times, hey22, g133); % multiplying test function with metric terms
M1_22 = Slice3DMult2D(hey22, hey'); % multiplying with dof basis

M33 = M1_22;

%% M34 

hey21 = bsxfun(@times, hey, ww);    % multiplying test function with weights
hey21 = bsxfun(@times, hey21, g134); % multiplying test function with metric terms
M1_21 = Slice3DMult2D(hey21, hex'); % multiplying with dof basis

M34 = M1_21;

%% M41

hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex12, g141); % multiplying test function with metric terms
M1_12 = Slice3DMult2D(hex12, hey'); % multiplying with dof basis

M41 = M1_12;

%% M42

hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex11 = bsxfun(@times, hex11, g142); % multiplying test function with metric terms
M1_11 = Slice3DMult2D(hex11, hex'); % multiplying with dof basis

M42 = M1_11;

%% M43

hex12 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex12 = bsxfun(@times, hex12, g143); % multiplying test function with metric terms
M1_12 = Slice3DMult2D(hex12, hey'); % multiplying with dof basis

M43 = M1_12;

%% M44

hex11 = bsxfun(@times, hex, ww);    % multiplying test function with weights
hex11 = bsxfun(@times, hex11, g144); % multiplying test function with metric terms
M1_11 = Slice3DMult2D(hex11, hex'); % multiplying with dof basis

M44 = M1_11;

MS = [M11 M12 M13 M14; M21 M22 M23 M24; M31 M32 M33 M34; M41 M42 M43 M44];

end