function [hp_new,ep_new] = new_basis(p, hp, ep)

hp_new = sin(hp*pi/2);

ep_new(1,:) = pi/2 .* cos(pi/2*hp(1,:)) .* ep(1,:);

for i = 2:p
    ep_new(i,:) = ep_new(i-1,:) + pi/2 .* cos(pi/2*hp(i,:)) .* (ep(i,:)-ep(i-1,:));
end

end

