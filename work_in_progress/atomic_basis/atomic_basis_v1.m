clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% domain

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

%% discretization

K = 1;
p = 4;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

%% 1D primal basis

pf = 50;
[xii, wii] = GLLnodes(pf);

[hp_plot, ep_plot] = MimeticpolyVal(xii,p,1);

lw = 1.5;
fs = 16;

[hp_new2, ep_new2] = new_basis(p, hp_plot, ep_plot);

hp_plot2 = sin(hp_plot*pi/2);

figure
hold on
grid on
for i = 1:p+1 
    plot(xii, ep_new2,'LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$h_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');


% for i = 1:p
%     dh_bar(i,:) = pi/2 .* ep_plot(i,:) .* cos(pi/2*hp_plot(i,:));
% end
% 
% ep_new(1,:) = -dh_bar(1,:);
% ep_new(2,:) = -dh_bar(1,:) -dh_bar(2,:);
% ep_new(3,:) = -dh_bar(1,:) -dh_bar(2,:) -dh_bar(3,:);

ep_new(1,:) = pi/2 .* cos(pi/2*hp_plot(1,:)) .* ep_plot(1,:);
ep_new(2,:) = ep_new(1,:) + pi/2 .* cos(pi/2*hp_plot(2,:)) .* (ep_plot(2,:)-ep_plot(1,:));
ep_new(3,:) = ep_new(2,:) + pi/2 .* cos(pi/2*hp_plot(3,:)) .* (ep_plot(3,:)-ep_plot(2,:));

figure
hold on
grid on
for i = 1:p
    plot(xii, ep_plot,'LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$e_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

% figure
% hold on
grid on
for i = 1:p
    plot(xii, ep_new,'--','LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$e_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% 1D dual nodal basis 

% M0 = zeros(p+1);
% 
% % for i = 1:p+1
% %     for k = 1:p+1
% %         for xx = 1:p+1
% %             M0(i,k) = M0(i,k) + hp(i,xx)*hp(k,xx)*wp(xx);
% %         end
% %     end
% % end
% 
% for i = 1:p+1
%     for k = 1:p+1
%         for xx = 1:pf+1
%             M0(i,k) = M0(i,k) + hp_plot(i,xx)*hp_plot(k,xx)*wii(xx);
%         end
%     end
% end
% 
% hp_dual_plot = M0\hp_plot;
% 
% figure
% hold on
% grid on
% for i = 1:p+1 
%     plot(xii, hp_dual_plot,'LineWidth',lw)
% end
% 
% xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
% ylabel('$$\widetilde{h}_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');
% 
% %% 1D dual edge basis 
% 
% M1 = zeros(p);
% 
% % for i = 1:p+1
% %     for k = 1:p+1
% %         for xx = 1:p+1
% %             M0(i,k) = M0(i,k) + hp(i,xx)*hp(k,xx)*wp(xx);
% %         end
% %     end
% % end
% 
% for i = 1:p
%     for k = 1:p
%         for xx = 1:pf+1
%             M1(i,k) = M1(i,k) + ep_plot(i,xx)*ep_plot(k,xx)*wii(xx);
%         end
%     end
% end
% 
% ep_dual_plot = M1\ep_plot;
% 
% figure
% hold on
% grid on
% for i = 1:p+1 
%     plot(xii, ep_dual_plot,'LineWidth',lw)
% end
% 
% xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
% ylabel('$$\widetilde{e}_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% numerical integration 

% sum_hp_dual = zeros(1,p+1);
% 
% for i = 1:p+1
%     for xx = 1:pf+1
%     sum_hp_dual(i) = sum_hp_dual(i) + hp_dual_plot(i,xx)*wii(xx);
%     end
% end

% sum_ep_dual = zeros(1,p);
% 
% for i = 1:p
%     for xx = 1:pf+1
%     sum_ep_dual(i) = sum_ep_dual(i) + ep_dual_plot(i,xx)*wii(xx);
%     end
% end

% sum_hp_prim = zeros(1,p+1);
% 
% for i = 1:p+1
%     for xx = 1:pf+1
%     sum_hp_prim(i) = sum_hp_prim(i) + hp_plot(i,xx)*wii(xx);
%     end
% end

%% edge integrals of dual polynomials

edge_map   = @(s,x1,x2) x1*0.5*(1-s) + x2*0.5*(1+s);

xii2 = edge_map(xii,xp(3),xp(4));
[hp_int, ep_int] = MimeticpolyVal(xii2,p,1);

% hp_dual_plot2 = interp1(xii,hp_dual_plot(1,:),xii2,'spline');

figure
plot(xii2, ep_int,'-o')

sum = 0;

dx_dxii = (xp(4)-xp(3))/2;

for xx = 1:pf+1
    sum = sum + ep_int(3,xx) * wii(xx) * dx_dxii;
end

sum

sum2 = 0;

ep_new_int(1,:) = pi/2 .* cos(pi/2*hp_int(1,:)) .* ep_int(1,:);
ep_new_int(2,:) = ep_new_int(1,:) + pi/2 .* cos(pi/2*hp_int(2,:)) .* (ep_int(2,:)-ep_int(1,:));
ep_new_int(3,:) = ep_new_int(2,:) + pi/2 .* cos(pi/2*hp_int(3,:)) .* (ep_int(3,:)-ep_int(2,:));



figure
plot(xii2, ep_new_int(2,:),'-o')

for xx = 1:pf+1
    sum2 = sum2 + ep_new_int(3,xx) * wii(xx) * dx_dxii;
end

sum2

%%

