function global_number = get_global_number(region_number, local_number, K, N, ...
    edge_anno, connectivity_manual, compass_sequence);
    loc2glo = loc2glo_regions(edge_anno, connectivity_manual, compass_sequence);
    max_local_number = 4*K*N;
    if local_number > max_local_number;
        error('Error: local_number exceeds the maximum possible local_number.')
    else
        local_edge_number = ceil(local_number/(K*N));
        global_edge_number = loc2glo(local_edge_number, region_number);
        global_number_all = ((global_edge_number-1)*K*N)+1:1:((global_edge_number-1)*K*N)+K*N;
        global_number_all = [global_number_all,global_number_all];
        if region_number > 1;
            if ismember(global_edge_number, loc2glo(:,1:region_number-1));           
                global_number_all = flip(global_number_all);
                global_number = global_number_all(mod(local_number,K*N)+K*N);
            else
                global_number = global_number_all(mod(local_number,K*N)+K*N);
            end
        else
            global_number = global_number_all(mod(local_number,K*N)+K*N);
        end
end
