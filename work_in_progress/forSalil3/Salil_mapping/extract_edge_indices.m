function [row, column, layer] = extract_edge_indices(reg_c, compass_c, Kx, Ky, sampling_point_no);
    seq_all = [];
    compass_seq_all = {};
    for i = 1:length(reg_c)
        switch compass_c{i}
            case 'N'
                seq = Ky:Ky:Kx*Ky;
            case 'S'
                seq = 1:Ky:((Kx-1)*Ky)+1;
            case 'E'
                seq = ((Kx-1)*Ky)+1:1:Kx*Ky;
            case 'W'
                seq = 1:1:Ky;
        end
        seq = (reg_c(i)-1)*(Kx*Ky)+seq;
        compass_seq = cell(1,length(seq));
        compass_seq(:) = {compass_c{i}};
        seq_all = [seq_all, seq];
        compass_seq_all = [compass_seq_all, compass_seq];
    end
    layer = [];
    row = [];
    column = [];
    for i = 1:length(seq_all);
        layer = [layer, seq_all(i).*ones(1,sampling_point_no)];
        switch compass_seq_all{i};
            case 'N'
                row_add = sampling_point_no.*ones(1,sampling_point_no);
                col_add = 1:1:sampling_point_no;
            case 'S'
                row_add = ones(1,sampling_point_no);
                col_add = 1:1:sampling_point_no;
            case 'E'
                row_add = 1:1:sampling_point_no;
                col_add = sampling_point_no.*ones(1,sampling_point_no);
            case 'W'
                row_add = 1:1:sampling_point_no;
                col_add = ones(1,sampling_point_no);
        end
        row = [row, row_add];
        column = [column, col_add];
    end        
end
 