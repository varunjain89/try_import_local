function [curvecoords, Jacobian] = mapping(s, t, mesh_ind, mesh, edge_type, ...
    nodecoords, p)

% Transfinite mapping of points from a unit square in the (s,t) coordinates
% 0<=(s,t)=<1 to a curved element domain
%
% Copyright Salil Luesutthiviboon (ANCE) 2019
% =========================================================================
%
% INPUTS
% ------
% s,t: 0<=(s,t)=<1
%
% mesh_ind: row index of interest in the 'mesh' matrix (It can do only one 
% element at a time)
%
% mesh: matrix containing the indices in the nodecoords matrix representing
% the points on the boundary of the mesh
%
% edge_type: specifying type of line connecting the points corresponding to
% the points in the 'mesh' matrix. Er zijn 3 mogelijkheden:
% 'li' = linear line
% 'af_lo' = curved PARSEC airfoil polynomial for the pressure side ('lower')
% 'af_up' = curved PARSEC airfoil polynomial for the suction side ('upper')
%
% nodecoords: contains the (x,y) coordinates of the nodes
%
% p: contains the inputs for the PARSEC function, c.f. [2]
%
% COMPUTED VARIABLES
% ------------------
% F_all: contains 4 mapping functions for 4 edges, the two rows are x and y
%
% dF_all: contains the derivatives of the 4 mapping functions for 4 edges,  
% the two rows are x and y
%
% OUTPUTS
% -------
% curvecoords: [2x1] contains the mapped x and y coordinates (corresponds
% to T(s,t) in Eq. (17) in [1] 
% 
% Jacobian: [2x2] contains the Jacobian of T(s,t), i.e.
% Jacobian = [dTx(s,t)/ds dTx(s,t)/dt; dTy(s,t)/ds dTy(s,t)/dt]
%
% REFERENCES
% ----------
% [1] Gordon, W. J., & Hall, C. A. (1973). Transfinite element methods:
% blending-function interpolation over arbitrary curved element domains.
% Numerische Mathematik, 21(2), 109-129.
% [2] Sobieczky, H. (1999). Parametric airfoils and wings. In Recent
% development of aerodynamic design methodologies (pp. 71-87). Vieweg+
% Teubner Verlag.
% =========================================================================

    F_all = cell(1, size(mesh, 2)-1);
    dF_all = cell(1, size(mesh, 2)-1);
    for i = 1:size(mesh, 2)-1
        % For the last two sides of the square, invert the sequence to be
        % parallel to the first two
        if i <= 2
            x1 = nodecoords(mesh(mesh_ind,i),1);
            x2 = nodecoords(mesh(mesh_ind,i+1),1);
            z1 = nodecoords(mesh(mesh_ind,i),2);
            z2 = nodecoords(mesh(mesh_ind,i+1),2);
        else
            x1 = nodecoords(mesh(mesh_ind,i+1),1);
            x2 = nodecoords(mesh(mesh_ind,i),1);
            z1 = nodecoords(mesh(mesh_ind,i+1),2);
            z2 = nodecoords(mesh(mesh_ind,i),2);
        end
        
        switch edge_type{mesh_ind, i}
            case 'li'
                F = @(f) [(f*(x2-x1)+x1); ...
                            (f*(z2-z1)+z1)];
                dF = @(f) [x2-x1; z2-z1];
            case 'af_up'
                F = @(f) [(f*(x2-x1)+x1); ...
                            get_y_parsec(p, (f*(x2-x1)+x1), 'up', 0)];
                dF = @(f) [x2-x1; ...
                    (x2-x1)*get_y_parsec(p, (f*(x2-x1)+x1), 'up', 1)];
            case 'af_lo'
                F = @(f) [(f*(x2-x1)+x1); ...
                            get_y_parsec(p, (f*(x2-x1)+x1), 'lo', 0)];
                dF = @(f) [x2-x1; ...
                    (x2-x1)*get_y_parsec(p, (f*(x2-x1)+x1), 'lo', 1)];
        end
        F_all{1,i} = F;
        dF_all{1,i} = dF;
        
    end
    % Eq1 (17) in [1]
    curvecoords = (1-s).*F_all{4}(t) + s.*F_all{2}(t) + (1-t).*F_all{1}(s) ...
                + t.*F_all{3}(s) - (1-s).*((1-t).*F_all{1}(0) + t.*F_all{3}(0)) ...
                - s.*((1-t).*F_all{1}(1) + t.*F_all{3}(1));
    dTds = -F_all{4}(t) + F_all{2}(t) + (1.0-t)*dF_all{1}(s) + t*dF_all{3}(s) + ...
                ((1.0-t)*F_all{1}(0) + t*F_all{3}(0)) - ((1.0-t)*F_all{1}(1) + t*F_all{3}(1));
    dTdt = (1.0-s)*dF_all{4}(t) + s*dF_all{2}(t) - F_all{1}(s) + F_all{3}(s) - ...
                         (1.0-s)*(-F_all{1}(0) + F_all{3}(0)) - s*(-F_all{1}(1) + F_all{3}(1));
    Jacobian = [dTds(1) dTdt(1); dTds(2) dTdt(2)];
    % end of program
end
    
        
        