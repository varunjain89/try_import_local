%% -- run after test_porous_airfoil_vXX.m --
sampling_point = pf+1;

upper_reg = [2 5 9];
upper_compass = {'N','N','N'};

lower_reg = [2 6 9];
lower_compass = {'W','S','E'};

% upper_reg = [4 4 4];
% upper_compass = {'W','S','E'};
% 
% lower_reg = [7 7 7];
% lower_compass = {'W','N','E'};

upper_reg = [1 1 4 8 8];
upper_compass = {'S','E','S','W','S'};

lower_reg = [3 3 7 10 10];
lower_compass = {'N','E','N','W','N'};

% upper_reg = [1 4 8];
% upper_compass = {'W','N','E'};
% 
% lower_reg = [3 7 10];
% lower_compass = {'W','S','E'};

%% find data indices
[row_upper, column_upper, layer_upper] = ...
    extract_edge_indices(upper_reg, upper_compass, Kx, Ky, sampling_point);

[row_lower, column_lower, layer_lower] = ...
    extract_edge_indices(lower_reg, lower_compass, Kx, Ky, sampling_point);

%% retrieve data
idx_upper = sub2ind(size(pp_ex),row_upper, column_upper, layer_upper);
pp_ex_upper = pp_ex(idx_upper);
rec_x_upper = rec_x(idx_upper);
rec_y_upper = rec_y(idx_upper);
rec_p_upper = rec_p(idx_upper);

idx_lower = sub2ind(size(pp_ex),row_lower, column_lower, layer_lower);
pp_ex_lower = pp_ex(idx_lower);
rec_x_lower = rec_x(idx_lower);
rec_y_lower = rec_y(idx_lower);
rec_p_lower = rec_p(idx_lower);

%%
figure()
subplot(212)
set(gca,'FontSize',16); set(gca,'LineWidth',2); hold on;
        set(gca,'fontname','times');  set(gcf,'color','w');
for i = 1:floor(length(rec_x_lower)/(sampling_point))
    plot(rec_x_lower(((i-1)*sampling_point)+1:sampling_point*i),...
        rec_y_lower(((i-1)*sampling_point)+1:sampling_point*i),'-k','LineWidth',1.5);
    hold on
    plot(rec_x_upper(((i-1)*sampling_point)+1:sampling_point*i),...
        rec_y_upper(((i-1)*sampling_point)+1:sampling_point*i),'-k','LineWidth',1.5);
end

% axis([0 1 -0.1 0.1])
% pbaspect([5 1 1])
xlabel('$X/c$ [-]','Interpreter','latex')
ylabel('$Y/c$ [-]','Interpreter','latex')
grid on

%%
subplot(211)
set(gca,'FontSize',16); set(gca,'LineWidth',2); hold on;
        set(gca,'fontname','times');  set(gcf,'color','w');
for i = 1:floor(length(rec_x_lower)/(sampling_point))
    h1=plot(rec_x_lower(((i-1)*sampling_point)+1:sampling_point*i),...
        pp_ex_lower(((i-1)*sampling_point)+1:sampling_point*i),'-k','LineWidth',1.5);
    hold on
    h2=scatter(rec_x_lower(((i-1)*sampling_point)+1:sampling_point*i),...
        rec_p_lower(((i-1)*sampling_point)+1:sampling_point*i),50,'or');
    
    h3=plot(rec_x_upper(((i-1)*sampling_point)+1:sampling_point*i),...
        pp_ex_upper(((i-1)*sampling_point)+1:sampling_point*i),'--k','LineWidth',1.5);
    hold on
    h4=scatter(rec_x_upper(((i-1)*sampling_point)+1:sampling_point*i),...
        rec_p_upper(((i-1)*sampling_point)+1:sampling_point*i),50,'^b');
end

legend([h1 h2 h3 h4],{'Exact solution lower surface','Reconstructed solution lower surface',...
    'Exact solution upper surface','Reconstructed solution upper surface'})

% pbaspect([1.618 1 1])
xlabel('$X/c$ [-]','Interpreter','latex')
ylabel('Extracted quantity [-]','Interpreter','latex')
grid on

%%
figure()
set(gca,'FontSize',16); set(gca,'LineWidth',2); hold on;
        set(gca,'fontname','times');  set(gcf,'color','w');
for i = 1:floor(length(rec_x_lower)/(sampling_point))
    h1=plot(rec_x_lower(((i-1)*sampling_point)+1:sampling_point*i),...
        pp_ex_lower(((i-1)*sampling_point)+1:sampling_point*i),'-k','LineWidth',1.5);
    h2=plot(rec_x_upper(((i-1)*sampling_point)+1:sampling_point*i),...
        pp_ex_upper(((i-1)*sampling_point)+1:sampling_point*i),'--k','LineWidth',1.5);
    
    h3=scatter(rec_x_lower(((i-1)*sampling_point)+1:sampling_point*i),...
        rec_p_lower(((i-1)*sampling_point)+1:sampling_point*i),50,'or');
    h4=scatter(rec_x_upper(((i-1)*sampling_point)+1:sampling_point*i),...
        rec_p_upper(((i-1)*sampling_point)+1:sampling_point*i),50,'^b');
end

% legend([h1 h2 h3 h4],{'Exact solution lower surface','Reconstructed solution lower surface',...
%     'Exact solution upper surface','Reconstructed solution upper surface'})

% pbaspect([1.618 1 1])
xlabel('$X/c$ [-]','Interpreter','latex')
ylabel('Extracted quantity [-]','Interpreter','latex')
grid on
        
        
