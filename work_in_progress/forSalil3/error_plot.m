close all
clear all
clc

K_trial = 3;
p_trial = 2:1:16;

p_plot_K3 = [];
error_plot_K3 = [];
time_plot_K3 = [];

for i_trial = 1:length(p_trial);
    K = K_trial;
    p = p_trial(i_trial);
    
    load(strcat('K_',num2str(K),'_p_',num2str(p),'.mat'));
    p_plot_K3 = [p_plot_K3, p];
    error_plot_K3 = [error_plot_K3, error.sqre];
    time_plot_K3 = [time_plot_K3, TimerVal];
    
end

K_trial = 1;
p_trial = 2:1:19;

p_plot_K1 = [];
error_plot_K1 = [];
time_plot_K1 = [];

for i_trial = 1:length(p_trial);
    K = K_trial;
    p = p_trial(i_trial);
    
    load(strcat('K_',num2str(K),'_p_',num2str(p),'.mat'));
    p_plot_K1 = [p_plot_K1, p];
    error_plot_K1 = [error_plot_K1, error.sqre];
    time_plot_K1 = [time_plot_K1, TimerVal];
    
end

K_trial = 2;
p_trial = 2:1:18;

p_plot_K2 = [];
error_plot_K2 = [];
time_plot_K2 = [];

for i_trial = 1:length(p_trial);
    K = K_trial;
    p = p_trial(i_trial);
    
    load(strcat('K_',num2str(K),'_p_',num2str(p),'.mat'));
    p_plot_K2 = [p_plot_K2, p];
    error_plot_K2 = [error_plot_K2, error.sqre];
    time_plot_K2 = [time_plot_K2, TimerVal];
    
end


figure();
set(gca,'FontSize',22); set(gca,'LineWidth',4); hold on;
set(gca,'fontname','times');  set(gcf,'color','w');
%set(gca, 'Layer', 'top'); 
set(gca,'YScale','log');

palette = [158,1,66
213,62,79
244,109,67
253,174,97
254,224,139
255,255,191
230,245,152
171,221,164
102,194,165
50,136,189
94,79,162]./255;

lh1 = plot(p_plot_K1,error_plot_K1,'-ko','Color',palette(11,:),...
    'LineWidth',2.5,'MarkerEdgeColor',palette(11,:),'MarkerSize',15,'MarkerFaceColor','w');
drawnow;
lh1.MarkerHandle.LineWidth = 1.5;

lh2 = plot(p_plot_K2,error_plot_K2,'-ks','Color',palette(10,:),...
    'LineWidth',2.5,'MarkerEdgeColor',palette(10,:),'MarkerSize',15,'MarkerFaceColor','w');
drawnow;
lh2.MarkerHandle.LineWidth = 1.5;

lh3 = plot(p_plot_K3,error_plot_K3,'-k^','Color',palette(9,:),...
    'LineWidth',2.5,'MarkerEdgeColor',palette(9,:),'MarkerSize',15,'MarkerFaceColor','w');
drawnow;
lh3.MarkerHandle.LineWidth = 1.5;

xlabel('$N$','Interpreter','latex');
ylabel('$L^2-$error','Interpreter','latex');
hl = legend('$K \times K=1 \times 1$','$K \times K=2 \times 2$','$K \times K=3 \times 3$')
set(hl,'Interpreter','latex','Location','northeast')

drawnow;
lineEntry = findobj(hl.EntryContainer, 'Object',lh1);
entryMarker = findobj(lineEntry.Icon.Transform, 'Description','Icon Marker');
entryMarker.LineWidth = 1.5;

lineEntry = findobj(hl.EntryContainer, 'Object',lh2);
entryMarker = findobj(lineEntry.Icon.Transform, 'Description','Icon Marker');
entryMarker.LineWidth = 1.5;

lineEntry = findobj(hl.EntryContainer, 'Object',lh3);
entryMarker = findobj(lineEntry.Icon.Transform, 'Description','Icon Marker');
entryMarker.LineWidth = 1.5;

pbaspect([1 1 1])
set(gcf, 'Position',  [700, 200, 700, 700])

%%

figure();
set(gca,'FontSize',22); set(gca,'LineWidth',4); hold on;
set(gca,'fontname','times');  set(gcf,'color','w');
%set(gca, 'Layer', 'top'); 
set(gca,'YScale','log');

lh1 = plot(p_plot_K1,time_plot_K1,'-ko','Color',palette(11,:),...
    'LineWidth',2.5,'MarkerEdgeColor',palette(11,:),'MarkerSize',15,'MarkerFaceColor','w');
drawnow;
lh1.MarkerHandle.LineWidth = 1.5;

lh2 = plot(p_plot_K2,time_plot_K2,'-ks','Color',palette(10,:),...
    'LineWidth',2.5,'MarkerEdgeColor',palette(10,:),'MarkerSize',15,'MarkerFaceColor','w');
drawnow;
lh2.MarkerHandle.LineWidth = 1.5;

lh3 = plot(p_plot_K3,time_plot_K3,'-k^','Color',palette(9,:),...
    'LineWidth',2.5,'MarkerEdgeColor',palette(9,:),'MarkerSize',15,'MarkerFaceColor','w');
drawnow;
lh3.MarkerHandle.LineWidth = 1.5;

xlabel('$N$','Interpreter','latex');
ylabel('Computation time [second]','Interpreter','latex');
hl = legend('$K \times K=1 \times 1$','$K \times K=2 \times 2$','$K \times K=3 \times 3$')
set(hl,'Interpreter','latex','Location','northeast')

drawnow;
lineEntry = findobj(hl.EntryContainer, 'Object',lh1);
entryMarker = findobj(lineEntry.Icon.Transform, 'Description','Icon Marker');
entryMarker.LineWidth = 1.5;

lineEntry = findobj(hl.EntryContainer, 'Object',lh2);
entryMarker = findobj(lineEntry.Icon.Transform, 'Description','Icon Marker');
entryMarker.LineWidth = 1.5;

lineEntry = findobj(hl.EntryContainer, 'Object',lh3);
entryMarker = findobj(lineEntry.Icon.Transform, 'Description','Icon Marker');
entryMarker.LineWidth = 1.5;

pbaspect([1 1 1])
set(gcf, 'Position',  [700, 200, 700, 700])


