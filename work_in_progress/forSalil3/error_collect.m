% K_trial = 3;
% p_trial = 2:1:20;
% 
% for i_trial = 1:length(p_trial);
%     K = K_trial
%     p = p_trial(i_trial)
%     tic
%     test_porous_airfoil_v11_forrun
%     TimerVal = toc
%     metric_dis_ggg = metric_dis.ggg;
%     metric_rec_ggg = metric_rec.ggg;
%     
%     save(strcat('K_',num2str(K),'_p_',num2str(p),'.mat'),...
%         'error','pp_ex','rec_p','rec_x','rec_y','p_h','q_h',...
%         'metric_dis_ggg','metric_rec_ggg','wfxy2','pf','TimerVal');
% end

K_trial = 2;
p_trial = 2:1:20;

for i_trial = 1:length(p_trial);
    K = K_trial
    p = p_trial(i_trial)
    tic
    test_porous_airfoil_v11_forrun
    TimerVal = toc
    metric_dis_ggg = metric_dis.ggg;
    metric_rec_ggg = metric_rec.ggg;
    
    save(strcat('K_',num2str(K),'_p_',num2str(p),'.mat'),...
        'error','pp_ex','rec_p','rec_x','rec_y','p_h','q_h',...
        'metric_dis_ggg','metric_rec_ggg','wfxy2','pf','TimerVal');
end