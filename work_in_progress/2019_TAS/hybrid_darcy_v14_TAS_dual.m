function [X_h3, lambda_h] = hybrid_darcy_v14_TAS_dual(K, p, c)

%% analytical solution

phi_an = @(x,y) sin(2*pi*x).*sin(2*pi*y);
u_an   = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
v_an   = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
f_an   = @(x,y) -8*pi*pi*sin(2*pi*x).*sin(2*pi*y);

%% domain

xbound = [0 1];
ybound = [0 1];
% c = 0.3;

%% discretization

% K = 100;
% p = 11;

ttl_nr_el = K^2;
local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;
ttl_loc_dof = local_nr_ed + local_nr_pp;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% calculate metric terms required for mass matrix

% evaluate jacobian at GLL points
% mesh1.eval_p_der();

mesh1.eval_p_der_v2();

% evaluate g11, g12, g22 for poisson flow

mesh1.eval.metric.ggg = metric.ggg(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta);
mesh1.eval.metric.g11 = metric.g11(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, mesh1.eval.p.ggg);
mesh1.eval.metric.g12 = metric.g12(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, mesh1.eval.p.ggg);
mesh1.eval.metric.g22 = metric.g22(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, mesh1.eval.p.ggg);

%% make mass matrix

M11 = zeros(local_nr_ed,local_nr_ed,ttl_nr_el);

for eln = 1:ttl_nr_el
    % this part is already implicitly multi threaded (it seems) and has 
    % to go to different clusters for further improvement in efficiency
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% make incidence matrix

E211 = incidence_matrix.E21(p);
E211 = full(E211);

%% calculate inverse of system

sys_inv_3D = inverse_gpu(M11,E211, ttl_nr_el, local_nr_ed, local_nr_pp);
clear M11;

K
p

% assemble into sparse 2D system
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
% A_inv = AssembleMatrices2(GM_local_dof',GM_local_dof',sys_inv_3D);

for eln = 1:ttl_nr_el
    q(eln) = {sparse(sys_inv_3D(:,:,eln))};
end

% q = num2cell(sys_inv_3D,[1,2]);
A_inv = blkdiag(q{:});

%% calculate connectivity matrix

N = full(incidence_matrix.OutwardNormalMatrix(p));
N_3D = repmat(N,[1 1 K^2]);

GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof_q = GM_local_dof(:,1:local_nr_ed);
% clear GM_local_dof;

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];
ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);

%% calculate reduction of RHS (source term - f)

volForm = twoForm_v2(mesh1);

F_3D = volForm.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + local_nr_ed;
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

%% evaluate lambda

lambda = (ass_conn3 * A_inv * ass_conn3')\(ass_conn3 * A_inv * RHS);

%% seperating lambda for each element 

lambda2 = [lambda; zeros(4*K*p,1)];

for eln = 1:ttl_nr_el
    lambda_h(:,eln) = lambda2(GM_lambda(:,eln));
end

%% 

X_h3 = cell(1,ttl_nr_el);

for eln = 1:ttl_nr_el
    X_h3(eln) = {sys_inv_3D(:,:,eln) * ([zeros(local_nr_ed,1); F_3D(:,eln)] - [N'*lambda_h(:,eln); zeros(local_nr_pp,1)])};
end

end

