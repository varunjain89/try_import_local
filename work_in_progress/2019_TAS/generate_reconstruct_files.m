clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% load solution files

load('X_h_Poisson_v2.mat')
load('lambda_h_Poisson_v2.mat')

%% 

K = 10;
p = 10;
c = 0.3;

phi_an = @(x,y) sin(2*pi*x).*sin(2*pi*y);
u_an   = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
v_an   = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
f_an   = @(x,y) -8*pi*pi*sin(2*pi*x).*sin(2*pi*y);

xbound = [0 1];
ybound = [0 1];

vname = sprintf('K_%d_N_%d', K, p);

X_h3 = X_h.c03.(genvarname([vname]));

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

ttl_nr_el = K^2;

%% separating cochains 

for eln = 1:ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:2*p*(p+1));
    p_h(:,eln) = X_h3{eln}(2*p*(p+1) + 1:2*p*(p+1)+ p^2);
end

%% dual to primal dof

mesh1.eval_p_der_v2();

M22s = zeros(p^2,p^2,ttl_nr_el);

tic
for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end
toc

%% post processing 

pf = 20;

mesh1.eval_pf_der(pf);

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

%% conservation of mass

E211 = incidence_matrix.E21(p);

volForm = twoForm_v2(mesh1);
F_3D = volForm.reduction(f_an);

for el = 1:ttl_nr_el
    divQ(:,el) = E211 * q_h(:,el);
end

Err_mass = divQ - F_3D;
[xf_3D, yf_3D, massf_h] = volForm.reconstruction(Err_mass);
massf_h = permute(massf_h, [2 1 3]); % transpose along dim = 3.

mass_ex = zeros(size(xf_3D));
[error.mass] = error_processor_v3(mass_ex, massf_h, wfxy2, mesh1.eval.pf.ggg);

%% reconstruction pressure

p_h_2 = zeros(size(p_h));

for eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

[xf_3D, yf_3D, pf_h] = volForm.reconstruction(p_h_2);
pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

pp_ex = phi_an(xf_3D, yf_3D);
[error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln))
% end
% colorbar

%% reconstruction flux

surForm = oneForm_v2(mesh1);

rec = surForm.reconstruction(q_h);

rec_qx = rec.qx;
rec_qy = rec.qy;

qx_ex = u_an(xf_3D, yf_3D);
qy_ex = v_an(xf_3D, yf_3D);

[error.qx] = error_processor_v3(qx_ex, -rec_qy, wfxy2, mesh1.eval.pf.ggg);
[error.qy] = error_processor_v3(qy_ex, rec_qx, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qx(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qy(:,:,eln))
% end
% colorbar

%% reconstruction div Q

[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(divQ);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

ff_ex = f_an(xf_3D, yf_3D);
[error.ff] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

%% Hdiv error

err_divQ = error.qx.sqre + error.qy.sqre + error.ff.sqre;
err_divQ = sqrt(err_divQ)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), ff_ex(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h(:,:,eln))
% end
% colorbar

%% H10 error 

% evaluate discrete grad p

% parfor el = 1:K^2
%     u_h(:,el) = E211' * p_h(:,el) + N' * lm_h(:,el);
% end
% 
% parfor el = 1:K^2
%     u_h_2(:,el) = M11(:,:,el)\u_h(:,el);
% end
% 
% gradp = surForm.reconstruction(u_h_2);
% 
% [error.gradp.x] = error_processor_v3(qx_ex, gradp.qx, wfxy2, mesh1.eval.pf.ggg);
% [error.gradp.y] = error_processor_v3(qy_ex, gradp.qy, wfxy2, mesh1.eval.pf.ggg);
% 
% err_H1p = err_H1(error.pp.sqre, error.gradp.x.sqre, error.gradp.y.sqre)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_x(:,:,eln))
% end
% colorbar

% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_y(:,:,eln))
% end
% colorbar

% p_h = full(p_h');
% fname = sprintf('data/orthogonal/alg_dual/K_%d_N_%d_phi.dat', K, p);
% save(fname, 'pf_h','-ascii');
% 
% u_h = full(u_h');
% fname = sprintf('data_2018/primal-algebraic-dual/c-00/K_%d_N_%d_ux.dat', K, p);
% save(fname, 'u_h','-ascii');
% 
% v_h = full(v_h);
% fname = sprintf('data_2018/primal-algebraic-dual/c-00/K_%d_N_%d_uy.dat', K, p);
% save(fname, 'v_h','-ascii');
% 
% fname = sprintf('data_2018/primal-algebraic-dual/c-00/K_%d_N_%d_xif.dat', K, p);
% save(fname, 'xif','-ascii');
% 
% fname = sprintf('data_2018/primal-algebraic-dual/c-00/K_%d_N_%d_etaf.dat', K, p);
% save(fname, 'etaf','-ascii');
% 
% w_h = meshgrid(wf);
% fname = sprintf('data_2018/primal-algebraic-dual/c-00/K_%d_N_%d_w_h.dat', K, p);
% save(fname, 'w_h','-ascii');

