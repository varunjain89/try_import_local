clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% analytical solution

phi_an = @(x,y) sin(2*pi*x).*sin(2*pi*y);
u_an   = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
v_an   = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
f_an   = @(x,y) -8*pi*pi*sin(2*pi*x).*sin(2*pi*y);

%% domain

xbound = [0 1];
ybound = [0 1];
c = 0.3;

%% discretization

K = 10;
p = 10;

ttl_nr_el = K^2;
local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;
ttl_loc_dof = local_nr_ed + local_nr_pp;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% calculate metric terms required for mass matrix

% evaluate jacobian at GLL points
mesh1.eval_p_der();

% evaluate g11, g12, g22 for poisson flow

mesh1.eval.metric.ggg = metric.ggg(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta);
mesh1.eval.metric.g11 = metric.g11(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, mesh1.eval.p.ggg);
mesh1.eval.metric.g12 = metric.g12(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, mesh1.eval.p.ggg);
mesh1.eval.metric.g22 = metric.g22(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, mesh1.eval.p.ggg);

%% make mass matrix

M11 = zeros(local_nr_ed,local_nr_ed,ttl_nr_el);

for eln = 1:ttl_nr_el
    % this part is already implicitly multi threaded (it seems) and has 
    % to go to different clusters for further improvement in efficiency
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% make incidence matrix

E211 = incidence_matrix.E21(p);
E211 = full(E211);

%% calculate inverse of system

sys_inv_3D = zeros(ttl_loc_dof,ttl_loc_dof,ttl_nr_el);

sizeM11 = size(M11);
pckts = (sizeM11(1)*sizeM11(2)*sizeM11(3)*8)/(60*1024*1024)
pckts = floor(pckts)

if pckts == 0
    limit = ttl_nr_el
else
limit = ttl_nr_el/pckts +1
limit = floor(limit)
end

temp_sys = [zeros(local_nr_ed) E211'; E211 zeros(local_nr_pp)];
SYS_3D_on_CPU = repmat(temp_sys,[1 1 limit]);

% reset(gpuDevice)
d = gpuDevice;

% pass incomplete system to GPU
SYS_3D_on_GPU = gpuArray(SYS_3D_on_CPU);

for in = 1:pckts
    in
    % add M11 to the 3-D system on gpu
    d.AvailableMemory
    SYS_3D_on_GPU(1:local_nr_ed,1:local_nr_ed,:) = gpuArray(M11(:,:,(in-1)*limit+1:in*limit));
    % do the inverse of the 3D system
    d.AvailableMemory
    SYSi_3DonGPU = pagefun(@inv,SYS_3D_on_GPU);
    % gather the inverse back on CPU
    temp3 = gather(SYSi_3DonGPU);
    sys_inv_3D(:,:,(in-1)*limit+1:in*limit) = temp3;
    d.AvailableMemory
end

rem = ttl_nr_el - limit * pckts

if (rem ~= 0)
temp2 = repmat(temp_sys,[1 1 rem]);
temp2(1:local_nr_ed,1:local_nr_ed,:) = M11(:,:,limit * pckts+1:end);
temp_g = gpuArray(temp2);
temp_ig = pagefun(@inv,temp_g);
sys_inv_3D(:,:,limit * pckts+1:end) = gather(temp_ig);
rem
end
% assemble into sparse 2D system
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
A_inv = AssembleMatrices2(GM_local_dof',GM_local_dof',sys_inv_3D);

%% calculate connectivity matrix

N = full(incidence_matrix.OutwardNormalMatrix(p));
N_3D = repmat(N,[1 1 K^2]);

GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof_q = GM_local_dof(:,1:local_nr_ed);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];
ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);

%% calculate reduction of RHS (source term - f)

volForm = twoForm_v2(mesh1);

F_3D = volForm.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + local_nr_ed;
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

%% evaluate lambda

lambda = (ass_conn3 * A_inv * ass_conn3')\(ass_conn3 * A_inv * RHS);

%% seperating lambda for each element 

lambda2 = [lambda; zeros(4*K*p,1)];

for eln = 1:ttl_nr_el
    lambda_h(:,eln) = lambda2(GM_lambda(:,eln));
end

%% 

X_h3 = cell(1,ttl_nr_el);

for eln = 1:ttl_nr_el
    X_h3(eln) = {sys_inv_3D(:,:,eln) * ([zeros(local_nr_ed,1); F_3D(:,eln)] - [N'*lambda_h(:,eln); zeros(local_nr_pp,1)])};
end

%% separating cochains 

for eln = 1:ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:2*p*(p+1));
    p_h(:,eln) = X_h3{eln}(2*p*(p+1) + 1:2*p*(p+1)+ p^2);
end

%% dual to primal dof

M22s = zeros(p^2,p^2,ttl_nr_el);

tic
for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end
toc

%% post processing 

pf = 20;

mesh1.eval_pf_der(pf);

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

%% conservation of mass

for el = 1:K^2
    divQ(:,el) = E211 * q_h(:,el);
end

Err_mass = divQ - F_3D;
[xf_3D, yf_3D, massf_h] = volForm.reconstruction(Err_mass);
massf_h = permute(massf_h, [2 1 3]); % transpose along dim = 3.

mass_ex = zeros(size(xf_3D));
[error.mass] = error_processor_v3(mass_ex, massf_h, wfxy2, mesh1.eval.pf.ggg);

%% reconstruction pressure

p_h_2 = zeros(size(p_h));

for eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

[xf_3D, yf_3D, pf_h] = volForm.reconstruction(p_h_2);
pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

pp_ex = phi_an(xf_3D, yf_3D);
[error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln))
% end
% colorbar

%% reconstruction flux

surForm = oneForm_v2(mesh1);

rec = surForm.reconstruction(q_h);

rec_qx = rec.qx;
rec_qy = rec.qy;

qx_ex = u_an(xf_3D, yf_3D);
qy_ex = v_an(xf_3D, yf_3D);

[error.qx] = error_processor_v3(qx_ex, -rec_qy, wfxy2, mesh1.eval.pf.ggg);
[error.qy] = error_processor_v3(qy_ex, rec_qx, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qx(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qy(:,:,eln))
% end
% colorbar

%% reconstruction div Q

[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(divQ);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

ff_ex = f_an(xf_3D, yf_3D);
[error.ff] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

%% Hdiv error

err_divQ = error.qx.sqre + error.qy.sqre + error.ff.sqre;
err_divQ = sqrt(err_divQ)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), ff_ex(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h(:,:,eln))
% end
% colorbar

%% H10 error 

% evaluate discrete grad p

% parfor el = 1:K^2
%     u_h(:,el) = E211' * p_h(:,el) + N' * lm_h(:,el);
% end
% 
% parfor el = 1:K^2
%     u_h_2(:,el) = M11(:,:,el)\u_h(:,el);
% end
% 
% gradp = surForm.reconstruction(u_h_2);
% 
% [error.gradp.x] = error_processor_v3(qx_ex, gradp.qx, wfxy2, mesh1.eval.pf.ggg);
% [error.gradp.y] = error_processor_v3(qy_ex, gradp.qy, wfxy2, mesh1.eval.pf.ggg);
% 
% err_H1p = err_H1(error.pp.sqre, error.gradp.x.sqre, error.gradp.y.sqre)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_x(:,:,eln))
% end
% colorbar

% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_y(:,:,eln))
% end
% colorbar