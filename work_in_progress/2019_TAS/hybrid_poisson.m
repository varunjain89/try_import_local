function [ X_h ] = hybrid_poisson(K, p)
%HYBRID_DARCY_SOL Summary of this function goes here
%   Detailed explanation goes here

%% case 2

% phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

a = 0.1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
% k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

% q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
% q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%%

xbound = [0 1];
ybound = [0 1];
c = 0.3;

ttl_nr_el = K^2;
% ttl_nr_pp = K^2*p^2;
% ttl_nr_ed = 2*K*p*(K*p + 1);

local_nr_ed = 2*p*(p+1);
% local_nr_pp = p^2;

%% domain / mesh mapping and derivatives

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

mesh1.eval_p_der();

%% LHS systems

mesh1.eval_dom_xy();

eval.p.dx_dxii = mesh1.eval.p.dx_dxii;
eval.p.dx_deta = mesh1.eval.p.dx_deta;
eval.p.dy_dxii = mesh1.eval.p.dy_dxii;
eval.p.dy_deta = mesh1.eval.p.dy_deta;

eval.K.k11 = k11_an(mesh1.eval.x, mesh1.eval.y);
eval.K.k12 = k12_an(mesh1.eval.x, mesh1.eval.y);
eval.K.k22 = k22_an(mesh1.eval.x, mesh1.eval.y);
eval_detK = eval.K.k11.*eval.K.k22 - eval.K.k12.^2;

eval_p_ggg = metric.ggg_v2(eval.p);
eval.p.g11 = flow.darcy.g11K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g12 = flow.darcy.g12K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g22 = flow.darcy.g22K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);

M11 = zeros(local_nr_ed, local_nr_ed, ttl_nr_el);

E211 = incidence_matrix.E21(p);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v4(p, eval.p, eln);
end

%%

ttl_loc_dof = 2*p*(p+1)+p^2;

GM_local_dof = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);

E211_2  = repmat(full(E211),[1 1 K^2]);

M11    = AssembleMatrices2_dd(GM_local_dof_q', GM_local_dof_q', M11);
E211_2 = AssembleMatrices2_dd(GM_local_dof_p', GM_local_dof_q', E211_2);

i1 = M11.rr;
j1 = M11.cc;
s1 = M11.data;

i2 = E211_2.rr;
j2 = E211_2.cc;
s2 = E211_2.data;

% this is transpose of assembled E21. I have simply swapped the x and y
% axis.

i3 = j2;
j3 = i2;
s3 = s2;

N = full(incidence_matrix.OutwardNormalMatrix(p));
N_3D = repmat(N,[1 1 K^2]);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
ass_conn = AssembleMatrices2_dd(GM_lambda, GM_local_dof_q', N_3D);

% [i4, j4, s4] = find(ass_conn);
i4 = ass_conn.rr;
j4 = ass_conn.cc;
s4 = ass_conn.data;

i4 = ttl_loc_dof*ttl_nr_el + i4;

% this is transpose of connectivity matrix. I have simply swapped the x
% and y coordinates. 

i5 = j4;
j5 = i4;
s5 = s4;

ii = [i1;i2;i3;i4;i5];
jj = [j1;j2;j3;j4;j5];
ss = [s1;s2;s3;s4;s5];

LHS = sparse(ii, jj, ss, ttl_loc_dof*ttl_nr_el + 2*K*p*(K+1), ttl_loc_dof*ttl_nr_el + 2*K*p*(K+1));

%% RHS

ff = twoForm();
ff.K = K;
ff.p = p;
ff.domain = mesh1.domain;

ff.el_bounds_x = mesh1.el.bounds.x;
ff.el_bounds_y = mesh1.el.bounds.y;

F_3D = ff.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + 2*p*(p+1);
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

RHS_con = zeros(2*K*p*(K+1),1);

RHS2 = [RHS; RHS_con];

%% implementing Dirichlet BC's

LHS2 = LHS(1:end-4*K*p,1:end-4*K*p);
RHS3 = RHS2(1:end-4*K*p,1);

%% solution

tic
X_h = LHS2\RHS3;
toc

end

