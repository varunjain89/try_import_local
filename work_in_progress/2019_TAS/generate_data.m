clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('../continuous_darcy')

%% generate h-convergence data

load('X_h_Poisson.mat')
load('lambda_h_Poisson.mat')

nh = 100;
% nh = 4;
cc = 0.0;

for nK = 32:4:nh
%     fname_p7 = sprintf('K_%d', nK);
%     [X_h.h.c00.p7.(genvarname([fname_p7])), lambda_h.h.c00.p7.(genvarname([fname_p7]))] = hybrid_darcy_v14_TAS_dual(nK,7,cc);

%     fname_p11 = sprintf('K_%d', nK);
%     [X_h.h.c00.p11.(genvarname([fname_p11])), lambda_h.h.c00.p11.(genvarname([fname_p11]))] = hybrid_darcy_v14_TAS_dual(nK,11,cc);
%     
    fname_p15 = sprintf('K_%d', nK);
    [X_h.h.c00.p15.(genvarname([fname_p15])), lambda_h.h.c00.p15.(genvarname([fname_p15]))] = hybrid_darcy_v14_TAS_dual(nK,15,cc);

    fname = sprintf('X_h_Poisson.mat');
    save(fname,'X_h');
    
    fname = sprintf('lambda_h_Poisson.mat');
    save(fname,'lambda_h');
    
    nK
end

%% generate p-convergence data 

% nn = 30;
% nn = 1;
% 
% for np = 1:nn
%     fname_K1 = sprintf('N_%d', np);
%     X_h.p.c03.K1.(genvarname([fname_K1])) = hybrid_darcy_sol_v3(1,np);
%     
%     fname_K5 = sprintf('N_%d', np);
%     X_h.p.c03.K5.(genvarname([fname_K5])) = hybrid_darcy_sol_v3(5,np);
%     
%     fname_K9 = sprintf('N_%d', np);
%     X_h.p.c03.K9.(genvarname([fname_K9])) = hybrid_darcy_sol_v3(9,np);
%     
% %     fname = sprintf('X_h_darcy_v4.mat');
% %     save(fname,'X_h');
%     np
% end

%% 

% load('X_h_darcy_v2.mat')
% 
% X_h2.h = X_h.h;
% 
% load('X_h_darcy_v3.mat')
% 
% X_h2.p = X_h.p;

% save('X_h_darcy_v4.mat','X_h2');

% load('X_h_darcy_v4.mat')

%% processing h-convergence data 

% nh2 = 100;
% 
% for nK = 4:2:nh2
%     for np = 1:3:7
%         K = nK;
%         p = np;
%         c = 0.0;
%         
%         xbound = [0 1];
%         ybound = [0 1];
%         
%         phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
%         
%         mesh1 = CrazyMesh(K,p,xbound,ybound,c);
%         mesh1.eval_p_der();
%         
%         el.bounds.x = linspace(-1,1,K+1);
%         el.bounds.y = linspace(-1,1,K+1);
%         
%         vname_K = sprintf('K_%d', K);
%         vname_p = sprintf('p%d', p);
%         
%         i = (np-1)/3 +1;
%         j = nK/2 - 1;
%         
%         error = hybrid_darcy_post_v3(K, p, X_h.h.c00.(genvarname([vname_p])).(genvarname([vname_K])), phi_an, mesh1, el.bounds);
%         
%         err_pp(i,j) = error.pp;
%         err_qx(i,j) = error.qx;
%         err_qy(i,j) = error.qy;
%         err_qH(i,j) = error.qH;
%         err_pH(i,j) = error.pH;
%         err_divQ(i,j) = error.divQ;
%         
%         nK
%     end
% end

%% save h - convergence data 

% hK = 1 ./(4:2:nh2);
% 
% % load('error_plots_data.mat')
% 
% err.h.c00.err_pp = err_pp;
% err.h.c00.err_qx = err_qx;
% err.h.c00.err_qy = err_qy;
% err.h.c00.err_qH = err_qH;
% err.h.c00.err_pH = err_pH;
% err.h.c00.err_divQ = err_divQ;
% err.h.c00.hK = hK;

% save('error_plots_data2.mat','err');

%% processing h-convergence data, c = 0.3

% nh2 = 4;
% nh2 = 100;
% 
% hK = 1 ./(4:2:nh2);
% 
% for nK = 4:2:nh2
%     for np = 1:3:7
%         K = nK;
%         p = np;
%         c = 0.3;
%         
%         xbound = [0 1];
%         ybound = [0 1];
%         
%         phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
%         
%         mesh1 = CrazyMesh(K,p,xbound,ybound,c);
%         mesh1.eval_p_der();
%         
%         el.bounds.x = linspace(-1,1,K+1);
%         el.bounds.y = linspace(-1,1,K+1);
%         
%         vname_c = sprintf('c%.1f', c);
%         vname_K = sprintf('K_%d', K);
%         vname_p = sprintf('p%d', p);
%         
%         error = hybrid_darcy_post_v3(K, p, X_h.h.c03.(genvarname([vname_p])).(genvarname([vname_K])), phi_an, mesh1, el.bounds);
%         
%         i = (np-1)/3 +1;
%         j = nK/2 - 1;
%         
%         err_pp(i,j) = error.pp;
%         err_qx(i,j) = error.qx;
%         err_qy(i,j) = error.qy;
%         err_qH(i,j) = error.qH;
%         err_pH(i,j) = error.pH;
%         err_divQ(i,j) = error.divQ;
%         
%         nK
%     end
% end
% 
%% save h - convergence data, c = 0.3
% 
% % load('error_plots_data.mat')
% 
% err.h.c03.err_pp = err_pp;
% err.h.c03.err_qx = err_qx;
% err.h.c03.err_qy = err_qy;
% err.h.c03.err_qH = err_qH;
% err.h.c03.err_pH = err_pH;
% err.h.c03.err_divQ = err_divQ;
% err.h.c03.hK = hK;
% 
% save('error_plots_data2.mat','err');

%% processing p-convergence data, c = 0.0

% nh2 = 4;
% 
% np2 = 27;
% 
% for nK = 1:4:9
%     for np = 1:np2
%         K = nK;
%         p = np;
%         c = 0.0;
%         
%         xbound = [0 1];
%         ybound = [0 1];
%         
%         phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
%         
%         mesh1 = CrazyMesh(K,p,xbound,ybound,c);
%         mesh1.eval_p_der();
%         
%         el.bounds.x = linspace(-1,1,K+1);
%         el.bounds.y = linspace(-1,1,K+1);
%         
%         vname_K = sprintf('K%d', K);
%         vname_p = sprintf('N_%d', p);
%         
%         i = (nK-1)/4 +1;
%         j = np;
%         
%         error = hybrid_darcy_post_v3(K, p, X_h.p.c00.(genvarname([vname_K])).(genvarname([vname_p])), phi_an, mesh1, el.bounds);
%         
%         err_pp(i,j) = error.pp;
%         err_qx(i,j) = error.qx;
%         err_qy(i,j) = error.qy;
%         err_qH(i,j) = error.qH;
%         err_pH(i,j) = error.pH;
%         err_divQ(i,j) = error.divQ;
%         
%         np
%     end
% end
% 
%% save p - convergence data, c = 0.0
% 
% load('error_plots_data.mat')
% 
% err.p.c00.err_pp = err_pp;
% err.p.c00.err_qx = err_qx;
% err.p.c00.err_qy = err_qy;
% err.p.c00.err_qH = err_qH;
% err.p.c00.err_pH = err_pH;
% err.p.c00.err_divQ = err_divQ;
% err.p.c00.np = 1:np;
% 
% save('error_plots_data2.mat','err');
% 
%% processing p-convergence data, c = 0.3
% 
% nh2 = 4;
% 
% np2 = 24;
% 
% for nK = 1:4:9
%     for np = 1:np2
%         K = nK;
%         p = np;
%         c = 0.3;
%         
%         xbound = [0 1];
%         ybound = [0 1];
%         
%         phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
%         
%         mesh1 = CrazyMesh(K,p,xbound,ybound,c);
%         mesh1.eval_p_der();
%         
%         el.bounds.x = linspace(-1,1,K+1);
%         el.bounds.y = linspace(-1,1,K+1);
%         
%         vname_K = sprintf('K%d', K);
%         vname_p = sprintf('N_%d', p);
%         
%         i = (nK-1)/4 +1;
%         j = np;
%         
%         error = hybrid_darcy_post_v3(K, p, X_h.p.c03.(genvarname([vname_K])).(genvarname([vname_p])), phi_an, mesh1, el.bounds);
%         
%         err_pp(i,j) = error.pp;
%         err_qx(i,j) = error.qx;
%         err_qy(i,j) = error.qy;
%         err_qH(i,j) = error.qH;
%         err_pH(i,j) = error.pH;
%         err_divQ(i,j) = error.divQ;
%         
%         np
%     end
% end
% 
% %% save p - convergence data, c = 0.3
% % 
% load('error_plots_data2.mat')
% % 
% % err.p.c03.err_pp = err_pp;
% % err.p.c03.err_qx = err_qx;
% % err.p.c03.err_qy = err_qy;
% % err.p.c03.err_qH = err_qH;
% % err.p.c03.err_pH = err_pH;
% % err.p.c03.err_divQ = err_divQ;
% % err.p.c03.np = 1:np;
% 
% % 
% % save('error_plots_data2.mat','err');
% 
% %%
% 
% % close all
% 
% % figure
% % loglog(err.h.hK(1:j),err_pH(1,:),'-o')
% % hold on
% % loglog(err.h.hK(1:j),err_pH(2,:),'-o')
% % loglog(err.h.hK(1:j),err_pH(3,:),'-o')
% % 
% % figure
% % loglog(err.h.hK(1:j),err_divQ(1,:),'-o')
% % hold on
% % loglog(err.h.hK(1:j),err_divQ(2,:),'-o')
% % loglog(err.h.hK(1:j),err_divQ(3,:),'-o')
% 
% 
% % load('error_plots_data.mat')
% 
% lw      = 2;  % this is line width for plots
% ms      = 8;   % this is marker size
% tsl     = 20;   % text size in legend
% 
% % slope1 = (log10(err.h.c00.err_pH(1,end-5))-log10(err.h.c00.err_pH(1,end)))/(log10(err.h.c00.hK(end-5))-log10(err.h.c00.hK(end)))
% % slope2 = (log10(err.h.c00.err_pH(2,end-5))-log10(err.h.c00.err_pH(2,end)))/(log10(err.h.c00.hK(end-5))-log10(err.h.c00.hK(end)))
% % slope3 = (log10(err.h.c00.err_pH(3,end-5-35))-log10(err.h.c00.err_pH(3,end-35)))/(log10(err.h.c00.hK(end-5-35))-log10(err.h.c00.hK(end-35)))
% %
% 
% x1 = err.h.c00.hK;
% y1 = err.h.c00.err_pp(2,:);
% slope1 = (log10(y1(end-5))-log10(y1(end)))/(log10(x1(end-5))-log10(x1(end)))
% 
% x2 = err.h.c03.hK;
% y2 = err.h.c03.err_pp(2,:);
% slope2 = (log10(y2(end-5))-log10(y2(end)))/(log10(x2(end-5))-log10(x2(end)))
% 
% x3 = err.h.c00.hK;
% y3 = err.h.c00.err_pp(3,:);
% slope3 = (log10(y3(end-35-5))-log10(y3(end-35)))/(log10(x3(end-35-5))-log10(x3(end-35)))
% 
% x4 = err.h.c03.hK;
% y4 = err.h.c03.err_pp(3,:);
% slope4 = (log10(y4(end-5-15))-log10(y4(end-15)))/(log10(x4(end-5-15))-log10(x4(end-15)))
% 
% figure
% % loglog(err.h.c00.hK,err.h.c00.err_pH(1,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(err.h.c03.hK,err.h.c03.err_pH(1,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(x1,y1,'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% loglog(x2,y2,'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(x3,y3,'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% loglog(x4,y4,'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% 
% % plot([err.h.c00.hK(end-25) err.h.c00.hK(end-35)], [err.h.c00.err_pp(2,end-25)+0.7e-7 err.h.c00.err_pp(2,end-35)+0.5e-6],'k')
% % plot([err.h.c03.hK(end-25) err.h.c03.hK(end-35)], [err.h.c03.err_pp(2,end-25)+0.3e-5 err.h.c03.err_pp(2,end-35)+0.2e-4],'k')
% 
% 
% annotation('line',[0.355 0.458],[0.229 0.295]);
% annotation('line',[0.317 0.420],[0.371 0.437]);
% annotation('textbox',[0.34 0.60 0.04 0.05],'String',{'4'},'LineStyle','none');
% 
% 
% % title('l2 error in p');
% % 'N = 1, c = 0.0','N = 1, c = 0.3',
% hlegend = legend('N = 4, o','N = 4, c','N = 7, o','N = 7, c','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$h$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('$\Vert p^h - p_{ex} \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% xlim([1e-2 3e-1])
% ylim([1e-15 5e+1])
% 
% % export_fig('L2_err_h_phi.pdf','-pdf','-r864','-painters','-transparent');
% 
% % figure
% % loglog(err.h.hK,err.h.err_qx(1,:),'-o')
% % hold on
% % loglog(err.h.hK,err.h.err_qx(2,:),'-o')
% % loglog(err.h.hK,err.h.err_qx(3,:),'-o')
% % 
% % loglog(err.h.c03.hK,err.h.c03.err_qx(1,:),'-d')
% % loglog(err.h.c03.hK,err.h.c03.err_qx(2,:),'-d')
% % loglog(err.h.c03.hK,err.h.c03.err_qx(3,:),'-d')
% % 
% % title('l2 error in qx');
% 
% % figure
% % loglog(err.h.hK,err.h.err_qy(1,:),'-o')
% % hold on
% % loglog(err.h.hK,err.h.err_qy(2,:),'-o')
% % loglog(err.h.hK,err.h.err_qy(3,:),'-o')
% % 
% % loglog(err.h.c03.hK,err.h.c03.err_qy(1,:),'-d')
% % loglog(err.h.c03.hK,err.h.c03.err_qy(2,:),'-d')
% % loglog(err.h.c03.hK,err.h.c03.err_qy(3,:),'-d')
% % 
% % title('l2 error in qy');
% 
% %% 
% 
% figure
% % title('Hdiv  error in q');
% % loglog(err.h.c00.hK,err.h.c00.err_qH(1,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(err.h.c03.hK,err.h.c03.err_qH(1,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(err.h.c00.hK,err.h.c00.err_qH(2,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% loglog(err.h.c03.hK,err.h.c03.err_qH(2,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(err.h.c00.hK,err.h.c00.err_qH(3,:),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% loglog(err.h.c03.hK,err.h.c03.err_qH(3,:),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% 
% hlegend = legend('N = 4, o','N = 4, c','N = 7, o','N = 7, c','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$h$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('$\Vert u^h - u_{ex} \Vert _{H(div; \Omega)}$', 'Interpreter','latex','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% xlim([1e-2 3e-1])
% ylim([1e-15 5e+1])
% 
% % export_fig('Hdiv_err_h_q.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% mass conservation 
% 
% figure
% % title('Hdiv  error in q');
% % loglog(err.h.c00.hK,err.h.c00.err_divQ(1,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(err.h.c03.hK,err.h.c03.err_divQ(1,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(err.h.c00.hK,err.h.c00.err_divQ(2,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% loglog(err.h.c03.hK,err.h.c03.err_divQ(2,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(err.h.c00.hK,err.h.c00.err_divQ(3,:),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% loglog(err.h.c03.hK,err.h.c03.err_divQ(3,:),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% 
% hlegend = legend('N = 4, o','N = 4, c','N = 7, o','N = 7, c','location','southwest');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$h$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('$\Vert \nabla \cdot u^h - f^h \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% xlim([1e-2 3e-1])
% % ylim([1e-15 5e+1])
% 
% % export_fig('l2_err_h_divQ.pdf','-pdf','-r864','-painters','-transparent');
% 
% %%
% 
% figure
% % semilogy(1:24,err.p.c00.err_pH(1,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % semilogy(1:24,err.p.c03.err_pH(1,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,err.p.c00.err_pp(2,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% semilogy(1:24,err.p.c03.err_pp(2,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,err.p.c00.err_pp(3,1:24),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% semilogy(1:24,err.p.c03.err_pp(3,1:24),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% 
% % title('l2 error in p');
% % 'K = 1, c = 0.0','K = 1, c = 0.3',
% hlegend = legend('K = 5, o','K = 5, c','K = 9, o','K = 9, c','location','northeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('$\Vert p^h - p_{ex} \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% % xlim([1e-2 3e-1])
% ylim([1e-14 1e+2])
% 
% % export_fig('L2_err_N_phi.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% 
% 
% figure
% % semilogy(1:24,err.p.c00.err_qH(1,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % semilogy(1:24,err.p.c03.err_qH(1,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,err.p.c00.err_qH(2,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% semilogy(1:24,err.p.c03.err_qH(2,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,err.p.c00.err_qH(3,1:24),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% semilogy(1:24,err.p.c03.err_qH(3,1:24),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% 
% % title('Hdiv  error in q');
% 
% hlegend = legend('K = 5, o','K = 5, c','K = 9, o','K = 9, c','location','northeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('$\Vert u^h - u_{ex} \Vert _{H(div;\ \Omega)}$', 'Interpreter','latex','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% % xlim([1e-2 3e-1])
% ylim([1e-14 1e+2])
% 
% % export_fig('Hdiv_err_N_q.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% mass conservation
% 
% figure
% % semilogy(1:24,err.p.c00.err_divQ(1,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % semilogy(1:24,err.p.c03.err_divQ(1,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,err.p.c00.err_divQ(2,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% semilogy(1:24,err.p.c03.err_divQ(2,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,err.p.c00.err_divQ(3,1:24),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% semilogy(1:24,err.p.c03.err_divQ(3,1:24),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% 
% % title('Hdiv  error in q');
% 
% hlegend = legend('K = 5, o','K = 5, c','K = 9, o','K = 9, c','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('$$\Vert \nabla \cdot u^h - f^h \Vert _{L^2(\Omega)}$$', 'Interpreter','latex','FontSize',tsl);
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% 
% % xlim([1e-2 3e-1])
% ylim([1e-15 3e-13])
% 
% % export_fig('l2_err_N_divQ.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% total degrees of freedom 
% 
% total_dof_hbrd = @(K,p) K^2*(2*p*(p+1) + p^2) + 2*K*p*(K-1);
% total_dof_cont = @(K,p) 2*K*p*(K*p+1) + K^2*p^2;
% 
% for nK = 4:2:100
%     for np = 1:3:7
%         i = (np-1)/3 +1;
%         j = nK/2 - 1;
%         dof.h.hbrd(i,j) = total_dof_hbrd(nK,np);
%         dof.h.cont(i,j) = total_dof_cont(nK,np);
%     end
% end
% 
% for nK = 1:4:9
%     for np = 1:25
%         i = (nK-1)/4 +1;
%         j = np;
%         dof.p.hbrd(i,j) = total_dof_hbrd(nK,np);
%         dof.p.cont(i,j) = total_dof_cont(nK,np);
%     end
% end
% 
% figure
% % title('Hdiv  error in q');
% % loglog(err.h.c00.hK,err.h.c00.err_divQ(1,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(err.h.c03.hK,err.h.c03.err_divQ(1,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% loglog(err.h.c00.hK(:),dof.h.hbrd(2,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% loglog(err.h.c03.hK(:),dof.h.cont(2,:),'--r','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% loglog(err.h.c00.hK(:),dof.h.hbrd(3,:),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% loglog(err.h.c03.hK(:),dof.h.cont(3,:),'--k','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% hlegend = legend('N = 4, hybrid','N = 4, continuous','N = 7, hybrid','N = 7, continuous','location','northeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$h$$', 'Interpreter','latex','FontSize',tsl);
% % ylabel('$$\text{Degrees}$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('Degrees of freedom','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% % xlim([1e-2 3e-1])
% % ylim([1e-15 5e+1])
% 
% % export_fig('ndof_h.pdf','-pdf','-r864','-painters','-transparent');
% 
% figure
% % semilogy(1:24,err.p.c00.err_divQ(1,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % semilogy(1:24,err.p.c03.err_divQ(1,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% semilogy(1:24,dof.p.hbrd(2,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% hold on
% semilogy(1:24,dof.p.cont(2,1:24),'--r','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% semilogy(1:24,dof.p.hbrd(3,1:24),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% semilogy(1:24,dof.p.cont(3,1:24),'--k','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% % title('Hdiv  error in q');
% 
% hlegend = legend('K = 5, hybrid','K = 5, continuous','K = 9, hyrid','K = 9, continuous','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',tsl);
% % ylabel('$$\mathsf{Degrees\ of\ freedom}$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('Degrees of freedom','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% % xlim([1e-2 3e-1])
% % ylim([1e-15 3e-13])
% 
% % export_fig('ndof_N.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% condition number 
% 
% % for nK = 4:2:100
% %     for np = 1:3:7
% %         i = (np-1)/3 +1;
% %         j = nK/2 - 1;
% %         tic
% %         cond.h.c00.hbrd(i,j) = hybrid_darcy_sol_cond_c00(nK,np);
% %         cond.h.c03.hbrd(i,j) = hybrid_darcy_sol_cond_c03(nK,np);
% %         toc
% % %         cond.h.cont(i,j) = total_dof_cont(nK,np);
% %         nK
% % %         save('condition_number.mat','cond');
% %     end
% % end
% 
% % for continuous
% load('condition_number2.mat')
% 
% % for nK = 4:2:50
% %     for np = 1:3:7
% %         i = (np-1)/3 +1;
% %         j = nK/2 - 1;
% %         tic
% %         cond.h.c00.cont(i,j) = cont_darcy_cond_c00(nK,np);
% %         cond.h.c03.cont(i,j) = cont_darcy_cond_c03(nK,np);
% %         toc
% % %         cond.h.cont(i,j) = total_dof_cont(nK,np);
% %         nK
% %         save('condition_number2.mat','cond');
% %     end
% % end
% 
% % save('condition_number.mat','cond');
% 
% % load('condition_number.mat')
% 
% % for nK = 1:4:9
% %     for np = 1:25
% %         i = (nK-1)/4 +1;
% %         j = np;
% %         tic
% %         cond.p.c00.hbrd(i,j) = hybrid_darcy_sol_cond_c00(nK,np);
% %         cond.p.c03.hbrd(i,j) = hybrid_darcy_sol_cond_c03(nK,np);
% %         toc
% % %         cond.p.cont(i,j) = total_dof_cont(nK,np);
% %         np
% % %         save('condition_number.mat','cond');
% %     end
% % end
% 
% % for continuous
% 
% % for nK = 1:4:9
% %     for np = 1:25
% %         i = (nK-1)/4 +1;
% %         j = np;
% %         tic
% %         cond.p.c00.cont(i,j) = cont_darcy_cond_c00(nK,np);
% %         cond.p.c03.cont(i,j) = cont_darcy_cond_c03(nK,np);
% %         toc
% % %         cond.p.cont(i,j) = total_dof_cont(nK,np);
% %         np
% %         save('condition_number2.mat','cond');
% %     end
% % end
% 
% % save('condition_number.mat','cond');
% 
% %% condition number for schuur compliment system
% 
% % for nK = 4:2:4
% %     for np = 7
% %         i = (np-1)/3 +1;
% %         j = nK/2 - 1;
% %         tic
% %         cond.h.c03.cont(i,j) = schuur_darcy_sol_cond_c03(nK,np);
% %         toc
% %         nK
% % %         save('condition_number2.mat','cond');
% %     end
% % end
% % 
% % for nK = 9
% %     for np = 1:25
% %         i = (nK-1)/4 +1;
% %         j = np;
% %         tic
% %         cond.p.c03.cont(i,j) = cont_darcy_cond_c03(nK,np);
% %         toc
% %         np
% % %         save('condition_number2.mat','cond');
% %     end
% % end
% 
% % save('condition_number.mat','cond');
% 
% %% condition number
% 
% figure
% % title('Hdiv  error in q');
% % loglog(err.h.c00.hK,err.h.c00.err_divQ(1,:),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(err.h.c03.hK,err.h.c03.err_divQ(1,:),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% % loglog(err.h.c00.hK(1:24),cond.h.c00.hbrd(2,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(err.h.c00.hK(1:24),cond.h.c00.cont(2,1:24),'--ro','MarkerSize',ms-2,'LineWidth',lw-1)
% % 
% % loglog(err.h.c03.hK(1:24),cond.h.c03.hbrd(2,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % loglog(err.h.c03.hK(1:24),cond.h.c03.cont(2,1:24),'--rd','MarkerSize',ms-2,'LineWidth',lw-1)
% % 
% % loglog(err.h.c00.hK(1:24),cond.h.c00.hbrd(3,1:24),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% % loglog(err.h.c00.hK(1:24),cond.h.c00.cont(3,1:24),'--ko','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% loglog(err.h.c03.hK(1:24),cond.h.c03.hbrd(3,1:24),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% hold on
% loglog(err.h.c03.hK(1:24),cond.h.c03.cont(3,1:24),'--kd','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% hlegend = legend('N = 7, c = 0.3, hybrd','N = 7, c = 0.3, cont','location','northeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$h$$', 'Interpreter','latex','FontSize',tsl);
% % ylabel('$$\mathsf{Condition\ number}$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('Condition number','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% % xlim([1e-2 3e-1])
% % ylim([1e-15 5e+1])
% 
% % export_fig('cond_h.pdf','-pdf','-r864','-painters','-transparent');
% 
% figure
% % semilogy(1:24,err.p.c00.err_divQ(1,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % semilogy(1:24,err.p.c03.err_divQ(1,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% 
% % loglog(1:24,cond.p.c00.hbrd(2,1:24),'-ro', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % hold on
% % loglog(1:24,cond.p.c00.cont(2,1:24),'--ro','MarkerSize',ms-2,'LineWidth',lw)
% 
% % loglog(1:24,cond.p.c03.hbrd(2,1:24),'-rd', 'MarkerFaceColor','r','MarkerSize',ms,'LineWidth',lw)
% % loglog(1:24,cond.p.c03.cont(2,1:24),'--rd','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% % loglog(1:24,cond.p.c00.hbrd(3,1:24),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% % loglog(1:24,cond.p.c00.cont(3,1:24),'--ko','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% loglog(1:24,cond.p.c03.hbrd(3,1:24),'-kd', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
% hold on
% loglog(1:24,cond.p.c03.cont(3,1:24),'--kd','MarkerSize',ms-2,'LineWidth',lw-1)
% 
% % title('Hdiv  error in q');
% 
% hlegend = legend('K = 9, c, hyrbid','K = 9, c, cont','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',tsl);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',tsl);
% % ylabel('$$\mathsf{Condition\ number}$$', 'Interpreter','latex','FontSize',tsl);
% ylabel('Condition number','FontSize',tsl);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% 
% % xlim([1e-2 3e-1])
% % ylim([1e-15 3e-13])
% 
% % export_fig('cond_N.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% connectivity matrix
% 
% % K = 3;
% % p = 6;
% % 
% % N = full(incidence_matrix.OutwardNormalMatrix(p));
% % N_3D = repmat(N,[1 1 K^2]);
% % ttl_nr_el = K^2;
% % ttl_loc_dof = 2*p*(p+1)+p^2;
% % 
% % GM_local_dof = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
% % GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
% % GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
% % ass_conn = AssembleMatrices2_dd(GM_lambda, GM_local_dof_q', N_3D);