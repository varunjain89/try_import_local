%% ---- test case flying carpet ----

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('Salil_mapping')

%%

% ff = @(x,y) (1-x.^2) .* (1-y.^2);

% ux = @(x,y) -2*x;
% uy = @(x,y) 0*ones(size(x));

% ux = @(x,y) 0*ones(size(x));
% uy = @(x,y) -2*y;
%
% ux = @(x,y) -2*x .* (1-y.^2)/4;
% uy = @(x,y) -2*y .* (1-x.^2)/8;

%%

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);

bc_left = @(x,y) phi_an(x,y);
bc_rght = @(x,y) phi_an(x,y);
bc_topp = @(x,y) phi_an(x,y);
bc_bott = @(x,y) phi_an(x,y);

% ff = @(x,y) 0*ones(size(x));
% bc_left = @(x,y) 1*ones(size(x));
% bc_rght = @(x,y) 0*ones(size(x));
% bc_topp = @(x,y) 0*ones(size(x));
% bc_bott = @(x,y) 0*ones(size(x));

%% define domain mapping and jacobians

% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.1;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%%

%% test 6

load ('p_PARSEC_SD7003.mat');

tt1 = get_y_parsec(p, 0.2, 'up', 0);
tt2 = get_y_parsec(p, 0.8, 'up', 0);

F_L_x = @(s,t) t*(-2.5 - 0.25) + 0.25 ;
F_L_y = @(s,t) t*(1.75 - tt1) + tt1;

F_B_x = @(s,t) s*(0.8 - 0.2) +0.2;
% F_B_y = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 0);
F_B_y = @(s,t) s*(tt2 - tt1) + tt1;

tt2 = get_y_parsec(p, 0.8, 'up', 0);

F_R_x = @(s,t) t*(4-0.8) + 0.8;
F_R_y = @(s,t) t*(1.75 - tt2) + tt2';

F_T_x = @(s,t) s*(4 + 2.5) - 2.5;  
F_T_y = @(s,t) 1.75;
 
dFLx_dt = @(s,t) -(0.25 + 2.5);
dFLy_dt = @(s,t) (1.75 - tt1);

dFRx_dt = @(s,t) (4-0.8);
dFRy_dt = @(s,t) (1.75 - tt2);

dFTx_ds = @(s,t) (4 + 2.5);
dFTy_ds = @(s,t) 0;

dFBx_ds = @(s,t) (0.8 - 0.2);
dFBy_ds = @(s,t) (tt2 - tt1);

domain.mapping = @(xii,eta) mesh.transfinite_mesh.mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
domain.dX_dxii = @(xii,eta) mesh.transfinite_mesh.dX_dxii(xii,eta,F_L_x,F_R_x,dFBx_ds,dFTx_ds);
domain.dX_deta = @(xii,eta) mesh.transfinite_mesh.dX_deta(xii,eta,F_B_x,F_T_x,dFLx_dt,dFRx_dt);
domain.dY_dxii = @(xii,eta) mesh.transfinite_mesh.dY_dxii(xii,eta,F_L_y,F_R_y,dFBy_ds,dFTy_ds);
domain.dY_deta = @(xii,eta) mesh.transfinite_mesh.dY_deta(xii,eta,F_B_y,F_T_y,dFLy_dt,dFRy_dt);

%%

% patch = 10
% 
% domain.mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
% domain.dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
% domain.dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
% domain.dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
% domain.dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);
    
%%

mesh1 = mmesh2(domain);

% mapping2 = @mesh.crazy_mesh.mapping;

%% check domain mapping

% mesh1.check_domain_mapping();

%% discretize the domain

K = 10;
Kx = K;
Ky = K;

p = 10;

mesh1.discretize(Kx, Ky);

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

%% check element mappings

% mesh1.check_element_mapping();

%% reconstruction parameters

pf = p + 5;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[rec_xii, rec_eta] = meshgrid(sx, sy);

jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

%% check mesh quality

plot_inv_g = 1./metric_rec.ggg;

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), plot_inv_g(:,:,eln),'linecolor','none')
end

colorbar
title('jacobian determinant')
set(gcf, 'Position',  [1300, 500, 550, 400])

%% discretization parameters

[basis_dis.xp, basis_dis.wp] = Gnodes2(p+1);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh1.evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

%% check reduction >>> reconstruction 2 forms

% ------- reduction -------

dof_f = zeros(loc_dof_p, mesh1.ttl_nr_el);

parfor eln = 1:mesh1.ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh1.el(eln).mapping, basis_dis, M22);
end

% ------- reconstruction -------

pf = 30;

% sx = linspace(-1,1,pf+1);
% sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

% [rec_xii, rec_eta] = meshgrid(sx, sy);
% jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
% metric_rec = flow.poisson.eval_metric(jac_rec);

for eln = 1:mesh1.ttl_nr_el
    
    rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
    
    ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
    
end

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'LineColor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'LineColor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'LineColor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])

%% check reduction >>> reconstruction 1 forms

% % ------- reduction -------
% 
% [xp, wp] = Gnodes2(p+1);
% [xii, eta] = meshgrid(xp);
% 
% jac1_dis = mesh1.evaluate_jacobian_all(xii, eta);
% metric_dis = flow.poisson.eval_metric(jac1_dis);
% 
% % eln = 1;
% 
% [basis_dis.hp, basis_dis.ep] = MimeticpolyVal(xp,p,1);
% basis_dis.xp = xp;
% basis_dis.wp = wp;
% 
% parfor eln = 1:mesh1.ttl_nr_el
%     M11 = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), -metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
%     dof_u(:,eln) = reduction.reduction_1form_v4(ux, uy, mesh1.el(eln).mapping, basis_dis, M11, jac1_dis,eln);
% end
% 
% % ------- reconstruction -------
% 
% pf = 30;
% 
% sx = linspace(-1,1,pf+1);
% sy = linspace(-1,1,pf+1);
% 
% [basis_rec.hfx, basis_rec.efx] = MimeticpolyVal(sx,p,1);
% [basis_rec.hfy, basis_rec.efy] = MimeticpolyVal(sy,p,1);
% 
% [rec_xii, rec_eta] = meshgrid(sx, sy);
% jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
% metric_rec = flow.poisson.eval_metric(jac_rec);
% 
% for eln = 1:mesh1.ttl_nr_el
%     temp2x(:,:,eln) = reconstruction.reconstruct1xform_v7(dof_u(1:p*(p+1),eln), dof_u(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln, basis_rec);
%     temp2y(:,:,eln) = reconstruction.reconstruct1yform_v7(dof_u(1:p*(p+1),eln), dof_u(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln);
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
% end
% 
% ux_ex = ux(rec_x,rec_y);
% uy_ex = uy(rec_x,rec_y);
% 
% %%
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln),'LineColor','none')
% end
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2x(:,:,eln)','LineColor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [0700, 500, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln) - temp2x(:,:,eln)','LineColor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
% 
% %%
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln),'LineColor','none')
% end
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 050, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2y(:,:,eln)','LineColor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [0700, 050, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln) - temp2y(:,:,eln)','LineColor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 050, 550, 400])

%% check manufactured solution

% ------- make connectivity matrix -------

GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh1.ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 mesh1.ttl_nr_el]);
con_E = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
con_E = [con_E zeros(2*K*p*(K+1),loc_dof_p)];

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A = zeros(ttl_loc_dof,ttl_loc_dof,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    A(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    inv_A(:,:,eln) = A(:,:,eln)\I;
end

sys_3D_inv(mesh1.ttl_nr_el) = {inv_A(:,:,mesh1.ttl_nr_el)};

for eln = 1:mesh1.ttl_nr_el
    sys_3D_inv(eln) = {inv_A(:,:,eln)};
end

% inv_A = blkdiag(sys_3D_inv{:});
inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);

% ------- evaluate RHS -------

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

%% boundary condition - Neumann

R_Neumann = zeros(2*K*p*(K+1),1);

% ------------- boundary condition left ----------------

% lambda_left = [];
% u_left_dof = zeros(Kx*p,1);
% R_Neumann(lambda_left) = u_left_dof;

% ------------- boundary condition rght ----------------

% lambda_rght = [];
% u_rght_dof = zeros(Kx*p,1);
% R_Neumann(lambda_rght) = u_rght_dof;

% ------------- boundary condition bott ----------------

% for eln = 1:Kx
%     eln2 = (eln-1)*Ky + 1;
%     for i=1:p
%         lambda = i;
%         lambda_bott(i,eln) = GM_lambda(lambda,eln2);
%     end
% end
% 
% lambda_bott = lambda_bott(:);
% 
% u_bott_dof = zeros(Kx*p,1);
% R_Neumann(lambda_bott) = u_bott_dof;
% 
% con_E(lambda_bott,:) = zeros(size(lambda_bott,1),size(con_E,2));
% con_E(lambda_bott,lambda_bott) = eye(size(lambda_bott,1));

% ------------- boundary condition topp ----------------

% for eln = 1:Kx
%     eln2 = eln*Kx;
%     for i=1:p
%         lambda = 3*p + i;
%         lambda_topp(i,eln) = GM_lambda(lambda,eln2);
%     end
% end
% 
% lambda_topp = lambda_topp(:);
% 
% u_topp_dof = zeros(Kx*p,1);
% R_Neumann(lambda_topp) = u_topp_dof;
% 
% con_E(lambda_topp,:) = zeros(size(lambda_topp,1),size(con_E,2));
% con_E(lambda_topp,lambda_topp) = eye(size(lambda_topp,1));

%% boundary condition - Dirichlet

BC = zeros(size(RHS1));

% ------------- boundary condition left ----------------

for eln = 1:Ky
    eln2 = eln;
    for j=1:p
        lambda = 2*p +j;
        lambda_left(j,eln) = GM_lambda(lambda,eln2);
        loc_edge = p*(p+1) + j;
        glb_edge = GM_local_dof_q(eln2, loc_edge);
        for tt=1:p+1
            [xx, yy] = mesh1.el(eln2).mapping(-1,basis_dis.xp(tt));
            BC(glb_edge) = BC(glb_edge) - bc_left(xx,yy) * basis_dis.ep(j,tt) * basis_dis.wp(tt);
        end
    end
end

lambda_left = lambda_left(:);

% ------------- boundary condition rght ----------------

for eln = 1:Ky
    eln2 = (Kx-1)*Ky+eln;
    for j=1:p
        lambda = p + j;
        lambda_rght(j,eln) = GM_lambda(lambda,eln2);
        loc_edge = 2*p^2 + p + j;
        glb_edge = GM_local_dof_q(eln2, loc_edge);
        for tt=1:p+1
            [xx, yy] = mesh1.el(eln2).mapping(+1,basis_dis.xp(tt));
            BC(glb_edge) = BC(glb_edge) + bc_rght(xx,yy) * basis_dis.ep(j,tt) * basis_dis.wp(tt);
        end
    end
end

lambda_rght = lambda_rght(:);

% ------------- boundary condition bott ----------------

for eln = 1:Kx
    eln2 = (eln-1)*Ky + 1;
    for i=1:p
        lambda = i;
        lambda_bott(i,eln) = GM_lambda(lambda,eln2);
        loc_edge = i;
        glb_edge = GM_local_dof_q(eln2, loc_edge);
        for tt=1:p+1
            [xx, yy] = mesh1.el(eln2).mapping(basis_dis.xp(tt),-1);
            BC(glb_edge) = BC(glb_edge) - bc_bott(xx,yy) * basis_dis.ep(i,tt) * basis_dis.wp(tt);
        end
    end
end

lambda_bott = lambda_bott(:);

% ------------- boundary condition topp ----------------

for eln = 1:Kx
    eln2 = eln*Kx;
    for i=1:p
        lambda = 3*p + i;
        lambda_topp(i,eln) = GM_lambda(lambda,eln2);
        loc_edge = p^2 + i;
        glb_edge = GM_local_dof_q(eln2, loc_edge);
        for tt=1:p+1
            [xx, yy] = mesh1.el(eln2).mapping(basis_dis.xp(tt),+1);
            BC(glb_edge) = BC(glb_edge) + bc_topp(xx,yy) * basis_dis.ep(i,tt) * basis_dis.wp(tt);
        end
    end
end

lambda_topp = lambda_topp(:);

% ------------- removing rows with Dirichlet BC's ----------------

lambda_bc = [lambda_left lambda_rght lambda_bott lambda_topp];

con_E(lambda_bc,:) = [];

R_Neumann(lambda_bc,:) = [];

%%

RHS1 = RHS1 + BC;

RHS3 = reshape(RHS1, size(RHS));

%%

% LHS = (con_E * inv_A * con_E');
% det(LHS)
% lambda = (con_E * inv_A * con_E')\(con_E * inv_A * RHS1);
lambda = (con_E * inv_A * con_E')\(con_E * inv_A * RHS1 - R_Neumann);

lambda2 = [lambda; zeros(4*K*p,1)];

lambda_h = zeros(4*p, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    lambda_h(:,eln) = lambda2(GM_lambda(:,eln));
end

X_h3 = cell(1,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    X_h3(eln) = {sys_3D_inv{eln} * (RHS3(:,eln) - [N'*lambda_h(:,eln); zeros(loc_dof_p,1)])};
end

%% separating cochains 

q_h = zeros(loc_dof_u, mesh1.ttl_nr_el);
p_h = zeros(loc_dof_p, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:loc_dof_u);
    p_h(:,eln) = X_h3{eln}(loc_dof_u+1:ttl_loc_dof);
end

%% reconstruction 

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

p_h_2 = zeros(size(p_h));

for eln = 1:mesh1.ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

rec_p = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);
rec_x = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);
rec_y = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);
pp_ex = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
end

%
figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'LineColor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'LineColor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln),'LineColor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])