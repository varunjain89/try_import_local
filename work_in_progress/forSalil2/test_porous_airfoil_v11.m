clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

% ff = @(x,y) (1-x.^2).*(1-y.^2);
% 
% ux = @(x,y) -2*x .* (1-y.^2);
% uy = @(x,y) -2*y .* (1-x.^2);

% phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
% 
% ff = @(x,y) 0*ones(size(x));
% bc_left = @(x,y) 1*ones(size(x));
% bc_rght = @(x,y) 0*ones(size(x));
% bc_topp = @(x,y) 0*ones(size(x));
% bc_bott = @(x,y) 0*ones(size(x));

%%

% ------ Case 1 -------
phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
% d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

% ------ Case 2 -------
% phi_an   = @(x,y) x;
% dp_dx    = @(x,y) 1*ones(size(x));
% dp_dy    = @(x,y) 0*ones(size(x));
% % d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
% d2p_dx2  = @(x,y) 0*ones(size(x));
% d2p_dy2  = @(x,y) 0*ones(size(x));

% ------ Case 3 -------
% phi_an   = @(x,y) y;
% dp_dx    = @(x,y) 0*ones(size(x));
% dp_dy    = @(x,y) 1*ones(size(x));
% % d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
% d2p_dx2  = @(x,y) 0*ones(size(x));
% d2p_dy2  = @(x,y) 0*ones(size(x));

% ------ analytical expression for K -------
% k11_an  = @(x,y) 1e-1*ones(size(x));
% k12_an  = @(x,y) 0*ones(size(x));
% k22_an  = @(x,y) 1e-1*ones(size(x));

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);
ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

bc_left = @(x,y) phi_an(x,y);
bc_rght = @(x,y) phi_an(x,y);
bc_topp = @(x,y) phi_an(x,y);
bc_bott = @(x,y) phi_an(x,y);

%% user inpput for mappings

nr_Region = 10;

region(nr_Region).mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,nr_Region);

for patch = 1:nr_Region
    region(patch).mapping = @(xii,eta) mesh.porous_airfoil.vectorized.mapping(xii,eta,patch);
    region(patch).dX_dxii = @(xii,eta) mesh.porous_airfoil.vectorized.dX_dxii(xii,eta,patch);
    region(patch).dX_deta = @(xii,eta) mesh.porous_airfoil.vectorized.dX_deta(xii,eta,patch);
    region(patch).dY_dxii = @(xii,eta) mesh.porous_airfoil.vectorized.dY_dxii(xii,eta,patch);
    region(patch).dY_deta = @(xii,eta) mesh.porous_airfoil.vectorized.dY_deta(xii,eta,patch);
end

mesh1 = mmesh3(region);

%% check domain mapping

% mesh1.check_domain_mapping();

%% define discretization for each region

K = 3;
Kx = K;
Ky = K;

p = 10;

mesh1.discretize(Kx, Ky);

%% check element mappings

% mesh1.check_element_mapping();

%% check mapping edge 

% s = linspace(-1,0,21);
% 
% [x,y] = mesh1.domain(7).mapping(s,-1*ones(size(s)));
% 
% figure
% hold on
% plot(x,y)
% title('bottom')
% 
% [x,y] = mesh1.domain(7).mapping(s,1*ones(size(s)));
% 
% figure
% plot(x,y)
% title('top')
% 
% [x,y] = mesh1.domain(7).mapping(-1*ones(size(s)),s);
% 
% figure
% plot(x,y)
% title('left')
% 
% [x,y] = mesh1.domain(7).mapping(1*ones(size(s)),s);
% 
% figure
% plot(x,y)
% title('right')
% 
% aa = 1;

%% check reduction >>> reconstruction 2 forms

% ------- reduction -------

[xp, wp] = Gnodes2(p+1);

[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(xp,p,1);
basis_dis.xp = xp;
basis_dis.wp = wp;

[xii, eta] = meshgrid(xp);
jac_dis = mesh1.evaluate_jacobian_all(xii, eta);
metric_dis = flow.poisson.eval_metric(jac_dis);

for eln = 1:mesh1.ttl_nr_el
    [dis_xy.x(:,:,eln),dis_xy.y(:,:,eln)] = mesh1.el(eln).mapping(xii,eta);
end

% K_dis = flow.darcy.material(k11_an, k12_an, k22_an, dis_xy);

jac_dis.ggg = metric_dis.ggg;

% metric_dis_K = flow.darcy.metric(jac_dis, K_dis);
% parfor eln = 1:mesh1.ttl_nr_el
%     M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
%     dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh1.el(eln).mapping, basis_dis, M22);
% end

%% ------- reconstruction -------

pf = p+5;
% 
% sx = linspace(-0.98,0.98,pf+1);
% sy = linspace(-0.98,0.98,pf+1);
% 
% [~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
% [~, basis_rec.efy] = MimeticpolyVal(sy,p,1);
% 
% [rec_xii, rec_eta] = meshgrid(sx, sy);
% jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
% metric_rec = flow.poisson.eval_metric(jac_rec);
% 
% parfor eln = 1:mesh1.ttl_nr_el
%     
%     rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%     
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
%     
%     ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
%     
% end

%% ------- plots -------

% figure
% hold on
% 
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'LineColor','none')
% end
% 
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
% 
% figure
% hold on
% 
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'LineColor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [0700, 500, 550, 400])
% 
% figure
% hold on
% 
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'LineColor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
% 
% figure
% hold on
% 
% for eln = 1:mesh1.ttl_nr_el
% %     if eln > K^2 * (nr_Region - 1)
% %         continue
% %     end
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

%% check reduction >>> reconstruction 1 forms

% ------- reduction -------

% [xp, wp] = Gnodes2(p+1);
% [xii, eta] = meshgrid(xp);
% 
% jac1_dis = mesh1.evaluate_jacobian_all(xii, eta);
% metric_dis = flow.poisson.eval_metric(jac1_dis);
% 
% eln = 4;
% 
% [basis_dis.hp, basis_dis.ep] = MimeticpolyVal(xp,p,1);
% basis_dis.xp = xp;
% basis_dis.wp = wp;

% parfor eln = 1:mesh1.ttl_nr_el
%     M11 = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), -metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
%     dof_u(:,eln) = reduction.reduction_1form_v4(ux, uy, mesh1.el(eln).mapping, basis_dis, M11, jac1_dis,eln);
% end

% ------- reconstruction -------

% pf = 30;

% sx = linspace(-0.99,0.99,pf+1);
% sy = linspace(-0.99,0.99,pf+1);
% 
% [basis_rec.hfx, basis_rec.efx] = MimeticpolyVal(sx,p,1);
% [basis_rec.hfy, basis_rec.efy] = MimeticpolyVal(sy,p,1);
% 
% [rec_xii, rec_eta] = meshgrid(sx, sy);
% jac_rec = mesh1.evaluate_jacobian_all(rec_xii', rec_eta');
% metric_rec = flow.poisson.eval_metric(jac_rec);
% 
% parfor eln = 1:mesh1.ttl_nr_el
%     temp2x(:,:,eln) = reconstruction.reconstruct1xform_v7(dof_u(1:p*(p+1),eln), dof_u(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln);
%     temp2y(:,:,eln) = reconstruction.reconstruct1yform_v7(dof_u(1:p*(p+1),eln), dof_u(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln);
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
% end
% 
% ux_ex = ux(rec_x,rec_y);
% uy_ex = uy(rec_x,rec_y);

%%

% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln))
% end
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2x(:,:,eln)')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [0700, 500, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln) - temp2x(:,:,eln)')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

%%

% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln))
% end
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 050, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2y(:,:,eln)')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [0700, 050, 550, 400])
% 
% figure
% hold on
% for eln = 1:mesh1.ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln) - temp2y(:,:,eln)')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 050, 550, 400])

%% check manufactured solution

nr_el_Rg = Kx*Ky;             

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

% ------- make connectivity matrix -------

nr_local_lambda = 4*p;
nr_int_lambda_dof = 2*K*p*(K-1);
nr_refine_el = K^2;

GM_lambda = zeros(nr_local_lambda, mesh1.ttl_nr_el);

temp_GM_lam = gather_matrix.GM_boundary_LM_nodes_v2_internal(K,p);
R = full(spones(temp_GM_lam));

for i = 1:nr_Region
    GM_lambda(:,(i-1)*nr_refine_el +1: i * nr_refine_el) = ((i-1) * nr_int_lambda_dof + temp_GM_lam).*R;
end

% ------- Salil gather matrix -------

GM_region        = nr_int_lambda_dof*nr_Region + mesh.porous_airfoil.GM_patch(K,p);

% -----------------------------------

for reg = 1:nr_Region
    % bottom / south 
    for eln = 1:Kx
        bot_eln = (eln-1)*Ky + 1;
        glb_eln = (reg - 1)*K^2 + bot_eln;
        GM_lambda(1:p,glb_eln) = GM_region((eln-1)*p+1:eln*p, reg);
    end
    
    % rght / east 
    for eln = 1:Ky
        est_eln = (Kx-1)*Ky + eln;
        glb_eln = (reg - 1)*K^2 + est_eln;
        GM_lambda(p+1:2*p,glb_eln) = GM_region(Kx*p + (eln-1)*p+1:Kx*p +eln*p, reg);
    end
    
    % topp / north 
    for eln = 1:Kx
        top_eln = eln*Ky;
        glb_eln = (reg - 1)*K^2 + top_eln;
        temp = 2*Kx*p + Ky*p -(eln-1)*p -(0:p-1);
        GM_lambda(3*p+1:4*p,glb_eln) = GM_region(temp, reg);
    end
    
    % left / west
    for eln = 1:Ky
        wst_eln = eln;
        glb_eln = (reg - 1)*K^2 + wst_eln;
        temp = 2*Kx*p + 2*Ky*p - (eln-1)*p -(0:p-1);
        GM_lambda(2*p+1:3*p,glb_eln) = GM_region(temp, reg);
    end
end

%
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh1.ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
%
N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 mesh1.ttl_nr_el]);
%
con_E = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);

nr_internal_edges = 17;
nr_boundary_edges = 6;
ttl_nr_lambda = nr_int_lambda_dof*nr_Region + nr_internal_edges*K*p + nr_boundary_edges * K *p;
con_E = [con_E zeros(ttl_nr_lambda,loc_dof_p)];

% ------- make mass matrix matrix -------

% [basis_dis.xp, basis_dis.wp] = Gnodes2(p+1);
% [basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);
% 
% [xii_dis, eta_dis] = meshgrid(basis_dis.xp);
% jac_dis = mesh1.evaluate_jacobian_all(xii_dis, eta_dis);
% metric_dis = flow.poisson.eval_metric(jac_dis);

M11 = zeros(loc_dof_u, loc_dof_u, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A = zeros(ttl_loc_dof,ttl_loc_dof,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    A(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    inv_A(:,:,eln) = A(:,:,eln)\I;
end

sys_3D_inv(mesh1.ttl_nr_el) = {inv_A(:,:,mesh1.ttl_nr_el)};

for eln = 1:mesh1.ttl_nr_el
    sys_3D_inv(eln) = {inv_A(:,:,eln)};
end

inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);

%% ------- evaluate RHS -------

for eln = 1:mesh1.ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh1.el(eln).mapping, basis_dis, M22);
end

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

%% boundary condition - Neumann

R_Neumann = zeros(ttl_nr_lambda,1);

%% boundary condition - Dirichlet

BC = zeros(size(RHS1));

% ------------- boundary condition left ----------------
count = 1;
for reg = 1:2:3
    for eln = 1:Ky
        eln2 = (reg-1)*K^2 + eln;
        for j=1:p
            lambda = 2*p +j;
            lambda_left(j,eln,count) = GM_lambda(lambda,eln2);
            loc_edge = p*(p+1) + j;
            glb_edge = GM_local_dof_q(eln2, loc_edge);
            for tt=1:p+1
                [xx, yy] = mesh1.el(eln2).mapping(-1,basis_dis.xp(tt));
                BC(glb_edge) = BC(glb_edge) - bc_left(xx,yy) * basis_dis.ep(j,tt) * basis_dis.wp(tt);
            end
            lambda_left2(j,eln,count) = BC(glb_edge); 
        end
    end
    count= count+1;
end

lambda_left = lambda_left(:);
% lambda_left2 = lambda_left2(:);

lambda_left2 = zeros(2*K*p,1);

% ------------- boundary condition rght ----------------
count = 1;
for reg = 8:2:10
    for eln = 1:Ky
        eln2 = (reg-1)*K^2 + (Kx-1)*Ky+eln;
        for j=1:p
            lambda = p + j;
            lambda_rght(j,eln,count) = GM_lambda(lambda,eln2);
            loc_edge = 2*p^2 + p + j;
            glb_edge = GM_local_dof_q(eln2, loc_edge);
            for tt=1:p+1
                [xx, yy] = mesh1.el(eln2).mapping(+1,basis_dis.xp(tt));
%                 bc_rght(xx,yy)
                BC(glb_edge) = BC(glb_edge) + bc_rght(xx,yy) * basis_dis.ep(j,tt) * basis_dis.wp(tt);
            end
            lambda_rght2(j,eln,count) = BC(glb_edge);
        end
    end
    count= count+1;
end

lambda_rght = lambda_rght(:);
% lambda_rght2 = lambda_rght2(:);

lambda_rght2 = zeros(2*K*p,1);

% ------------- boundary condition bott ----------------

for reg = 7
    for eln = 1:Kx
        eln2 = (reg-1)*K^2 + (eln-1)*Ky + 1;
        for i=1:p
            lambda = i;
            lambda_bott(i,eln) = GM_lambda(lambda,eln2);
            loc_edge = i;
            glb_edge = GM_local_dof_q(eln2, loc_edge);
            for tt=1:p+1
                [xx, yy] = mesh1.el(eln2).mapping(basis_dis.xp(tt),-1);
                BC(glb_edge) = BC(glb_edge) - bc_bott(xx,yy) * basis_dis.ep(i,tt) * basis_dis.wp(tt);
            end
%             lambda_bott2(i,eln) = BC(glb_edge);
        end
    end
end

lambda_bott = lambda_bott(:);
% lambda_bott2 = lambda_bott2(:);

lambda_bott2 = zeros(K*p,1);

% ------------- boundary condition topp ----------------

for reg = 4
    for eln = 1:Kx
        eln2 = (reg-1)*K^2 + eln*Kx;
        for i=1:p
            lambda = 3*p + i;
            lambda_topp(i,eln) = GM_lambda(lambda,eln2);
            loc_edge = p^2 + i;
            glb_edge = GM_local_dof_q(eln2, loc_edge);
            for tt=1:p+1
                [xx, yy] = mesh1.el(eln2).mapping(basis_dis.xp(tt),+1);
                BC(glb_edge) = BC(glb_edge) + bc_topp(xx,yy) * basis_dis.ep(i,tt) * basis_dis.wp(tt);
            end
%             lambda_topp2(i,eln) = BC(glb_edge);
        end
    end
end

lambda_topp = lambda_topp(:);
% lambda_topp2 = lambda_topp2(:);

lambda_topp2 = zeros(K*p,1);

% ------------- removing rows with Dirichlet BC's ----------------

lambda_dichlet_bc = [lambda_left; lambda_rght; lambda_bott; lambda_topp];

con_E(lambda_dichlet_bc,:)     = [];
R_Neumann(lambda_dichlet_bc,:) = [];

%% implementing BCs in RHS

RHS1 = RHS1 + BC;

%% solve lambda's

lambda = (con_E * inv_A * con_E')\(con_E * inv_A * RHS1 - R_Neumann);

int_lambda = nr_int_lambda_dof*nr_Region + nr_internal_edges*K*p;

% lambda_topp2 = lambda(int_lambda+1:int_lambda+K*p);
% lambda_bott2 = lambda(int_lambda+K*p+1:int_lambda+2*K*p);
% lambda2 = lambda;
% lambda2(lambda_left) = lambda_left2;
% lambda2(lambda_rght) = lambda_rght2;
% lambda2(lambda_bott) = lambda_bott2;
% lambda2(lambda_topp) = lambda_topp2;

lambda2 = [lambda(1:int_lambda); lambda_left2; lambda_topp2; lambda_bott2; lambda_rght2];

lambda_h = zeros(4*p, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    lambda_h(:,eln) = lambda2(GM_lambda(:,eln));
end

%% solve local system 

X_h3 = cell(1,mesh1.ttl_nr_el);

RHS3 = reshape(RHS1, size(RHS));

for eln = 1:mesh1.ttl_nr_el
    X_h3(eln) = {sys_3D_inv{eln} * (RHS3(:,eln) - [N'*lambda_h(:,eln); zeros(loc_dof_p,1)])};
end

%% separating cochains 

q_h = zeros(loc_dof_u, mesh1.ttl_nr_el);
p_h = zeros(loc_dof_p, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:loc_dof_u);
    p_h(:,eln) = X_h3{eln}(loc_dof_u+1:ttl_loc_dof);
end

%% reconstruction 

% pf = 20;

sx = linspace(-0.98,0.98,pf+1);
sy = linspace(-0.98,0.98,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

p_h_2 = zeros(size(p_h));

for eln = 1:mesh1.ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

% parfor eln = 1:mesh1.ttl_nr_el
%     M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
%     dof_p(:,eln) = reduction.reduction_2form_v2(phi_an, mesh1.el(eln).mapping, basis_dis, M22);
% end

rec_p = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);
rec_x = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);
rec_y = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);
pp_ex = zeros(size(sx,2),size(sy,2),mesh1.ttl_nr_el);

%% 

for eln = 1:mesh1.ttl_nr_el
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%     rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(dof_p(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
end

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'LineColor','none')
end

colorbar
title('exact solution p')
set(gcf, 'Position',  [0100, 500, 550, 400])
% caxis([-1 1])

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'LineColor','none')
end
colorbar
title('reconstructed solution p')
set(gcf, 'Position',  [700, 500, 550, 400])
% caxis([-1 1])

figure
hold on

for eln = 1:mesh1.ttl_nr_el
    surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln))
end
colorbar
title('difference p')
set(gcf, 'Position',  [1300, 500, 550, 400])

%% reconstruct fluxes / normal velocity: q = -v dx + u dy

%% ------- reduction -------
% 
% [xp, wp] = Gnodes2(p+1);
% [xii, eta] = meshgrid(xp);
% 
% jac1_dis = mesh1.evaluate_jacobian_all(xii, eta);
% metric_dis = flow.poisson.eval_metric(jac1_dis);
% 
% eln = 4;
% 
% [basis_dis.hp, basis_dis.ep] = MimeticpolyVal(xp,p,1);
% basis_dis.xp = xp;
% basis_dis.wp = wp;
% 
% parfor eln = 1:mesh1.ttl_nr_el
%     M11 = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), -metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
%     dof_u(:,eln) = reduction.reduction_1form_v4(ux, uy, mesh1.el(eln).mapping, basis_dis, M11, jac1_dis,eln);
% end

%% ------- reconstruction -------

% pf = 30;

s = linspace(-0.98,0.98,pf+1);

[basis_rec.hf, basis_rec.ef] = MimeticpolyVal(s,p,1);

[rec_xii, rec_eta] = meshgrid(s);
jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

for eln = 1:mesh1.ttl_nr_el
    temp2y(:,:,eln) = reconstruction.reconstruct1yform_v9(q_h(1:p*(p+1),eln), q_h(p*(p+1)+1:2*p*(p+1),eln),p, pf, jac_rec, metric_rec,eln,basis_rec);
    temp2x(:,:,eln) = reconstruction.reconstruct1xform_v9(q_h(1:p*(p+1),eln), q_h(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln,basis_rec);
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh1.el(eln).mapping(rec_xii, rec_eta);
end

ux_ex = ux(rec_x,rec_y);
uy_ex = uy(rec_x,rec_y);

%%

figure
hold on
for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln))
end
colorbar
title('exact solution u_x')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on
for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2x(:,:,eln)')
end
colorbar
title('reconstructed solution u_x')
set(gcf, 'Position',  [0700, 500, 550, 400])

figure
hold on
for eln = 1:mesh1.ttl_nr_el
    surf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln) - temp2x(:,:,eln)')
end
colorbar
title('difference u_x')
set(gcf, 'Position',  [1300, 500, 550, 400])

%%

figure
hold on
for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln))
end
colorbar
title('exact solution u_y')
set(gcf, 'Position',  [0100, 050, 550, 400])

figure
hold on
for eln = 1:mesh1.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2y(:,:,eln)')
end
colorbar
title('reconstructed solution u_y')
set(gcf, 'Position',  [0700, 050, 550, 400])

figure
hold on
for eln = 1:mesh1.ttl_nr_el
    surf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln) - temp2y(:,:,eln)')
end
colorbar
title('difference u_y')
set(gcf, 'Position',  [1300, 050, 550, 400])
