%% ---- test case flying carpet ----

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%%

ff = @(x,y) (1-x.^2);

ux = @(x,y) -2*x;
uy = @(x,y) 0*ones(size(x));

% ux = @(x,y) 0*ones(size(x));
% uy = @(x,y) -2*y;
% 
% ux = @(x,y) -2*x .* (1-y.^2);
% uy = @(x,y) -2*y .* (1-x.^2);

%% define domain mapping and jacobians

xbound = [1.14 2.34];
ybound = [0.3 1.7];

% xbound = [0 1];
% ybound = [0 1];

c = 0.1;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

mesh1 = mmesh2(domain);

mapping2 = @mesh.crazy_mesh.mapping;

%% check domain mapping

% mesh1.check_domain_mapping();

%% discretize the domain

K = 1;
Kx = K;
Ky = K;

p = 10;

mesh1.discretize(Kx, Ky);

%% check element mappings

% mesh1.check_element_mapping();

%% evaluate jacobian and metric terms

[xp, wp] = GLLnodes(p);
[xii, eta] = meshgrid(xp);

jac1 = mesh1.evaluate_jacobian_all(xii, eta);

%% evaluate metric terms 

metric1 = flow.poisson.eval_metric(jac1);

% %% check jacobians : 2 form reduction --> reconstruction
% 
% % ------- reduction -------
% 
[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);
% 
basis.hp = hp;
basis.ep = ep;
% 
% M22s = mass_matrix.M2_2(p, metric1.ggg(:,:,1));
% 
% dof_f = reduction.reduction_2form_v1(ff, mesh1.el(1).mapping, basis, M22s);
% 
% % ------- reconstruction -------
% 
pf = 30
% 
s = linspace(-1,1,pf+1);
% 
[rec_xii, rec_eta] = meshgrid(s);
% 
[x,y] = mesh1.el(1).mapping(rec_xii, rec_eta);
% 
% ff_ex = ff(x, y);
% 
% % figure
% % contourf(x,y,ff_ex)
% % colorbar
% 
jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric2 = flow.poisson.eval_metric(jac_rec);
% 
% [basis.hf, basis.ef] = MimeticpolyVal(s,p,1);
% 
% rec_f = reconstruction.reconstruct2form_v5(dof_f, basis, metric2.ggg(:,:,1));
% 
el_mapping = mesh1.el(1).mapping;
% 
[rec_x, rec_y] = el_mapping(rec_xii,rec_eta,1);
% 
% % figure
% % contourf(rec_x, rec_y, rec_f)
% % colorbar

%% check jacobians : 1 form reduction --> reconstruction

% ------- reduction -------

M11 = mass_matrix.M1_3(p, metric1.g11(:,:,1), -metric1.g12(:,:,1), metric1.g22(:,:,1));



[dof_u, Mf] = reduction.reduction_1form_v2(ux, uy, el_mapping, basis, M11, jac1);

u_1x_h = reduction.of1xform(ux, uy, xbound, ybound, c, p, mapping2, domain.dX_dxii, domain.dY_dxii);
u_1y_h = reduction.of1yform(ux, uy, xbound, ybound, c, p, mapping2, domain.dX_deta, domain.dY_deta);

% ------- reconstruction -------

temp2x = reconstruction.reconstruct1xform_v6(dof_u(1:p*(p+1)), dof_u(p*(p+1)+1:2*p*(p+1)), p, pf, jac_rec, metric2);
temp2y = reconstruction.reconstruct1yform_v6(dof_u(1:p*(p+1)), dof_u(p*(p+1)+1:2*p*(p+1)), p, pf, jac_rec, metric2);
% temp2x = reconstruction.reconstruct1xform_v6(u_1x_h, u_1y_h, p, pf, jac_rec, metric2);
% temp2y = reconstruction.reconstruct1yform_v6(u_1x_h, u_1y_h, p, pf, jac_rec, metric2);

ux_ex = ux(x,y);
uy_ex = uy(x,y);

%% 

figure
contourf(rec_x, rec_y, ux_ex)
colorbar

figure
contourf(rec_x, rec_y, temp2x')
colorbar
title('reconstruction')

figure
contourf(rec_x, rec_y, ux_ex - temp2x')
colorbar
title('difference')

%% 

% figure
% contourf(rec_x, rec_y, uy_ex)
% colorbar
% 
% figure
% contourf(rec_x, rec_y, temp2y')
% colorbar
% title('reconstruction')
% 
% figure
% contourf(rec_x, rec_y, uy_ex - temp2y')
% colorbar
% title('difference')

%% check manufactured solution

%% 
% 
% Mf = Mf';
% 
% dof2 = [u_1x_h; u_1y_h];
% 
% dual_dof = M11 * dof2;