%% ---- test case flying carpet ----

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%%

ff = @(x,y) (1-x.^2);

%% define domain mapping and jacobians

xbound = [0 1];
ybound = [0 1];
c = 0.15;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

mesh1 = mmesh2(domain);

%% check domain mapping

% mesh1.check_domain_mapping();

%% discretize the domain

K = 3;
Kx = K;
Ky = K;

p = 2;

mesh1.discretize(Kx, Ky);

%% check element mappings

% mesh1.check_element_mapping();

%% evaluate jacobians

[xp, wp] = GLLnodes(p);
[xii, eta] = meshgrid(xp);

%----here check if jacobian in terms of x and y or in terms of xii and eta?

jac1 = mesh1.evaluate_jacobian_all(xii, eta);

%% evaluate metric terms 

metric1 = flow.poisson.eval_metric(jac1);

%% check jacobians : 2 form reduction --> reconstruction

%% reduction 

element_bounds_x = linspace(-1,1,K+1);
element_bounds_y = linspace(-1,1,K+1);

dof_f = reduction.of2form_multi_element_v7(ff, p, domain, K, element_bounds_x, element_bounds_y);

M22s = mass_matrix.M2_2(p, metric1.ggg(:,:,1));

[xp, wp] = GLLnodes(p);
[~, ep] = MimeticpolyVal(xp,p,1);

Mf = zeros(1,p^2);

for i = 1:p
    for j = 1:p
        volij = (i-1)*p +j;
        for ix = 1:p+1
            for iy = 1:p+1
                [ix2, iy2] = mesh1.el(1).mapping(xp(ix),xp(iy));
                Mf(volij) = Mf(volij) + ep(i,ix)*ep(j,iy)*ff(ix2,iy2)*wp(ix)*wp(iy);
            end
        end
    end
end


R_ff = inv(M22s) * Mf';

diff = dof_f(:,1) - R_ff

dof_f = R_ff; 

%% reconstruction 

s = linspace(-1,1,31);

[rec_xii, rec_eta] = meshgrid(s);

[x,y] = mesh1.el(1).mapping(rec_xii, rec_eta);

ff_ex = ff(x, y);

figure
contourf(x,y,ff_ex)
colorbar


jac2    = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric2 = flow.poisson.eval_metric(jac2);

[xp, wp] = GLLnodes(p);
[basis.hf, basis.ef] = MimeticpolyVal(s,p,1);

rec_f = reconstruction.reconstruct2form_v5(dof_f(:,1), basis, metric2.ggg(:,:,1));

el_mapping = mesh1.el(1).mapping;

[rec_x, rec_y] = el_mapping(rec_xii,rec_eta,1);

figure
contourf(rec_x, rec_y, rec_f)

colorbar

% R_f = 

% M22s = zeros(p^2,p^2, mesh1.ttl_nr_el);
% 
% for eln = 1:mesh1.ttl_nr_el
%     M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
% end
% 
% [xp, wp] = GLLnodes(p);
% [~, ep] = MimeticpolyVal(xp,p,1);
% 
% Mf = zeros(1,p^2);
% 
% for i = 1:p
%     for j = 1:p
%         volij = (i-1)*p +j;
%         for ix = 1:p+1
%             for iy = 1:p+1
%                 Mf(volij) = Mf(volij) + ep(i,ix)*ep(j,iy)*ff(ix,iy)*wp(ix)*wp(iy);
%             end
%         end
%     end
% end
% 
% 
% R_ff = inv(M22) * Mf';



%% check jacobians : 1 form reduction --> reconstruction

%% check manufactured solution
