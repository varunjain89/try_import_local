%% ---- test case flying carpet ----

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%%

ff = @(x,y) (1-x.^2);

ux = @(x,y) -2*x;
uy = @(x,y) 0*ones(size(x));

% ux = @(x,y) 0*ones(size(x));
% uy = @(x,y) -2*y;
% 
ux = @(x,y) -2*x .* (1-y.^2);
uy = @(x,y) -2*y .* (1-x.^2);

%% define domain mapping and jacobians

xbound = [1.14 2.34];
ybound = [0.3 1.7];

% xbound = [0 1];
% ybound = [0 1];

c = 0.1;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

mesh1 = mmesh2(domain);

% mapping2 = @mesh.crazy_mesh.mapping;

%% check domain mapping

% mesh1.check_domain_mapping();

%% discretize the domain

K = 3;
Kx = K;
Ky = K;

p = 15;

mesh1.discretize(Kx, Ky);

%% check element mappings

% mesh1.check_element_mapping();

%% check reduction >>> reconstruction 2 forms

% ------- reduction -------

eln = 1;

el_mapping = @(eln) mesh1.el(eln).mapping;

[xp, wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(xp,p,1);

[xii, eta] = meshgrid(xp);
jac_dis = mesh1.evaluate_jacobian_all(xii, eta);
metric_dis = flow.poisson.eval_metric(jac_dis);

M22 = mass_matrix.M2_2(p, metric_dis.ggg(:,:,eln));
dof_f = reduction.reduction_2form_v1(ff, el_mapping(eln), basis_dis, M22);

% ------- reconstruction -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,0,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

rec_f = reconstruction.reconstruct2form_v6(dof_f, basis_rec, metric_rec.ggg(:,:,eln));

[rec_x,rec_y] = mesh1.el(1).mapping(rec_xii, rec_eta);

ff_ex = ff(rec_x, rec_y);

figure

subplot(1,3,1)
contourf(rec_x,rec_y,ff_ex)
colorbar
title('exact solution')

subplot(1,3,2)
contourf(rec_x, rec_y, rec_f)
colorbar
title('reconstructed solution')

subplot(1,3,3)
contourf(rec_x, rec_y, rec_f - ff_ex)
colorbar
title('difference')

set(gcf, 'Position',  [200, 300, 1600, 400])

%% evaluate jacobian and metric terms

% [xp, wp] = GLLnodes(p);
% [xii, eta] = meshgrid(xp);
% % 
% jac1 = mesh1.evaluate_jacobian_all(xii, eta);
% % 
% % %% evaluate metric terms 
% % 
% metric1 = flow.poisson.eval_metric(jac1);
% % 
% % % %% check jacobians : 2 form reduction --> reconstruction
% % % 
% % % % ------- reduction -------
% % % 
% [xp, wp] = GLLnodes(p);
% [hp, ep] = MimeticpolyVal(xp,p,1);
% % % 
% basis.hp = hp;
% basis.ep = ep;
% % % 
% % M22s = mass_matrix.M2_2(p, metric1.ggg(:,:,1));
% % 
% % dof_f = reduction.reduction_2form_v1(ff, mesh1.el(1).mapping, basis, M22s);
% % 
% % % % ------- reconstruction -------
% % % 
% pf = 30
% % % 
% s = linspace(-1,1,pf+1);
% % % 
% [rec_xii, rec_eta] = meshgrid(s);
% % 
% % [x,y] = mesh1.el(1).mapping(rec_xii, rec_eta);
% % 
% % ff_ex = ff(x, y);
% % 
% % figure
% % contourf(x,y,ff_ex)
% % colorbar
% % 
% jac_rec = mesh1.evaluate_jacobian_all(rec_xii, rec_eta);
% metric2 = flow.poisson.eval_metric(jac_rec);
% % 
% [basis.hf, basis.ef] = MimeticpolyVal(s,p,1);
% % 
% % rec_f = reconstruction.reconstruct2form_v5(dof_f, basis, metric2.ggg(:,:,1));
% % 
% % el_mapping = mesh1.el(1).mapping;
% % 
% % [rec_x, rec_y] = el_mapping(rec_xii,rec_eta,1);
% % 
% % figure
% % contourf(rec_x, rec_y, rec_f)
% % colorbar
% 
% %% check jacobians : 1 form reduction --> reconstruction
% 
% % ------- reduction -------
% 
% M11 = mass_matrix.M1_3(p, metric1.g11(:,:,1), -metric1.g12(:,:,1), metric1.g22(:,:,1));
% [dof_u, Mf] = reduction.reduction_1form_v2(ux, uy, mesh1.el(1).mapping, basis, M11, jac1);
% 
% % ------- reconstruction -------
% 
% temp2x = reconstruction.reconstruct1xform_v6(dof_u(1:p*(p+1)), dof_u(p*(p+1)+1:2*p*(p+1)), p, pf, jac_rec, metric2);
% temp2y = reconstruction.reconstruct1yform_v6(dof_u(1:p*(p+1)), dof_u(p*(p+1)+1:2*p*(p+1)), p, pf, jac_rec, metric2);
% 
% ux_ex = ux(rec_x,rec_y);
% uy_ex = uy(rec_x,rec_y);
% 
% %% 
% 
% figure
% contourf(rec_x, rec_y, ux_ex)
% colorbar
% 
% figure
% contourf(rec_x, rec_y, temp2x')
% colorbar
% title('reconstruction')
% 
% figure
% contourf(rec_x, rec_y, ux_ex - temp2x')
% colorbar
% title('difference')
% 
% %% 
% 
% figure
% contourf(rec_x, rec_y, uy_ex)
% colorbar
% 
% figure
% contourf(rec_x, rec_y, temp2y')
% colorbar
% title('reconstruction')
% 
% figure
% contourf(rec_x, rec_y, uy_ex - temp2y')
% colorbar
% title('difference')

%% check manufactured solution
