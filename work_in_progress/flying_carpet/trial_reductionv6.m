%% Created on 3rd March, 2018
%% This script test the reduction functions

clc
close all
clear variables

addpath('../../MSEM')

%% mesh properties

x_bound = [0 1];
y_bound = [0 1];
c = 0.3;

%% domain properties

K = 1;
p = 30;
nf = 40;

%% mesh constants

nr_nodes = (p+1)^2;
nr_edges = 2*p*(p+1);
nr_surfs = p^2;

%% plot mesh 

% [x_gll, w] = GLLnodes(p);
% xi_fine = linspace(-1,1,nf+1);
% 
% [xi_plot, eta_plot] = meshgrid(x_gll);
% 
% [s, t] = meshgrid(xi_fine);
% [hf,ef] = MimeticpolyVal(xi_fine,p,1);
% 
% [x_plot, y_plot] = Mapping_CrazyMesh(x_bound, y_bound, c, xi_plot, eta_plot);
% [x_recn, y_recn] = Mapping_CrazyMesh(x_bound, y_bound, c, s, t);
% 
% figure
% plot(x_plot, y_plot, '-+')
% hold on
% plot(y_plot, x_plot, '-+')
%

mapping = @mesh.crazy_mesh.mapping;

dX_dxi  = @(xi,eta) 0.5*(x_bound(2)-x_bound(1))*(1+ c*pi*cos(pi*xi).*sin(pi*eta));
dX_deta = @(xi,eta) 0.5*(x_bound(2)-x_bound(1))*c*pi*sin(pi*xi).*cos(pi*eta);
dY_dxi  = @(xi,eta) 0.5*(y_bound(2)-y_bound(1))*c*pi*cos(pi*xi).*sin(pi*eta);
dY_deta = @(xi,eta) 0.5*(y_bound(2)-y_bound(1))*(1+ c*pi*sin(pi*xi).*cos(pi*eta));

%% reduction and reconstruction : 1 - cochain 

%% reduction to 1x - cochain

u_an = @(x,y) cos(2*pi*x).*sin(2*pi*y);
v_an = @(x,y) sin(2*pi*x).*cos(2*pi*y);

% [x_gll, w] = GLLnodes(p);
% 
% % % reduction
% u_1x_h = reduction1xcochain(u_an, v_an, p, x_bound, y_bound, c);
% u_1x_h = zeros(nr_edges/2,1);
% 
% p_in = p+10;
% [quad_int_xi, w_int_xi] = GLLnodes(p_in);
% 
% 
% for i = 1:p
%     for j = 1:p+1
%         edgeij = (j-1)*p + i;
%         
%         xi_i1 = x_gll(i);
%         xi_i2 = x_gll(i+1);
%         eta_j = x_gll(j);
%         
%         xi  = 0.5*(xi_i1 + xi_i2)   + 0.5*(xi_i2 - xi_i1).* quad_int_xi;
%         eta = eta_j;
%         
%         [x_int, y_int] = mapping(x_bound, y_bound, c, xi ,eta);
% %         [x_int, y_int] = map_local_edge(x_bound, y_bound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
% %         plot(x_int, y_int,'+')          % this line plots and make
% %                                         comparison of integration pts
%         u_int = u_an(x_int, y_int);
%         v_int = v_an(x_int, y_int);
%         
%         dxi_ds  = 0.5*(xi_i2 - xi_i1);
%         
%         dx_ds  = dX_dxi(xi, eta)*dxi_ds;
%         dy_ds  = dY_dxi(xi, eta)*dxi_ds;
%         
%         for k = 1:p_in+1
%             u_1x_h(edgeij) = u_1x_h(edgeij)+ (u_int(k)*dx_ds(k) + v_int(k)*dy_ds(k)) * w_int_xi(k);
%         end
%     end
% end

u_1x_h = reduction.of1xform(u_an, v_an, x_bound, y_bound, c, p, mapping, dX_dxi, dY_dxi);
% u_1x_h'

%% reduction to 1y - cochain

% u_1y_h = zeros(nr_edges/2,1);
% 
% [x_gll, w] = GLLnodes(p);
% 
% p_in = p+10;
% [quad_int_xi, w_int_xi] = GLLnodes(p_in);
% 
% % figure
% % hold on
% for i = 1:p+1
%     for j = 1:p
%         edgeij = (i-1)*p + j;
%         
%         xi_i  = x_gll(i);
%         eta_j1 = x_gll(j);
%         eta_j2 = x_gll(j+1);
%         
%         xi = xi_i;
%         eta = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* quad_int_xi;
%         
%         [x_int, y_int] = mapping(x_bound, y_bound, c, xi ,eta);
% %         [x_int, y_int] = map_local_y_edge(x_bound, y_bound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
% %         plot(x_int, y_int,'+')          % this line plots and make
%                                         % comparison of integration pts
%         u_int = u_an(x_int, y_int);
%         v_int = v_an(x_int, y_int);
%         
%         deta_dt = 0.5*(eta_j2 - eta_j1);
%         
%         dx_dt = dX_deta(xi, eta)*deta_dt;
%         dy_dt = dY_deta(xi, eta)*deta_dt;
%         
% %         dx_deta_int = local_dx_deta(x_bound, y_bound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
% %         dy_deta_int = local_dy_deta(x_bound, y_bound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         
%         for k = 1:p_in+1
%             u_1y_h(edgeij) = u_1y_h(edgeij)+ (u_int(k)*dx_dt(k) + v_int(k)*dy_dt(k)) * w_int_xi(k);
%         end
%     end
% end

u_1y_h = reduction.of1yform(u_an, v_an, x_bound, y_bound, c, p, mapping, dX_deta, dY_deta);
% u_1y_h'

%% reconstruction of 1x - cochain

u = zeros(nf+1,nf+1);

% [xif, wf] = GLLnodes(nf);
xif = linspace(-1, 1, nf+1);

[xi_recn, eta_recn] = meshgrid(xif);

[hf,ef] = MimeticpolyVal(xif,p,1);

g = dX_dxi(xi_recn,eta_recn).*dY_deta(xi_recn,eta_recn) - dX_deta(xi_recn,eta_recn).*dY_dxi(xi_recn,eta_recn);

dY_deta1 = dY_deta(xi_recn,eta_recn);
dY_dxi1  = dY_dxi(xi_recn,eta_recn);
% dX_dxi1  = dX_dxi(xi_recn,eta_recn);

for k = 1:nf+1
    for l = 1:nf+1
        
        for i =1:p
            for j =1:p+1
                edgeij = (j-1)*p+i;
                u(k,l) = u(k,l) - u_1x_h(edgeij) *ef(i,k) *hf(j,l) * dY_dxi1(k,l) / g(k,l);
            end
        end
        
        for i=1:p+1
            for j=1:p
                edgeij = (i-1)*p +j;
                u(k,l) = u(k,l) + u_1y_h(edgeij) *hf(i,k) *ef(j,l) * dY_deta1(k,l) / g(k,l);
            end
        end
         
    end
end

[x_recn, y_recn] = mapping(x_bound, y_bound, c, xi_recn, eta_recn);

figure
contourf(x_recn, y_recn, u)
colorbar

% figure
% plot(x_recn, y_recn,'+')

u2 = u_an(x_recn, y_recn);

figure
contourf(x_recn, y_recn, u2)
colorbar

figure
contourf(x_recn, y_recn, u - u2)
colorbar

% figure
% contourf(x_recn, y_recn, u2 - u' - 2*u2)
% colorbar

% %% reconstruction of 1y - cochain
% 
% v = zeros(nf+1,nf+1);
% 
% % [xif, wf] = GLLnodes(nf);
% xif = linspace(-1, 1, nf+1);
% 
% [xi_recn, eta_recn] = meshgrid(xif);
% 
% [hf,ef] = MimeticpolyVal(xif,p,1);
% 
% g = dX_dxi(xi_recn,eta_recn).*dY_deta(xi_recn,eta_recn) - dX_deta(xi_recn,eta_recn).*dY_dxi(xi_recn,eta_recn);
% 
% % dY_deta1 = dY_deta(xi_recn,eta_recn);
% % dY_dxi1  = dY_dxi(xi_recn,eta_recn);
% % dX_deta1  = dX_dxi(xi_recn,eta_recn);
% % dX_dxi1  = dX_dxi(xi_recn,eta_recn);
% 
% for k = 1:nf+1
%     for l = 1:nf+1
%         
%         for i =1:p
%             for j =1:p+1
%                 edgeij = (j-1)*p+i;
%                 u(k,l) = u(k,l) + u_1x_h(edgeij) *ef(i,k) *hf(j,l) * dY_deta1(k,l) / g(k,l);
%             end
%         end
%         
%         for i=1:p+1
%             for j=1:p
%                 edgeij = (i-1)*p +j;
%                 u(k,l) = u(k,l) - u_1y_h(edgeij) *hf(i,k) *ef(j,l) * dY_dxi1(k,l) / g(k,l);
%             end
%         end
%          
%     end
% end
% 
% [x_recn, y_recn] = mapping(x_bound, y_bound, c, xi_recn, eta_recn);
% 
% figure
% contourf(x_recn, y_recn, u')
% colorbar
% 
% % figure
% % plot(x_recn, y_recn,'+')
% 
% u2 = u_an(x_recn, y_recn);
% 
% figure
% contourf(x_recn, y_recn, u2)
% colorbar
% 
% figure
% contourf(x_recn, y_recn, -u2' + u)
% colorbar
% 
% % figure
% % contourf(x_recn, y_recn, u2 - u' - 2*u2)
% % colorbar