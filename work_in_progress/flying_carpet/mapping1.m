function [x, y] = mapping1(s, t)

a = -0.9;
b = 0.8;

x = a/2 * (1 - s) + b/2 * (1 + s);
y = a/2 * (1 - t) + b/2 * (1 + t);

end

