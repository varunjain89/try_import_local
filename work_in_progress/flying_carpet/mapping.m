function [x ,y] = mapping(k,l)

xbound = [0 1];
ybound = [0.1 0.9];
c = 0.15;

mapping2 = @(x,y) mesh.crazy_mesh.mapping(xbound, ybound, c, x, y);

[s, t] = mapping1(k, l);
[x, y] = mapping2(s, t);

end

