%% 26 March, 2019 
%% i'm trying n - level hybridmethod to further reduce the problem size

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% analytical solution

% case 2

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

a = 1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% domain

xbound = [0 1];
ybound = [0 1];
c = 0.3;

%% discretization

K = 9;
p = 3;

ttl_nr_el = K^2;
local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% LHS systems

% evaluate jacobian at GLL points
% mesh1.eval_p_der();
mesh1.eval_p_der_v2();

% evaluate material properties at GLL points
mesh1.eval_dom_xy(); 
eval.K = flow.darcy.material(k11_an, k12_an, k22_an, mesh1.eval.xy);

% evaluate metric terms for M11
eval.p = flow.darcy.metric(mesh1.eval.p, eval.K);

mesh1.eval.metric = eval.p;

M11 = zeros(2*p*(p+1),2*p*(p+1),ttl_nr_el);

for eln = 1:ttl_nr_el
    % this part is already implicitly multi threaded (it seems) and has 
    % to go to different clusters for further improvement in efficiency
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% 

E211    = incidence_matrix.E21(p);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 K^2]);

sys_3D = cell(1,ttl_nr_el);

tic
for eln = 1:ttl_nr_el
    sys_3D(eln) = {[M11(:,:,eln) E211'; E211 sparse(p^2,p^2)]};
end
toc

A = blkdiag(sys_3D{:});

sys_3D_inv = cell(1,ttl_nr_el);

tic
parfor eln = 1:ttl_nr_el
    sys_3D_inv(eln) = {inv([M11(:,:,eln) E211'; E211 sparse(p^2,p^2)])};
end
toc

tic
A_inv = blkdiag(sys_3D_inv{:});
toc

%% assembly of system

ttl_loc_dof = 2*p*(p+1)+p^2;
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);

%%

temp_GM_lam = gather_matrix.GM_boundary_LM_nodes_v2_internal(3,p);

R = full(spones(temp_GM_lam));

nr_Reg = 3^2;

for i = 1:nr_Reg
    temp_GM_lam2(:,(i-1)*3^2 +1: i * 3^2) = ((i-1) * 36 + temp_GM_lam).*R;
end

r = 3;

cc = 3;

temp_GM_lam2_2 = gather_matrix.GM_lambda_n_hybrid(r,cc,p);

xx = 324;
% yy = 

count = 0;

% bottom edges
for i = 1:3
    for j = 2:3
        reg = (i-1)*3 + j;
        count = count + 1;
        for Ri = 1:3
            eleID = (reg-1)*9 + (Ri-1)*3 + 1;
            temp_GM_lam2(1:p,eleID) = (xx + (count-1)*9 + (Ri-1)*3 + 1 : xx + (count-1)*9 + (Ri-1)*3 + 3);
        end
    end
end
   
% left edges
count = 0;
for i = 2:3
    for j = 1:3
        reg = (i-1)*3 + j;
        count = count + 1;
        for Ri = 1:3
            eleID = (reg-1)*9 + Ri;
            temp_GM_lam2(2*p+1:3*p,eleID) = (xx + 54 + (count-1)*9 + (Ri-1)*3 + 1 : xx + 54 + (count-1)*9 + (Ri-1)*3 + 3);
        end
    end
end

% right edges
for i = 1:2
    for j = 1:3
        reg = (i-1)*3 + j;
        for Ri = 1:3
            eleIDR = (reg-1)*9 + 2*3 + Ri;
            eleIDL = (reg-1 +3 )*9 + Ri;
            temp_GM_lam2(1*p+1:2*p,eleIDR) = temp_GM_lam2(2*p+1:3*p,eleIDL);
        end
    end
end

% top edges
for i = 1:3
    for j = 1:2
        reg = (i-1)*3 + j;
        for Ri = 1:3
            eleIDT = (reg-1)*9 + Ri * 3;
            eleIDB = reg * 9 + (Ri-1)*3 + 1;
            temp_GM_lam2(3*p+1:4*p,eleIDT) = temp_GM_lam2(1:p,eleIDB);
        end
    end
end


B = 1:9;
B = reshape(B,[3,3]);

for el = 1:3
    B2((el-1)*3+1:el*3,:) = (el-1)*9 + B;
end

B2 = B2(:);
B2(28:54) = 27 +B2(1:27);
B2(55:81) = 54 +B2(1:27);

for i = 1:81
    temp_GM_lam3(:,i) = temp_GM_lam2(:,B2(i));
end

AA = 1:p;

for i = 1:K
    eleid_bot = (i-1)*K + 1;
    temp_GM_lam3(1:p, eleid_bot) = AA + 2*K*(K-1)*p + (i-1)*p;
    
    eleid_right = K*(K-1) + i;
    temp_GM_lam3(p+1:2*p, eleid_right) = AA + 2*K*(K-1)*p + K*p + (i-1)*p;
    
    eleid_left = i;
    temp_GM_lam3(2*p+1:3*p, eleid_left) = AA + 2*K*(K-1)*p + 2*K*p + (i-1)*p;
    
    eleid_top = K + (i-1)*K;
    temp_GM_lam3(3*p+1:4*p, eleid_top) = AA + 2*K*(K-1)*p + 3*K*p + (i-1)*p;
end

ass_conn22 = AssembleMatrices2(temp_GM_lam3, GM_local_dof_q', N_3D);

ass_conn2 = ass_conn22;

%% RHS

volForm = twoForm_v2(mesh1);

F_3D = volForm.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + 2*p*(p+1);
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

%% calculate lambda 

% ii2 = [i1;i2;i3];
% jj2 = [j1;j2;j3];
% ss2 = [s1;s2;s3];
% 
% size_LHS2 = ttl_loc_dof*ttl_nr_el;
% A = sparse(ii2, jj2, ss2, size_LHS2, size_LHS2);

% spy(A)
% implementing dirichlet BCs
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];
ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);

%%

temp2 = full((ass_conn3 * A_inv*ass_conn3'));
spy(temp2)
% this1 = det(temp2)
% this2 = cond(temp2)

lambda = (ass_conn3 * A_inv*ass_conn3')\(ass_conn3*A_inv*RHS);

%% seperating lambda for each element 

lambda2 = [lambda; zeros(4*K*p,1)];

for eln = 1:ttl_nr_el
    lambda_h(:,eln) = lambda2(GM_lambda(:,eln));
end

%% 

X_h3 = cell(1,ttl_nr_el);

for eln = 1:ttl_nr_el
    X_h3(eln) = {sys_3D_inv{eln} * ([zeros(2*p*(p+1),1); F_3D(:,eln)] - [N'*lambda_h(:,eln); zeros(p^2,1)])};
end

%% separating cochains 

for eln = 1:ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:2*p*(p+1));
    p_h(:,eln) = X_h3{eln}(2*p*(p+1) + 1:2*p*(p+1)+ p^2);
end

%% implementing Dirichlet BC's

% LHS  = LHS(1:end-4*K*p,1:end-4*K*p);
% RHS3 = RHS2(1:end-4*K*p,1);
% 
% % cond(LHS)
% % condest(LHS)
% 
% %% solution
% 
% % figure
% % spy(LHS)
% % set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
% % export_fig('spy_hybrid_K3_N6_c03.pdf','-pdf','-r864','-painters','-transparent');
% 
% tic
% X_h = LHS\RHS3;
% toc
% 
% %% separating cochains
% 
% loc_nr_ed = 2*p*(p+1);
% loc_nr_pp = p^2;
% loc_nr_lm = 4*p;
% 
% q_h = zeros(loc_nr_ed, ttl_nr_el);
% p_h = zeros(loc_nr_pp, ttl_nr_el);
% lm_h = zeros(loc_nr_lm, ttl_nr_el);
% 
% GM_lambda2 = GM_lambda + ttl_loc_dof*ttl_nr_el;
% GM_lambda2 = GM_lambda2';
% X_h2 = [X_h; zeros(4*K*p,1)];
% 
% for eln = 1:ttl_nr_el
%     q_h(1:loc_nr_ed,eln) = X_h(GM_local_dof_q(eln,:));
%     p_h(1:loc_nr_pp,eln) = X_h(GM_local_dof_p(eln,:));
%     lm_h(1:loc_nr_lm,eln) = X_h2(GM_lambda2(eln,:));
% end

%% dual to primal dof

M22s = zeros(p^2,p^2,ttl_nr_el);

tic
parfor eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end
toc

%% post processing 

pf = 20;

mesh1.eval_pf_der(pf);

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

%% conservation of mass

parfor el = 1:K^2
    divQ(:,el) = E211 * q_h(:,el);
end

Err_mass = divQ - F_3D;
[xf_3D, yf_3D, massf_h] = volForm.reconstruction(Err_mass);
massf_h = permute(massf_h, [2 1 3]); % transpose along dim = 3.

mass_ex = zeros(size(xf_3D));
[error.mass] = error_processor_v3(mass_ex, massf_h, wfxy2, mesh1.eval.pf.ggg);

%% reconstruction pressure

p_h_2 = zeros(size(p_h));

parfor eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

[xf_3D, yf_3D, pf_h] = volForm.reconstruction(p_h_2);
pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

pp_ex = phi_an(xf_3D, yf_3D);
[error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln))
% end
% colorbar

%% reconstruction flux

surForm = oneForm_v2(mesh1);

rec = surForm.reconstruction(q_h);

rec_qx = rec.qx;
rec_qy = rec.qy;

qx_ex = q_x_an(xf_3D, yf_3D);
qy_ex = q_y_an(xf_3D, yf_3D);

[error.qx] = error_processor_v3(qx_ex, -rec_qx, wfxy2, mesh1.eval.pf.ggg);
[error.qy] = error_processor_v3(qy_ex, -rec_qy, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qx(:,:,eln))
% end
% colorbar

%% reconstruction div Q

[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(divQ);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

ff_ex = f_an(xf_3D, yf_3D);
[error.ff] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

%% Hdiv error

err_divQ = error.qx.sqre + error.qy.sqre + error.ff.sqre;
err_divQ = sqrt(err_divQ)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), ff_ex(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h(:,:,eln))
% end
% colorbar

%% H10 error 

% evaluate discrete grad p

% parfor el = 1:K^2
%     u_h(:,el) = E211' * p_h(:,el) + N' * lm_h(:,el);
% end
% 
% parfor el = 1:K^2
%     u_h_2(:,el) = M11(:,:,el)\u_h(:,el);
% end
% 
% gradp = surForm.reconstruction(u_h_2);
% 
% [error.gradp.x] = error_processor_v3(qx_ex, gradp.qx, wfxy2, mesh1.eval.pf.ggg);
% [error.gradp.y] = error_processor_v3(qy_ex, gradp.qy, wfxy2, mesh1.eval.pf.ggg);
% 
% err_H1p = err_H1(error.pp.sqre, error.gradp.x.sqre, error.gradp.y.sqre)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_x(:,:,eln))
% end
% colorbar

% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_y(:,:,eln))
% end
% colorbar

% move evaluation of domain pts function in calculation of jacobian file
% make one function for calculation of jacobian file, but that can store
% multiple polynomial degrees... 
% add the coordinates on in the reconstruction file
% add a plotting function for reconstruction figures