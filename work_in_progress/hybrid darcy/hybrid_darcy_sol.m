function [ X_h ] = hybrid_darcy_sol(K, p)
%HYBRID_DARCY_SOL Summary of this function goes here
%   Detailed explanation goes here

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);


a = 0.1;
k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

          
%%

xbound = [0 1];
ybound = [0 1];
c = 0.0;

ttl_nr_el = K^2;

local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% domain / mesh mapping and derivatives

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

mesh1.eval_p_der();

el.bounds.x = linspace(-1,1,K+1);
el.bounds.y = linspace(-1,1,K+1);

%% LHS systems

[xp, wp] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_x = zeros(p+1, p+1, ttl_nr_el);
eval_y = zeros(p+1, p+1, ttl_nr_el);

for elx = 1:K
    for ely = 1:K
        eln = (elx -1)*K +ely;
        [eval_x(:,:,eln),eval_y(:,:,eln)] = mesh1.el.mapping(xip,etap,elx,ely);
    end
end

eval.p.dx_dxii = mesh1.eval_p_dx_dxii;
eval.p.dx_deta = mesh1.eval_p_dx_deta;
eval.p.dy_dxii = mesh1.eval_p_dy_dxii;
eval.p.dy_deta = mesh1.eval_p_dy_deta;

eval.K.k11 = k11_an(eval_x, eval_y);
eval.K.k12 = k12_an(eval_x, eval_y);
eval.K.k22 = k22_an(eval_x, eval_y);
eval_detK = eval.K.k11.*eval.K.k22 - eval.K.k12.^2;

eval_p_ggg = metric.ggg_v2(eval.p);
eval.p.g11 = flow.darcy.g11K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g12 = flow.darcy.g12K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g22 = flow.darcy.g22K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);

M11 = zeros(local_nr_ed, local_nr_ed, ttl_nr_el);
M22 = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);

E211 = incidence_matrix.E21(p);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v4(p, eval.p, eln);
end

LHS_3D = zeros(2*p*(p+1) + p^2, 2*p*(p+1) + p^2, K^2);

for eln = 1:ttl_nr_el
    LHS_3D(:,:,eln) = [M11(:,:,eln) E211';
                      E211 zeros(p^2)];
end

q = num2cell(LHS_3D,[1,2]);
LHS_2D = blkdiag(q{:});

% spy(LHS_2D)
LHS_2D = sparse(LHS_2D);

ttl_loc_dof = 2*p*(p+1)+p^2;

GM_local_dof = gather_matrix.gather_matrix_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);

N = full(incidence_matrix.OutwardNormalMatrix(p));

N_3D = repmat(N,[1 1 K^2]);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);

ass_conn = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn zeros(2*K*p*(K+1),p^2)];

% full(ass_conn2);
% spy(ass_conn2)

LHS = [LHS_2D ass_conn2';
       ass_conn2 zeros(2*K*p*(K+1))];

%% RHS

F_3D = reduction.of2form_multi_element_v6(f_an, p, mesh1.domain, K, el.bounds.x, el.bounds.y);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + 2*p*(p+1);
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

RHS_con = zeros(2*K*p*(K+1),1);

RHS2 = [RHS; RHS_con];

%% implementing Dirichlet BC's

LHS2 = LHS(1:end-4*K*p,1:end-4*K*p);
RHS3 = RHS2(1:end-4*K*p,1);

%% solution

tic
X_h = LHS2\RHS3;
toc

end

