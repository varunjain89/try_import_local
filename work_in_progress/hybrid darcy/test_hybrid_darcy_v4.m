clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% analytical solution

% phi_an   = @(x,y) (x.^2 -8*x + 15).*(y.^2 -11*y + 28);
% dp_dx    = @(x,y) (2*x-8) .*(y.^2 -11*y + 28);
% dp_dy    = @(x,y) (2*y-11).*(x.^2 -8*x + 15);
% d2p_dxdy = @(x,y) (2*x-8).*(2*y-11);
% d2p_dx2  = @(x,y) 2*(y.^2 -11*y + 28);
% d2p_dy2  = @(x,y) 2*(x.^2 -8*x + 15);

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

% k11_an  = @(x,y) 4*ones(size(x));
% k12_an  = @(x,y) 3*ones(size(x));
% k21_an  = @(x,y) 3*ones(size(x));
% k22_an  = @(x,y) 5*ones(size(x));
% dk11_dx = @(x,y) 0*ones(size(x));
% dk12_dx = @(x,y) 0*ones(size(x));
% dk12_dy = @(x,y) 0*ones(size(x));
% dk22_dy = @(x,y) 0*ones(size(x));

a = 0.1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

% -K22(x,y).*dphi_dy(x,y) - K12(x,y).*dphi_dx(x,y),...  % x component
% K12(x,y).*dphi_dy(x,y) + K11(x,y).*dphi_dx(x,y));    % y component
                  
q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%%

xbound = [0 1];
ybound = [0 1];
c = 0.2;

%% discretization

K = 6;
p = 6;

ttl_nr_el = K^2;
ttl_nr_pp = K^2*p^2;
ttl_nr_ed = 2*K*p*(K*p + 1);

local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% domain / mesh mapping and derivatives

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

mesh1.eval_p_der();

el.bounds.x = linspace(-1,1,K+1);
el.bounds.y = linspace(-1,1,K+1);

%% LHS systems

[xp, wp] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_x = zeros(p+1, p+1, ttl_nr_el);
eval_y = zeros(p+1, p+1, ttl_nr_el);

for elx = 1:K
    for ely = 1:K
        eln = (elx -1)*K +ely;
        [eval_x(:,:,eln),eval_y(:,:,eln)] = mesh1.el.mapping(xip,etap,elx,ely);
    end
end

eval.p.dx_dxii = mesh1.eval_p_dx_dxii;
eval.p.dx_deta = mesh1.eval_p_dx_deta;
eval.p.dy_dxii = mesh1.eval_p_dy_dxii;
eval.p.dy_deta = mesh1.eval_p_dy_deta;

eval.K.k11 = k11_an(eval_x, eval_y);
eval.K.k12 = k12_an(eval_x, eval_y);
eval.K.k22 = k22_an(eval_x, eval_y);
eval_detK = eval.K.k11.*eval.K.k22 - eval.K.k12.^2;

eval_p_ggg = metric.ggg_v2(eval.p);
eval.p.g11 = flow.darcy.g11K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g12 = flow.darcy.g12K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);
eval.p.g22 = flow.darcy.g22K_v2(eval.p, eval_p_ggg, eval.K, eval_detK);

M11 = zeros(local_nr_ed, local_nr_ed, ttl_nr_el);
M22 = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);

E211 = incidence_matrix.E21(p);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v4(p, eval.p, eln);
end

LHS_3D = zeros(2*p*(p+1) + p^2, 2*p*(p+1) + p^2, K^2);

for eln = 1:ttl_nr_el
    LHS_3D(:,:,eln) = [M11(:,:,eln) E211';
                      E211 zeros(p^2)];
end

q = num2cell(LHS_3D,[1,2]);
LHS_2D = blkdiag(q{:});

% spy(LHS_2D)
LHS_2D = sparse(LHS_2D);

ttl_loc_dof = 2*p*(p+1)+p^2;

GM_local_dof = gather_matrix.gather_matrix_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);

N = full(incidence_matrix.OutwardNormalMatrix(p));

N_3D = repmat(N,[1 1 K^2]);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);

ass_conn = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn zeros(2*K*p*(K+1),p^2)];

% full(ass_conn2);
% spy(ass_conn2)

LHS = [LHS_2D ass_conn2';
       ass_conn2 zeros(2*K*p*(K+1))];

   % [is, js, ss] = find(ass_conn2);

%% RHS

F_3D = reduction.of2form_multi_element_v6(f_an, p, mesh1.domain, K, el.bounds.x, el.bounds.y);

for eln = 1:ttl_nr_el
    temp = (eln-1)*ttl_loc_dof + 2*p*(p+1);
    RHS(temp+1:temp+p^2,1) = F_3D(:,eln);
end

RHS_con = zeros(2*K*p*(K+1),1);

RHS2 = [RHS; RHS_con];

%% implementing Dirichlet BC's

LHS2 = LHS(1:end-4*K*p,1:end-4*K*p);
RHS3 = RHS2(1:end-4*K*p,1);

%% solution

tic
X_h = LHS2\RHS3;
toc

%% separating cochains

loc_nr_ed = 2*p*(p+1);
loc_nr_pp = p^2;

q_h = zeros(loc_nr_ed, ttl_nr_el);
p_h = zeros(loc_nr_pp, ttl_nr_el);

for eln = 1:ttl_nr_el
    q_h(1:loc_nr_ed,eln) = X_h(GM_local_dof_q(eln,:));
    p_h(1:loc_nr_pp,eln) = X_h(GM_local_dof_p(eln,:));
end

M22s = zeros(p^2,p^2,ttl_nr_el);

for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_2(p, eval_p_ggg(:,:,eln));
end

p_h_2 = zeros(size(p_h));

for eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

%% post processing 

mesh1.eval_pf_der();

pf = p+5;

gf = zeros(pf+1,pf+1,ttl_nr_el);

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

eval.pf.dx_dxii = mesh1.eval_pf_dx_dxii;
eval.pf.dx_deta = mesh1.eval_pf_dx_deta;
eval.pf.dy_dxii = mesh1.eval_pf_dy_dxii;
eval.pf.dy_deta = mesh1.eval_pf_dy_deta;

eval.pf.ggg = metric.ggg_v2(eval.pf);

[xf_3D, yf_3D, pf_h] = reconstruction.of2form_multi_element_v4(p_h_2, p, pf, mesh1.el.mapping, eval.pf.ggg, K);

figure
hold on
for eln = 1:ttl_nr_el
    contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln)')
end
colorbar

% ------- error calculation-----%

phi_an_h = phi_an(xf_3D, yf_3D);

err_phi = zeros(size(phi_an_h));

for eln = 1:ttl_nr_el
    err_phi(:,:,eln) = phi_an_h(:,:,eln) - pf_h(:,:,eln)';
end

err_phi = (err_phi).^2 .* wfxy2 .* eval.pf.ggg;
l2err_phi = sum(sum(sum(err_phi)));

l2err_phi2 = sqrt(l2err_phi);
l2err_phi2

error.p = error_processor(phi_an_h, pf_h, wfxy2, eval.pf.ggg)

%% reconstruction of 1-form 

% q_h   = oneForm;
% q_h.p = p;

half_edges = p*(p+1);

qx_h = zeros(p*(p+1),ttl_nr_el);
qy_h = zeros(size(qx_h));

for eln = 1:ttl_nr_el
    qx_h(:,eln) = q_h(1:half_edges,eln);
    qy_h(:,eln) = -q_h(half_edges+1:end,eln);
end

rec_qx = zeros(pf+1, pf+1, ttl_nr_el);
rec_qy = zeros(size(rec_qx));

xf_3D2 = zeros(size(rec_qx));
yf_3D2 = zeros(size(rec_qx));

for elx = 1:K
    for ely = 1:K
        eln = (elx-1)*K + ely;
        
        temp2 = reconstruction.reconstruct1xform_v3(qx_h(:,eln), qy_h(:,eln), p, pf, mesh1, eln);
        rec_qx(:,:,eln) = full(temp2)';
        
        temp2 = reconstruction.reconstruct1yform_v3(qx_h(:,eln), qy_h(:,eln), p, pf, mesh1, eln);
        rec_qy(:,:,eln) = full(temp2)';
                
        [xf, yf] = mesh1.el.mapping(xif,etaf,elx,ely);

        xf_3D2(:,:,eln) = xf;
        yf_3D2(:,:,eln) = yf;
    end
end

figure
hold on
for el = 1:K^2
    contourf(xf_3D2(:,:,el), yf_3D2(:,:,el), rec_qx(:,:,el))
end
title('calculated qx')
colorbar

figure
hold on
for el = 1:K^2
    contourf(xf_3D2(:,:,el), yf_3D2(:,:,el), rec_qy(:,:,el))
end
title('calculated qy')
colorbar

%% exact values

q_x_an_h = q_x_an(xf_3D2, yf_3D2);
q_y_an_h = q_y_an(xf_3D2, yf_3D2);

figure
hold on
for el = 1:K^2
    contourf(xf_3D2(:,:,el), yf_3D2(:,:,el), -q_x_an_h(:,:,el))
end
title('exact qx')
colorbar

figure
hold on
for el = 1:K^2
    contourf(xf_3D2(:,:,el), yf_3D2(:,:,el), -q_y_an_h(:,:,el))
end
title('exact qy')
colorbar