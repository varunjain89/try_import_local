clc
close all
clear variables

xx = linspace(0,1,101);

[xf, yf] = meshgrid(xx);

p = spectral_porosity(xx);
p = abs(p);

a = 2;
b = 0.3;

k = a*10.^(b*p);
k = real(k);

figure
plot(xx,p)


max(p)/min(p)