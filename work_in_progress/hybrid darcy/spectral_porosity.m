function [por] = spectral_porosity(x)
%SPECTRAL_POROSITY Summary of this function goes here
%   Detailed explanation goes here

% Created on : 22 Oct, 2018

nfx = 256;        % nf is number of fourier modes


r = 0.707;      % lacunarity or sparsity 
Df = 2.2;       % feractal parameter 
 
phi_nx = (2*pi) * rand(1,nfx);
% phi_ny = (2*pi) * rand(1,nfy);

por = 0;
for i = 1:nfx
%     for j = 1:nfy
        por = por + r^(Df*nfx).*cos(r^(-nfx)*x + phi_nx(i));
        por = por + r^(Df*nfx).*cos(r^(-nfx)*x - phi_nx(i));
%     end
end

end

