function [ error ] = hybrid_darcy_post_v2(K, p, X_h, phi_an, mesh1, el_bounds)
%HYBRID_DARCY_POST Summary of this function goes here
%   Detailed explanation goes here

% created on 16 October, 2018

%% post processing

pf = p+5;

%% processing potential 

% separating cochains
ttl_nr_el = K^2;
loc_nr_ed = 2*p*(p+1);
loc_nr_pp = p^2;

q_h = zeros(loc_nr_ed, ttl_nr_el);
p_h = zeros(loc_nr_pp, ttl_nr_el);

ttl_loc_dof = 2*p*(p+1)+p^2;

GM_local_dof = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);

for eln = 1:ttl_nr_el
    q_h(1:loc_nr_ed,eln) = X_h(GM_local_dof_q(eln,:));
    p_h(1:loc_nr_pp,eln) = X_h(GM_local_dof_p(eln,:));
end

eval_p_ggg = metric.ggg_v2(mesh1.eval.p);

M22s = zeros(p^2,p^2,ttl_nr_el);
for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v3(p, eval_p_ggg(:,:,eln), mesh1.basis, mesh1.wp);
end

p_h_2 = zeros(size(p_h));

for eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

%%

mesh1.eval_pf_der();

pf = p+5;

gf = zeros(pf+1,pf+1,ttl_nr_el);

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

eval.pf.dx_dxii = mesh1.eval_pf_dx_dxii;
eval.pf.dx_deta = mesh1.eval_pf_dx_deta;
eval.pf.dy_dxii = mesh1.eval_pf_dy_dxii;
eval.pf.dy_deta = mesh1.eval_pf_dy_deta;

eval.pf.ggg = metric.ggg_v2(eval.pf);

%%

pp_h = twoForm();

pp_h.domain = mesh1.domain;
pp_h.K = K;
pp_h.p = p;

pp_h.el_bounds_x = mesh1.el.bounds.x;
pp_h.el_bounds_y = mesh1.el.bounds.y;

pp_h.cochain    = p_h_2;
pp_h.pf         = pf;
pp_h.el_mapping   = mesh1.el.mapping;
pp_h.eval_pf_ggg  = eval.pf.ggg;

% [xf_3D, yf_3D, pf_h] = pp_h.reconstruction();
[xf_3D, yf_3D, pf_h] = pp_h.reconstruction(mesh1.basis);

pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

%% processing flux

qq = oneForm;
qq.K = K;
qq.p = p;
qq.pf = pf;

qq.reconstruction(q_h, ttl_nr_el, mesh1);

rec_qx = qq.rec_qx;
rec_qy = qq.rec_qy;

%% exact solution

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

a = 0.1;

k11_an  = @(x,y) (1e-3*x.^2 + y.^2 + a)./(x.^2 + y.^2 + a);
k12_an  = @(x,y) (1e-3 -1).*x.*y./(x.^2 + y.^2 + a);
k21_an  = @(x,y) k12(x,y);
k22_an  = @(x,y) (x.^2 + 1e-3*y.^2 + a)./(x.^2 + y.^2 + a);
dk11_dx = @(x,y) (2e-3*x.*(x.^2 + y.^2 +a)-2*x.*(1e-3*x.^2 + y.^2 + a))./(x.^2 + y.^2 + a).^2;
dk12_dx = @(x,y) ((1e-3 - 1)*y.*(x.^2 + y.^2 +a)-2*x.^2.*y*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk12_dy = @(x,y) ((1e-3 - 1)*x.*(x.^2 + y.^2 +a)-2*y.^2.*x*(1e-3-1))./(x.^2 + y.^2 + a).^2;
dk22_dy = @(x,y) (2e-3*y.*(x.^2 + y.^2 +a)-2*y.*(1e-3*y.^2 + x.^2 + a))./(x.^2 + y.^2 + a).^2;

q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);
q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);
          
qx_ex = q_x_an(xf_3D, yf_3D);
qy_ex = q_y_an(xf_3D, yf_3D);
pp_ex = phi_an(xf_3D, yf_3D);

%% error calculation 

error.pp = error_processor(pp_ex, pf_h, wfxy2, eval.pf.ggg);
error.qx = error_processor(qx_ex, -rec_qx, wfxy2, eval.pf.ggg);
error.qy = error_processor(qy_ex, -rec_qy, wfxy2, eval.pf.ggg);

%% Hdiv error

[pp_sqrt, pp_sqre] = error_processor_v2(pp_ex, pf_h, wfxy2, eval.pf.ggg);
[qx_sqrt, qx_sqre] = error_processor_v2(qx_ex, -rec_qx, wfxy2, eval.pf.ggg);
[qy_sqrt, qy_sqre] = error_processor_v2(qy_ex, -rec_qy, wfxy2, eval.pf.ggg);

% evaluate discrete divQ

E211 = incidence_matrix.E21(p);

for el = 1:K^2
    divQ(:,el) = E211 * q_h(:,el);
end

% reconstruct divQ using 2-form reconstruction

divQ_h = twoForm();

divQ_h.domain = mesh1.domain;
divQ_h.K = K;
divQ_h.p = p;

divQ_h.el_bounds_x = mesh1.el.bounds.x;
divQ_h.el_bounds_y = mesh1.el.bounds.y;

divQ_h.cochain    = divQ;
divQ_h.pf         = pf;
divQ_h.el_mapping   = mesh1.el.mapping;
divQ_h.eval_pf_ggg  = eval.pf.ggg;

[xf_3D, yf_3D, divQf_h] = divQ_h.reconstruction(mesh1.basis);

% for el = 1:K^2
%     
% end

% determine error

ff_ex = f_an(xf_3D, yf_3D);

divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

[ff_sqrt, ff_sqre] = error_processor_v2(ff_ex, divQf_h, wfxy2, eval.pf.ggg);

err_QHdiv = qx_sqre + qy_sqre + ff_sqre;
err_QHdiv = sqrt(err_QHdiv);

error.qH = err_QHdiv;

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), ff_ex(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h(:,:,eln))
% end
% colorbar

%% H1 error



end
