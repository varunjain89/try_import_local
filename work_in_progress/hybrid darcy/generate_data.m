clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% generate h-convergence data

nh = 100;
nh = 4;

for nK = 4:2:nh
    fname_p1 = sprintf('K_%d', nK);
    X_h.h.c00.p1.(genvarname([fname_p1])) = hybrid_darcy_sol_v3(nK,1);
    
    fname_p4 = sprintf('K_%d', nK);
    X_h.h.c00.p4.(genvarname([fname_p4])) = hybrid_darcy_sol_v3(nK,4);
    
    fname_p7 = sprintf('K_%d', nK);
    X_h.h.c00.p7.(genvarname([fname_p7])) = hybrid_darcy_sol_v3(nK,7);

    fname = sprintf('X_h_darcy_v2.mat');
%     save(fname,'X_h');
    nK
end

%% generate p-convergence data 

% nn = 30;
nn = 1;

for np = 1:nn
    fname_K1 = sprintf('N_%d', np);
    X_h.p.c00.K1.(genvarname([fname_K1])) = hybrid_darcy_sol_v3(1,np);
    
    fname_K5 = sprintf('N_%d', np);
    X_h.p.c00.K5.(genvarname([fname_K5])) = hybrid_darcy_sol_v3(5,np);
    
    fname_K9 = sprintf('N_%d', np);
    X_h.p.c00.K9.(genvarname([fname_K9])) = hybrid_darcy_sol_v3(9,np);
    
    fname = sprintf('X_h_darcy_v3.mat');
%     save(fname,'X_h');
    np
end

%% 

load('X_h_darcy_v2.mat')

for nK = 4:2:10
    for np = 1:3:7
        K = nK;
        p = np;
        c = 0.0;
        
        xbound = [0 1];
        ybound = [0 1];
        
        phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
        
        mesh1 = CrazyMesh(K,p,xbound,ybound,c);
        mesh1.eval_p_der();
        
        el.bounds.x = linspace(-1,1,K+1);
        el.bounds.y = linspace(-1,1,K+1);
        
        vname_K = sprintf('K_%d', K);
        vname_p = sprintf('p%d', p);
        
        error = hybrid_darcy_post(K, p, X_h.h.c00.(genvarname([vname_p])).(genvarname([vname_K])), phi_an, mesh1, el.bounds)
        nK
    end
end

hK = 1 ./(4:2:10);

% slope1 = (log10(err_h(1,nh-5))-log10(err_h(1,nh)))/(log10(h(nh-5))-log10(h(nh)))
% slope2 = (log10(err_h(2,nh-5))-log10(err_h(2,nh)))/(log10(h(nh-5))-log10(h(nh)))
% slope3 = (log10(err_h(3,nh-5))-log10(err_h(3,nh)))/(log10(h(nh-5))-log10(h(nh)))
% 
% figure
% loglog(h(5:end),err_h(1,5:end),'-o')
% hold on
% loglog(h(5:end),err_h(2,5:end),'-o')
% loglog(h(5:end),err_h(3,5:end),'-o')

