clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% test 1

patch = 4;

% mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
% dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
% dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
% dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
% dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

%% test 2

xbound = [0 1];
ybound = [0 1];
c = 0.0;

% mapping = @(xii,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xii, eta);
% dX_dxii = @(xii,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xii, eta);
% dX_deta = @(xii,eta) mesh.crazy_mesh.dx_deta(xbound, c, xii, eta);
% dY_dxii = @(xii,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xii, eta);
% dY_deta = @(xii,eta) mesh.crazy_mesh.dy_deta(ybound, c, xii, eta);

%% test 3

xbound = [0 1];
ybound = [0 1];
c = 0.4;

domain.mapping = @(xii,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xii, eta);
domain.dX_dxii = @(xii,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xii, eta);
domain.dX_deta = @(xii,eta) mesh.crazy_mesh.dx_deta(xbound, c, xii, eta);
domain.dY_dxii = @(xii,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xii, eta);
domain.dY_deta = @(xii,eta) mesh.crazy_mesh.dy_deta(ybound, c, xii, eta);

dd.bounds.x = linspace(-1,1,3+1);
dd.bounds.y = linspace(-1,1,3+1);
            
% mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain.mapping, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain.dX_dxii, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain.dX_deta, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain.dY_dxii, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain.dY_deta, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);

%% test 4

F_L_x = @(s,t) 1 + t *sqrt(2);
F_R_x = @(s,t) 0*ones(size(s));
F_B_x = @(s,t) cos(s*pi/2);
F_T_x = @(s,t) (1 + sqrt(2)) * cos(s * pi / 2) ;
F_L_y = @(s,t) 0*ones(size(s));
F_R_y = @(s,t) 1 + t *sqrt(2);
F_B_y = @(s,t) sin(s*pi/2);
F_T_y = @(s,t) (1 + sqrt(2)) * sin(s * pi / 2) ;

dFBx_ds = @(s,t) - (pi/2) * sin(s*pi/2);
dFTx_ds = @(s,t) - (pi/2) * (1 + sqrt(2)) * sin(s * pi / 2) ;
dFLx_dt = @(s,t) sqrt(2)*ones(size(s));
dFRx_dt = @(s,t) 0*ones(size(s));
dFBy_ds = @(s,t) (pi/2) * cos(s*pi/2);
dFTy_ds = @(s,t) (pi/2) * (1 + sqrt(2)) * cos(s * pi / 2) ;
dFLy_dt = @(s,t) 0*ones(size(s));
dFRy_dt = @(s,t) sqrt(2)*ones(size(s));

mapping = @(xii,eta) mesh.transfinite_mesh.mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
dX_dxii = @(xii,eta) mesh.transfinite_mesh.dX_dxii(xii,eta,F_L_x,F_R_x,dFBx_ds,dFTx_ds);
dX_deta = @(xii,eta) mesh.transfinite_mesh.dX_deta(xii,eta,F_B_x,F_T_x,dFLx_dt,dFRx_dt);
dY_dxii = @(xii,eta) mesh.transfinite_mesh.dY_dxii(xii,eta,F_L_y,F_R_y,dFBy_ds,dFTy_ds);
dY_deta = @(xii,eta) mesh.transfinite_mesh.dY_deta(xii,eta,F_B_y,F_T_y,dFLy_dt,dFRy_dt);

%% define mesh

p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

%% break the domain/patch into elements

K = 3;

Kx = K;
Ky = K;

ttl_nr_el = Kx * Ky;

pp = 3;

p_foil.discretize(Kx, Ky, pp);

%% visualize domain

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

%% plot domain

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        [x,y] = p_foil.el.mapping(xiip, etap, elx, ely);
        plot(x, y, '+')
    end
end

title('domain mapping')

%% plot region

xp2 = linspace(-1,1,30);
[xL,yL] = p_foil.domain.mapping(-1, xp2);
[xR,yR] = p_foil.domain.mapping(+1, xp2);
[xB,yB] = p_foil.domain.mapping(xp2, -1);
[xT,yT] = p_foil.domain.mapping(xp2, +1);

figure
hold on 

plot(xL, yL,'k')
plot(xR, yR,'k')
plot(xB, yB,'k')
plot(xT, yT,'k')

%% plot elements

tt = 0.98;
xp3 = linspace(-tt,tt,30);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        
        [xL,yL] = p_foil.el.mapping(-tt, xp3, elx, ely);
        [xR,yR] = p_foil.el.mapping(+tt, xp3, elx, ely);
        [xB,yB] = p_foil.el.mapping(xp3, -tt, elx, ely);
        [xT,yT] = p_foil.el.mapping(xp3, +tt, elx, ely);
        
        plot(xL, yL,'k')
        plot(xR, yR,'k')
        plot(xB, yB,'k')
        plot(xT, yT,'k')
    end
end

set(gca,'visible','off')
title('domain mapping')

%%

xp(1)   = -tt;
xp(end) = +tt;

[xiip2, etap2] = meshgrid(xp);

for elx = 1:Kx
    for ely = 1:Ky
        
        [xS,yS] = p_foil.el.mapping(xiip, etap, elx, ely);
        
        plot(xS, yS,'k')
        plot(xS', yS','k')
    end
end

%% add spectral lines

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        
        [xS,yS] = p_foil.el.mapping(xiip, etap, elx, ely);
        
        plot(xS, yS,'k')
        plot(xS', yS','k')
    end
end

%% check mappings

check.red_rec_2form(phi_an, p_foil);

%% construyct mass matrices

p_foil.eval_jacobian(pp);
p_foil.eval_metric();

M11 = mass_matrix.M1_v10(p_foil);

%% construct incidence matrix

E211     = incidence_matrix.E21(pp);
E211_3D  = repmat(full(E211),[1 1 ttl_nr_el]);
E211_3Dt = repmat(full(E211'),[1 1 ttl_nr_el]);

%% construct 3D system 

local_dof_t = 3*pp^2 + 2*pp;
local_dof_u = 2*pp^2 + 2*pp;
local_dof_p = pp^2;

SYS_3D = zeros(local_dof_t, local_dof_t, ttl_nr_el);

SYS_3D(1:local_dof_u,1:local_dof_u,:) = M11;
SYS_3D(local_dof_u+1:local_dof_t,1:local_dof_u,:) = E211_3D;
SYS_3D(1:local_dof_u,local_dof_u+1:local_dof_t,:) = E211_3Dt;

%% take inverse of 3D system

SYS_3Di = zeros(local_dof_t, local_dof_t, ttl_nr_el);

for eln = 1:ttl_nr_el
    SYS_3Di(:,:,eln) = inv(SYS_3D(:,:,eln));
end

A_cell = num2cell(SYS_3Di,[1,2]);
A_inv = blkdiag(A_cell{:});

%% gather matrices

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, local_dof_t);
GM_local_dof_q = GM_local_dof(:,1:local_dof_u)';
GM_local_dof_p = GM_local_dof(:,local_dof_u+1:local_dof_t);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,pp);

%% connectivity matrix 

lambda_tot = 2*K*pp*(K+1);
lambda_int = 2*K*pp*(K-1);

N = incidence_matrix.OutwardNormalMatrix(pp);
N_3D = repmat(full(N),[1 1 ttl_nr_el]);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q, N_3D);
ass_conn2 = [ass_conn2 zeros(lambda_tot,local_dof_p)];

% this part removes lagrange multipliers from the patch/region boundary
ass_conn3 = ass_conn2(1:lambda_int,:);

%% calculate RHS

%% reduction of source term

volForm = twoForm_v2(p_foil);

F_3D = volForm.reduction(f_an);

for eln = 1:ttl_nr_el
    temp = (eln-1)*local_dof_t + local_dof_u;
    RHS(temp+1:temp+local_dof_p,1) = F_3D(:,eln);
end

%% implement boundary conditions

% bottom edges

% for elx = 1:Kx
%     eln = 
%     for i = 1:pp
%         local_edge = 
%         edge_dof = 
%         for r = 1:pp+1
%             phi_bc = 
%             RHS(edge_dof) = RHS(edge_dof) + e(i,r)*phi_bc;
%         end
%     end
% end
% 
% % right edge
% 
% for ely = 1:Ky
%     eln = (Kx-1)*el
%     for j = 1:pp
%         local_edge = 
%         edge_dof = 
%         for s = 1:pp+1
%             phi_bc = 
%             RHS(edge_dof) = RHS(edge_dof) + e(j,s)*phi_bc;
%         end
%     end
% end
% 
% % top edges
% 
% for elx = 1:Kx
%     eln = 
%     for i = 1:pp
%         local_edge = 
%         edge_dof = 
%         for r = 1:pp+1
%             phi_bc = 
%             RHS(edge_dof) = RHS(edge_dof) + e(i,r)*phi_bc;
%         end
%     end
% end
% 
% % left edge
% 
% for ely = 1:Ky
%     eln = ely;
%     for j = 1:pp
%         local_edge = 
%         edge_dof = 
%         for s = 1:pp+1
%             phi_bc = 
%             RHS(edge_dof) = RHS(edge_dof) + e(j,s)*phi_bc;
%         end
%     end
% end


%% calculte lambda 

lambda = (ass_conn3 * A_inv*ass_conn3')\(ass_conn3*A_inv*RHS);

%% 

K = 100
N = 1

lam2D = 2 * K * N * (K-1)
full2D = K^2 *(3 * N^2 + 2 * N) + lam2D

lam2D / full2D

lam3D = 3 * K^2 * N^2 * (K-1)
full3D = K^3 *(4 * N^3 + 3 * N^2) + lam3D

lam3D / full3D
