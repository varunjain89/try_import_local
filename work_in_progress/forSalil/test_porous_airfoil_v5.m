clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

%% domain

nr_Region = 10;

for patch = 1:nr_Region
    region(patch).mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
    region(patch).dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
    region(patch).dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
    region(patch).dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
    region(patch).dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);
end

%% test region mappings

% [xp, wp] = GLLnodes(5);
% [xiip, etap] = meshgrid(xp);
% 
% figure
% hold on
% 
% for i = 1:10
%     [x,y] = region(i).mapping(xiip, etap);
%     plot(x,y,'+')
% end

%% discretization

K = 3;
p = 3;

ttl_nr_el = K^2;
local_edge = 2*p*(p+1);
local_surf = p^2;

%% define mesh

pfoil = PorousAirfoilMesh3(region);

%%

pfoil.discretize(K, K, p);

%% LHS systems

% evaluate_basis_dicretize_gauss
% evaluate_basis_dicretize_GLL

% evaluate_jacobian_dicretize_gauss
% evaluate_jacobian_dicretize_GLL

% evaluate metric terms

% evaluate jacobian at gauss points
pfoil.eval_jacobian_gauss(p);

% evaluate metric terms for M11
pfoil.eval_metric();

%% 

for eln = 1:ttl_nr_el*nr_Region
    M11(:,:,eln) = mass_matrix.M1_v7_gauss(pfoil, eln);
end

%% 

E211    = incidence_matrix.E21(p);

sys_3D = cell(1,ttl_nr_el*nr_Region);

tic
for eln = 1:ttl_nr_el*nr_Region
    sys_3D(eln) = {[M11(:,:,eln) E211'; E211 sparse(local_surf,local_surf)]};
end
toc

A = blkdiag(sys_3D{:});

sys_3D_inv = cell(1,ttl_nr_el);

tic
for eln = 1:ttl_nr_el*nr_Region
    sys_3D_inv(eln) = {inv([M11(:,:,eln) E211'; E211 sparse(local_surf,local_surf)])};
end
toc

tic
A_inv = blkdiag(sys_3D_inv{:});
toc

%% assembly of system

ttl_loc_dof     = local_edge + local_surf;
GM_local_dof    = gather_matrix.GM_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);
GM_local_dof_q  = GM_local_dof(:,1:local_edge);
GM_local_dof_p  = GM_local_dof(:,local_edge + 1 : ttl_loc_dof);
GM_lambda       = gather_matrix.GM_boundary_LM_nodes(K,p);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 ttl_nr_el]);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];

ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);
N_reg = ass_conn2(2*K*p*(K-1)+1:2*K*p*(K+1),:);

N_reg_3D = repmat(full(N_reg),[1 1 nr_Region]);

tic
for reg = 1:nr_Region
    connectivity(reg) = {ass_conn3};
end
toc

connectivity_el = blkdiag(connectivity{:});

GM_region = mesh.porous_airfoil.GM_patch(K,p);

GM_local_dof_reg    = gather_matrix.GM_total_local_dof_v3(nr_Region, ttl_loc_dof*ttl_nr_el);

connectivity_reg = AssembleMatrices2(GM_region, GM_local_dof_reg', N_reg_3D);

nr_inernal_edges = 17;
connectivity_reg2 = connectivity_reg(1:nr_inernal_edges * K * p,:);

connectivity_1 = [connectivity_el; connectivity_reg2];

%% boundary left 

% xp = xp(1:end-1);
% 
% figure
% hold on
% 
% [xB,yB] = region(1).mapping(xp,-1*ones(size(xp)));
% [xL,yL] = region(1).mapping(-1*ones(size(xp)),xp);
% [xT,yT] = region(1).mapping(xp, 1*ones(size(xp)));
% [xR,yR] = region(1).mapping( 1*ones(size(xp)),xp);
% 
% plot(xB,yB,'r')
% plot(xR,yR,'b')
% plot(xT,yT,'g')
% plot(xL,yL,'k')

% for el = 1:K
%     [x,y] = pfoil.domain(1).mapping(-1,xp);
% end

%% boundary edges left

% region 1 

for el = 1:K
    BC_left_edges1((el-1)*p + 1: (el-1)*p + p) = (el -1)*ttl_loc_dof + p*(p+1) + 1 : (el -1)*ttl_loc_dof + p*(p+1) + p;
end

% region 3

reg = 3;

for el = 1:K
    BC_left_edges2((el-1)*p + 1: (el-1)*p + p) = (reg-1)*ttl_nr_el*ttl_loc_dof+ (el-1)*ttl_loc_dof + p*(p+1) + 1 : (reg-1)*ttl_nr_el*ttl_loc_dof+ (el -1)*ttl_loc_dof + p*(p+1) + p;
end

BC_left = [BC_left_edges1 BC_left_edges2]; 

total_dof = nr_Region * ttl_nr_el * ttl_loc_dof;

RHS = sparse(total_dof,1);
RHS(BC_left) = 1;

%% calculate lambda 

% lambda 1 = [lambda_red + lambda_blue]

lambda1_LHS = (connectivity_1 * A_inv * connectivity_1');

lambda1 = (connectivity_1 * A_inv * connectivity_1')\(connectivity_1 * A_inv * RHS);

%% separate lambda element wise

%% this part also contains boundary condition - check this
lambda_bc = zeros(6*K*p,1);

%% separating lambda of each region 

lambda2 = [lambda1; lambda_bc];

internal_lambda = 2*K*p*(K-1);

GM_lambda3 = gather_matrix.GM_total_local_dof_v3(nr_Region, internal_lambda);

GM_lambda4 = nr_Region * internal_lambda + GM_region';

GM_lambda5 = [GM_lambda3 GM_lambda4]';

for eln = 1:nr_Region
    lambda_h(:,eln) = lambda2(GM_lambda5(:,eln));
end

lambda_h = full(lambda_h);

%%

for reg = 1:nr_Region
    temp = lambda_h(:,reg)';
    for eln = 1:ttl_nr_el
        eln2 = (reg-1)*ttl_nr_el + eln; 
        lambda_h2(:,eln2) = temp(GM_lambda(:,eln));
    end
end
%% 

X_h3 = cell(1,nr_Region*ttl_nr_el);

for eln = 1:nr_Region*ttl_nr_el
    X_h3(eln) = {sys_3D_inv{eln} * ([zeros(2*p*(p+1),1); F_3D(:,eln)] - [N'*lambda_h2(:,eln); zeros(p^2,1)])};
end

%% separating cochains 

for eln = 1:ttl_nr_el
    q_h(:,eln) = X_h3{eln}(1:2*p*(p+1));
    p_h(:,eln) = X_h3{eln}(2*p*(p+1) + 1:2*p*(p+1)+ p^2);
end

%% implementing Dirichlet BC's

% LHS  = LHS(1:end-4*K*p,1:end-4*K*p);
% RHS3 = RHS2(1:end-4*K*p,1);
% 
% % cond(LHS)
% % condest(LHS)
% 
% %% solution
% 
% % figure
% % spy(LHS)
% % set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
% % export_fig('spy_hybrid_K3_N6_c03.pdf','-pdf','-r864','-painters','-transparent');
% 
% tic
% X_h = LHS\RHS3;
% toc
% 
% %% separating cochains
% 
% loc_nr_ed = 2*p*(p+1);
% loc_nr_pp = p^2;
% loc_nr_lm = 4*p;
% 
% q_h = zeros(loc_nr_ed, ttl_nr_el);
% p_h = zeros(loc_nr_pp, ttl_nr_el);
% lm_h = zeros(loc_nr_lm, ttl_nr_el);
% 
% GM_lambda2 = GM_lambda + ttl_loc_dof*ttl_nr_el;
% GM_lambda2 = GM_lambda2';
% X_h2 = [X_h; zeros(4*K*p,1)];
% 
% for eln = 1:ttl_nr_el
%     q_h(1:loc_nr_ed,eln) = X_h(GM_local_dof_q(eln,:));
%     p_h(1:loc_nr_pp,eln) = X_h(GM_local_dof_p(eln,:));
%     lm_h(1:loc_nr_lm,eln) = X_h2(GM_lambda2(eln,:));
% end

%% dual to primal dof

M22s = zeros(p^2,p^2,ttl_nr_el);

tic
parfor eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end
toc

%% post processing 

pf = 20;

mesh1.eval_pf_der(pf);

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

%% conservation of mass

parfor el = 1:K^2
    divQ(:,el) = E211 * q_h(:,el);
end

Err_mass = divQ - F_3D;
[xf_3D, yf_3D, massf_h] = volForm.reconstruction(Err_mass);
massf_h = permute(massf_h, [2 1 3]); % transpose along dim = 3.

mass_ex = zeros(size(xf_3D));
[error.mass] = error_processor_v3(mass_ex, massf_h, wfxy2, mesh1.eval.pf.ggg);

%% reconstruction pressure

p_h_2 = zeros(size(p_h));

parfor eln = 1:ttl_nr_el
    p_h_2(:,eln) = M22s(:,:,eln)\p_h(:,eln);
end

[xf_3D, yf_3D, pf_h] = volForm.reconstruction(p_h_2);
pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

pp_ex = phi_an(xf_3D, yf_3D);
[error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

figure
hold on
for eln = 1:ttl_nr_el
    contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln))
end
colorbar

%% reconstruction flux

surForm = oneForm_v2(mesh1);

rec = surForm.reconstruction(q_h);

rec_qx = rec.qx;
rec_qy = rec.qy;

qx_ex = q_x_an(xf_3D, yf_3D);
qy_ex = q_y_an(xf_3D, yf_3D);

[error.qx] = error_processor_v3(qx_ex, -rec_qx, wfxy2, mesh1.eval.pf.ggg);
[error.qy] = error_processor_v3(qy_ex, -rec_qy, wfxy2, mesh1.eval.pf.ggg);

figure
hold on
for eln = 1:ttl_nr_el
    contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_qx(:,:,eln))
end
colorbar

%% reconstruction div Q

[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(divQ);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

ff_ex = f_an(xf_3D, yf_3D);
[error.ff] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

%% Hdiv error

err_divQ = error.qx.sqre + error.qy.sqre + error.ff.sqre;
err_divQ = sqrt(err_divQ)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), ff_ex(:,:,eln))
% end
% colorbar
% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h(:,:,eln))
% end
% colorbar

%% H10 error 

% evaluate discrete grad p

% parfor el = 1:K^2
%     u_h(:,el) = E211' * p_h(:,el) + N' * lm_h(:,el);
% end
% 
% parfor el = 1:K^2
%     u_h_2(:,el) = M11(:,:,el)\u_h(:,el);
% end
% 
% gradp = surForm.reconstruction(u_h_2);
% 
% [error.gradp.x] = error_processor_v3(qx_ex, gradp.qx, wfxy2, mesh1.eval.pf.ggg);
% [error.gradp.y] = error_processor_v3(qy_ex, gradp.qy, wfxy2, mesh1.eval.pf.ggg);
% 
% err_H1p = err_H1(error.pp.sqre, error.gradp.x.sqre, error.gradp.y.sqre)

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_x(:,:,eln))
% end
% colorbar

% 
% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), rec_gradp_y(:,:,eln))
% end
% colorbar

% move evaluation of domain pts function in calculation of jacobian file
% make one function for calculation of jacobian file, but that can store
% multiple polynomial degrees... 
% add the coordinates on in the reconstruction file
% add a plotting function for reconstruction figures