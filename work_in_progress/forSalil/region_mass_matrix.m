function [M11] = region_mass_matrix(pfoil)
%REGION_MASS_MATRIX Summary of this function goes here
%   Detailed explanation goes here

p = pfoil.p;
K = pfoil.K;

local_edge = 2*p*(p+1);
ttl_nr_el = K^2;

M11 = zeros(local_edge,local_edge,ttl_nr_el);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v7(pfoil, eln);
end


end

