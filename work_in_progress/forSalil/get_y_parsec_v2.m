function output_parsec = get_y_parsec_v2(p, x, side, derivative);

% For a specified set of PARSEC airfoil geometry, and the chordwise
% location, get the vertical coordinate, *or the derivative*
%
% Copyright Salil Luesutthiviboon (ANCE) 2019
% =========================================================================
%
% NEW IN v2
% ---------
% able to handle x as a [1xN] vector 
%
% INPUTS
% ------
% p: [1x11] PARSEC airfoil design variables, c.f. [1]
%
% x: chordwise location
%
% side: Er zijn 2 mogelijkheden
% 'lo' = pressure side ('lower')
% 'up' = suction side ('upper')
%
% derivative: just the point (0) or the derivative of the curve (1)
%
% OUTPUTS
% -------
% output_parsec: just the point (0) or the derivative of the curve (1) at
% the specified chordwise location and the specified side
%
% REFERENCES
% ----------
% [1] Sobieczky, H. (1999). Parametric airfoils and wings. In Recent
% development of aerodynamic design methodologies (pp. 71-87). Vieweg+
% Teubner Verlag.
% =========================================================================

    a = parsec_2(p);
    upper_collect = zeros(length(x),6);
    d_upper_collect = zeros(length(x),6);
    lower_collect = zeros(length(x),6);
    d_lower_collect = zeros(length(x),6);
    
    for j = 1:6
        upper_collect(:,j) = a(j).*x.^(j-0.5);
        d_upper_collect(:,j) = (j-0.5).*a(j).*x.^(j-1.5);
        lower_collect(:,j) = a(j+6).*x.^(j-0.5);
        d_lower_collect(:,j) = (j-0.5).*a(j+6).*x.^(j-1.5);
    end
    if derivative == 0
        y_upper = sum(upper_collect,2);
        y_lower = sum(lower_collect,2);
    else
        y_upper = sum(d_upper_collect,2);
        y_lower = sum(d_lower_collect,2);
    end
    switch side
        case 'up'
            output_parsec = y_upper;
        case 'lo'
            output_parsec = y_lower;
    end
    % end of program
end
