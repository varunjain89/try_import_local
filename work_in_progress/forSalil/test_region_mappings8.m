clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% test 1

patch = 4;

mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

%% test 5

% load ('p_PARSEC_SD7003.mat');
% 
% tt1 = get_y_parsec(p, 0.2, 'up', 0);
% tt2 = get_y_parsec(p, 0.8, 'up', 0);
% 
% F_L_x = @(s,t) t*(-2.5 - 0.25) + 0.25 ;
% F_L_y = @(s,t) t*(1.75 - tt1) + tt1;
% F_B_x = @(s,t) s*(0.8 - 0.2) +0.2;
% F_B_y = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 0);
% F_R_x = @(s,t) t*(4-0.8) + 0.8;
% F_R_y = @(s,t) t*(1.75 - tt2) + tt2';
% F_T_x = @(s,t) s*(4 + 2.5) - 2.5;  
% F_T_y = @(s,t) 1.75;
%  
% dFLx_dt = @(s,t) (-2.5 - 0.25);
% dFLy_dt = @(s,t) (1.75 - tt1);
% dFRx_dt = @(s,t) (4-0.8);
% dFRy_dt = @(s,t) (1.75 - tt2);
% dFTx_ds = @(s,t) (4 + 2.5);
% dFTy_ds = @(s,t) 0;
% dFBx_ds = @(s,t) (0.8 - 0.2);
% dFBy_ds = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 1);
% 
% mapping2 = @(xii,eta) mesh.transfinite_mesh.mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
% dX_dxii2 = @(xii,eta) mesh.transfinite_mesh.dX_dxii(xii,eta,F_L_x,F_R_x,dFBx_ds,dFTx_ds);
% dX_deta2 = @(xii,eta) mesh.transfinite_mesh.dX_deta(xii,eta,F_B_x,F_T_x,dFLx_dt,dFRx_dt);
% dY_dxii2 = @(xii,eta) mesh.transfinite_mesh.dY_dxii(xii,eta,F_L_y,F_R_y,dFBy_ds,dFTy_ds);
% dY_deta2 = @(xii,eta) mesh.transfinite_mesh.dY_deta(xii,eta,F_B_y,F_T_y,dFLy_dt,dFRy_dt);

%% define mesh

p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

% p_foil2 = PorousAirfoilMesh(mapping2,dX_dxii2,dX_deta2,dY_dxii2,dY_deta2);

%% break the domain/patch into elements

K = 4;

Kx = K;
Ky = K;

ttl_nr_el = Kx * Ky;

pp = 4

p_foil.discretize(Kx, Ky, pp);
% p_foil2.discretize(Kx, Ky, pp);

%% visualize domain

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

xiip2 = xiip(:);
etap2 = etap(:);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        for xx = 1:pp+1
            for yy = 1:pp+1
                [x,y] = p_foil.el.mapping(xiip(xx,yy), etap(xx,yy), elx, ely);
                plot(x, y, '+')
            end
        end
    end
end

title('domain mapping')

%% source term 

% volForm = twoForm(p_foil);
volForm = twoForm();

% reduction of source term

F_3D = volForm.reduction(f_an);

% reconstruction of source term

pf = 20;
p_foil.eval_pf_jacobian(pf);
p_foil.eval_pf_metric();

% p_foil2.eval_pf_jacobian(pf);
% p_foil2.eval_pf_metric();


%% plot calculated source term

[xf_3D, yf_3D, massf_h] = volForm.reconstruction(F_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), massf_h(:,:,eln)')
    end
end

colorbar
title('calculated solution')

%% plot exact source term

f_ex = f_an(xf_3D, yf_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln))
    end
end

title('exact solution')
colorbar

%% plot velocity vector 

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
       surf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln)-massf_h(:,:,eln)')
    end
end

title('difference')
colorbar

% check.red_rec_2form(f_an, p_foil);


