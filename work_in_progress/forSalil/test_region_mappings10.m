clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% test 1

patch = 4;

mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
% mappingxx = @(xii,eta) mesh.porous_airfoil.mapping3(xii,eta,patch);
dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

%% define mesh

p_foil = PorousAirfoilMesh2(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

%% break the domain/patch into elements

K = 2;

Kx = K;
Ky = K;

ttl_nr_el = Kx * Ky;

pp = 2;

p_foil.discretize(Kx, Ky, pp);
% p_foil2.discretize(Kx, Ky, pp);

%% visualize domain

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

% xiip2 = xiip(:);
% etap2 = etap(:);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        [x,y] = p_foil.el.mapping(xiip, etap, elx, ely);
%         [x2,y2] = mappingxx(xiip, etap);
        plot(x, y, '+')
    end
end

title('domain mapping')

%% source term 

volForm = twoForm_v3(p_foil);

% reduction of source term

F_3D = volForm.reduction(f_an)

% reconstruction of source term

pf = 20;
p_foil.eval_pf_jacobian(pf);
p_foil.eval_pf_metric();

% p_foil2.eval_pf_jacobian(pf);
% p_foil2.eval_pf_metric();


%% plot calculated source term

[xf_3D, yf_3D, massf_h] = volForm.reconstruction(F_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), massf_h(:,:,eln)')
    end
end

colorbar
title('calculated solution')

%% plot exact source term

f_ex = f_an(xf_3D, yf_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln))
    end
end

title('exact solution')
colorbar

%% plot velocity vector 

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
       surf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln)-massf_h(:,:,eln)')
    end
end

title('difference')
colorbar

% check.red_rec_2form(f_an, p_foil);


