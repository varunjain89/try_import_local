function output_parsec = get_y_parsec(p, x, side, derivative);

% For a specified set of PARSEC airfoil geometry, and the chordwise
% location, get the vertical coordinate, *or the derivative*
%
% Copyright Salil Luesutthiviboon (ANCE) 2019
% =========================================================================
%
% INPUTS
% ------
% p: [1x11] PARSEC airfoil design variables, c.f. [1]
%
% x: chordwise location
%
% side: Er zijn 2 mogelijkheden
% 'lo' = pressure side ('lower')
% 'up' = suction side ('upper')
%
% derivative: just the point (0) or the derivative of the curve (1)
%
% OUTPUTS
% -------
% output_parsec: just the point (0) or the derivative of the curve (1) at
% the specified chordwise location and the specified side
%
% REFERENCES
% ----------
% [1] Sobieczky, H. (1999). Parametric airfoils and wings. In Recent
% development of aerodynamic design methodologies (pp. 71-87). Vieweg+
% Teubner Verlag.
% =========================================================================

    a = parsec_2(p);
    upper_collect = [];
    d_upper_collect = [];
    lower_collect = [];
    d_lower_collect = [];
    for j = 1:6
        upper_collect = [upper_collect, a(j)*x^(j-0.5)];
        d_upper_collect = [d_upper_collect, (j-0.5)*a(j)*x^(j-1.5)];
        lower_collect = [lower_collect, a(j+6)*x^(j-0.5)];
        d_lower_collect = [d_lower_collect, (j-0.5)*a(j+6)*x^(j-1.5)];
    end
    if derivative == 0
        y_upper = sum(upper_collect);
        y_lower = sum(lower_collect);
    else
        y_upper = sum(d_upper_collect);
        y_lower = sum(d_lower_collect);
    end
    switch side
        case 'up'
            output_parsec = y_upper;
        case 'lo'
            output_parsec = y_lower;
    end
    % end of program
end
