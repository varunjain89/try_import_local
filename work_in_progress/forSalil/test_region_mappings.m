clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% define mesh

patch = 4;

mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

%% break the domain/patch into elements

K = 3;

Kx = K;
Ky = K;

p_foil.discretize(Kx, Ky);

%% visualize domain

K = 1;
pp = 4

ttl_nr_el = K^2;

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

% plot reference element
figure
plot(xiip,etap,'+')

% % plot complete domain
% figure
% hold on
% for patch = 1:10
% [x, y] = mesh.porous_airfoil.mapping(xiip,etap,patch);
% plot(x,y,'+')
% end

% plot domain
figure
[x,y] = p_foil.domain.mapping(xiip,etap);
plot(x,y,'+')

% plot domain element - by - element

figure
hold on

for ell = 1:ttl_nr_el
    [x,y] = p_foil.el.mapping()
end


%% determine metric terms

p_foil.eval_jacobian(pp,K);
p_foil.eval_metric();

%% make mass matrix

ttl_nr_el = 1;

M11 = zeros(2*pp*(pp+1),2*pp*(pp+1),ttl_nr_el);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v7(p_foil, eln);
end

%% make incidence matrix

E211 = incidence_matrix.E21(pp);

%% source term 

volForm = twoForm_v2(p_foil);

F_3D = volForm.reduction(f_an);

%% implement boundary conditions 

% bottom boundary 

% left boundary 

% top boundary

% right boundary

%% solve the system

%% post process pressure

%% post process velocities
