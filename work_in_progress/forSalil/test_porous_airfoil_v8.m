clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

%% user inpput for mappings

nr_Region = 10;

region(nr_Region).mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,nr_Region);

for patch = 1:nr_Region
    region(patch).mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
    region(patch).dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
    region(patch).dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
    region(patch).dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
    region(patch).dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);
end

pfoil = PorousAirfoilMesh5(region);

%% define discretization for each region

K = 2;
p = 3;

pfoil.discretize(K, K, p);

%% some random variables

nr_el_Rg = K^2;             
nr_ed_el = 2*p*(p+1);
nr_sf_el = p^2;

nr_tt_el    = nr_el_Rg*nr_Region;
ttl_loc_dof = nr_ed_el + nr_sf_el;
internal_lambda = 2*K*p*(K-1);


%% LHS systems

% evaluate jacobian at gauss points
pfoil.eval_jacobian_gauss(p);

% evaluate metric terms for M11
pfoil.eval_metric();

%% 

for eln = 1:nr_tt_el
    M11(:,:,eln) = mass_matrix.M1_v7_gauss(pfoil, eln);
end

E211    = incidence_matrix.E21(p);

%% assemble system 

sys_3D = cell(1,nr_tt_el);

for eln = 1:nr_tt_el
    sys_3D(eln) = {[M11(:,:,eln) E211'; E211 sparse(nr_sf_el,nr_sf_el)]};
end

A = blkdiag(sys_3D{:});

% spy(A)

sys_3D_inv = cell(1,nr_tt_el);

for eln = 1:nr_tt_el
    sys_3D_inv(eln) = {inv([M11(:,:,eln) E211'; E211 sparse(nr_sf_el,nr_sf_el)])};
end

A_inv = blkdiag(sys_3D_inv{:});

%% assembly of system

connectivity_el = incidence_matrix.connectivity_elements(nr_Region, K, p, nr_el_Rg, ttl_loc_dof, nr_ed_el);

GM_local_dof    = gather_matrix.GM_total_local_dof_v3(nr_el_Rg, ttl_loc_dof);
GM_local_dof_q  = GM_local_dof(:,1:nr_ed_el);
GM_local_dof_p  = GM_local_dof(:,nr_ed_el + 1 : ttl_loc_dof);
GM_lambda       = gather_matrix.GM_boundary_LM_nodes(K,p);

%% user input

GM_region        = mesh.porous_airfoil.GM_patch(K,p);

%%

GM_local_dof_reg = gather_matrix.GM_total_local_dof_v3(nr_Region, ttl_loc_dof*nr_el_Rg);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 nr_el_Rg]);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];
N_reg = ass_conn2(2*K*p*(K-1)+1:2*K*p*(K+1),:);
N_reg_3D = repmat(full(N_reg),[1 1 nr_Region]);

connectivity_reg = AssembleMatrices2(GM_region, GM_local_dof_reg', N_reg_3D);

nr_inernal_edges = 17;
connectivity_reg2 = connectivity_reg(1:nr_inernal_edges * K * p,:);

connectivity_1 = [connectivity_el; connectivity_reg2];

%% boundary conditions left

% region 1 

% BC_left = @(x,y) .......
    

for el = 1:K
    BC_left_edges1((el-1)*p + 1: (el-1)*p + p) = (el -1)*ttl_loc_dof + p*(p+1) + 1 : (el -1)*ttl_loc_dof + p*(p+1) + p;
end

% region 3

reg = 3;

for el = 1:K
    BC_left_edges2((el-1)*p + 1: (el-1)*p + p) = (reg-1)*nr_el_Rg*ttl_loc_dof+ (el-1)*ttl_loc_dof + p*(p+1) + 1 : (reg-1)*nr_el_Rg*ttl_loc_dof+ (el -1)*ttl_loc_dof + p*(p+1) + p;
end

BC_left = [BC_left_edges1 BC_left_edges2]; 

total_dof = nr_Region * nr_el_Rg * ttl_loc_dof;

RHS = sparse(total_dof,1);
RHS(BC_left) = 1;

%% calculate lambda 

% lambda 1 = [lambda_red + lambda_blue]

lambda1_LHS = (connectivity_1 * A_inv * connectivity_1');

% coefficients for reconstruction 
lambda1 = (connectivity_1 * A_inv * connectivity_1')\(connectivity_1 * A_inv * RHS);

%% separate lambda element wise, %% separating lambda of each region 

lambda_bc = zeros(6*K*p,1);
lambda2 = [lambda1; lambda_bc];



GM_lambda3 = gather_matrix.GM_total_local_dof_v3(nr_Region, internal_lambda);
GM_lambda4 = nr_Region * internal_lambda + GM_region';
GM_lambda5 = [GM_lambda3 GM_lambda4]';

for eln = 1:nr_Region
    lambda_h(:,eln) = lambda2(GM_lambda5(:,eln));
end

lambda_h = full(lambda_h);

%% internal solution for element 

for reg = 1:nr_Region
    temp = lambda_h(:,reg)';
    for eln = 1:nr_el_Rg
        eln2 = (reg-1)*nr_el_Rg + eln; 
        lambda_h2(:,eln2) = temp(GM_lambda(:,eln));
    end
end

X_h3 = cell(1,nr_tt_el);

F_3D = zeros(p^2,nr_tt_el);

for eln = 1:nr_tt_el
    X_h3(eln) = {sys_3D_inv{eln} * ([zeros(2*p*(p+1),1); F_3D(:,eln)] - [N'*lambda_h2(:,eln); zeros(p^2,1)])};
end

%% separating cochains 

for eln = 1:nr_tt_el
    u_h(:,eln) = X_h3{eln}(1:2*p*(p+1));
    p_h(:,eln) = X_h3{eln}(2*p*(p+1) + 1:2*p*(p+1)+ p^2);
end

%% dual to primal dof

M22s = zeros(p^2,p^2,nr_tt_el);

for eln = 1:nr_tt_el
    M22s(:,:,eln) = mass_matrix.M2_v5_pfoil(pfoil,eln);
end

pf = p + 5;

pfoil.evaluate_jacobian_reconstruct_gauss(pf);

pfoil.eval_pf_metric();

%% 

[xp] = Gnodes(pf);
[xiip, etap] = meshgrid(xp);



reconstruct2form_v4(p_h(:,1), basis, pfoil.eval.pf.ggg(:,:,1))


4563