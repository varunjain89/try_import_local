clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% test 1

patch = 4;

mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

%% test 5

lin_map = @(x1, x2, z) z*(x2 - x1) + x1;
dlin_map = @(x1, x2) (x2 - x1);

load ('p_PARSEC_SD7003.mat');

tt1 = get_y_parsec(p, 0.2, 'up', 0);
tt2 = get_y_parsec(p, 0.8, 'up', 0);

F_L_x = @(s,t) lin_map(0.25, -2.5, t);
F_L_y = @(s,t) lin_map(tt1, 1.75, t);
F_B_x = @(s,t) lin_map(0.2, 0.8, s);
F_B_y = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 0);
F_R_x = @(s,t) lin_map(0.8, 4, t);
F_R_y = @(s,t) lin_map(tt2, 1.75, t);
F_T_x = @(s,t) lin_map(-2.5, 4, s);
F_T_y = @(s,t) lin_map(1.75, 1.75, s);
 
dFLx_dt = @(s,t) dlin_map(0.25, -2.5);
dFLy_dt = @(s,t) dlin_map(tt1, 1.75);
dFBx_ds = @(s,t) dlin_map(0.2, 0.8);
dFBy_ds = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 1);
dFRx_dt = @(s,t) dlin_map(0.8, 4);
dFRy_dt = @(s,t) dlin_map(tt2, 1.75);
dFTx_ds = @(s,t) dlin_map(-2.5, 4);
dFTy_ds = @(s,t) dlin_map(1.75, 1.75);

% 
% mapping = @(xii,eta) mesh.transfinite_mesh.mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
% dX_dxii = @(xii,eta) mesh.transfinite_mesh.dX_dxii(xii,eta,F_L_x,F_R_x,dFBx_ds,dFTx_ds);
% dX_deta = @(xii,eta) mesh.transfinite_mesh.dX_deta(xii,eta,F_B_x,F_T_x,dFLx_dt,dFRx_dt);
% dY_dxii = @(xii,eta) mesh.transfinite_mesh.dY_dxii(xii,eta,F_L_y,F_R_y,dFBy_ds,dFTy_ds);
% dY_deta = @(xii,eta) mesh.transfinite_mesh.dY_deta(xii,eta,F_B_y,F_T_y,dFLy_dt,dFRy_dt);

%% define mesh

p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

%% break the domain/patch into elements

K = 4;

Kx = K;
Ky = K;

ttl_nr_el = Kx * Ky;

pp = 4

p_foil.discretize(Kx, Ky, pp);

%% visualize domain

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        for xx = 1:pp+1
            for yy = 1:pp+1
                [x,y] = p_foil.el.mapping(xiip(xx,yy), etap(xx,yy), elx, ely);
                plot(x, y, '+')
            end
        end
    end
end

title('domain mapping')

%% source term 

volForm = twoForm_v2(p_foil);

% reduction of source term

F_3D = volForm.reduction(f_an);

% reconstruction of source term

pf = 20;
p_foil.eval_pf_jacobian(pf);
p_foil.eval_pf_metric();

%% plot calculated source term

[xf_3D, yf_3D, massf_h] = volForm.reconstruction(F_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), massf_h(:,:,eln)')
    end
end

colorbar
title('calculated solution')

%% plot exact source term

f_ex = f_an(xf_3D, yf_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln))
    end
end

title('exact solution')
colorbar

%% plot velocity vector 

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
       surf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln)-massf_h(:,:,eln)')
    end
end

title('difference')
colorbar

% check.red_rec_2form(f_an, p_foil);


