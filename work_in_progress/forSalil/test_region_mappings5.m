clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')
addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% test 1

patch = 4;

mapping = @(xii,eta) mesh.porous_airfoil.mapping(xii,eta,patch);
dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

%% test 2

% xbound = [0 1];
% ybound = [0 1];
% c = 0.0;
% 
% mapping = @(xii,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xii, eta);
% dX_dxii = @(xii,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xii, eta);
% dX_deta = @(xii,eta) mesh.crazy_mesh.dx_deta(xbound, c, xii, eta);
% dY_dxii = @(xii,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xii, eta);
% dY_deta = @(xii,eta) mesh.crazy_mesh.dy_deta(ybound, c, xii, eta);

%% test 3

% xbound = [0 1];
% ybound = [0 1];
% c = 0.4;
% 
% domain.mapping = @(xii,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xii, eta);
% domain.dX_dxii = @(xii,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xii, eta);
% domain.dX_deta = @(xii,eta) mesh.crazy_mesh.dx_deta(xbound, c, xii, eta);
% domain.dY_dxii = @(xii,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xii, eta);
% domain.dY_deta = @(xii,eta) mesh.crazy_mesh.dy_deta(ybound, c, xii, eta);
% 
% dd.bounds.x = linspace(-1,1,3+1);
% dd.bounds.y = linspace(-1,1,3+1);
%             
% mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain.mapping, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain.dX_dxii, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain.dX_deta, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain.dY_dxii, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);
% dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain.dY_deta, dd.bounds.x, dd.bounds.y, 2, 2, xi, eta);

%% define mesh

p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

%% break the domain/patch into elements

K = 2;

Kx = K;
Ky = K;

ttl_nr_el = Kx * Ky;

pp = 8

p_foil.discretize(Kx, Ky, pp);

%% visualize domain

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        [x,y] = p_foil.el.mapping(xiip, etap, elx, ely);
        plot(x, y, '+')
    end
end


%% determine metric terms

% p_foil.eval_jacobian(pp);
% p_foil.eval_metric();

% %% make mass matrix
% 
% M11 = zeros(2*pp*(pp+1),2*pp*(pp+1),ttl_nr_el);
% 
% for eln = 1:ttl_nr_el
%     M11(:,:,eln) = mass_matrix.M1_v7(p_foil, eln);
% end

%% make incidence matrix

E211 = incidence_matrix.E21(pp);

%% source term 

volForm = twoForm_v2(p_foil);

% reduction of source term

F_3D = volForm.reduction(f_an);

% reconstruction of source term

pf = 20;
p_foil.eval_pf_jacobian(pf);
p_foil.eval_pf_metric();

% plot calculated source term

[xf_3D, yf_3D, massf_h] = volForm.reconstruction(F_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), massf_h(:,:,eln)')
    end
end

colorbar

% plot exact source term

f_ex = f_an(xf_3D, yf_3D);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        eln = (elx -1) * Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln))
    end
end

colorbar


%% implement boundary conditions 

% bottom boundary 

% left boundary 

% top boundary

% right boundary

%% solve the system

%% post process pressure

%% post process velocities
