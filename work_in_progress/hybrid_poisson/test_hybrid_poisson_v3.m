%% 27 March, 2019 
%% i'm trying n - level hybridmethod to further reduce the problem size

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% analytical solution

phi_an = @(x,y) sin(2*pi*x).*sin(2*pi*y);
u_an   = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
v_an   = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
f_an   = @(x,y) -8*pi*pi*sin(2*pi*x).*sin(2*pi*y);

xbound = [0 1];
ybound = [0 1];
c = 0.0;

%% discretization

K = 9;
p = 3;

ttl_nr_el = K^2;
ttl_nr_pp = K^2*p^2;
ttl_nr_ed = 2*K*p*(K*p + 1);

local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% domain mappings

%% mesh mapping and derivatives

element_bounds_x = linspace(-1,1,K+1);
element_bounds_y = linspace(-1,1,K+1);

domain_mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

el_mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain_mapping, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain_dX_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain_dX_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain_dY_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain_dY_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);

%% LHS systems

%% calculate metric terms

[xp, wp] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_p_dx_dxii(:,:,el) = el_dX_dxii(xip,etap,elx,ely);
        eval_p_dx_deta(:,:,el) = el_dX_deta(xip,etap,elx,ely);
        eval_p_dy_dxii(:,:,el) = el_dY_dxii(xip,etap,elx,ely);
        eval_p_dy_deta(:,:,el) = el_dY_deta(xip,etap,elx,ely);
    end
end

eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);
eval_p_g11 = metric.g11(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g12 = metric.g12(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g22 = metric.g22(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);


M11 = zeros(local_nr_ed, local_nr_ed, ttl_nr_el);
M22 = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);

E211 = incidence_matrix.E21(p);

for el = 1:ttl_nr_el
    M11(:,:,el) = mass_matrix.M1_3(p, eval_p_g11(:,:,el), eval_p_g12(:,:,el), eval_p_g22(:,:,el));
end

LHS_3D = zeros(2*p*(p+1) + p^2, 2*p*(p+1) + p^2, K^2);

for el = 1:K^2
    LHS_3D(:,:,el) = [M11(:,:,el) E211';
                      E211 zeros(p^2)];
end

q = num2cell(LHS_3D,[1,2]);
LHS_2D = blkdiag(q{:});

% spy(LHS_2D)

LHS_2D = sparse(LHS_2D);

ttl_loc_dof = 2*p*(p+1)+p^2;
ttl_nr_el = K^2;

GM_local_dof = gather_matrix.gather_matrix_total_local_dof_v3(ttl_nr_el, ttl_loc_dof);


N = full(incidence_matrix.OutwardNormalMatrix(p));

N_3D = repmat(N,[1 1 K^2]);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof_q = GM_local_dof(:,1:2*p*(p+1));
GM_local_dof_p = GM_local_dof(:,2*p*(p+1)+1:2*p*(p+1)+p^2);

ass_conn = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn zeros(2*K*p*(K+1),p^2)];

%% 

% LHS = [LHS_2D ass_conn2';
%        ass_conn2 zeros(2*K*p*(K+1))];

% [is, js, ss] = find(ass_conn2);

%% RHS

F_3D = reduction.of2form_multi_element_5(f_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);

for el = 1:ttl_nr_el
    RHS((el-1)*ttl_loc_dof + 2*p*(p+1)+1 : (el-1)*ttl_loc_dof + 2*p*(p+1)+ p^2,1) = F_3D(:,el);
end

% RHS_con = zeros(2*K*p*(K+1),1);

% RHS2 = [RHS; RHS_con];

%% calculating for lambda's

% implementing dirichlet BCs
ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);

% tic
% A1 = inv(LHS_2D);
% toc

II = sparse(eye(size(LHS_2D)));

tic
A1 = LHS_2D\II;
toc

temp2 = full((ass_conn3 * A1*ass_conn3'));
spy(temp2)
this1 = det(temp2)
this2 = cond(temp2)

lambda = (ass_conn3*A1*ass_conn3')\(ass_conn3*A1*RHS);

%% 

X2_h = A1*(RHS - ass_conn3'*lambda);

%% implementing Dirichlet BC's 

% LHS2 = LHS(1:end-4*K*p,1:end-4*K*p);
% RHS3 = RHS2(1:end-4*K*p,1);

%% solution 

% tic
% X_h = LHS2\RHS3;
% toc

%% 

loc_nr_ed = 2*p*(p+1);
loc_nr_pp = p^2;

q_h = zeros(loc_nr_ed, ttl_nr_el);
p_h = zeros(loc_nr_pp, ttl_nr_el);

for el = 1:ttl_nr_el
    q_h(1:loc_nr_ed,el) = X2_h(GM_local_dof_q(el,:));
    p_h(1:loc_nr_pp,el) = X2_h(GM_local_dof_p(el,:));
end

for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, eval_p_ggg(:,:,el));
end

for el = 1:ttl_nr_el
    p_h_2(:,el) = M22s(:,:,el)\p_h(:,el);
end

%%

%% post processing 

pf = 30;

gf = zeros(pf+1,pf+1,K^2);

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, K^2);

eval_pf_dx_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dx_deta = zeros(pf+1,pf+1,K^2);
eval_pf_dy_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dy_deta = zeros(pf+1,pf+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_pf_dx_dxii(:,:,el) = el_dX_dxii(xif,etaf,elx,ely);
        eval_pf_dx_deta(:,:,el) = el_dX_deta(xif,etaf,elx,ely);
        eval_pf_dy_dxii(:,:,el) = el_dY_dxii(xif,etaf,elx,ely);
        eval_pf_dy_deta(:,:,el) = el_dY_deta(xif,etaf,elx,ely);
    end
end

eval_pf_ggg = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
eval_pf_g11 = metric.g11(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta, eval_pf_ggg);
eval_pf_g12 = metric.g12(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta, eval_pf_ggg);
eval_pf_g22 = metric.g22(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta, eval_pf_ggg);

[xf_3D, yf_3D, pf_h] = reconstruction.of2form_multi_element_v4(p_h_2, p, pf, el_mapping, eval_pf_ggg, K);

figure
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), pf_h(:,:,el)')
end
colorbar

% ------- error calculation-----%

% figure
% hold on
% for el = 1:K^2
%     plot(xf_3D(:,:,el), yf_3D(:,:,el),'+')
% end



phi_an_h = phi_an(xf_3D, yf_3D);

for el = 1:K^2
    err_phi(:,:,el) = phi_an_h(:,:,el) - pf_h(:,:,el)';
end

err_phi = (err_phi).^2 .* wfxy2 .* eval_pf_ggg;
l2err_phi = sum(sum(sum(err_phi)));

l2err_phi2 = sqrt(l2err_phi)

%% reconstruction of 1-form 

half_edges = p*(p+1);

for el = 1:K^2
    qx_h(:,el) = q_h(1:half_edges,el);
    qy_h(:,el) = -q_h(half_edges+1:end,el);
end


for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;

        temp = reconstruction.reconstruct1xform_2(qx_h(:,el), qy_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));
        rec_qx(:,:,el) = full(temp)';
        
        temp = reconstruction.reconstruct1yform_2(qx_h(:,el), qy_h(:,el), p, pf, eval_pf_dx_dxii(:,:,el), eval_pf_dx_deta(:,:,el), eval_pf_dy_dxii(:,:,el), eval_pf_dy_deta(:,:,el));
        rec_qy(:,:,el) = full(temp)';
                
        [xf, yf] = el_mapping(xif,etaf,elx,ely);

        xf_3D2(:,:,el) = xf;
        yf_3D2(:,:,el) = yf;
    end
end

figure
hold on
for el = 1:K^2
    contourf(xf_3D2(:,:,el), yf_3D2(:,:,el), rec_qx(:,:,el))
end
colorbar

figure
hold on
for el = 1:K^2
    contourf(xf_3D2(:,:,el), yf_3D2(:,:,el), rec_qy(:,:,el))
end
colorbar


