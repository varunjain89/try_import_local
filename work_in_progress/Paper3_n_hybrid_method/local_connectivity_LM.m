function [N3] = local_connectivity_LM(p,mesh,K_connecting_edges,level)

p_eff = K_connecting_edges *p;

nr_int_lambda = 2 * p_eff * (mesh(level+1).K.Kx - 1);

local_bott = 0*p_eff + (1:p_eff);
local_rght = 1*p_eff + (1:p_eff);
local_left = 2*p_eff + (1:p_eff);
local_topp = 3*p_eff + (1:p_eff);

global_bott = nr_int_lambda + local_bott;
global_rght = nr_int_lambda + local_rght;
global_left = nr_int_lambda + local_left;
global_topp = nr_int_lambda + local_topp;

nr_loc_lambda = nr_int_lambda + 4*p_eff;

nr_boundary_lambda = 4*p_eff;

N_bott = sparse(local_bott, global_bott,-1,nr_boundary_lambda,nr_loc_lambda);
N_topp = sparse(local_topp, global_topp,+1,nr_boundary_lambda,nr_loc_lambda);
N_left = sparse(local_left, global_left,-1,nr_boundary_lambda,nr_loc_lambda);
N_rght = sparse(local_rght, global_rght,+1,nr_boundary_lambda,nr_loc_lambda);

N3 = N_bott + N_topp + N_left + N_rght;

end

