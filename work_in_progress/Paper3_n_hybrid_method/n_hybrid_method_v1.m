clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% define mapping
% 
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

mesh1 = mmesh2(domain);

%% check domain mapping

% mesh1.check_domain_mapping();

%% discretize the domain

K_coarse = 3;
K_fine = 2;

K = K_fine;
Kx = K;
Ky = K;

p = 3;

mesh1.discretize(Kx, Ky);

ttl_nr_el_coarse = K_coarse^2;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;
ttl_lambda = 2*K*p*(K+1);
intl_lambda = 2*K*p*(K-1);

ttl_loc_dof_fine = ttl_loc_dof * K_fine^2;

%% check element mappings

% mesh1.check_element_mapping();

%% check manufactured solution

%% 1. make local systems 

% -------- mass matrix

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh1.evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

M11 = zeros(loc_dof_u, loc_dof_u, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ---------- incidence matrix 

E211 = full(incidence_matrix.E21(p));

% ---------- connectivity matrix 1

GM_lambda = gather_matrix.GM_boundary_LM_nodes(K,p);
GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh1.ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 mesh1.ttl_nr_el]);
con_E = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
con_E = [con_E zeros(2*K*p*(K+1),loc_dof_p)];

con_E1 = full(con_E(1:intl_lambda,:));
con_E2 = full(con_E(intl_lambda+1:end,:));

nr_coarse_elements = 9;

N_3D2 = repmat(full(con_E2),[1 1 nr_coarse_elements]);

GM_local_dof2   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el_coarse, ttl_loc_dof_fine);

GM_lambda_coarse = gather_matrix.GM_boundary_LM_nodes(K_coarse,K_fine*p);

con_E3 = full(AssembleMatrices2(GM_lambda_coarse, GM_local_dof2', N_3D2));

% ------- make local systems for elements -------

A = zeros(ttl_loc_dof,ttl_loc_dof,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    A(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    inv_A(:,:,eln) = A(:,:,eln)\I;
end

sys_3D_inv(mesh1.ttl_nr_el) = {inv_A(:,:,mesh1.ttl_nr_el)};

for eln = 1:mesh1.ttl_nr_el
    sys_3D_inv(eln) = {inv_A(:,:,eln)};
end

% inv_A = blkdiag(sys_3D_inv{:});
inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);