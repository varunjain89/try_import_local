function [temp25] = element_map(local_map, K_fine,K_coarse)

K3 = K_fine;
K2 = K_coarse;

temp75 = reshape(local_map,K3,K3);

for i = 1:K2^2
    GM_level_1(:,:,i) = (i-1) * K3^2 + temp75;
end

GM_level_1 = reshape(GM_level_1,K3,K3,K2,K2);

temp24 = permute(GM_level_1, [1 3 2 4]);

temp25 = temp24(:);

end

