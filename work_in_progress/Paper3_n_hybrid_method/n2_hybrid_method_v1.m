clc
% close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% manufactured solution

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);
ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

%% define mapping

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

nr_levels = 1;

K1 = 1;
K(1).x = K1;
K(1).y = K1;

p = 1;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

ttl_nr_BCs = K1 * p * 4;

%% domain definition

mesh = n_hybrid_mesh.empty;

for lvl = 1:nr_levels
    % define domain
    if lvl ==1
        mesh(lvl) = n_hybrid_mesh(domain);
    else
        mesh(lvl) = n_hybrid_mesh(mesh(lvl-1).el);
    end
    
    % define refinement
    mesh(lvl).discretize(K(lvl).x, K(lvl).y);
    
    % check element mappings
%     mesh(lvl).check_element_mapping();
end

%% element map

%% make local system

% ------- calculate metric terms -------

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A_3D = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    A_3D(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);

A1 = AssembleMatrices2(GM_local_dof', GM_local_dof', A_3D);

%% connectivity matrices

% ----- N1 -----

N1 = -incidence_matrix.OutwardNormalMatrix(p);
full(N1)

N1 = [N1 zeros(size(N1,1),1)];

% ----- check -----
A1 = [M11 E211'; E211 zeros(p^2)]

LHS1 = [A1 N1'; N1 zeros(size(N1,1))];
full(LHS1)

% ----- N2 -----

N2 = eye(4*p);

N2(2,2) = -1;
N2(4,4) = -1;

N2

LHS2 = [A1 N1' zeros(size(A1,1),size(N2,1));
        N1 zeros(size(N1,1)) N2';
        zeros(size(N2,1),size(A1,2)) N2 zeros(size(N2,1))];

full(LHS2)



%% RHS - f

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

dof_f = zeros(loc_dof_p, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh(nr_levels).el(eln).mapping, basis_dis, M22);
end

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

RHS = [RHS1; zeros(size(E(1).E,1),1);zeros(size(E(2).E,1),1);zeros(size(E(3).E,1),1)];

%% ------- reconstruction metric -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh(nr_levels).evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

%% reconstruction of source term 

% for eln = 1:mesh(nr_levels).ttl_nr_el
%
%     rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
%
%     ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
%
% end

% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'linecolor','none')
% end
%
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'linecolor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [700, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'linecolor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

%% n-hybrid calculations 

for eln = 1:mesh(nr_levels).ttl_nr_el
    A1_inv(:,:,eln) = inv(A_3D(:,:,eln));
end

A1_inv_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A1_inv);

A2 = E(1).E * A1_inv_2D * E(1).E';

A2_inv_2D = inv(A2);

A3 = E(2).E * A2_inv_2D * E(2).E';

A3_inv_2D = inv(A3);

A4 = E(3).E * A3_inv_2D * E(3).E';

A4_inv_2D = inv(A4);

lambda3 = -A4\(E(3).E * A3_inv_2D * E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1);

lambda2 = -A3\(E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1 + E(3).E'*lambda3);

lambda1 = A2\(E(1).E * A1_inv_2D * RHS1 + E(2).E'*lambda2);

X = A1\(RHS1 - E(1).E'*lambda1);

%% LHS - full system 

% LHS = [A1     E(1).E' zeros(size(A1,1), size(E(2).E,1))   zeros(size(A1,1), size(E(3).E,1));
%        E(1).E   zeros(size(E(1).E,1))   E(2).E' zeros(size(E(1).E,1),size(E(3).E,1));
%        zeros(size(E(2).E,1),size(A1,1))    E(2).E  zeros(size(E(2).E,1))   E(3).E';
%        zeros(size(E(3).E,1),size(A1,1))    zeros(size(E(3).E,1),size(E(1).E,1))   E(3).E  zeros(size(E(3).E,1))];

%% solution full system 

% X = LHS\RHS;

X1 = X(1:size(A1,1));

for eln = 1:mesh(nr_levels).ttl_nr_el
    p_h(:,eln) = X1(GM_local_dof_p(:,eln));
end

%% pressure dual to primal

p_h_2 = zeros(size(p_h));

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

%% reconstruction

for eln = 1:mesh(nr_levels).ttl_nr_el 
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
end

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'linecolor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'linecolor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln),'linecolor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])
