function [A_i, LHS_j] = n_hybrid_A_i(A_j,E_j)

sz = size(E_j,2);

LHS_j = E_j * A_j * E_j';
inv_LHS_j = inv(LHS_j);

A_i = A_j * (speye(sz) - E_j' * inv_LHS_j * E_j * A_j);

end

