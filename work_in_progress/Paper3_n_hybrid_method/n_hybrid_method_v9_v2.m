clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% manufactured solution

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);
ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

%% define mapping
%
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

nr_levels = 2;

K1 = 2;
K(1).x = K1;
K(1).y = K1;

K2 = 1;
K(2).x = K2;
K(2).y = K2;

p = 1;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

ttl_nr_BCs = K1 * K2 *p * 4;

%%

mesh = n_hybrid_mesh.empty;

for lvl = 1:nr_levels
    % define domain
    if lvl ==1
        mesh(lvl) = n_hybrid_mesh(domain);
    else
        mesh(lvl) = n_hybrid_mesh(mesh(lvl-1).el);
    end
    
    % define refinement
    mesh(lvl).discretize(K(lvl).x, K(lvl).y);
    
    % check element mappings
    %     mesh(levels).check_element_mapping();
end

%% element map

% level 1

temp75 = reshape(1:K2^2,K2,K2);

for i = 1:K1^2
    GM_level_2(:,:,i) = (i-1) * K2^2 + temp75;
end

GM_level_2 = reshape(GM_level_2,K2,K2,K1,K1);

temp24 = permute(GM_level_2, [1 3 2 4]);
element_map_1 = temp24(:);

% level 2 

element_map_2 = 1:K1^2

%% make mass matrices

% ------- calculate metric terms -------

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A_3D = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    A_3D(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    inv_A(:,:,eln) = A_3D(:,:,eln)\I;
end

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);

inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);

GM_local_dof = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);
A1 = AssembleMatrices2(GM_local_dof', GM_local_dof', A_3D);

% figure
% spy(inv_A)

%% connectivity matrices

% the new unit noormal matrix / or the second local connectivity matrix is
% given by the (number of local boundaries \times nr_local_lambda)
% therefore the 3D matrix is (number of local boundaries \times nr_local_lambda \times number of coarse blocks)

% first you create local unit normal vector
% then you create GM for lambda_i
% then you create GM for lambda_i-1

%%

GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
GM_local_dof_p = GM_local_dof(:,loc_dof_u+1:loc_dof_u+p^2)';

for lvl = 1:nr_levels
    % first we create connectivity for most refined level
    % this is last level in the program, but defined as 1st level in the
    % document
    
    level = nr_levels + 1 - lvl;
    
    % ------ gather matrix 1 ------
    
    if lvl == 1
        GM(lvl).l2 = GM_local_dof_q';
    else
        GM(lvl).l2 = gather_matrix.GM_total_local_dof_v3(mesh(level).ttl_nr_el, ttl_nr_loc_lambda)';
    end
    
    % ------ gather matrix 2 ------
    if lvl == 1
        K_connecting_edges = 1;
    else
        K_connecting_edges = K_connecting_edges * K(level+1).x;
    end
    
    GM_connecting_lm = gather_matrix.GM_boundary_LM_nodes(K(level).x,K_connecting_edges*p);
    
    ttl_nr_loc_lambda = (max(max(GM_connecting_lm)));
    
    for i = 1:mesh(level).ttl_nr_domain
        GM(lvl).local_lambda(:,(i-1)*mesh(level).lcl_nr_el +1:i * mesh(level).lcl_nr_el) = (i-1) * ttl_nr_loc_lambda + GM_connecting_lm;
    end
    
    % ------ local connectivity matrix ------
    if lvl == 1
        N(lvl).local = incidence_matrix.OutwardNormalMatrix(p);
    else
        local_bott = 1:K_connecting_edges*p;
        local_rght = K_connecting_edges*p + (1:K_connecting_edges*p);
        local_left = 2*K_connecting_edges*p + (1:K_connecting_edges*p);
        local_topp = 3*K_connecting_edges*p + (1:K_connecting_edges*p);
        
        nr_int_lambda = 2*K_connecting_edges * p * (mesh(level+1).K.Kx - 1);
        
        global_bott = nr_int_lambda + local_bott;
        global_rght = nr_int_lambda + local_rght;
        global_left = nr_int_lambda + local_left;
        global_topp = nr_int_lambda + local_topp;
        
        nr_loc_lambda = nr_int_lambda + 4*K_connecting_edges*p;
        
        N_bott = sparse(local_bott, global_bott,-1,4*K_connecting_edges*p,nr_loc_lambda);
        N_topp = sparse(local_topp, global_topp,+1,4*K_connecting_edges*p,nr_loc_lambda);
        N_left = sparse(local_left, global_left,-1,4*K_connecting_edges*p,nr_loc_lambda);
        N_rght = sparse(local_rght, global_rght,+1,4*K_connecting_edges*p,nr_loc_lambda);
        
        N3 = N_bott + N_topp + N_left + N_rght;
        %         N3 = N_bott + N_topp
        N(lvl).local = N3;
    end
    
    N2(lvl).N = repmat(full(N(lvl).local),[1 1 mesh(level).ttl_nr_el]);
    E(lvl).E = AssembleMatrices2(GM(lvl).local_lambda, GM(lvl).l2, N2(lvl).N);
    
    %     figure
    %     spy(E(lvl).E)
end

% adding pressure degrees of freedom in the last element
E(1).E = [E(1).E zeros(size(E(1).E,1),p^2)];

%% removing boundary conditions at all levels

% ------------- level 1

K_eff = K1*K2;

index_BC_left_level_1 = GM(1).local_lambda(2*p+1:3*p,element_map_1(1:K_eff));
index_BC_rght_level_1 = GM(1).local_lambda(1*p+1:2*p,element_map_1(K_eff^2 - K_eff + 1: K_eff^2));
index_BC_bott_level_1 = GM(1).local_lambda(1:p,element_map_1(1:K_eff:(K_eff-1)*K_eff+1));
index_BC_topp_level_1 = GM(1).local_lambda(3*p+1:4*p,element_map_1(K_eff:K_eff:K_eff^2));

index_BC_left_level_1 = index_BC_left_level_1(:);
index_BC_rght_level_1 = index_BC_rght_level_1(:);
index_BC_bott_level_1 = index_BC_bott_level_1(:);
index_BC_topp_level_1 = index_BC_topp_level_1(:);

index_BC_level_1 = [index_BC_left_level_1; index_BC_rght_level_1; index_BC_bott_level_1; index_BC_topp_level_1];

E(1).E(index_BC_level_1,:) = [];

% temp23 = 2*K3*p*(K3+1);
% temp23 = temp23 * (K2*K3)^2;
% temp23 = temp23 - ttl_nr_BCs
% 
% if size(E(1).E,1) ~= temp23
%     disp('Error !!! Size of connecting matrix is incorrect !!!')
%     pause;
%     return
% else
%     disp('At least E1 is correct ;-) ')
% end

% ------------- level 2

p2= K2*p

K_eff = K1;

index_BC_left_level_2 = GM(2).local_lambda(2*p2+1:3*p2,element_map_2(1:K_eff));
index_BC_rght_level_2 = GM(2).local_lambda(1*p2+1:2*p2,element_map_2(K_eff^2 - K_eff+1: K_eff^2));
index_BC_bott_level_2 = GM(2).local_lambda(1:p2,element_map_2(1:K_eff:(K_eff-1)*K_eff+1));
index_BC_topp_level_2 = GM(2).local_lambda(3*p2+1:4*p2,element_map_2(K_eff:K_eff:K_eff^2));

index_BC_left_level_2 = index_BC_left_level_2(:);
index_BC_rght_level_2 = index_BC_rght_level_2(:);
index_BC_bott_level_2 = index_BC_bott_level_2(:);
index_BC_topp_level_2 = index_BC_topp_level_2(:);

index_BC_level_2 = [index_BC_left_level_2; index_BC_rght_level_2; index_BC_bott_level_2; index_BC_topp_level_2];

E(2).E(index_BC_level_2,:) = [];
E(2).E(:,index_BC_level_1) = [];

%%

LHS_temp = [A1      E(1).E' zeros(20,4);
            E(1).E  zeros(8)   E(2).E';
            zeros(4,20)   E(2).E zeros(4)];

det(LHS_temp)

A2  = E(1).E * inv(A1) * E(1).E';
det(A2)

A3 = E(2).E * inv(A2) * E(2).E';
det(A3)
        
%%

% removing dirichlet boundaries

% % E(1).E = E(1).E(1:2*mesh.K.Kx*p*(mesh.K.Kx -1),:);
% K(1).x * K(2).x * K(3).x;
% 2*K(1).x * K(2).x * K(3).x * p * (K(1).x -1);
% 
% E(3).E = E(3).E(1:2*K(1).x * K(2).x * K(3).x * p * (K(1).x -1),:);
% 
% E(3).E(:,index_BC_level_2) = [];

%% RHS - f

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

dof_f = zeros(loc_dof_p, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh(nr_levels).el(eln).mapping, basis_dis, M22);
end

% ------- reconstruction -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh(nr_levels).evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

% for eln = 1:mesh(nr_levels).ttl_nr_el
%
%     rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
%
%     ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
%
% end

% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'linecolor','none')
% end
%
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'linecolor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [700, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'linecolor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

%%

lambda_2 = A3\(-E(2).E * inv(A2) * E(1).E * inv(A1) * RHS1)

lambda_1 = A2\(E(1).E * inv(A1) * RHS1 + E(2).E' * lambda_2)

% RHS = [RHS1; zeros(size(E(1).E,1),1)];
RHS = [RHS1; zeros(size(E(1).E,1),1); zeros(size(E(2).E,1),1)];
% RHS = [RHS1; zeros(size(E(1).E,1),1);zeros(size(E(2).E,1),1);zeros(size(E(3).E,1),1)];

%%

% for eln = 1:mesh(nr_levels).ttl_nr_el
%     A1_inv(:,:,eln) = inv(A_3D(:,:,eln));
% end
% 
% A1_inv_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A1_inv);
% 
% A2 = E(1).E * A1_inv_2D * E(1).E';
% 
% det(A2)
% 
% % pause
% % return
% 
% A2_inv_2D = inv(A2);
% 
% A3 = E(2).E * A2_inv_2D * E(2).E';
% 
% det(A3)
% 
% % pause
% % return
% 
% A3_inv_2D = inv(A3);
% 
% A4 = E(3).E * A3_inv_2D * E(3).E';
% 
% A4_inv_2D = inv(A4);
% 
% lambda3 = -A4\(E(3).E * A3_inv_2D * E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1);
% 
% lambda2 = -A3\(E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1 + E(3).E'*lambda3);
% 
% lambda1 = A2\(E(1).E * A1_inv_2D * RHS1 + E(2).E'*lambda2);
% 
% A1 = A_2D;



%% X = A1\(RHS1 - E(1).E'*lambda_1);

% figure
% spy(A1_inv_2D)
%
% figure
% spy(A2)
%
% figure
% spy(A3)
%
% figure
% spy(A4)

%%
% LHS = [A_2D     E(1).E' zeros(size(A_2D,1), size(E(2).E,1))   zeros(size(A_2D,1), size(E(3).E,1));
%        E(1).E   zeros(size(E(1).E,1))   E(2).E' zeros(size(E(1).E,1),size(E(3).E,1));
%        zeros(size(E(2).E,1),size(A_2D,1))    E(2).E  zeros(size(E(2).E,1))   E(3).E';
%        zeros(size(E(3).E,1),size(A_2D,1))    zeros(size(E(3).E,1),size(E(1).E,1))   E(3).E  zeros(size(E(3).E,1))];
% det(LHS)
%
% condest(LHS)
%
% figure
% spy(LHS)
%
% %%
X = LHS_temp\RHS;
%
% X1 = X(1:size(A_2D,1));

X1 = X;

for eln = 1:mesh(nr_levels).ttl_nr_el
    p_h(:,eln) = X1(GM_local_dof_p(:,eln));
end

p_h_2 = zeros(size(p_h));

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

%% reconstruction

for eln = 1:mesh(nr_levels).ttl_nr_el
    
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
    
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
    
end

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'linecolor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'linecolor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln),'linecolor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])

% figure
% hold on

% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

