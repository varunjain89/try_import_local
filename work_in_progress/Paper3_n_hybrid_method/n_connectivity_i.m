function [EN_ii] = n_connectivity_i(K_macro_elements,K_local_elements,K_finee_elements,ass_conn2,p,ttl_loc_dof,int_lam)

effective_p = K_finee_elements * p;
nr_int_lam = 2*K_local_elements*effective_p*(K_local_elements-1);

total_nr_macro_elements = K_macro_elements^2;

%% Step 1: create local connectiviity 

if K_finee_elements == 1
    N = incidence_matrix.OutwardNormalMatrix(p);
    N = [N zeros(size(N,1),p^2)];
else
    N = ass_conn2(int_lam+1:end,:);
end

N_3D = repmat(full(N),[1 1 K_local_elements^2]);

GM_local_lambda = gather_matrix.GM_boundary_LM_nodes(K_local_elements,effective_p);
GM_local_dof_qq = gather_matrix.GM_total_local_dof_v3(K_local_elements^2, ttl_loc_dof*K_finee_elements^2);
local_connectivity = AssembleMatrices2(GM_local_lambda, GM_local_dof_qq', N_3D);

EN = local_connectivity(1:nr_int_lam,:);
EN2_3D = repmat(full(EN),[1 1 total_nr_macro_elements]);

%% Step 2: expand local connectivity to macro elements 

% K_macro_elements = K1;
% K_local_elements = K2;
% K_finee_elements = K3;

GM_lam2 = gather_matrix.GM_total_local_dof_v3(total_nr_macro_elements, nr_int_lam);

loc_dof_in = ttl_loc_dof*(K_local_elements*K_finee_elements)^2;
GM_local_dof3   = gather_matrix.GM_total_local_dof_v3(total_nr_macro_elements, loc_dof_in);

EN_ii = AssembleMatrices2(GM_lam2', GM_local_dof3', EN2_3D);

end

