clc
close all
clear variables

addpath('../..')
% addpath('../../MSEM')
% addpath('../../export_fig')
% addpath('Salil_mapping')

%% analytical solution

phi_an   = @(x,y) sin(pi*x).*sin(pi*y);
dp_dx    = @(x,y) pi*cos(pi*x).*sin(pi*y);
dp_dy    = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2p_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2p_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2p_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);

k11_an  = @(x,y) 1*ones(size(x));
k12_an  = @(x,y) 0*ones(size(x));
k21_an  = @(x,y) 0*ones(size(x));
k22_an  = @(x,y) 1*ones(size(x));
dk11_dx = @(x,y) 0*ones(size(x));
dk12_dx = @(x,y) 0*ones(size(x));
dk12_dy = @(x,y) 0*ones(size(x));
dk22_dy = @(x,y) 0*ones(size(x));

%%

q_y_an = @(x,y)  k11_an(x,y).*dp_dx(x,y) + k12_an(x,y).*dp_dy(x,y);
q_x_an = @(x,y) -k22_an(x,y).*dp_dy(x,y) - k12_an(x,y).*dp_dx(x,y);

f_an   = @(x,y) dk11_dx(x,y).*dp_dx(x,y) + k11_an(x,y).*d2p_dx2(x,y)...
              + dk12_dx(x,y).*dp_dy(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk12_dy(x,y).*dp_dx(x,y) + k12_an(x,y).*d2p_dxdy(x,y)...
              + dk22_dy(x,y).*dp_dy(x,y) + k22_an(x,y).*d2p_dy2(x,y);

%% test 1

patch = 4;

mapping = @(xii,eta) mesh.porous_airfoil.mapping_plot(xii,eta,patch);
dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,patch);
dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,patch);
dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,patch);
dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,patch);

%% test 5

lin_map = @(x1, x2, z) z*(x2 - x1) + x1;
dlin_map = @(x1, x2) (x2 - x1);

load ('p_PARSEC_SD7003.mat');

tt1 = get_y_parsec(p, 0.2, 'up', 0);
tt2 = get_y_parsec(p, 0.8, 'up', 0);

F_L_x = @(s,t) lin_map(0.25, -2.5, t);
F_L_y = @(s,t) lin_map(tt1, 1.75, t);
F_B_x = @(s,t) lin_map(0.2, 0.8, s);
F_B_y = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 0);
F_R_x = @(s,t) lin_map(0.8, 4, t);
F_R_y = @(s,t) lin_map(tt2, 1.75, t);
F_T_x = @(s,t) lin_map(-2.5, 4, s);
F_T_y = @(s,t) lin_map(1.75, 1.75, s);
 
dFLx_dt = @(s,t) dlin_map(0.25, -2.5);
dFLy_dt = @(s,t) dlin_map(tt1, 1.75);
dFBx_ds = @(s,t) dlin_map(0.2, 0.8);
dFBy_ds = @(s,t) get_y_parsec(p, s*(0.8 - 0.2) +0.2, 'up', 1);
dFRx_dt = @(s,t) dlin_map(0.8, 4);
dFRy_dt = @(s,t) dlin_map(tt2, 1.75);
dFTx_ds = @(s,t) dlin_map(-2.5, 4);
dFTy_ds = @(s,t) dlin_map(1.75, 1.75);
 
% mapping = @(xii,eta) mesh.transfinite_mesh.mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y);
% dX_dxii = @(xii,eta) mesh.transfinite_mesh.dX_dxii(xii,eta,F_L_x,F_R_x,dFBx_ds,dFTx_ds);
% dX_deta = @(xii,eta) mesh.transfinite_mesh.dX_deta(xii,eta,F_B_x,F_T_x,dFLx_dt,dFRx_dt);
% dY_dxii = @(xii,eta) mesh.transfinite_mesh.dY_dxii(xii,eta,F_L_y,F_R_y,dFBy_ds,dFTy_ds);
% dY_deta = @(xii,eta) mesh.transfinite_mesh.dY_deta(xii,eta,F_B_y,F_T_y,dFLy_dt,dFRy_dt);

%% define mesh

p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);

%% break the domain/patch into elements

K = 3;

Kx = K;
Ky = K;

ttl_nr_el = Kx * Ky;

pp = 3

p_foil.discretize(Kx, Ky, pp);

%% visualize domain

[xp, wp] = GLLnodes(pp);
[xiip, etap] = meshgrid(xp);

figure
hold on

for elx = 1:Kx
    for ely = 1:Ky
        for xx = 1:pp+1
            for yy = 1:pp+1
                [x,y] = p_foil.el.mapping(xiip(xx,yy), etap(xx,yy), elx, ely);
                plot(x, y, '+')
            end
        end
    end
end

title('domain mapping')

%% plot region

xp2 = linspace(-1,1,30);
[xL,yL] = p_foil.domain.mapping(-1*ones(size(xp2)), xp2);
[xR,yR] = p_foil.domain.mapping(+1*ones(size(xp2)), xp2);
[xB,yB] = p_foil.domain.mapping(xp2, -1*ones(size(xp2)));
[xT,yT] = p_foil.domain.mapping(xp2, +1*ones(size(xp2)));

figure
hold on 

plot(xL, yL,'k')
plot(xR, yR,'k')
plot(xB, yB,'k')
plot(xT, yT,'k')

title('region/domain boundary ')

%% check 

% figure 
% 
% t = (1/16:1/8:1)'*2*pi;
% x = cos(t);
% y = sin(t);
% 
% fill(x,y,[0.4 0.4 0.4])
% axis square

figure
hold on

af = [2 5 6 9];

for rg = 1:4
    
    mapping = @(xii,eta) mesh.porous_airfoil.mapping_plot(xii,eta,af(rg));
    dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,af(rg));
    dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,af(rg));
    dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,af(rg));
    dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,af(rg));
    
    p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);
    
    p_foil.discretize(Kx, Ky, pp);
    
    xp2 = linspace(-1,1,30);
    [xL,yL] = p_foil.domain.mapping(-1*ones(size(xp2)), xp2);
    [xR,yR] = p_foil.domain.mapping(+1*ones(size(xp2)), xp2);
    [xB,yB] = p_foil.domain.mapping(xp2, -1*ones(size(xp2)));
    [xT,yT] = p_foil.domain.mapping(xp2, +1*ones(size(xp2)));
        
%     plot(xL, yL,'k', 'linewidth', 2)
%     plot(xT, yT,'k', 'linewidth', 2)
%     plot(xR, yR,'k', 'linewidth', 2)
%     plot(xB, yB,'k', 'linewidth', 2)
    
    
    fill([xL xT fliplr(xR) fliplr(xB)],[yL yT fliplr(yR) fliplr(yB)],[0.8 0.8 0.8])

end

%% plot elements

cc = 0.6;

tt = 1;
xp3 = linspace(-tt,tt,30);

% figure
% hold on

for rg = 1:10
    
    mapping = @(xii,eta) mesh.porous_airfoil.mapping_plot(xii,eta,rg);
    dX_dxii = @(xii,eta) mesh.porous_airfoil.dX_dxii(xii,eta,rg);
    dX_deta = @(xii,eta) mesh.porous_airfoil.dX_deta(xii,eta,rg);
    dY_dxii = @(xii,eta) mesh.porous_airfoil.dY_dxii(xii,eta,rg);
    dY_deta = @(xii,eta) mesh.porous_airfoil.dY_deta(xii,eta,rg);
    
    p_foil = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta);
    
    p_foil.discretize(Kx, Ky, pp);
    
    for elx = 1:Kx
        for ely = 1:Ky
            
            [xL,yL] = p_foil.el.mapping(-tt*ones(size(xp3)), xp3, elx, ely);
            [xR,yR] = p_foil.el.mapping(+tt*ones(size(xp3)), xp3, elx, ely);
            [xB,yB] = p_foil.el.mapping(xp3, -tt*ones(size(xp3)), elx, ely);
            [xT,yT] = p_foil.el.mapping(xp3, +tt*ones(size(xp3)), elx, ely);
            
            %             plot(xL, yL,'k', 'linewidth', 0.7)
            %             plot(xR, yR,'k', 'linewidth', 0.7)
            %             plot(xB, yB,'k', 'linewidth', 0.7)
            %             plot(xT, yT,'k', 'linewidth', 0.7)
            plot(xL, yL,'Color', [cc cc cc], 'linewidth', 2)
            plot(xR, yR,'Color', [cc cc cc], 'linewidth', 2)
            plot(xB, yB,'Color', [cc cc cc], 'linewidth', 2)
            plot(xT, yT,'Color', [cc cc cc], 'linewidth', 2)
        end
    end
    
    set(gca,'visible','off')
    % title('domain mapping')
    
    %%
    
    xp(1)   = -tt;
    xp(end) = +tt;
    
    [xiip2, etap2] = meshgrid(xp);
    
    for elx = 1:Kx
        for ely = 1:Ky
            
            [xS,yS] = p_foil.el.mapping(xiip2, etap2, elx, ely);
            
            plot(xS(:,2:end-1), yS(:,2:end-1),':','Color', [0.4 0.4 0.4], 'linewidth', 1)
            plot(xS(2:end-1,:)', yS(2:end-1,:)',':','Color', [0.4 0.4 0.4], 'linewidth', 1)
        end
    end
    
    %%
    
    xp4 = (xp(1:end-1) + xp(2:end))/2;
    
    size_red = 2;
    
    for elx = 1:Kx
        for ely = 1:Ky
            [x,y] = p_foil.el.mapping(-1*ones(size(xp4)), xp4, elx, ely);
            
            plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
        end
    end
    
    for elx = 1:Kx
        for ely = 1:Ky
            [x,y] = p_foil.el.mapping(1*ones(size(xp4)), xp4, elx, ely);
            
            plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
        end
    end
    
    for elx = 1:Kx
        for ely = 1:Ky
            [x,y] = p_foil.el.mapping(xp4, -1*ones(size(xp4)), elx, ely);
            
            plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
        end
    end
    
    for elx = 1:Kx
        for ely = 1:Ky
            [x,y] = p_foil.el.mapping(xp4, 1*ones(size(xp4)), elx, ely);
            
            plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
        end
    end
    
    xp2 = linspace(-1,1,30);
    [xL,yL] = p_foil.domain.mapping(-1*ones(size(xp2)), xp2);
    [xR,yR] = p_foil.domain.mapping(+1*ones(size(xp2)), xp2);
    [xB,yB] = p_foil.domain.mapping(xp2, -1*ones(size(xp2)));
    [xT,yT] = p_foil.domain.mapping(xp2, +1*ones(size(xp2)));
        
    plot(xL, yL,'k', 'linewidth', 2)
    plot(xR, yR,'k', 'linewidth', 2)
    plot(xB, yB,'k', 'linewidth', 2)
    plot(xT, yT,'k', 'linewidth', 2)
    
    size_blue = 3;
    
    for elx = 1
        for ely = 1:Ky
            [x,y] = p_foil.el.mapping(-1*ones(size(xp4)), xp4, elx, ely);
            
            plot(x, y,'bo','MarkerFaceColor','b','markerSize',size_blue)
        end
    end
    
%     figure
    for elx = Kx
        for ely = 1:Ky
            [x,y] = p_foil.el.mapping(1*ones(size(xp4)), xp4, elx, ely);
            
            plot(x, y,'bo','MarkerFaceColor','b','markerSize',size_blue)
        end
    end

    for elx = 1:Kx
        for ely = 1
            [x,y] = p_foil.el.mapping(xp4, -1*ones(size(xp4)), elx, ely);
            
            plot(x, y,'bo','MarkerFaceColor','b','markerSize',size_blue)
        end
    end
    
    for elx = 1:Kx
        for ely = Ky
            [x,y] = p_foil.el.mapping(xp4, 1*ones(size(xp4)), elx, ely);
            
            plot(x, y,'bo','MarkerFaceColor','b','markerSize',size_blue)
        end
    end
    
end
