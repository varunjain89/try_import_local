clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% define mapping
%
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

nr_levels = 2;

K(1).x = 3;
K(1).y = 3;

K(2).x = 2;
K(2).y = 2;

K(3).x = 2;
K(3).y = 2;

% K(4).x = 2;
% K(4).y = 2;

p = 3;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

%%

mesh(1) = mmesh3(domain);

%% check domain mapping

mesh(1).check_domain_mapping();

%% discretize the domain

mesh(1).discretize(K(1).x,K(1).y);

%% check element mappings

mesh(1).check_element_mapping();

%%

for levels = 2:nr_levels
    % define domain
    mesh(levels) = mmesh3(mesh(levels-1).el);
    
    % define refinement
    mesh(levels).discretize(K(levels).x, K(levels).y);
    
    % check element mappings
    mesh(levels).check_element_mapping();
end

%% make mass matrices

% ------- calculate metric terms -------

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A_3D = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    A_3D(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    inv_A(:,:,eln) = A_3D(:,:,eln)\I;
end

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);

inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);

GM_local_dof = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);
A_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A_3D);

% figure
% spy(inv_A)

%% connectivity matrices

% the new unit noormal matrix / or the second local connectivity matrix is
% given by the (number of local boundaries \times nr_local_lambda)
% therefore the 3D matrix is (number of local boundaries \times nr_local_lambda \times number of coarse blocks)

% first you create local unit normal vector
% then you create GM for lambda_i
% then you create GM for lambda_i-1

%% connectivity 1

GM_lambda = gather_matrix.GM_boundary_LM_nodes(K(nr_levels).x,p);
ttl_nr_loc_lambda = (max(max(GM_lambda)));
nr_loc_intl_lambda = K(nr_levels).x * p * (K(nr_levels).y -1) + K(nr_levels).y * p * (K(nr_levels).x -1);

for i = 1:mesh(nr_levels).ttl_nr_domain
    GM_lambda_1(:,(i-1)*K(nr_levels).x*K(nr_levels).y+1:(i-1)*K(nr_levels).x*K(nr_levels).y+4) = (i-1) * ttl_nr_loc_lambda + GM_lambda;
end

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 mesh(nr_levels).ttl_nr_el]);

con_E = AssembleMatrices2(GM_lambda_1, GM_local_dof_q', N_3D);
con_E = [con_E zeros(size(con_E,1),loc_dof_p)];

E1 = con_E;

% for i = 1:mesh(nr_levels).ttl_nr_domain
%     E1((i-1)*nr_loc_intl_lambda+1:i*nr_loc_intl_lambda,:) = con_E((i-1)*ttl_nr_loc_lambda +1 : (i-1)*ttl_nr_loc_lambda + nr_loc_intl_lambda,:);
% end

% figure
% spy(E1)

%% connectivity 2

GM_lambda_2 = gather_matrix.GM_boundary_LM_nodes(K(nr_levels-1).x,K(nr_levels).x*p);
GM_lambda_1_2 = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels-1).ttl_nr_el,ttl_nr_loc_lambda)';

nr_local_mu = 2 * K(nr_levels).x * p + 2 * K(nr_levels).y * p;
nr_local_lm = K(nr_levels).x * p * (K(nr_levels).y + 1) + K(nr_levels).y * p * (K(nr_levels).x + 1);

N2 = zeros(nr_local_mu, nr_local_lm);

for elx = 1:K(nr_levels).x
    for i = 1:p
        % bottom side
        mu_id_bot = (elx-1)*p + i;
        local_ele_id_bot = (elx-1)*K(nr_levels).y + 1; 
        lm_id_bot = GM_lambda_1(i,local_ele_id_bot);
        N2(mu_id_bot, lm_id_bot) = -1;
        
        % top side
        mu_id_top = 2*K(nr_levels).y*p + K(nr_levels).x*p + (elx-1)*p + i;
        local_ele_id_top = elx*K(nr_levels).y; 
        lm_id_top = GM_lambda_1(3*p+i,local_ele_id_top);
        N2(mu_id_top, lm_id_top) = +1;
    end
end

for ely = 1:K(nr_levels).y
    for j = 1:p
        % left side
        mu_id_left = K(nr_levels).y*p + K(nr_levels).x*p + (ely-1)*p + j;
        local_ele_id_left = ely;
        lm_id_left = GM_lambda_1(2*p+j,local_ele_id_left);
        N2(mu_id_left, lm_id_left) = -1;
        
        % right side
        mu_id_right = K(nr_levels).x*p + (ely-1)*p + j;
        local_ele_id_rght = (K(nr_levels).x-1)*K(nr_levels).y+ely; 
        lm_id_rght = GM_lambda_1(p+j,local_ele_id_rght);
        N2(mu_id_right, lm_id_rght) = +1;
    end
end

N2_3D = repmat(full(N2),[1 1 mesh(nr_levels-1).ttl_nr_el]);

E2 = AssembleMatrices2(GM_lambda_2, GM_lambda_1_2, N2_3D);

% removing boundary conditions
E2 = E2(1:72,:);

%% generate connectivity matrix at level - i

level = 3

ttl_nr_local_lambda = mesh()
GM_local_lm = gather_matrix.GM_total_local_dof_v3(mesh(level).ttl_nr_domain,mesh(level).ttl_nr_local_lambda)


%%

LHS = [A_2D E1' zeros(size(A_2D,1),size(E2,1));
    E1 zeros(size(E1,1)) E2';
    zeros(size(E2,1),size(A_2D,2)) E2 zeros(size(E2,1))];

det(LHS)

figure
spy(LHS)

%% RHS - f

% F = 

%%

% inv_l1 = inv(E1*inv_A*E1');
% inv_l2 = 
% 
% lambda_2 = -(E2*inv(E1*inv_A*E1')*E2')\(E2*inv(E1*inv_A*E1')*(E1*inv_A*F));
% 
% lambda_1 = ()