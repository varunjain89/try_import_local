clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% define mapping
% 
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

nr_levels = 2;

K(1).x = 3;
K(1).y = 3;

K(2).x = 2;
K(2).y = 2;

% K(3).x = 2;
% K(3).y = 2;
% 
% K(4).x = 2;
% K(4).y = 2;

p = 3;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

%% 

mesh(1) = mmesh3(domain);

%% check domain mapping

mesh(1).check_domain_mapping();

%% discretize the domain

mesh(1).discretize(K(1).x,K(1).y);

%% check element mappings

mesh(1).check_element_mapping();

%%

for levels = 2:nr_levels
    % define domain
    mesh(levels) = mmesh3(mesh(levels-1).el);
    
    % define refinement
    mesh(levels).discretize(K(levels).x, K(levels).y);
    
    % check element mappings
    mesh(levels).check_element_mapping();
end

%% connectivity matrix

% nr_local_lambda = 4*p;
% GM_lambda = zeros(nr_local_lambda, mesh(nr_levels).ttl_nr_el);
% 
% nr_int_lambda_dof = 2*K(nr_levels).x*p*(K(nr_levels).x-1);
% nr_refine_el = K(nr_levels).x * K(nr_levels).y;
% 
% temp_GM_lam = gather_matrix.GM_boundary_LM_nodes_v2_internal(K(nr_levels).x,p);
% R = full(spones(temp_GM_lam));
% 
% for i = 1:mesh(nr_levels -1).ttl_nr_el
%     GM_lambda(:,(i-1)*nr_refine_el +1: i * nr_refine_el) = ((i-1) * nr_int_lambda_dof + temp_GM_lam).*R;
% end

for levels = nr_levels:nr_levels
    nr_int_lambda_dof = 2*K(levels).x*p*(K(levels).x-1);
    
    temp_GM_lam = gather_matrix.GM_boundary_LM_nodes_v2_internal(K(levels).x,p);
    
    R = full(spones(temp_GM_lam));
    
    nr_refine_el = K(levels).x * K(levels).y;
    
    for i = 1:mesh(levels-1).ttl_nr_el
        GM_lambda(:,(i-1)*nr_refine_el +1: i * nr_refine_el) = ((i-1) * nr_int_lambda_dof + temp_GM_lam).*R;
    end
end

GM_lambda = gather_matrix.GM_boundary_LM_nodes(K(nr_levels).x,p);


GM_local_dof   = gather_matrix.GM_total_local_dof_v3(K(nr_levels).x * K(nr_levels).y, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);

N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 K(nr_levels).x * K(nr_levels).y]);

con_E = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
con_E = [con_E zeros(2*K(nr_levels).x*p*(K(nr_levels).x+1),loc_dof_p)];

intl_lambda = 2*K(nr_levels).x*p*(K(nr_levels).x-1);

con_E1 = full(con_E(1:intl_lambda,:));

con_E2 = full(con_E(intl_lambda+1:end,:));

nr_coarse_elements = 9;

N_3D2 = repmat(full(con_E2),[1 1 nr_coarse_elements]);

% GM_local_dof2   = gather_matrix.GM_total_local_dof_v3(ttl_nr_el_coarse, ttl_loc_dof_fine);

% GM_lambda_coarse = gather_matrix.GM_boundary_LM_nodes(K_coarse,K_fine*p);
% 
% con_E3 = full(AssembleMatrices2(GM_lambda_coarse, GM_local_dof2', N_3D2));
