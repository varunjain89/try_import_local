clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% define mapping
% 
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain1.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain1.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain1.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain1.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain1.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%%

mesh1 = mmesh3(domain1);

% mesh2.check_domain_mapping();

%% 

K = 3;
Kx = K;
Ky = K;

p = 2;

mesh1.discretize(Kx, Ky);

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;


%%

mesh1.check_element_mapping();


%% 

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);
[xii_dis, eta_dis] = meshgrid(basis_dis.xp);

jac_dis1 = mesh1.evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis1 = flow.poisson.eval_metric(jac_dis1);

M11 = zeros(loc_dof_u, loc_dof_u, mesh1.ttl_nr_el);

for eln = 1:mesh1.ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis1.g11(:,:,eln), metric_dis1.g12(:,:,eln), metric_dis1.g22(:,:,eln), basis_dis);
end

%%

% the new unit noormal matrix / or the second local connectivity matrix is
% given by the (number of local boundaries \times nr_local_lambda)
% therefore the 3D matrix is (number of local boundaries \times nr_local_lambda \times number of coarse blocks)

nr_local_mu = 2 * Kx + 2 * Ky;
nr_local_lm = Kx * p * (Ky + 1) + Ky * p * (Kx + 1);

N2 = zeros(nr_local_mu, nr_local_lm);
N2_3D = repmat(full(N2),[1 1 mesh1.ttl_nr_el]);

GM_local_dof_lm = gather_matrix.GM_total_local_dof_v3(mesh1.ttl_nr_el, nr_local_lm);

GM_mu = gather_matrix.GM_boundary_LM_nodes(K,p);

con_E2 = AssembleMatrices2(GM_mu, GM_local_dof_lm', N2_3D);
