clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% manufactured solution

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);
ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

%% define mapping
%
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1.75];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

nr_levels = 3;

K1 = 2;
K(1).x = K1;
K(1).y = K1;

K2 = 2;
K(2).x = K2;
K(2).y = K2;

K3 = 2;
K(3).x = K3;
K(3).y = K3;

p = 5;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

ttl_nr_BCs = K1 * K2 * K3 *p * 4;

%%

mesh = n_hybrid_mesh.empty;

for lvl = 1:nr_levels
    % define domain
    if lvl ==1
        mesh(lvl) = n_hybrid_mesh(domain);
    else
        mesh(lvl) = n_hybrid_mesh(mesh(lvl-1).el);
    end
    
    % define refinement
    mesh(lvl).discretize(K(lvl).x, K(lvl).y);
    
    % check element mappings
        mesh(lvl).check_element_mapping();
end

%% element map

% level 1

temp75 = reshape(1:K3^2,K3,K3);

for i = 1:K2^2
    GM_level_1(:,:,i) = (i-1) * K3^2 + temp75;
end

GM_level_1 = reshape(GM_level_1,K3,K3,K2,K2);

temp24 = permute(GM_level_1, [1 3 2 4]);

temp25 = temp24(:);

temp25 = element_map(temp75,K3,K2);

temp26 = reshape(temp25, K2*K3, K2*K3);

for i = 1:K1^2
    GM_level_11(:,:,i) = (i-1) * (K2*K3)^2 + temp26;
end

GM_level_11 = reshape(GM_level_11,K2*K3,K2*K3,K1,K1);

temp27 = permute(GM_level_11, [1 3 2 4]);

element_map_1 = temp27(:);

element_map_1 = element_map(temp26,K2*K3,K1);

% level 2

temp75 = reshape(1:K2^2,K2,K2);
for i = 1:K1^2
    GM_level_2(:,:,i) = (i-1) * K2^2 + temp75;
end

GM_level_2 = reshape(GM_level_2,K2,K2,K1,K1);

temp24 = permute(GM_level_2, [1 3 2 4]);
element_map_2 = temp24(:);

%% make mass matrices

% ------- calculate metric terms -------

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A_3D = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    A_3D(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    inv_A(:,:,eln) = A_3D(:,:,eln)\I;
end

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);

inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);

GM_local_dof = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);
A_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A_3D);

% figure
% spy(inv_A)

%% connectivity matrices

int_lam = 2*K3*p*(K3-1)
GM_lam   = gather_matrix.GM_total_local_dof_v3((K2*K3)^2, int_lam);

GM_local_dof2   = gather_matrix.GM_total_local_dof_v3((K1*K2)^2, ttl_loc_dof*K3^2);

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(K3^2, ttl_loc_dof);
GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
GM_lambda = gather_matrix.GM_boundary_LM_nodes(K3,p);
N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 K3^2]);
ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn2 zeros(2*K3*p*(K3+1),p^2)];
EN1 = ass_conn2(1:int_lam,:);
EN1_3D = repmat(full(EN1),[1 1 (K1*K2)^2]);

EN_11 = AssembleMatrices2(GM_lam', GM_local_dof2', EN1_3D);

%% EN22

GM_lambda2 = gather_matrix.GM_boundary_LM_nodes(K2,K3*p);

GM_local_dof_q2 = gather_matrix.GM_total_local_dof_v3(K2^2, ttl_loc_dof*K3^2);

N2 = ass_conn2(int_lam+1:end,:);
N2_3D = repmat(full(N2),[1 1 K2^2]);

ass_conn3 = AssembleMatrices2(GM_lambda2, GM_local_dof_q2', N2_3D);

p2 = K3*p
int_lam2 = 2*K2*p2*(K2-1)

EN2 = ass_conn3(1:int_lam2,:);

EN2_3D = repmat(full(EN2),[1 1 K1^2]);

GM_lam2 = gather_matrix.GM_total_local_dof_v3(K1^2, int_lam2);

GM_local_dof3   = gather_matrix.GM_total_local_dof_v3(K1^2, ttl_loc_dof*(K3*K2)^2);

EN_22 = AssembleMatrices2(GM_lam2', GM_local_dof3', EN2_3D);

% figure
% spy(EN_22)

%% EN33

GM_lambda3 = gather_matrix.GM_boundary_LM_nodes(K1,K2*K3*p);

GM_local_dof_q3 = gather_matrix.GM_total_local_dof_v3(K1^2, ttl_loc_dof*(K2*K3)^2);

% get the local N
N3 = ass_conn3(int_lam2+1:end,:);
N3_3D = repmat(full(N3),[1 1 K1^2]);

ass_conn4 = AssembleMatrices2(GM_lambda3, GM_local_dof_q3', N3_3D);

p3 = K2*K3*p
int_lam3 = 2*K1*p3*(K1-1)

EN3 = ass_conn4(1:int_lam3,:);

EN_33 = EN3;

%% 

A1 = inv_A;

B1 = EN_11' * inv(EN_11 * A1 * EN_11') * EN_11 * A1;

A2 = inv_A * (eye((K1*K2*K3)^2*ttl_loc_dof) - B1);

B2 = EN_22' * inv(EN_22 * A2 * EN_22') * EN_22 * A2;

A3 = inv_A * (eye((K1*K2*K3)^2*ttl_loc_dof) - B1) * (eye((K1*K2*K3)^2*ttl_loc_dof) - B2);

lambda_1 = EN_11 * A1 * EN_11';

lambda_2 = EN_22 * A2 * EN_22';

lambda_3 = EN_33 * A3 * EN_33';

figure
spy(lambda_1)

figure
spy(lambda_2)

figure
spy(lambda_3)

%% RHS - f

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

dof_f = zeros(loc_dof_p, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh(nr_levels).el(eln).mapping, basis_dis, M22);
end

% ------- reconstruction -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh(nr_levels).evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

% for eln = 1:mesh(nr_levels).ttl_nr_el
%
%     rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
%
%     ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
%
% end

% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'linecolor','none')
% end
%
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'linecolor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [700, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'linecolor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

%%

% RHS = [RHS1; zeros(size(E(1).E,1),1)];
RHS = [RHS1; zeros(size(E(1).E,1),1);zeros(size(E(2).E,1),1);zeros(size(E(3).E,1),1)];

%%

for eln = 1:mesh(nr_levels).ttl_nr_el
    A1_inv(:,:,eln) = inv(A_3D(:,:,eln));
end

A1_inv_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A1_inv);

A2 = E(1).E * A1_inv_2D * E(1).E';

det(A2)

% pause
% return

A2_inv_2D = inv(A2);

A3 = E(2).E * A2_inv_2D * E(2).E';

det(A3)

% pause
% return

A3_inv_2D = inv(A3);

A4 = E(3).E * A3_inv_2D * E(3).E';

A4_inv_2D = inv(A4);

lambda3 = -A4\(E(3).E * A3_inv_2D * E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1)

lambda2 = -A3\(E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1 + E(3).E'*lambda3);

lambda1 = A2\(E(1).E * A1_inv_2D * RHS1 + E(2).E'*lambda2);

A1 = A_2D;

X = A1\(RHS1 - E(1).E'*lambda1);

% figure
% spy(A1_inv_2D)
%
% figure
% spy(A2)
%
% figure
% spy(A3)
%
% figure
% spy(A4)

%%
LHS = [A_2D     E(1).E' zeros(size(A_2D,1), size(E(2).E,1))   zeros(size(A_2D,1), size(E(3).E,1));
       E(1).E   zeros(size(E(1).E,1))   E(2).E' zeros(size(E(1).E,1),size(E(3).E,1));
       zeros(size(E(2).E,1),size(A_2D,1))    E(2).E  zeros(size(E(2).E,1))   E(3).E';
       zeros(size(E(3).E,1),size(A_2D,1))    zeros(size(E(3).E,1),size(E(1).E,1))   E(3).E  zeros(size(E(3).E,1))];
det(LHS)

condest(LHS)

figure
spy(LHS)

%%
X = LHS\RHS;

X1 = X(1:size(A_2D,1));

% X1 = X;

for eln = 1:mesh(nr_levels).ttl_nr_el
    p_h(:,eln) = X1(GM_local_dof_p(:,eln));
end

p_h_2 = zeros(size(p_h));

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

%% reconstruction

for eln = 1:mesh(nr_levels).ttl_nr_el
    
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
    
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
    
end

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'linecolor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'linecolor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln),'linecolor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])

% figure
% hold on

% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

