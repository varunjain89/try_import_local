function [index_BC_level_1] = index_BC(K_eff,p_eff,element_map,GM,level)

p = p_eff;
element_map_1 = element_map;

index_BC_left_level_1 = GM(level).local_lambda(2*p+1:3*p,element_map_1(1:K_eff));
index_BC_rght_level_1 = GM(level).local_lambda(1*p+1:2*p,element_map_1(K_eff^2 - K_eff +1: K_eff^2));
index_BC_bott_level_1 = GM(level).local_lambda(1:p,element_map_1(1:K_eff:(K_eff-1)*K_eff+1));
index_BC_topp_level_1 = GM(level).local_lambda(3*p+1:4*p,element_map_1(K_eff:K_eff:K_eff^2));

index_BC_left_level_1 = index_BC_left_level_1(:);
index_BC_rght_level_1 = index_BC_rght_level_1(:);
index_BC_bott_level_1 = index_BC_bott_level_1(:);
index_BC_topp_level_1 = index_BC_topp_level_1(:);

index_BC_level_1 = [index_BC_left_level_1; index_BC_rght_level_1; index_BC_bott_level_1; index_BC_topp_level_1];

end

