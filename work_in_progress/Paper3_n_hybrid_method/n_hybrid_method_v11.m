clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% manufactured solution

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);
ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

%% define mapping

xbound = [0 1];
ybound = [0 1];

c = 0.0;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

nr_levels = 2;

K1 = 3;
K(1).x = K1;
K(1).y = K1;

K2 = 1;
K(2).x = K2;
K(2).y = K2;

p = 1;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

% ttl_nr_BCs = K1 * K2 * K3 *p * 4;

%% domain definition

mesh = n_hybrid_mesh.empty;

for lvl = 1:nr_levels
    % define domain
    if lvl ==1
        mesh(lvl) = n_hybrid_mesh(domain);
    else
        mesh(lvl) = n_hybrid_mesh(mesh(lvl-1).el);
    end
    
    % define refinement
    mesh(lvl).discretize(K(lvl).x, K(lvl).y);
    
    % check element mappings
%     mesh(lvl).check_element_mapping();
end

%% element map

% level 2

element_map_2 = element_map(1:K2^2,K2,K1);

%% make local system

% ------- calculate metric terms -------

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A_3D = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    A_3D(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);

A1 = AssembleMatrices2(GM_local_dof', GM_local_dof', A_3D);

%% connectivity matrices

% the new unit noormal matrix / or the second local connectivity matrix is
% given by the (number of local boundaries \times nr_local_lambda)
% therefore the 3D matrix is (number of local boundaries \times nr_local_lambda \times number of coarse blocks)

% first you create local unit normal vector
% then you create GM for lambda_i
% then you create GM for lambda_i-1

GM_local_dof_q = GM_local_dof(:,1:loc_dof_u);
GM_local_dof_p = GM_local_dof(:,loc_dof_u+1:loc_dof_u+p^2)';

for lvl = 1:nr_levels
    % first we create connectivity for most refined level
    % this is last level in the program, but defined as 1st level in the
    % document
    
    level = nr_levels + 1 - lvl;
    
    % ------ gather matrix 1 ------
    
    if lvl == 1
        GM(lvl).l2 = GM_local_dof_q';
    else
        GM(lvl).l2 = gather_matrix.GM_total_local_dof_v3(mesh(level).ttl_nr_el, ttl_nr_loc_lambda)';
    end
    
    % ------ gather matrix 2 ------
    if lvl == 1
        K_connecting_edges = 1;
    else
        K_connecting_edges = K_connecting_edges * K(level+1).x;
    end
    
    GM_connecting_lm = gather_matrix.GM_boundary_LM_nodes(K(level).x,K_connecting_edges*p);
    
    ttl_nr_loc_lambda = (max(max(GM_connecting_lm)));
    
    for i = 1:mesh(level).ttl_nr_domain
        GM(lvl).local_lambda(:,(i-1)*mesh(level).lcl_nr_el +1:i * mesh(level).lcl_nr_el) = (i-1) * ttl_nr_loc_lambda + GM_connecting_lm;
    end
    
    % ------ local connectivity matrix ------
    if lvl == 1
        N(lvl).local = incidence_matrix.OutwardNormalMatrix(p);
    else
        N(lvl).local = local_connectivity_LM(p,mesh,K_connecting_edges,level);
    end
    
    N2(lvl).N = repmat(full(N(lvl).local),[1 1 mesh(level).ttl_nr_el]);
    
    E(lvl).E = AssembleMatrices2(GM(lvl).local_lambda, GM(lvl).l2, N2(lvl).N);
    
end

% adding pressure degrees of freedom in the last element
E(1).E = [E(1).E zeros(size(E(1).E,1),p^2)];

%% add additional constraints to remove singularity

Keff = K1*K2*K3;
% Keff = K1*K2;

nr_internal_elements = (Keff-2)^2

% index of lambda's internal elements
% Q) you can choose all the lambda's or only boundary lambda's - make a
% smart choice 

total_nr_lamda = total_nr_lambda(K3,p);

counter = 1;

for i = K3+1:Keff-K3
    for j = K3+1:Keff-K3
        local_ele_id = (i-1)*Keff +j;
        glbal_ele_id = element_map_1(local_ele_id);
        singularity(counter,:) = GM_local_dof_p(:,glbal_ele_id);
%         glbal_ele_id = element_map_2(local_ele_id);
%         singularity(counter,:) = (glbal_ele_id-1)*total_nr_lamda + (1:total_nr_lamda);
        counter = counter+1;
    end
end

singularity = singularity(:);

AA = 1:counter-1;

AA = repmat(AA,p^2,1);
% AA = repmat(AA,total_nr_lamda,1);

AA = AA';
AA = AA(:);

E_sing = sparse(AA,singularity,1,counter-1,Keff^2*ttl_loc_dof);
% E_sing = sparse(AA,singularity,1,counter-1,Keff^2*total_nr_lamda);

%add to existing E1

E(1).E = [E(1).E; E_sing];

E(2).E = [E(2).E sparse(size(E(2).E,1),size(E_sing,1))];

3
%% removing boundary conditions at all levels

% ------------- level 1

p_eff = p;
K_eff = K1*K2*K3;

index_BC_level_1 = index_BC(K_eff,p_eff,element_map_1,GM,1);

% E(1).E(index_BC_level_1,:) = [];

% temp23 = 2*K3*p*(K3+1);
% temp23 = temp23 * (K2*K1)^2;
% temp23 = temp23 - ttl_nr_BCs;

% if size(E(1).E,1) ~= temp23
%     disp('Error !!! Size of connecting matrix is incorrect !!!')
% %     pause;
% %     return
% else
%     disp('At least size of E1 is correct ;-) ')
% end

% ------------- level 2

p_eff = K3*p;
K_eff = K1*K2;

index_BC_level_2 = index_BC(K_eff,p_eff,element_map_2,GM,2);

% E(2).E(index_BC_level_2,:) = [];
% E(2).E(:,index_BC_level_1) = [];

% ------------- level 3

% E(3).E = E(3).E(1:2*K(1).x * K(2).x * K(3).x * p * (K(1).x -1),:);
% E(3).E(:,index_BC_level_2) = [];

%% RHS - f

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

dof_f = zeros(loc_dof_p, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh(nr_levels).el(eln).mapping, basis_dis, M22);
end

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

RHS = [RHS1; zeros(size(E(1).E,1),1);zeros(size(E(2).E,1),1);zeros(size(E(3).E,1),1)];

%% ------- reconstruction metric -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh(nr_levels).evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

%% reconstruction of source term 

% for eln = 1:mesh(nr_levels).ttl_nr_el
%
%     rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
%
%     ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
%
% end

% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'linecolor','none')
% end
%
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'linecolor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [700, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'linecolor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

%% n-hybrid calculations 

for eln = 1:mesh(nr_levels).ttl_nr_el
    A1_inv(:,:,eln) = inv(A_3D(:,:,eln));
end

A1_inv_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A1_inv);

A2 = E(1).E * A1_inv_2D * E(1).E';

A2_inv_2D = inv(A2);

A3 = E(2).E * A2_inv_2D * E(2).E';

A3_inv_2D = inv(A3);

A4 = E(3).E * A3_inv_2D * E(3).E';

A4_inv_2D = inv(A4);

lambda3 = -A4\(E(3).E * A3_inv_2D * E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1);

lambda2 = -A3\(E(2).E * A2_inv_2D * E(1).E * A1_inv_2D * RHS1 + E(3).E'*lambda3);

lambda1 = A2\(E(1).E * A1_inv_2D * RHS1 + E(2).E'*lambda2);

X = A1\(RHS1 - E(1).E'*lambda1);

%% LHS - full system 

% LHS = [A1     E(1).E' zeros(size(A1,1), size(E(2).E,1))   zeros(size(A1,1), size(E(3).E,1));
%        E(1).E   zeros(size(E(1).E,1))   E(2).E' zeros(size(E(1).E,1),size(E(3).E,1));
%        zeros(size(E(2).E,1),size(A1,1))    E(2).E  zeros(size(E(2).E,1))   E(3).E';
%        zeros(size(E(3).E,1),size(A1,1))    zeros(size(E(3).E,1),size(E(1).E,1))   E(3).E  zeros(size(E(3).E,1))];

%% solution full system 

% X = LHS\RHS;

X1 = X(1:size(A1,1));

for eln = 1:mesh(nr_levels).ttl_nr_el
    p_h(:,eln) = X1(GM_local_dof_p(:,eln));
end

%% pressure dual to primal

p_h_2 = zeros(size(p_h));

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

%% reconstruction

for eln = 1:mesh(nr_levels).ttl_nr_el 
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
end

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'linecolor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'linecolor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln),'linecolor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])
