clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')

%% manufactured solution

phi_an   = @(x,y) sin(2*pi*x).*sin(2*pi*y);
dp_dx    = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy    = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dxdy = @(x,y) 4*pi^2*cos(2*pi*x).*cos(2*pi*y);
d2p_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ff = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);
ux = @(x,y) dp_dx(x,y);
uy = @(x,y) dp_dy(x,y);

%% define mapping
%
% xbound = [1.14 2.34];
% ybound = [0.3 1.7];

xbound = [0 1];
ybound = [0 1];

c = 0.15;

domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

%% define levels of mesh refinement

%--------- changes here 
nr_levels = 4;

K1 = 2;
K(1).x = K1;
K(1).y = K1;

K2 = 2;
K(2).x = K2;
K(2).y = K2;

K3 = 2;
K(3).x = K3;
K(3).y = K3;

K4 = 2;
K(4).x = K4;
K(4).y = K4;

p = 3;

loc_dof_u = 2*p*(p+1);
loc_dof_p = p^2;
ttl_loc_dof = loc_dof_u + loc_dof_p;

%%

mesh = n_hybrid_mesh.empty;

for lvl = 1:nr_levels
    % define domain
    if lvl ==1
        mesh(lvl) = n_hybrid_mesh(domain);
    else
        mesh(lvl) = n_hybrid_mesh(mesh(lvl-1).el);
    end
    
    % define refinement
    mesh(lvl).discretize(K(lvl).x, K(lvl).y);
    
    % check element mappings
%         mesh(lvl).check_element_mapping();
end

%% make mass matrices

% ------- calculate metric terms -------

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

% ------- make mass matrix matrix -------

M11 = zeros(loc_dof_u, loc_dof_u, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
end

% ------- make incidence matrix -------

E211 = full(incidence_matrix.E21(p));

% ------- make local systems for elements -------

A_3D = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    A_3D(:,:,eln) = [M11(:,:,eln) E211'; E211 zeros(loc_dof_p)];
end

I = eye(ttl_loc_dof);

inv_A = zeros(ttl_loc_dof,ttl_loc_dof,mesh(nr_levels).ttl_nr_el);

tic
for eln = 1:mesh(nr_levels).ttl_nr_el
    inv_A(:,:,eln) = A_3D(:,:,eln)\I;
end
t1 = toc

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);

inv_A = AssembleMatrices2(GM_local_dof', GM_local_dof', inv_A);

GM_local_dof = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);
A_2D = AssembleMatrices2(GM_local_dof', GM_local_dof', A_3D);

% figure
% spy(inv_A)

%% connectivity matrices

[EN_11, N2] = n_connectivity_i2(K1*K2*K3,K4,1,p,ttl_loc_dof,[]);
[EN_22, N3] = n_connectivity_i2(K1*K2,K3,K4,p,ttl_loc_dof,N2);
[EN_33, N4] = n_connectivity_i2(K1,K2,K3*K4,p,ttl_loc_dof,N3);
[EN_44, N5] = n_connectivity_i2(1,K1,K2*K3*K4,p,ttl_loc_dof,N4);

%% 

A11 = inv_A;

tic

[A22, LHS_1] = n_hybrid_A_i(A11,EN_11);

[A33, LHS_2] = n_hybrid_A_i(A22,EN_22);

[A44, LHS_3] = n_hybrid_A_i(A33,EN_33);

LHS_4 = EN_44 * A44 * EN_44';

t2 = toc

figure
spy(LHS_1)

figure
spy(LHS_2)

figure
spy(LHS_3)

figure
spy(LHS_4)

%% RHS - f

% [basis_dis.xp, basis_dis.wp] = GLLnodes(p);
% [basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);
% 
% [xii_dis, eta_dis] = meshgrid(basis_dis.xp);
% jac_dis = mesh(nr_levels).evaluate_jacobian_all(xii_dis, eta_dis);
% metric_dis = flow.poisson.eval_metric(jac_dis);

dof_f = zeros(loc_dof_p, mesh(nr_levels).ttl_nr_el);

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(ff, mesh(nr_levels).el(eln).mapping, basis_dis, M22);
end

% ------- reconstruction -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh(nr_levels).evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

% for eln = 1:mesh(nr_levels).ttl_nr_el
%
%     rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
%
%     [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
%
%     ff_ex(:,:,eln) = ff(rec_x(:,:,eln), rec_y(:,:,eln));
%
% end

% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'linecolor','none')
% end
%
% colorbar
% title('exact solution')
% set(gcf, 'Position',  [0100, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln),'linecolor','none')
% end
% colorbar
% title('reconstructed solution')
% set(gcf, 'Position',  [700, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'linecolor','none')
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])
%
% figure
% hold on
%
% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

RHS(loc_dof_u+1:ttl_loc_dof,:) = dof_f;

RHS1 = RHS(:);

RHS = [RHS1; zeros(size(EN_11,1),1);zeros(size(EN_22,1),1);zeros(size(EN_33,1),1);zeros(size(EN_44,1),1)];

%% solution 

F = RHS1;

F5 = RHS1;

tic

lambda_4 = LHS_4 \ (EN_44 * A44 * F5);

F4 = F - EN_44' * lambda_4;

lambda_3 = LHS_3 \ (EN_33 * A33 * F4);

F3 = F4 - EN_33' * lambda_3;

lambda_2 = LHS_2 \ (EN_22 * A22 * F3);

F2 = F3 - EN_22' * lambda_2;

lambda_1 = LHS_1 \ (EN_11 * A11 * F2);

F1 = F2 - EN_11' * lambda_1;

X1 = A_2D \ F1;

t3 = toc

%%
% X = LHS\RHS;
% 
% X1 = X(1:size(A_2D,1));

% X1 = X;

GM_local_dof   = gather_matrix.GM_total_local_dof_v3(mesh(nr_levels).ttl_nr_el, ttl_loc_dof);
GM_local_dof_p = GM_local_dof(:,loc_dof_u+1:loc_dof_u+p^2)';

for eln = 1:mesh(nr_levels).ttl_nr_el
    p_h(:,eln) = X1(GM_local_dof_p(:,eln));
end

p_h_2 = zeros(size(p_h));

for eln = 1:mesh(nr_levels).ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    p_h_2(:,eln) = M22\p_h(:,eln);
end

%% reconstruction

for eln = 1:mesh(nr_levels).ttl_nr_el
    
    rec_p(:,:,eln) = reconstruction.reconstruct2form_v6(p_h_2(:,eln), basis_rec, metric_rec.ggg(:,:,eln));
    
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh(nr_levels).el(eln).mapping(rec_xii, rec_eta);
    
    pp_ex(:,:,eln) = phi_an(rec_x(:,:,eln), rec_y(:,:,eln));
    
end

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),pp_ex(:,:,eln),'linecolor','none')
end

colorbar
title('exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_p(:,:,eln),'linecolor','none')
end
colorbar
title('reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh(nr_levels).ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln),'linecolor','none')
end
colorbar
title('difference')
set(gcf, 'Position',  [1300, 500, 550, 400])

% figure
% hold on

% for eln = 1:mesh(nr_levels).ttl_nr_el
%     surf(rec_x(:,:,eln), rec_y(:,:,eln), rec_p(:,:,eln) - pp_ex(:,:,eln))
% end
% colorbar
% title('difference')
% set(gcf, 'Position',  [1300, 500, 550, 400])

