clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% domain

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

%% discretization

K = 1;
p = 2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

pf = 10;
[xii, wii] = GLLnodes(pf);

[hp_int, ep_int] = MimeticpolyVal(xii,p,1);

linmap = @(x1,x2,xii) x1/2 .* (1-xii) + x2/2 .* (1+xii);


%% edge 1

xii_plot1 = linmap(xp(1),xp(2),xii)

eta_plot1 = flip(xii_plot1)

figure

plot(xii_plot1, eta_plot1,'-o')
hold on

%% edge 2

xii_plot2 = linmap(xp(1),xp(2),xii)

eta_plot2 = linmap(xp(2),xp(3),xii)
eta_plot2 = flip(eta_plot2)

plot(xii_plot2, eta_plot2,'-o')

%% edge 3

xii_plot3 = linmap(xp(2),xp(3),xii)

eta_plot3 = linmap(xp(1),xp(2),xii)
eta_plot3 = flip(eta_plot3)

plot(xii_plot3, eta_plot3,'-o')

%% sum 1

[hp_int1, ep_int1] = MimeticpolyVal(xii_plot1,p,1);

ep_int1 = flip(ep_int1)

t_bas = hp_int1(2,:) .* ep_int1(1,:) .* wii

sum(t_bas)

%% sum 2

eta_plot2 = linmap(xp(2),xp(3),xii)

[hp_int2, ep_int2] = MimeticpolyVal(eta_plot2,p,1);

ep_int2

ep_int2 = flip(ep_int2,2)

t_bas = hp_int1(2,:) .* ep_int2(1,:) .* wii

sum(t_bas)

%% sum 3

[hp_int3, ep_int3] = MimeticpolyVal(xii_plot3,p,1);

% ep_int2 = flip(ep_int2)

t_bas = hp_int3(3,:) .* ep_int1(1,:) .* wii

sum(t_bas)
