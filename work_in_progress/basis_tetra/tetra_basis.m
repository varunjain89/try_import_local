clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% domain

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

%% discretization

K = 1;
p = 1;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

pf = 5;
[xii, wii] = GLLnodes(pf);

[hp_int, ep_int] = MimeticpolyVal(xii,p,1);

ep_int

ep_int = flip(ep_int)

xii 

eta = flip(xii)

plot(xii, flip(xii),'-o')


t_bas = hp_int(2,:) .* ep_int .* wii

sum1 = sum(t_bas)

%% 


