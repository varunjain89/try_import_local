%% Created on 18th Sep, 2018 

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../finished_programs/elasticity_eq')

%% analytical solution

E = 1;
nu = 0.3;

% u_an     = @(x,y) 2*ones(size(x));
% v_an     = @(x,y) 5*ones(size(x));
% du_dx_an = @(x,y) 0*ones(size(x));
% du_dy_an = @(x,y) 0*ones(size(x));
% dv_dx_an = @(x,y) 0*ones(size(x));
% dv_dy_an = @(x,y) 0*ones(size(x));
% d2u_dx2  = @(x,y) 0*ones(size(x));
% d2u_dy2  = @(x,y) 0*ones(size(x));
% d2v_dxdy = @(x,y) 0*ones(size(x));
% d2v_dy2  = @(x,y) 0*ones(size(x));
% d2v_dx2  = @(x,y) 0*ones(size(x));
% d2u_dxdy = @(x,y) 0*ones(size(x));

% u_an     = @(x,y) -3*y;
% v_an     = @(x,y) 3*x;
% du_dx_an = @(x,y) 0*ones(size(x));
% du_dy_an = @(x,y) -3*ones(size(x));
% dv_dx_an = @(x,y) +3*ones(size(x));
% dv_dy_an = @(x,y) 0*ones(size(x));
% d2u_dx2  = @(x,y) 0*ones(size(x));
% d2u_dy2  = @(x,y) 0*ones(size(x));
% d2v_dxdy = @(x,y) 0*ones(size(x));
% d2v_dy2  = @(x,y) 0*ones(size(x));
% d2v_dx2  = @(x,y) 0*ones(size(x));
% d2u_dxdy = @(x,y) 0*ones(size(x));

% u_an     = @(x,y) x.*(1-x).*y.*(1-y);
% v_an     = @(x,y) x.*(1-x).*y.*(1-y);
% du_dx_an = @(x,y) (1-2.*x).*y.*(1-y);
% du_dy_an = @(x,y) x.*(1-x).*(1-2*y);
% dv_dx_an = @(x,y) (1-2*x).*y.*(1-y);
% dv_dy_an = @(x,y) x.*(1-x).*(1-2*y);
% d2u_dx2  = @(x,y) -2*y.*(1-y);
% d2u_dy2  = @(x,y) -2*x.*(1-x);
% d2v_dxdy = @(x,y) (1-2*x).*(1-2*y);
% d2v_dy2  = @(x,y) -2*x.*(1-x);
% d2v_dx2  = @(x,y) -2*y.*(1-y);
% d2u_dxdy = @(x,y) (1-2*x).*(1-2*y);

% u_an     = @(x,y) (1-x.^2).*(1-y.^2);
% v_an     = @(x,y) (1-x.^2).*(1-y.^2);
% du_dx_an = @(x,y) -2*x.*(1-y.^2);
% du_dy_an = @(x,y) -2*y.*(1-x.^2);
% dv_dx_an = @(x,y) -2*x.*(1-y.^2);
% dv_dy_an = @(x,y) -2*y.*(1-x.^2);
% d2u_dx2  = @(x,y) -2*(1-y.^2);
% d2u_dy2  = @(x,y) -2*(1-x.^2);
% d2v_dxdy = @(x,y) 4*x.*y;
% d2v_dy2  = @(x,y) -2*(1-x.^2);
% d2v_dx2  = @(x,y) -2*(1-y.^2);
% d2u_dxdy = @(x,y) 4*x.*y;

u_an     = @(x,y) sin(pi*x).*sin(pi*y);
v_an     = @(x,y) sin(pi*x).*sin(pi*y);
du_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
du_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
dv_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
dv_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2u_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2v_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);

% u_an     = @(x,y) sin(pi*(x-1)).*sin(pi*(y-1));
% v_an     = @(x,y) sin(pi*(x-1)).*sin(pi*(y-1));
% du_dx_an = @(x,y) pi*cos(pi*(x-1)).*sin(pi*(y-1));
% du_dy_an = @(x,y) pi*sin(pi*(x-1)).*cos(pi*(y-1));
% dv_dx_an = @(x,y) pi*cos(pi*(x-1)).*sin(pi*(y-1));
% dv_dy_an = @(x,y) pi*sin(pi*(x-1)).*cos(pi*(y-1));
% d2u_dx2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2u_dy2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2v_dxdy = @(x,y) pi^2*cos(pi*(x-1)).*cos(pi*(y-1));
% d2v_dy2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2v_dx2  = @(x,y) -pi^2*sin(pi*(x-1)).*sin(pi*(y-1));
% d2u_dxdy = @(x,y) pi^2*cos(pi*(x-1)).*cos(pi*(y-1));

% u_an     = @(x,y) sin(2*pi*x).*cos(2*pi*y);
% v_an     = @(x,y) cos(2*pi*x).*sin(2*pi*y);
% du_dx_an = @(x,y) 2*pi*cos(2*pi*x).*cos(2*pi*y);
% du_dy_an = @(x,y) -2*pi*sin(2*pi*x).*sin(2*pi*y);
% dv_dx_an = @(x,y) -2*pi*sin(2*pi*x).*sin(2*pi*y);
% dv_dy_an = @(x,y) 2*pi*cos(2*pi*x).*cos(2*pi*y);
% d2u_dx2  = @(x,y) -4*pi^2*sin(2*pi*x).*cos(2*pi*y);
% d2u_dy2  = @(x,y) -4*pi^2*sin(2*pi*x).*cos(2*pi*y);
% d2v_dxdy = @(x,y) -4*pi^2*sin(2*pi*x).*cos(2*pi*y);
% d2v_dy2  = @(x,y) -4*pi^2*cos(2*pi*x).*sin(2*pi*y);
% d2v_dx2  = @(x,y) -4*pi^2*cos(2*pi*x).*sin(2*pi*y);
% d2u_dxdy = @(x,y) -4*pi^2*cos(2*pi*x).*sin(2*pi*y);

e_xx_an = @(x,y) du_dx_an(x,y);
e_yy_an = @(x,y) dv_dy_an(x,y);
e_xy_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));
e_yx_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));

w_an      = @(x,y) 0.5*(dv_dx_an(x,y) - du_dy_an(x,y));
w_an      = @(x,y) w_an(x,y);

tau_xx_an = @(x,y) E/(1-nu^2)*(e_xx_an(x,y) + nu*e_yy_an(x,y));
tau_yy_an = @(x,y) E/(1-nu^2)*(e_yy_an(x,y) + nu*e_xx_an(x,y));
tau_xy_an = @(x,y) E/(1+nu)*e_xy_an(x,y);
tau_yx_an = @(x,y) E/(1+nu)*e_yx_an(x,y);
fx_an     = @(x,y) -E/(1-nu^2) * ( d2u_dx2(x,y) + 0.5*(1-nu)*d2u_dy2(x,y) + 0.5*(1+nu)*d2v_dxdy(x,y) );
fy_an     = @(x,y) -E/(1-nu^2) * ( d2v_dy2(x,y) + 0.5*(1-nu)*d2v_dx2(x,y) + 0.5*(1+nu)*d2u_dxdy(x,y) );
tq_an     = @(x,y) -y.*fx_an(x,y) + x.*fy_an(x,y);

lambdaX_an  = @(x,y) u_an(x,y) + y.*w_an(x,y);
lambdaY_an  = @(x,y) v_an(x,y) - x.*w_an(x,y);

% lambdaX_an  = @(x,y) u_an(x,y);
% lambdaY_an  = @(x,y) v_an(x,y);
lambdaW_an  = @(x,y) w_an(x,y);

TQx_an = @(x,y) x*tau_yy_an(x,y) - y*tau_yx_an(x,y);
TQy_an = @(x,y) x*tau_xy_an(x,y) - y*tau_xx_an(x,y);

%% domain

% xbound = [3.3 4.9];
% ybound = [2.1 5.3];

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

% factor = 0.5*(xbound(2) - xbound(1));

%% number of elements

K = 1;

%% polynomial order of scheme 

p = 8;

ttl_nr_el = K^2;
local_nr_pp = p^2;

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

total_local_dof = local_nr_tau_xx + local_nr_tau_xy + local_nr_tau_yx + local_nr_tau_yy;

%% mapping functions

domain_mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

[element_bounds_x, ~] = GLLnodes(K);
element_bounds_y = element_bounds_x;

el_mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain_mapping, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain_dX_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain_dX_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain_dY_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain_dY_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);

%% calculate metric terms

[xp, ~] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_p_dx_dxii(:,:,el) = el_dX_dxii(xip,etap,elx,ely);
        eval_p_dx_deta(:,:,el) = el_dX_deta(xip,etap,elx,ely);
        eval_p_dy_dxii(:,:,el) = el_dY_dxii(xip,etap,elx,ely);
        eval_p_dy_deta(:,:,el) = el_dY_deta(xip,etap,elx,ely);
    end
end

%% calculation of metric terms

[g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44] = elasticity_non_staggered.metric_terms_Yi(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);

%% local numbering

[LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = elasticity_non_staggered.local_numbering(p);

%% defining class objects for variables / dof 

tauX    = oneForm;
tauX.p  = p;

tauY    = oneForm;
tauY.p  = p;

TQ      = oneForm;
TQ.p    = p;

lambda3   = oneForm;
lambda3.p = p;

lambdaX2 = twoForm;
lambdaX2.K           = K;
lambdaX2.p           = p;
lambdaX2.el_mapping  = el_mapping;

lambdaY2 = twoForm;
lambdaY2.K           = K;
lambdaY2.p           = p;
lambdaY2.el_mapping  = el_mapping;

lambdaW2 = twoForm;
lambdaW2.K           = K;
lambdaW2.p           = p;
lambdaW2.el_mapping  = el_mapping;

%% constitutive mass matrix 

% MS2 = elasticity_non_staggered.full_mass_matrix_v2(p, nu);
MS2 = elasticity_non_staggered.full_mass_matrix_g(p, g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44);

% MS = elasticity_non_staggered.full_mass_matrix(p, nu);

MS = MS2;

%% conservation of linear momentum 

Ex = elasticity_non_staggered.incidence_matrix.linear_momentum_x(p, LN_tau_xx, LN_tau_yx);
Ey = elasticity_non_staggered.incidence_matrix.linear_momentum_y(p, LN_tau_yy, LN_tau_xy);

%% reconstruction

[xp, ~] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_p_dx_dxii(:,:,el) = el_dX_dxii(xip,etap,elx,ely);
        eval_p_dx_deta(:,:,el) = el_dX_deta(xip,etap,elx,ely);
        eval_p_dy_dxii(:,:,el) = el_dY_dxii(xip,etap,elx,ely);
        eval_p_dy_deta(:,:,el) = el_dY_deta(xip,etap,elx,ely);
    end
end

% eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);

%% conservation of angular momentum 

% TQ_shear = elasticity_non_staggered.torque_shear(p);
% TQ_norm  = elasticity_non_staggered.torque_normal(p);

% TQ_shear = elasticity_non_staggered.torque.torque_shear_v3(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);
% TQ_norm  = elasticity_non_staggered.torque.torque_normal_v3(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);

% spy(TQ_norm)

% TQ_shear = elasticity_non_staggered.torque.torque_shear_with_Torque_v1(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);
% TQ_norm  = elasticity_non_staggered.torque.torque_normal_with_Torque_v1(p, xbound, ybound, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta);

% spy(TQ_norm)

% TQ = (TQ_shear + TQ_norm);

% spy(TQ)

am_E21 = elasticity_non_staggered.incidence_matrix.AM_incidence_matrix(p);
% AM4    = am_E21*TQ;

%% new mass matrix for 2nd constittutive law 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

% p_int = p+1;
% [quad_int_xi, wp_int] = GLLnodes(p_int);

[x_int,y_int] = meshgrid(xp);
x_int = x_int';
y_int = y_int';

% plot(x_int,y_int,'+r')

MAM = zeros(2*p*(p+1),4*p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        
%         xi_i1 = xp(i);
%         xi_i2 = xp(i+1);
%         eta_j = xp(j);
        
        % points on physical domain
        %         [x_int,y_int] = map_local_edge(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         [x_int,y_int] = el_mapping(quad_int_xi,quad_int_xi,1,1);
        
        %------ with Txx 
%         for k = 1:p+1
%             for l = 1:p
%                 dofkl = LN_tau_xx(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - 0*x_int(xq,yq)*nu*ep(i,xq)*hp(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Tyx
        for k = 1:p
            for l = 1:p+1
                dofkl = p*(p+1) + LN_tau_yx(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - y_int(xq,yq)*ep(i,xq)*hp(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Txy
%         for k = 1:p+1
%             for l = 1:p
%                 dofkl = 2*p*(p+1) + LN_tau_xy(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + 0*ep(i,xq)*hp(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Tyy
        for k = 1:p
            for l =1:p+1
                dofkl = 3*p*(p+1) + LN_tau_yy(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + x_int(xq,yq)*ep(i,xq)*hp(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
    end
end

for i = 1:p+1
    for j =1:p
        edgeij = p*(p+1) + (i-1)*p +j;
        
%         xi_i  = xp(i);
%         eta_j1 = xp(j);
%         eta_j2 = xp(j+1);
        
        % points on physical domain
%         [x_int,y_int] = map_local_y_edge(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
        %------ with Txx 
        for k = 1:p+1
            for l = 1:p
                dofkl = LN_tau_xx(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - y_int(xq,yq)*hp(i,xq)*ep(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Tyx
%         for k = 1:p
%             for l = 1:p+1
%                 dofkl = p*(p+1) + LN_tau_yx(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - *hp(i,xq)*ep(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Txy
        for k = 1:p+1
            for l = 1:p
                dofkl = 2*p*(p+1) + LN_tau_xy(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + x_int(xq,yq)*hp(i,xq)*ep(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Tyy
%         for k = 1:p
%             for l =1:p+1
%                 dofkl = 3*p*(p+1) + LN_tau_yy(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + y_int(xq,yq)*nu*hp(i,xq)*ep(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end

    end
end

% spy(MAM)

%% mass matrix for torque 

eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);
eval_p_g11 = metric.g11(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g12 = metric.g12(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g22 = metric.g22(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);

M11 = mass_matrix.M1_3(p, eval_p_g11(:,:,el), eval_p_g12(:,:,el), eval_p_g22(:,:,el));

%% LHS system 

% W = AM4;

LHS = [MS                       -MAM'                       zeros(4*p*(p+1),2*p*(p+1))  Ex'                     Ey'                     zeros(4*p*(p+1),p^2);
       -MAM                     zeros(2*p*(p+1))            M11                         zeros(2*p*(p+1),p^2)    zeros(2*p*(p+1),p^2)    zeros(2*p*(p+1),p^2); 
       zeros(2*p*(p+1),4*p*(p+1))   -M11'                    zeros(2*p*(p+1))            zeros(2*p*(p+1),p^2)    zeros(2*p*(p+1),p^2)   +am_E21'             ;
       Ex                       zeros(p^2,2*p*(p+1))        zeros(p^2,2*p*(p+1))        zeros(p^2)              zeros(p^2)              zeros(p^2)          ;
       Ey                       zeros(p^2,2*p*(p+1))        zeros(p^2,2*p*(p+1))        zeros(p^2)              zeros(p^2)              zeros(p^2)          ;
       zeros(p^2,4*p*(p+1))     zeros(p^2,2*p*(p+1))        am_E21                      zeros(p^2)              zeros(p^2)              zeros(p^2)         ];

% figure
spy(LHS)

% figure
% imshow(LHS)
% cmap = [1 1 1;jet];
% imagesc(LHS)
% colormap(cmap)
% colorbar

% det(LHS)
% cond(LHS)

% A = inv(LHS);

%% RHS

RHS = zeros(total_local_dof + 4*p*(p+1) + 3*p^2, 1);

Fx_h = reduction.of2form_multi_element_5(fx_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);
Fy_h = reduction.of2form_multi_element_5(fy_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);
TQ_h = reduction.of2form_multi_element_5(tq_an, p, domain_mapping, domain_dX_dxii, domain_dX_deta, domain_dY_dxii, domain_dY_deta, K, element_bounds_x, element_bounds_y);

RHS(total_local_dof + 4*p*(p+1) + 1 : total_local_dof + 4*p*(p+1) + p^2)         = -Fx_h;
RHS(total_local_dof + 4*p*(p+1) + p^2 + 1 : total_local_dof + 4*p*(p+1) + 2*p^2) = -Fy_h;
RHS(total_local_dof + 4*p*(p+1) + 2*p^2 + 1:total_local_dof + 4*p*(p+1) + 3*p^2) = -TQ_h;
% RHS(total_local_dof + 2*p^2 + 1:total_local_dof + 3*p^2) = zeros(p^2,1);

%% boudnary condition

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);
% bottom boundary

for i = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,1);
    for xqd = 1:p+1
        %here i have used minus sign because unit normal is outward acting
        [x_rq, y_rq] = el_mapping(xp(xqd),-1,1,1);
        RHS(edgeij) = RHS(edgeij) - ep(i,xqd)*v_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),-1)*wp(xqd);
    end
end

for i = 1:p
    edgeij = local_nr_tau_xx + LN_tau_yx(i,1);
    for xqd = 1:p+1
        [x_rq, y_rq] = el_mapping(xp(xqd),-1,1,1);
        RHS(edgeij) = RHS(edgeij) - ep(i,xqd)*u_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(xp(xqd),-1)*wp(xqd);
    end
end

% top boundary

for i = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,p+1);
    for xqd = 1:p+1
        [x_rq, y_rq] = el_mapping(xp(xqd),1,1,1);
        %here i have used minus sign because unit normal is outward acting
        RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),1)*wp(xqd);
    end
end

for i = 1:p
    edgeij = local_nr_tau_xx + LN_tau_yx(i,p+1);
    for xqd = 1:p+1
        [x_rq, y_rq] = el_mapping(xp(xqd),1,1,1);
        RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(xp(xqd),1)*wp(xqd);
    end
end

% left boundary

for j = 1:p
    edgeij = LN_tau_xx(1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = el_mapping(-1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) - ep(j,yqd)*u_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(-1,xp(yqd))*wp(yqd);
    end
end

for j = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = el_mapping(-1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) - ep(j,yqd)*v_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(-1,xp(yqd))*wp(yqd);
    end
end

% right boundary

for j = 1:p
    edgeij = LN_tau_xx(p+1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = el_mapping(1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(1,xp(yqd))*wp(yqd);
    end
end

for j = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(p+1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = el_mapping(1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(1,xp(yqd))*wp(yqd);
    end
end

% RHS(1:total_local_dof) = 1/factor*RHS(1:total_local_dof);

%% boundary condition for torque equation 

BC_W = zeros(2*p*(p+1),1);
% bottom 

for i = 1:p
    edgeij = i;
    for xqd = 1:p+1
        [x_rq, y_rq] = el_mapping(xp(xqd),-1,1,1);
        BC_W(edgeij) = BC_W(edgeij) - ep(i,xqd)*lambdaW_an(x_rq,y_rq)*wp(xqd);
    end
end

% top 

for i = 1:p
    edgeij = p^2 + i;
    for xqd = 1:p+1
        [x_rq, y_rq] = el_mapping(xp(xqd),+1,1,1);
        BC_W(edgeij) = BC_W(edgeij) + ep(i,xqd)*lambdaW_an(x_rq,y_rq)*wp(xqd);
    end
end

% left 

for j = 1:p
    edgeij = p*(p+1) + j;
    for yqd = 1:p+1
        [x_rq, y_rq] = el_mapping(-1,xp(yqd),1,1);
        BC_W(edgeij) = BC_W(edgeij) - ep(j,yqd)*lambdaW_an(x_rq,y_rq)*wp(yqd);
    end
end

% right 

for j = 1:p
    edgeij = p*(p+1) + p^2 +j;
    for yqd = 1:p+1
        [x_rq, y_rq] = el_mapping(+1,xp(yqd),1,1);
        BC_W(edgeij) = BC_W(edgeij) + ep(j,yqd)*lambdaW_an(x_rq,y_rq)*wp(yqd);
    end
end

% RHS(6*p*(p+1) + 1 : 8*p*(p+1)) = BC_W;

%% direct solver

X_h = LHS\RHS;

% RHS';
% X_h';

%% separating cochains

temp = p*(p+1);

TXX_h = X_h(1:temp);
TYX_h = X_h(temp+1:2*temp);
TXY_h = X_h(2*temp+1:3*temp);
TYY_h = X_h(3*temp+1:4*temp);
lambda3.Xcochain = X_h(4*temp+1:5*temp);
lambda3.Ycochain = X_h(5*temp+1:6*temp);
TQ.Xcochain = X_h(6*temp+1:7*temp);
TQ.Ycochain = X_h(7*temp+1:8*temp);
U_h   = X_h(8*temp+1:8*temp+p^2);
V_h   = X_h(8*temp+p^2+1:8*temp+2*p^2);
W_h   = X_h(8*temp+2*p^2+1:8*temp+3*p^2);

%% reconstruction

[xp, wp] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_p_dx_dxii(:,:,el) = el_dX_dxii(xip,etap,elx,ely);
        eval_p_dx_deta(:,:,el) = el_dX_deta(xip,etap,elx,ely);
        eval_p_dy_dxii(:,:,el) = el_dY_dxii(xip,etap,elx,ely);
        eval_p_dy_deta(:,:,el) = el_dY_deta(xip,etap,elx,ely);
    end
end

eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);

M22s = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);

for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, eval_p_ggg(:,:,el));
end

U_h_2 = M22s\U_h;
V_h_2 = M22s\V_h;
W_h_2 = M22s\W_h;
% W_h_2 = W_h;

%% reconstruction

pf = 30;

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);

eval_pf_dx_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dx_deta = zeros(pf+1,pf+1,K^2);
eval_pf_dy_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dy_deta = zeros(pf+1,pf+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_pf_dx_dxii(:,:,el) = el_dX_dxii(xif,etaf,elx,ely);
        eval_pf_dx_deta(:,:,el) = el_dX_deta(xif,etaf,elx,ely);
        eval_pf_dy_dxii(:,:,el) = el_dY_dxii(xif,etaf,elx,ely);
        eval_pf_dy_deta(:,:,el) = el_dY_deta(xif,etaf,elx,ely);
    end
end

eval_pf_ggg = metric.ggg(eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);

%% reconstruction of lambdaX

lambdaX2.pf          = pf;
lambdaX2.cochain     = U_h_2;
lambdaX2.eval_pf_ggg = eval_pf_ggg;

[xf_3D, yf_3D, uf_h] = lambdaX2.reconstruction();

figure
subplot(4,4,13)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), uf_h(:,:,el)')
end
title('computed U');
colorbar

u_plot = lambdaX_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,9)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el))
% contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el),'edgecolor','none')
title('analytical U');
colorbar

%% reconstruction of lambdaY

lambdaY2.pf          = pf;
lambdaY2.cochain     = V_h_2;
lambdaY2.eval_pf_ggg = eval_pf_ggg;

[xf_3D, yf_3D, vf_h] = lambdaY2.reconstruction();

subplot(4,4,14)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), vf_h(:,:,el)')
end
title('computed V');
colorbar

v_plot = lambdaY_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,10)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), v_plot(:,:,el))
title('analytical V');
colorbar

%% reconstruction of W - omega / rotation

lambdaW2.pf          = pf;
lambdaW2.cochain     = W_h_2;
lambdaW2.eval_pf_ggg = eval_pf_ggg;

[xf_3D, yf_3D, wf_h] = lambdaW2.reconstruction();

% [xf_3D, yf_3D, wf_h] = reconstruction.of2form_multi_element_v4(W_h_2, p, pf, el_mapping, eval_pf_ggg, K);

subplot(4,4,15)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), wf_h(:,:,el)')
end
title('computed W');
colorbar

w_plot = w_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,11)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), w_plot(:,:,el))
title('analytical W');
colorbar

%% reconstruction of txx

tauX.pf   = pf;

tauX.Xcochain = TYX_h;
tauX.Ycochain = TXX_h;

tauX.eval_pf_dx_dxii = eval_pf_dx_dxii;
tauX.eval_pf_dx_deta = eval_pf_dx_deta;
tauX.eval_pf_dy_dxii = eval_pf_dy_dxii;
tauX.eval_pf_dy_deta = eval_pf_dy_deta;

tau_xx_f_h = tauX.reconstructionY();

subplot(4,4,5)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xx_f_h(:,:,el)')
title('computed tau_xx');
colorbar

txx_plot = tau_xx_an(xf_3D, yf_3D) + 1e-15 * rand(31,31);

subplot(4,4,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txx_plot(:,:,el))
title('analytical tau_xx');
colorbar

%% reconstruction of tyx

tau_yx_f_h = tauX.reconstructionX();

subplot(4,4,8)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yx_f_h(:,:,el)')
title('computed tau_yx');
colorbar

tyx_plot = tau_yx_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(4,4,4)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyx_plot(:,:,el))
title('analytical tau_yx');
colorbar

% figure 
% contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yx_f_h(:,:,el)')
% title('computed tau_yx');
% colorbar

% figure
% spy(LHS)

%% reconstruction of txy

tauY.pf   = pf;

tauY.Xcochain = TYY_h;
tauY.Ycochain = TXY_h;

tauY.eval_pf_dx_dxii = eval_pf_dx_dxii;
tauY.eval_pf_dx_deta = eval_pf_dx_deta;
tauY.eval_pf_dy_dxii = eval_pf_dy_dxii;
tauY.eval_pf_dy_deta = eval_pf_dy_deta;

tau_xy_f_h = tauY.reconstructionY();

subplot(4,4,7)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xy_f_h(:,:,el)')
title('computed tau_xy');
colorbar

txy_plot = tau_xy_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(4,4,3)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txy_plot(:,:,el))
title('analytical tau_xy');
colorbar

%% reconstruction of tyy

tau_yy_f_h = tauY.reconstructionX();

subplot(4,4,6)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yy_f_h(:,:,el)')
title('computed tau_yy');
colorbar

tyy_plot = tau_yy_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(4,4,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyy_plot(:,:,el))
title('analytical tau_yy');
colorbar

%% reconstruction linear momentum in X

TR = [TXX_h; TYX_h; TXY_h; TYY_h];

LM_X_h = Ex * TR + Fx_h;

[~, ~, lm_x_h] = reconstruction.of2form_multi_element_v4(LM_X_h, p, pf, el_mapping, eval_pf_ggg, K);

%% reconstruction linear momentum in Y

LM_Y_h = Ey * TR + Fy_h;

[~, ~, lm_y_h] = reconstruction.of2form_multi_element_v4(LM_Y_h, p, pf, el_mapping, eval_pf_ggg, K);

%% reconstruction angular momentum

% Err_AM_h = W * TR + TQ_h;

% [xf_3D, yf_3D, err_am_h] = reconstruction.of2form_multi_element_v4(Err_AM_h, p, pf, el_mapping, eval_pf_ggg, K);

%% reconstruction of TQx

TQ.pf   = pf;

TQ.eval_pf_dx_dxii = eval_pf_dx_dxii;
TQ.eval_pf_dx_deta = eval_pf_dx_deta;
TQ.eval_pf_dy_dxii = eval_pf_dy_dxii;
TQ.eval_pf_dy_deta = eval_pf_dy_deta;

TQx_f_h = TQ.reconstructionX();

figure
subplot(2,4,5)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), TQx_f_h')
title('computed TQx');
colorbar

TQx_plot = TQx_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(2,4,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), TQx_plot)
title('analytical TQx');
colorbar

TQy_f_h = TQ.reconstructionY();

subplot(2,4,6)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), TQy_f_h')
title('computed TQy');
colorbar

TQy_plot = TQy_an(xf_3D, yf_3D)  + 1e-15 * rand(31,31);

subplot(2,4,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), TQy_plot)
title('analytical TQy');
colorbar

%% reconstruction of lambda3

lambda3.pf   = pf;

lambda3.eval_pf_dx_dxii = eval_pf_dx_dxii;
lambda3.eval_pf_dx_deta = eval_pf_dx_deta;
lambda3.eval_pf_dy_dxii = eval_pf_dy_dxii;
lambda3.eval_pf_dy_deta = eval_pf_dy_deta;

lambda3x_f_h = lambda3.reconstructionX();

set(0,'defaultfigureposition',[0 0 1900 1000])

subplot(2,4,7)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), lambda3x_f_h')
title('computed lambda3x');
colorbar

lambda3y_f_h = lambda3.reconstructionY();

subplot(2,4,8)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), lambda3y_f_h')
title('computed lambda3y');
colorbar

%% error estimates

wff = kron(wf,wf');

% tau_xx 
err_tauXX = txx_plot - tau_xx_f_h';
l2_err_tauXX = elasticity_non_staggered.l2_error(err_tauXX, wff);

% tau_yy
err_tauYY = tyy_plot - tau_yy_f_h';
l2_err_tauYY = elasticity_non_staggered.l2_error(err_tauYY, wff);

% tau_xy 
err_tauXY = txy_plot - tau_xy_f_h';
l2_err_tauXY = elasticity_non_staggered.l2_error(err_tauXY, wff);

% tau_yx
err_tauYX = tyx_plot - tau_yx_f_h';
l2_err_tauYX = elasticity_non_staggered.l2_error(err_tauYX, wff);

% lambda - X
err_lambdaX = u_plot - uf_h';
l2_err_lambdaX = elasticity_non_staggered.l2_error(err_lambdaX, wff);

% lambda - Y
err_lambdaY = v_plot - vf_h';
l2_err_lambdaY = elasticity_non_staggered.l2_error(err_lambdaY, wff);

% lambda - w 
err_lambdaW = w_plot - wf_h';
l2_err_lambdaW = elasticity_non_staggered.l2_error(err_lambdaW, wff);

% symmetry 
err_symm = tau_xy_f_h' - tau_yx_f_h';
l2_err_symm = elasticity_non_staggered.l2_error(err_symm, wff);

% linear momentum in X
l2_err_lmX = elasticity_non_staggered.l2_error(lm_x_h, wff);

% linear momentum in Y
l2_err_lmY = elasticity_non_staggered.l2_error(lm_y_h, wff);

% angular momentum 
% l2_err_am = elasticity_non_staggered.l2_error(err_am_h, wff);