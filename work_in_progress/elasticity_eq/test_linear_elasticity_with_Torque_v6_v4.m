%% Created on 18th Sep, 2018 

clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../elasticity_eq1')
addpath('../../finished_programs/elasticity_eq')

%% analytical solution

E = 1;
nu = 0.3;

u_an     = @(x,y) sin(pi*x).*sin(pi*y);
v_an     = @(x,y) sin(pi*x).*sin(pi*y);
du_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
du_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
dv_dx_an = @(x,y) pi*cos(pi*x).*sin(pi*y);
dv_dy_an = @(x,y) pi*sin(pi*x).*cos(pi*y);
d2u_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);
d2v_dy2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2v_dx2  = @(x,y) -pi^2*sin(pi*x).*sin(pi*y);
d2u_dxdy = @(x,y) pi^2*cos(pi*x).*cos(pi*y);

e_xx_an = @(x,y) du_dx_an(x,y);
e_yy_an = @(x,y) dv_dy_an(x,y);
e_xy_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));
e_yx_an = @(x,y) 0.5*(du_dy_an(x,y) + dv_dx_an(x,y));

w_an      = @(x,y) 0.5*(dv_dx_an(x,y) - du_dy_an(x,y));
w_an      = @(x,y) w_an(x,y);

tau_xx_an = @(x,y) E/(1-nu^2)*(e_xx_an(x,y) + nu*e_yy_an(x,y));
tau_yy_an = @(x,y) E/(1-nu^2)*(e_yy_an(x,y) + nu*e_xx_an(x,y));
tau_xy_an = @(x,y) E/(1+nu)*e_xy_an(x,y);
tau_yx_an = @(x,y) E/(1+nu)*e_yx_an(x,y);
fx_an     = @(x,y) -E/(1-nu^2) * ( d2u_dx2(x,y) + 0.5*(1-nu)*d2u_dy2(x,y) + 0.5*(1+nu)*d2v_dxdy(x,y) );
fy_an     = @(x,y) -E/(1-nu^2) * ( d2v_dy2(x,y) + 0.5*(1-nu)*d2v_dx2(x,y) + 0.5*(1+nu)*d2u_dxdy(x,y) );
tq_an     = @(x,y) -y.*fx_an(x,y) + x.*fy_an(x,y);

lambdaX_an  = @(x,y) u_an(x,y) + y.*w_an(x,y);
lambdaY_an  = @(x,y) v_an(x,y) - x.*w_an(x,y);

tqX_an    = @(x,y) x.*tau_yy_an(x,y) - y.*tau_yx_an(x,y);
tqY_an    = @(x,y) x.*tau_xy_an(x,y) - y.*tau_xx_an(x,y);

% lambdaX_an  = @(x,y) u_an(x,y);
% lambdaY_an  = @(x,y) v_an(x,y);
lambdaW_an  = @(x,y) w_an(x,y);

%% domain

% xbound = [3.3 4.9];
% ybound = [2.1 5.3];

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

%% number of elements

K = 1;
p = 3;

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

ttl_nr_el = K^2;
local_nr_pp = p^2;

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

total_local_dof = local_nr_tau_xx + local_nr_tau_xy + local_nr_tau_yx + local_nr_tau_yy;

%% calculate jacobian terms terms for mass matrix

mesh1.eval_p_der();

%% calculation of metric terms

[gg] = metric_terms_Yi_v2(mesh1.eval.p);

%% local numbering

[LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = elasticity_non_staggered.local_numbering(p);

%% constitutive mass matrix 

MS = full_mass_matrix_g_v2_vectorize_v2(p, gg);

%% conservation of linear momentum 

Ex = elasticity_non_staggered.incidence_matrix.linear_momentum_x(p, LN_tau_xx, LN_tau_yx);
Ey = elasticity_non_staggered.incidence_matrix.linear_momentum_y(p, LN_tau_yy, LN_tau_xy);

%% conservation of angular momentum 

am_E21 = elasticity_non_staggered.incidence_matrix.AM_incidence_matrix(p);

%% new mass matrix for 2nd constittutive law 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

% p_int = p+1;
% [quad_int_xi, wp_int] = GLLnodes(p_int);

[x_int,y_int] = meshgrid(xp);

% x_int2 = x_int(:);
% y_int2 = y_int(:);
% 
% [hp2, ep2] = MimeticpolyVal(y_int2,p,1);

x_int = x_int';
y_int = y_int';

% plot(x_int,y_int,'+r')

MAM = zeros(2*p*(p+1),4*p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        
%         xi_i1 = xp(i);
%         xi_i2 = xp(i+1);
%         eta_j = xp(j);
        
        % points on physical domain
        %         [x_int,y_int] = map_local_edge(xbound, ybound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         [x_int,y_int] = el_mapping(quad_int_xi,quad_int_xi,1,1);
        
        %------ with Txx 
%         for k = 1:p+1
%             for l = 1:p
%                 dofkl = LN_tau_xx(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - 0*x_int(xq,yq)*nu*ep(i,xq)*hp(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Tyx
        for k = 1:p
            for l = 1:p+1
                dofkl = p*(p+1) + LN_tau_yx(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - y_int(xq,yq)*ep(i,xq)*hp(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Txy
%         for k = 1:p+1
%             for l = 1:p
%                 dofkl = 2*p*(p+1) + LN_tau_xy(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + 0*ep(i,xq)*hp(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Tyy
        for k = 1:p
            for l =1:p+1
                dofkl = 3*p*(p+1) + LN_tau_yy(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + x_int(xq,yq)*ep(i,xq)*hp(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
    end
end

for i = 1:p+1
    for j =1:p
        edgeij = p*(p+1) + (i-1)*p +j;
        
%         xi_i  = xp(i);
%         eta_j1 = xp(j);
%         eta_j2 = xp(j+1);
        
        % points on physical domain
%         [x_int,y_int] = map_local_y_edge(xbound, ybound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
        %------ with Txx 
        for k = 1:p+1
            for l = 1:p
                dofkl = LN_tau_xx(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - y_int(xq,yq)*hp(i,xq)*ep(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Tyx
%         for k = 1:p
%             for l = 1:p+1
%                 dofkl = p*(p+1) + LN_tau_yx(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) - *hp(i,xq)*ep(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end
        
        %------ with Txy
        for k = 1:p+1
            for l = 1:p
                dofkl = 2*p*(p+1) + LN_tau_xy(k,l);
                for xq = 1:p+1
                    for yq = 1:p+1
                        MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + x_int(xq,yq)*hp(i,xq)*ep(j,yq)*hp(k,xq)*ep(l,yq)*wp(xq)*wp(yq);
                    end
                end
            end
        end
        
        %------ with Tyy
%         for k = 1:p
%             for l =1:p+1
%                 dofkl = 3*p*(p+1) + LN_tau_yy(k,l);
%                 for xq = 1:p+1
%                     for yq = 1:p+1
%                         MAM(edgeij,dofkl) = MAM(edgeij,dofkl) + y_int(xq,yq)*nu*hp(i,xq)*ep(j,yq)*ep(k,xq)*hp(l,yq)*wp(xq)*wp(yq);
%                     end
%                 end
%             end
%         end

    end
end

% spy(MAM)

%% mass matrix for torque 

eval_p_ggg = metric.ggg(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta);
eval_p_g11 = metric.g11(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, eval_p_ggg);
eval_p_g12 = metric.g12(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, eval_p_ggg);
eval_p_g22 = metric.g22(mesh1.eval.p.dx_dxii, mesh1.eval.p.dx_deta, mesh1.eval.p.dy_dxii, mesh1.eval.p.dy_deta, eval_p_ggg);

el=1;
M11 = mass_matrix.M1_3(p, eval_p_g11(:,:,el), eval_p_g12(:,:,el), eval_p_g22(:,:,el));

%% LHS system 

LHS = [MS                       -(M11\MAM)'                 zeros(4*p*(p+1),2*p*(p+1))  Ex'                     Ey'                     zeros(4*p*(p+1),p^2);
       -M11\MAM                 zeros(2*p*(p+1))            eye(2*p*(p+1))              zeros(2*p*(p+1),p^2)    zeros(2*p*(p+1),p^2)    zeros(2*p*(p+1),p^2); 
       zeros(2*p*(p+1),4*p*(p+1))   eye(2*p*(p+1))          zeros(2*p*(p+1))            zeros(2*p*(p+1),p^2)    zeros(2*p*(p+1),p^2)    am_E21'             ;
       Ex                       zeros(p^2,2*p*(p+1))        zeros(p^2,2*p*(p+1))        zeros(p^2)              zeros(p^2)              zeros(p^2)          ;
       Ey                       zeros(p^2,2*p*(p+1))        zeros(p^2,2*p*(p+1))        zeros(p^2)              zeros(p^2)              zeros(p^2)          ;
       zeros(p^2,4*p*(p+1))     zeros(p^2,2*p*(p+1))        am_E21                      zeros(p^2)              zeros(p^2)              zeros(p^2)         ];

% spy(LHS)
det(LHS)

%% RHS

RHS = zeros(total_local_dof + 4*p*(p+1) + 3*p^2, 1);

volForm = twoForm_v2(mesh1);

Fx_h = volForm.reduction(fx_an);
Fy_h = volForm.reduction(fy_an);
TQ_h = volForm.reduction(tq_an);

RHS(total_local_dof + 4*p*(p+1) + 1 : total_local_dof + 4*p*(p+1) + p^2)         = -Fx_h;
RHS(total_local_dof + 4*p*(p+1) + p^2 + 1 : total_local_dof + 4*p*(p+1) + 2*p^2) = -Fy_h;
RHS(total_local_dof + 4*p*(p+1) + 2*p^2 + 1:total_local_dof + 4*p*(p+1) + 3*p^2) = -TQ_h;

%% TQ from projected f !!! 

M22s = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);
for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, mesh1.eval.p.ggg(:,:,el));
end

TQfx = zeros(p^2);
TQfy = zeros(p^2);

for i = 1:p
    for j = 1:p
        edgeij = (i-1)*p +j;
        for k = 1:p
            for l =1:p
                edgekl = (k-1)*p +l;
                for px = 1:p+1
                    for py = 1:p+1
                        TQfx(edgeij, edgekl) = TQfx(edgeij, edgekl) + ep(i,px)*ep(j,py)*x_int(px,py)*ep(k,px)*ep(l,py)*wp(px)*wp(py);
                    end
                end
            end
        end
    end
end

for i = 1:p
    for j = 1:p
        edgeij = (i-1)*p +j;
        for k = 1:p
            for l =1:p
                edgekl = (k-1)*p +l;
                for px = 1:p+1
                    for py = 1:p+1
                        TQfy(edgeij, edgekl) = TQfy(edgeij, edgekl) + ep(i,px)*ep(j,py)*y_int(px,py)*ep(k,px)*ep(l,py)*wp(px)*wp(py);
                    end
                end
            end
        end
    end
end

TQQ = TQfx * Fy_h - TQfy * Fx_h;
TQQ2 = M22s\TQQ;

% RHS(total_local_dof + 4*p*(p+1) + 2*p^2 + 1:total_local_dof + 4*p*(p+1) + 3*p^2) = -TQQ2;

%% another conservation of angular momentum :-/

% TQQ3 = TQfx * Ey - TQfy * Ex;
% TQQ3 = M22s\TQQ3;
% 
% LHS2 = [MS      Ex'         Ey'         TQQ3';
%         Ex      zeros(p^2)  zeros(p^2)  zeros(p^2);
%         Ey      zeros(p^2)  zeros(p^2)  zeros(p^2);
%         TQQ3    zeros(p^2)  zeros(p^2)  zeros(p^2)];
% 
% spy(LHS2)
%     
% a = det(LHS2)

%% boudnary condition

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);
% bottom boundary

for i = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,1);
    for xqd = 1:p+1
        %here i have used minus sign because unit normal is outward acting
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),-1,1,1);
        RHS(edgeij) = RHS(edgeij) - ep(i,xqd)*v_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),-1)*wp(xqd);
    end
end

for i = 1:p
    edgeij = local_nr_tau_xx + LN_tau_yx(i,1);
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),-1,1,1);
        RHS(edgeij) = RHS(edgeij) - ep(i,xqd)*u_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(xp(xqd),-1)*wp(xqd);
    end
end

% top boundary

for i = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,p+1);
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),1,1,1);
        %here i have used minus sign because unit normal is outward acting
        RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),1)*wp(xqd);
    end
end

for i = 1:p
    edgeij = local_nr_tau_xx + LN_tau_yx(i,p+1);
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),1,1,1);
        RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*u_an(xp(xqd),1)*wp(xqd);
    end
end

% left boundary

for j = 1:p
    edgeij = LN_tau_xx(1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(-1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) - ep(j,yqd)*u_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(-1,xp(yqd))*wp(yqd);
    end
end

for j = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(-1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) - ep(j,yqd)*v_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(-1,xp(yqd))*wp(yqd);
    end
end

% right boundary

for j = 1:p
    edgeij = LN_tau_xx(p+1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(1,xp(yqd))*wp(yqd);
    end
end

for j = 1:p
    edgeij = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(p+1,j);
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(1,xp(yqd),1,1);
        RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*v_an(1,xp(yqd))*wp(yqd);
    end
end

%% vorticity boundary conditions 

RHS2 = zeros(2*p*(p+1),1);

% bottom worticity 

for i = 1:p
    edgeij = i;
    for xqd = 1:p+1
        %here i have used minus sign because unit normal is outward acting
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),-1,1,1);
        RHS2(edgeij) = RHS2(edgeij) - ep(i,xqd)*w_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),-1)*wp(xqd);
    end
end
    
% right vortcity 

for j = 1:p
    edgeij = p*(p+1) + p^2 + j;
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(1,xp(yqd),1,1);
        RHS2(edgeij) = RHS2(edgeij) + ep(j,yqd)*w_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(1,xp(yqd))*wp(yqd);
    end
end

% top vorticity 

for i = 1:p
    edgeij = p^2 + i;
    for xqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(xp(xqd),1,1,1);
        %here i have used minus sign because unit normal is outward acting
        RHS2(edgeij) = RHS2(edgeij) - ep(i,xqd)*w_an(x_rq,y_rq)*wp(xqd);
%         RHS(edgeij) = RHS(edgeij) + ep(i,xqd)*v_an(xp(xqd),1)*wp(xqd);
    end
end

% left vorticity 

for j = 1:p
    edgeij = p*(p+1) + j;
    for yqd = 1:p+1
        [x_rq, y_rq] = mesh1.el.mapping(-1,xp(yqd),1,1);
        RHS2(edgeij) = RHS2(edgeij) - ep(j,yqd)*w_an(x_rq,y_rq)*wp(yqd);
%         RHS(edgeij) = RHS(edgeij) + ep(j,yqd)*u_an(-1,xp(yqd))*wp(yqd);
    end
end

% RHS(6*p*(p+1)+1:8*p*(p+1),1) = RHS2;

%% direct solver

X_h = LHS\RHS;

%% separating cochains

temp = p*(p+1);

TXX_h = X_h(1:temp);
TYX_h = X_h(temp+1:2*temp);
TXY_h = X_h(2*temp+1:3*temp);
TYY_h = X_h(3*temp+1:4*temp);
alphX = X_h(4*temp+1:5*temp);
alphY = X_h(5*temp+1:6*temp);
TQx   = X_h(6*temp+1:7*temp);
TQy   = X_h(7*temp+1:8*temp);
U_h   = X_h(8*temp+1:8*temp+p^2);
V_h   = X_h(8*temp+p^2+1:8*temp+2*p^2);
W_h   = X_h(8*temp+2*p^2+1:8*temp+3*p^2);

%% reconstruction

M22s = zeros(local_nr_pp, local_nr_pp, ttl_nr_el);
for el = 1:ttl_nr_el
    M22s(:,:,el) = mass_matrix.M2_2(p, mesh1.eval.p.ggg(:,:,el));
end

U_h_2 = M22s\U_h;
V_h_2 = M22s\V_h;
W_h_2 = M22s\W_h;

%% reconstruction

pf = 30;
mesh1.eval_pf_der(pf);

%% reconstruction of lambdaX

[xf_3D, yf_3D, uf_h] = volForm.reconstruction(U_h_2);

figure
subplot(4,4,13)
hold on
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), uf_h(:,:,el)')
end
title('computed U');
colorbar

u_plot = lambdaX_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,9)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), u_plot(:,:,el))
title('analytical U');
colorbar

%% reconstruction of lambdaY

[xf_3D, yf_3D, vf_h] = volForm.reconstruction(V_h_2);

subplot(4,4,14)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), vf_h(:,:,el)')
end
title('computed V');
colorbar

v_plot = lambdaY_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,10)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), v_plot(:,:,el))
title('analytical V');
colorbar

%% reconstruction of W - omega / rotation

[xf_3D, yf_3D, wf_h] = volForm.reconstruction(W_h_2);

subplot(4,4,15)
for el = 1:K^2
    contourf(xf_3D(:,:,el), yf_3D(:,:,el), wf_h(:,:,el)')
end
title('computed W');
colorbar

w_plot = w_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,11)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), w_plot(:,:,el))
title('analytical W');
colorbar

%% reconstruction of txx

surForm = oneForm_v2(mesh1);

SIG_X = [TYX_h; TXX_h];
SIG_Y = [TYY_h; TXY_h];

recX = surForm.reconstruction(SIG_X);
recY = surForm.reconstruction(SIG_Y);

tau_yx_f_h = recX.qx;
tau_xx_f_h = -recX.qy;

tau_yy_f_h = recY.qx;
tau_xy_f_h = -recY.qy;

subplot(4,4,5)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xx_f_h(:,:,el)')
title('computed tau_xx');
colorbar

txx_plot = tau_xx_an(xf_3D, yf_3D) + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txx_plot(:,:,el))
title('analytical tau_xx');
colorbar

%% reconstruction of tyy

subplot(4,4,6)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yy_f_h(:,:,el)')
title('computed tau_yy');
colorbar

tyy_plot = tau_yy_an(xf_3D, yf_3D)  + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyy_plot(:,:,el))
title('analytical tau_yy');
colorbar

%% reconstruction of txy

subplot(4,4,7)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_xy_f_h(:,:,el))
title('computed tau_xy');
colorbar

txy_plot = tau_xy_an(xf_3D, yf_3D)  + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,3)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), txy_plot(:,:,el))
title('analytical tau_xy');
colorbar

%% reconstruction of tyx

subplot(4,4,8)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tau_yx_f_h(:,:,el)')
title('computed tau_yx');
colorbar

tyx_plot = tau_yx_an(xf_3D, yf_3D)  + 1e-15 * rand(pf+1,pf+1);

subplot(4,4,4)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tyx_plot(:,:,el))
title('analytical tau_yx');
colorbar

%% reconstruction linear momentum in X

TR = [TXX_h; TYX_h; TXY_h; TYY_h];

LM_X_h = Ex * TR + Fx_h;

[~, ~, lm_x_h] = volForm.reconstruction(LM_X_h);

%% reconstruction linear momentum in Y

LM_Y_h = Ey * TR + Fy_h;

[~, ~, lm_y_h] = volForm.reconstruction(LM_Y_h);

%% reconstruction angular momentum

%% reconstruction of TQ

TQ = [TQx; TQy];

recTQ = surForm.reconstruction(TQ);

figure
subplot(1,2,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), recTQ.qx(:,:,el)')
title('computed TQx');
colorbar

tqX_an_ff = tqX_an(xf_3D,yf_3D);

subplot(1,2,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tqX_an_ff(:,:,el)')
title('analytical TQx');
colorbar

figure
subplot(1,2,2)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), -recTQ.qy(:,:,el)')
title('computed TQy');
colorbar

tqY_an_ff = tqY_an(xf_3D,yf_3D);

subplot(1,2,1)
contourf(xf_3D(:,:,el), yf_3D(:,:,el), tqY_an_ff(:,:,el)')
title('analytical TQy');
colorbar

%% reconstruction of alpha 

alphA = [alphX; alphY];

alphA_2(:,el) = M11(:,:,el)\alphA(:,el);

recalphA = surForm.reconstruction(alphA_2);

figure
contourf(xf_3D(:,:,el), yf_3D(:,:,el), recalphA.qx(:,:,el)')
title('computed recalphAx');
colorbar

figure
contourf(xf_3D(:,:,el), yf_3D(:,:,el), -recalphA.qy(:,:,el)')
title('computed recalphAy');
colorbar

%% error estimates

[~, wf] = GLLnodes(pf);
wff = kron(wf,wf');

% tau_xx 
err_tauXX = txx_plot - tau_xx_f_h';
l2_err_tauXX = elasticity_non_staggered.l2_error(err_tauXX, wff);

% tau_yy
err_tauYY = tyy_plot - tau_yy_f_h';
l2_err_tauYY = elasticity_non_staggered.l2_error(err_tauYY, wff);

% tau_xy 
err_tauXY = txy_plot - tau_xy_f_h';
l2_err_tauXY = elasticity_non_staggered.l2_error(err_tauXY, wff);

% tau_yx
err_tauYX = tyx_plot - tau_yx_f_h';
l2_err_tauYX = elasticity_non_staggered.l2_error(err_tauYX, wff);

% lambda - X
err_lambdaX = u_plot - uf_h';
l2_err_lambdaX = elasticity_non_staggered.l2_error(err_lambdaX, wff);

% lambda - Y
err_lambdaY = v_plot - vf_h';
l2_err_lambdaY = elasticity_non_staggered.l2_error(err_lambdaY, wff);

% lambda - w 
err_lambdaW = w_plot - wf_h';
l2_err_lambdaW = elasticity_non_staggered.l2_error(err_lambdaW, wff);

% symmetry 
err_symm = tau_xy_f_h' - tau_yx_f_h';
l2_err_symm = elasticity_non_staggered.l2_error(err_symm, wff);

% linear momentum in X
l2_err_lmX = elasticity_non_staggered.l2_error(lm_x_h, wff);

% linear momentum in Y
l2_err_lmY = elasticity_non_staggered.l2_error(lm_y_h, wff);

% angular momentum 
% l2_err_am = elasticity_non_staggered.l2_error(err_am_h, wff);