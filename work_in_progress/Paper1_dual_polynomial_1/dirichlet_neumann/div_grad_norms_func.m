function [phi_H1,q_Hdiv] = div_grad_norms_func(p, c)

%% domain

xbound = [0 1];
ybound = [0 1];

%% boundary conditions

bc_bott = @(x,y) 0*ones(size(x));
bc_topp = @(x,y) -log(1-3*x.*(1-x));
bc_left = @(x,y) 0*ones(size(x));
bc_rght = @(x,y) -sin(pi*y);

%% discretization

K = 1;
% p = 8;

ttl_nr_el = K^2;
% local_nr_ed = 2*p*(p+1);
% local_nr_pp = p^2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% M1 mass matrix

% evaluate jacobian at GLL points
mesh1.eval_p_der_v2();

% evaluate metric terms for M11
eval.p = flow.poisson.eval_metric(mesh1.eval.p);

mesh1.eval.metric = eval.p;

M11 = zeros(2*p*(p+1),2*p*(p+1),ttl_nr_el);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% M2 mass matrix

M22s = zeros(p^2,p^2,ttl_nr_el);

for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end

%% E21 incidence matrix

E211    = incidence_matrix.E21(p);


%% LHS - Neumann

LHS_Neumann = E211' * M22s * E211 + M11;

%% RHS 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);
            
% bottom

BC_bott = zeros(p,1);

for i=1:p
    edgei = i;
    for ss=1:p+1
        [xx, yy] = mesh1.domain.mapping(xp(ss),-1);
        BC_bott(edgei) = BC_bott(edgei) + bc_bott(xx,yy) * ep(i,ss) * wp(ss);
    end
end

% top

BC_topp = zeros(p,1);

for i=1:p
    edgei = i;
    for ss=1:p+1
        [xx, yy] = mesh1.domain.mapping(xp(ss),+1);
        BC_topp(edgei) = BC_topp(edgei) + bc_topp(xx,yy) * ep(i,ss) * wp(ss);
    end
end

% left

BC_left = zeros(p,1);

for j=1:p
    edgej = j;
    for tt=1:p+1
        [xx, yy] = mesh1.domain.mapping(-1,xp(tt));
        BC_left(edgej) = BC_left(edgej) + bc_left(xx,yy) * ep(j,tt) * wp(tt);
    end
end

% right

BC_rght = zeros(p,1);

for j=1:p
    edgej = j;
    for tt=1:p+1
        [xx, yy] = mesh1.domain.mapping(+1,xp(tt));
        BC_rght(edgej) = BC_rght(edgej) + bc_rght(xx,yy) * ep(j,tt) * wp(tt);
    end
end

BC(1:p)                 = -BC_bott;
BC(p^2+1:p^2+p)         = BC_topp;
BC(p^2+p+1:p^2+2*p)     = -BC_left;
BC(2*p^2+p+1:2*p^2+2*p) = BC_rght;

%% RHS - Neumann

RHS_Neuman = BC';

%% XX - Neumann

XX_Neumann = LHS_Neumann\RHS_Neuman;

%% separating cochains - Neumann

q_h = XX_Neumann;
div_q_h = E211 * q_h;

rel = M22s * E211 * q_h;

%% LHS - Dirichlet 

LHS_Dirichlet = E211 * inv(M11) * E211' + inv(M22s);

%% RHS - Dirichlet 

RHS_Dirichlet = E211 * inv(M11) * BC';

%% XX - Dirichlet 

XX_Dirichlet = LHS_Dirichlet\RHS_Dirichlet;

phi_dual = XX_Dirichlet;

%% error in relation

err1 = max(XX_Dirichlet - rel)

%% jacobian derivatives for post processing

pf = p+20;
mesh1.eval_pf_der(pf);

%% post processing div q

fs = 16

volForm = twoForm_v2(mesh1);

[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(div_q_h);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

% pp_ex = phi_an(xf_3D, yf_3D);
% [error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h,250,'LineColor','none')
% end
% colormap('jet')
% colorbar('TickLabelInterpreter','latex','FontSize',fs)
% 
% xlabel('$$ x $$', 'Interpreter','latex','FontSize',fs);
% ylabel('$$ y $$', 'Interpreter','latex','FontSize',fs);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% post processing of phi

%% dual to primal dof

phi_primal = M22s\phi_dual;

[xf_3D, yf_3D, phif_h] = volForm.reconstruction(phi_primal);
phif_h = permute(phif_h, [2 1 3]); % transpose along dim = 3.

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), divQf_h - phif_h,250,'LineColor','none')
% end
% 
% colormap('jet')
% 
% colorbar('TickLabelInterpreter','latex','FontSize',fs)
% 
% xlabel('$$ x $$', 'Interpreter','latex','FontSize',fs);
% ylabel('$$ y $$', 'Interpreter','latex','FontSize',fs);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% evaluation of norms

phi_H1 = sqrt(phi_dual' * inv(M22s) * phi_dual + (RHS_Neuman' - phi_dual' * E211) * inv(M11) * (RHS_Neuman - E211' * phi_dual))

q_Hdiv = sqrt(q_h' * M11 * q_h + q_h' * E211' * M22s * E211 * q_h)

end

