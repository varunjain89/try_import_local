clc
close all
clear variables

addpath('../../..')
addpath('../../../MSEM')
addpath('../../../export_fig')

%% c = 0.0

c = 0.0;

for p = 2:2:18
    i = p/2;
    [c1(i).phi, c1(i).qqq] = div_grad_norms_func(p, c);
    p
end

%% c = 0.15

c = 0.15;

for p = 2:2:18
    i = p/2;
    [c2(i).phi, c2(i).qqq] = div_grad_norms_func(p, c);
    p
end

%% c = 0.3

c = 0.3;

for p = 2:2:18
    i = p/2;
    [c3(i).phi, c3(i).qqq] = div_grad_norms_func(p, c);
    p
end
