clc
close all
clear variables

addpath('../../..')
addpath('../../../MSEM')
% addpath('../../export_fig')

%% case 1, c= 0.0

c1 = 0.0;

for p = 2:2:30
    i = p/2;
    p
    
    fname_K1 = sprintf('N_%d', p);
    [XX_h.c1.(genvarname([fname_K1])), error.c1(i)] = posson_dual_v1_func(p, c1);
    
    fname = sprintf('XX_h_Poisson_dual.mat');
    save(fname,'XX_h');
end

%% case 2, c = 0.3

c2 = 0.3;

for p = 2:2:30
    i = p/2;
    p
    
    fname_K1 = sprintf('N_%d', p);
    [XX_h.c2.(genvarname([fname_K1])), error.c2(i)] = posson_dual_v1_func(p, c2);
    
    fname = sprintf('XX_h_Poisson_dual.mat');
    save(fname,'XX_h');
end

%% 

save('error_data_dual.mat','error');
