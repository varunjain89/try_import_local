function [data_divQ_error_x, data_divQ_error_y] = divQ_error(XX_h, p)


E211    = incidence_matrix.E21(p);

q_h = XX_h(1:2*p*(p+1));

divQ = E211 * q_h;


[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(divQ);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

ff_ex = f_an(xf_3D, yf_3D);

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

[error.divQ] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

end

