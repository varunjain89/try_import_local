clc
close all
clear variables

addpath('../../../..')
addpath('../../../../MSEM')
% addpath('../../export_fig')

%% analytical solution

% case 2

phi_an  = @(x,y) sin(2*pi*x).*sin(2*pi*y);

dp_dx   = @(x,y) 2*pi*cos(2*pi*x).*sin(2*pi*y);
dp_dy   = @(x,y) 2*pi*sin(2*pi*x).*cos(2*pi*y);
d2p_dx2 = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);
d2p_dy2 = @(x,y) -4*pi^2*sin(2*pi*x).*sin(2*pi*y);

ux_an   = @(x,y) dp_dx(x,y);
uy_an   = @(x,y) dp_dy(x,y);
f_an    = @(x,y) d2p_dx2(x,y) + d2p_dy2(x,y);

%% domain

xbound = [0 1];
ybound = [0 1];
c = 0.0;

%% discretization

K = 1;
p = 15;

ttl_nr_el = K^2;
local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% M1 mass matrix

% evaluate jacobian at GLL points
% mesh1.eval_p_der();
mesh1.eval_p_der_v2();

% evaluate material properties at GLL points
mesh1.eval_dom_xy(); 
% eval.K = flow.darcy.material(k11_an, k12_an, k22_an, mesh1.eval.xy);

% evaluate metric terms for M11
% eval.p = flow.darcy.metric(mesh1.eval.p, eval.K);
eval.p = flow.poisson.eval_metric(mesh1.eval.p);

mesh1.eval.metric = eval.p;

M11 = zeros(2*p*(p+1),2*p*(p+1),ttl_nr_el);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% E21 incidence matrix

E211    = incidence_matrix.E21(p);

%% F source term

volForm = twoForm_v2(mesh1);
F_3D = volForm.reduction(f_an);

%% LHS 

LHS = [M11 E211'; E211 zeros(p^2)];

%% RHS

RHS(local_nr_ed + 1: local_nr_ed + local_nr_pp) = F_3D;

%% XX

XX_h = LHS\RHS';

%% separating cochains 

q_h = XX_h(1:2*p*(p+1));
p_h = XX_h(2*p*(p+1) + 1:2*p*(p+1)+ p^2);

%% dual to primal dof

M22s = zeros(p^2,p^2,ttl_nr_el);

for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end

p2_h = M22s\p_h;

%% jacobian derivatives for post processing

pf = p+5;
mesh1.eval_pf_der(pf);

%%

[~, wf] = GLLnodes(pf);
[wfx, wfy] = meshgrid(wf);

wfxy  = wfx .* wfy;
wfxy2 = repmat(wfxy, 1, 1, ttl_nr_el);

%% post processing potential

[xf_3D, yf_3D, pf_h] = volForm.reconstruction(p2_h);
pf_h = permute(pf_h, [2 1 3]); % transpose along dim = 3.

% figure
% hold on
% for eln = 1:ttl_nr_el
%     contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), pf_h(:,:,eln))
% end
% colorbar

%% H10 error 

% evaluate discrete grad p

N = incidence_matrix.OutwardNormalMatrix(p);

for el = 1:K^2
%     u_h(:,el) = E211' * p_h(:,el) + N' * lm_h(:,el);
    u_h(:,el) = E211' * p_h(:,el);
end

for el = 1:K^2
    u_h_2(:,el) = M11(:,:,el)\u_h(:,el);
end

surForm = oneForm_v2(mesh1);

gradp = surForm.reconstruction(u_h_2);

qx_ex = ux_an(xf_3D, yf_3D);
qy_ex = uy_an(xf_3D, yf_3D);
pp_ex = phi_an(xf_3D, yf_3D);

[error.gradp.x] = error_processor_v3(qx_ex, -gradp.qx', wfxy2, mesh1.eval.pf.ggg);
[error.gradp.y] = error_processor_v3(qy_ex, gradp.qy', wfxy2, mesh1.eval.pf.ggg);
[error.pp] = error_processor_v3(pp_ex, pf_h, wfxy2, mesh1.eval.pf.ggg);

err_H1p = err_H1(error.pp.sqre, error.gradp.x.sqre, error.gradp.y.sqre)

%% post processing flux 

ux_ex = ux_an(xf_3D, yf_3D);
uy_ex = uy_an(xf_3D, yf_3D);


%% Hdiv error

rec = surForm.reconstruction(q_h);

rec_qx = rec.qx;
rec_qy = rec.qy;

jac_rec = mesh1.eval.pf;
metric_rec = mesh1.eval.pf;

sx = linspace(-1,1,pf+1);
[basis_rec.hfx, basis_rec.efx] = MimeticpolyVal(sx,p,1);

% rec_qx2 = reconstruction.reconstruct1xform_v8(q_h(1:p*(p+1)),q_h(p*(p+1)+1:2*p*(p+1)),p,pf,jac_rec,metric_rec,1,basis_rec);
% rec_qx2 = reconstruction.reconstruct1xform_v8(q_h(p*(p+1)+1:2*p*(p+1)),q_h(1:p*(p+1)),p,pf,jac_rec,metric_rec,1,basis_rec);

figure
contourf(xf_3D, yf_3D, rec_qx')

figure
contourf(xf_3D, yf_3D, rec_qx2')

figure
contourf(xf_3D, yf_3D, -rec_qy')


for el = 1:K^2
    divQ(:,el) = E211 * q_h(:,el);
end

[xf_3D, yf_3D, divQf_h] = volForm.reconstruction(divQ);
divQf_h = permute(divQf_h, [2 1 3]); % transpose along dim = 3.

ff_ex = f_an(xf_3D, yf_3D);

[error.qx] = error_processor_v3(qx_ex, rec_qx', wfxy2, mesh1.eval.pf.ggg);
[error.qy] = error_processor_v3(qy_ex, -rec_qy', wfxy2, mesh1.eval.pf.ggg);
[error.ff] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

% figure
% contourf(xf_3D, yf_3D, temp2')
% figure
% contourf(xf_3D, yf_3D, ff_ex')
% colorbar
% % 
% figure
% contourf(xf_3D, yf_3D, divQf_h)
% colorbar

% ------------------------------------------------ 

% dof_u = q_h;
% jac2 = mesh1.eval.pf;
% metric2 = mesh1.eval.pf;
% 
% temp2 = reconstruction.reconstruct1xform_v6(dof_u(1:p*(p+1)), -dof_u(p*(p+1)+1:2*p*(p+1)), p, pf, jac2, metric2);
% 
% figure
% contourf(xf_3D, yf_3D, temp2')

% ------------------------------------------------ 

err_divQ = error.qx.sqre + error.qy.sqre + error.ff.sqre;
err_divQ = sqrt(err_divQ)

%% divQ^h - f

[error.divQ] = error_processor_v3(ff_ex, divQf_h, wfxy2, mesh1.eval.pf.ggg);

%% f^h - f

[xf_3D, yf_3D, ff_h] = volForm.reconstruction(F_3D);

[error.fh] = error_processor_v3(ff_ex, ff_h, wfxy2, mesh1.eval.pf.ggg);

%% divQ^h - f^h

[error.divQ2] = error_processor_v3(ff_h, divQf_h, wfxy2, mesh1.eval.pf.ggg);
