clc
close all
clear variables

addpath('../../..')
addpath('../../../MSEM')
addpath('../../../export_fig')
addpath('primal_dual')
addpath('primal_primal')

% load('XX_h_Poisson_dual.mat')
load('error_data.mat')

error_dual = error;

load('error_data_primal.mat')

error_primal = error;

clear error

%%

lw      = 1.5; % this is line width for plots
ms      = 10;  % this is marker size


%% 

dp = 50;

%% div Q^h - f & f^h - f

% [data_divQ_error_x, data_divQ_error_y] = divQ_error(XX_h, p);

for i = 1:dp/2
    dual_data1_divQh_error_y(i) = error_dual.c1(i).divQ.sqrt;
    dual_data2_divQh_error_y(i) = error_dual.c2(i).divQ.sqrt;
    
    dual_data1_fh_error_y(i) = error_dual.c1(i).fh.sqrt;
    dual_data2_fh_error_y(i) = error_dual.c2(i).fh.sqrt;
    
    primal_data1_divQh_error_y(i) = error_primal.c1(i).divQ.sqrt;
    primal_data2_divQh_error_y(i) = error_primal.c2(i).divQ.sqrt;
    
    primal_data1_fh_error_y(i) = error_primal.c1(i).fh.sqrt;
    primal_data2_fh_error_y(i) = error_primal.c2(i).fh.sqrt;
end

figure

semilogy(2:2:dp, dual_data1_divQh_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on 

semilogy(2:2:dp, dual_data1_fh_error_y, '-ro', 'MarkerFaceColor','r','LineWidth',lw)
semilogy(2:2:dp, dual_data2_divQh_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
semilogy(2:2:dp, dual_data2_fh_error_y, '-r<', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('$$\Vert \nabla \cdot \mathbf{q}^h - f \Vert _{L^2(K)}, c = 0.0$$','$$\Vert f^h - f \Vert _{L^2(K)}, c= 0.0 $$','$$\Vert \nabla \cdot \mathbf{q}^h - f \Vert _{L^2(K)}, c=0.3$$','$$\Vert f^h - f \Vert _{L^2(K)}, c=0.3$$','location','northeast');
set(hlegend,'Interpreter','latex','FontSize',14);


xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$$\Vert \cdot \Vert _{L^2(K)}$$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

ylim([1e-13 1e+3])

%% div Q^h - f^h

for i = 1:dp/2
    dual_data1_divQ2h_error_y(i) = error_dual.c1(i).divQ2.sqrt;
    dual_data2_divQ2h_error_y(i) = error_dual.c2(i).divQ2.sqrt;
    
    primal_data1_divQ2h_error_y(i) = error_primal.c1(i).divQ2.sqrt;
    primal_data2_divQ2h_error_y(i) = error_primal.c2(i).divQ2.sqrt;
end

figure

semilogy(2:2:dp, dual_data1_divQ2h_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on 
semilogy(2:2:dp, primal_data1_divQ2h_error_y, '-ro', 'MarkerFaceColor','r','LineWidth',lw,'MarkerSize',ms)

semilogy(2:2:dp, dual_data2_divQ2h_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
semilogy(2:2:dp, primal_data2_divQ2h_error_y, '-r<', 'MarkerFaceColor','r','LineWidth',lw,'MarkerSize',ms)

% hlegend = legend('Primal dual, $c=0.0$','Primal dual, $c=0.3$','location','southeast');
hlegend = legend('Primal dual, $c=0.0$','Primal primal, $c=0.0$','Primal dual, $c=0.3$','Primal primal, $c=0.3$','location','southeast');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$$\Vert \nabla \cdot \mathbf{q}^h - f^h \Vert _{L^2(K)}$$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

%% error phi H1

for i = 1:dp/2
%     dual_data1_phiH1_error_y(i) = error_dual.c1(i).H1p;
%     dual_data2_phiH1_error_y(i) = error_dual.c2(i).H1p;
%     
%     primal_data1_phiH1_error_y(i) = error_primal.c1(i).H1p;
%     primal_data2_phiH1_error_y(i) = error_primal.c2(i).H1p;
    
    dual_data1_phiH1_error_y(i) = error_dual.c1(i).pp.sqrt;
    dual_data2_phiH1_error_y(i) = error_dual.c2(i).pp.sqrt;
    
    primal_data1_phiH1_error_y(i) = error_primal.c1(i).pp.sqrt;
    primal_data2_phiH1_error_y(i) = error_primal.c2(i).pp.sqrt;
end

figure

semilogy(2:2:dp, dual_data1_phiH1_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on
semilogy(2:2:dp, primal_data1_phiH1_error_y, '-ro', 'MarkerFaceColor','r','LineWidth',lw)

semilogy(2:2:dp, dual_data2_phiH1_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
semilogy(2:2:dp, primal_data2_phiH1_error_y, '-r<', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('Primal dual, $c=0.0$','Primal primal, $c=0.0$','Primal dual, $c=0.3$','Primal primal, $c=0.3$','location','northeast');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$\Vert \phi ^h - \phi _{ex} \Vert _{L^2(K)}$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

ylim([1e-14 1e+2])

%% error q Hdiv

for i = 1:dp/2
%     dual_data1_qHdiv_error_y(i) = error_dual.c1(i).QHdiv;
%     dual_data2_qHdiv_error_y(i) = error_dual.c2(i).QHdiv;
%     
%     primal_data1_qHdiv_error_y(i) = error_primal.c1(i).QHdiv;
%     primal_data2_qHdiv_error_y(i) = error_primal.c2(i).QHdiv;
    
    dual_data1_qHdiv_error_y(i) = error_dual.c1(i).QHdiv;
    dual_data2_qHdiv_error_y(i) = error_dual.c2(i).QHdiv;
    
    primal_data1_qHdiv_error_y(i) = error_primal.c1(i).QHdiv;
    primal_data2_qHdiv_error_y(i) = error_primal.c2(i).QHdiv;
end

figure

semilogy(2:2:dp, dual_data1_qHdiv_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on
semilogy(2:2:dp, primal_data1_qHdiv_error_y, '-ro', 'MarkerFaceColor','r','LineWidth',lw)

semilogy(2:2:dp, dual_data2_qHdiv_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
semilogy(2:2:dp, primal_data2_qHdiv_error_y, '-r<', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('Primal dual, $c=0.0$','Primal primal, $c=0.0$','Primal dual, $c=0.3$','Primal primal, $c=0.3$','location','northeast');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$\Vert \mathbf{q} ^h - \mathbf{q} _{ex} \Vert _{H(\mathrm{div};K)}$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

ylim([1e-12 1e+3])