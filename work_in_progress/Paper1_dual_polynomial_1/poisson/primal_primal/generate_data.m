clc
close all
clear variables

addpath('../../../..')
addpath('../../../../MSEM')
% addpath('../../export_fig')

%% case 1, c= 0.0

c1 = 0.0;

for p = 2:2:50
    i = p/2;
    p
    
    fname_K1 = sprintf('N_%d', p);
    [XX_h.c1.(genvarname([fname_K1])), error.c1(i)] = poisson_primal_v1_func(p, c1);
    
    fname = sprintf('XX_h_Poisson_primal.mat');
    save(fname,'XX_h');
    save('error_data_primal.mat','error');
end

%% case 2, c = 0.3

c2 = 0.3;

for p = 2:2:50
    i = p/2;
    p
    
    fname_K1 = sprintf('N_%d', p);
    [XX_h.c2.(genvarname([fname_K1])), error.c2(i)] = poisson_primal_v1_func(p, c2);
    
    fname = sprintf('XX_h_Poisson_primal.mat');
    save(fname,'XX_h');
    save('error_data_primal.mat','error');
end