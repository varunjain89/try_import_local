clc
close all
clear variables

addpath('../../..')
addpath('../../../../MSEM')
addpath('../../../../export_fig')

load('XX_h_Poisson_primal.mat')
load('error_data_primal.mat')

%%

lw      = 1.5; % this is line width for plots
ms      = 10;  % this is marker size

dp = 30;        % data p

%% div Q^h - f & f^h - f

% [data_divQ_error_x, data_divQ_error_y] = divQ_error(XX_h, p);

for i = 1: dp/2
    data1_divQh_error_y(i) = error.c1(i).divQ.sqrt;
    data2_divQh_error_y(i) = error.c2(i).divQ.sqrt;
    
    data1_fh_error_y(i) = error.c1(i).fh.sqrt;
    data2_fh_error_y(i) = error.c2(i).fh.sqrt;
end

figure

semilogy(2:2:dp, data1_divQh_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on 

semilogy(2:2:dp, data1_fh_error_y, '-ro', 'MarkerFaceColor','r','LineWidth',lw)
semilogy(2:2:dp, data2_divQh_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
semilogy(2:2:dp, data2_fh_error_y, '-r<', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('$$\Vert \nabla \cdot \mathbf{q}^h - f \Vert _{L^2(\Omega)}, c = 0.0$$','$$\Vert f^h - f \Vert _{L^2(\Omega)}, c= 0.0 $$','$$\Vert \nabla \cdot \mathbf{q}^h - f \Vert _{L^2(\Omega)}, c=0.3$$','$$\Vert f^h - f \Vert _{L^2(\Omega)}, c=0.3$$','location','southwest');
set(hlegend,'Interpreter','latex','FontSize',14);


xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$$\Vert \cdot \Vert _{L^2(\Omega)}$$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

%% div Q^h - f^h

for i = 1:dp/2
    data1_divQ2h_error_y(i) = error.c1(i).divQ2.sqrt;
    data2_divQ2h_error_y(i) = error.c2(i).divQ2.sqrt;
end

figure

semilogy(2:2:dp, data1_divQ2h_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on 

semilogy(2:2:dp, data2_divQ2h_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)

hlegend = legend('Primal dual, $c=0.0$','Primal dual, $c=0.3$','location','southeast');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$$\Vert \nabla \cdot \mathbf{q}^h - f^h \Vert _{L^2(\Omega)}$$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

%% error phi H1

for i = 1:dp/2
    data1_phiH1_error_y(i) = error.c1(i).H1p;
    data2_phiH1_error_y(i) = error.c2(i).H1p;
end

figure

semilogy(2:2:dp, data1_phiH1_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on 

semilogy(2:2:dp, data2_phiH1_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)

hlegend = legend('Primal dual, $c=0.0$','Primal dual, $c=0.3$','location','southwest');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$\Vert \phi ^h - \phi _{ex} \Vert _{H1(\Omega)}$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

%% error q Hdiv

for i = 1:dp/2
    data1_qHdiv_error_y(i) = error.c1(i).QHdiv;
    data2_qHdiv_error_y(i) = error.c2(i).QHdiv;
end

figure

semilogy(2:2:dp, data1_qHdiv_error_y, '-ko', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)
hold on 

semilogy(2:2:dp, data2_qHdiv_error_y, '-k<', 'MarkerFaceColor','k','LineWidth',lw,'MarkerSize',ms)

hlegend = legend('Primal dual, $c=0.0$','Primal dual, $c=0.3$','location','southwest');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$\Vert \mathbf{q} ^h - \mathbf{q} _{ex} \Vert _{H(\mathrm{div};\Omega)}$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');
