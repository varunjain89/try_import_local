classdef CrazyMesh < handle
    %CRAZYMESH Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        K
        p
        pf
        el
        domain
        
        xp
        wp
        basis
        xf
        wf
        
        eval
        
        ttl_nr_el
    end
    
    methods
        function obj = CrazyMesh(K,p,xbound,ybound,c)
            % constructor method
            
            obj.p = p;
            obj.K = K;
            
            obj.ttl_nr_el = K^2;
            
%             obj.pf = obj.p + 5;
%             obj.pf = 30;
            
            [obj.xp, obj.wp] = GLLnodes(obj.p);
            [obj.basis.hp, obj.basis.ep] = MimeticpolyVal(obj.xp,obj.p,1);
            
%             [obj.xf, obj.wf] = GLLnodes(obj.pf);
%             [obj.basis.hf, obj.basis.ef] = MimeticpolyVal(obj.xf,obj.p,1);
            
            obj.el.bounds.x = linspace(-1,1,K+1);
            obj.el.bounds.y = linspace(-1,1,K+1);

            obj.domain.mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
            obj.domain.dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
            obj.domain.dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
            obj.domain.dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
            obj.domain.dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

            obj.el.mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(obj.domain.mapping, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(obj.domain.dX_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(obj.domain.dX_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(obj.domain.dY_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(obj.domain.dY_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            
            obj.el.mapping_xii_eta = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element_xii_eta(obj.domain.mapping, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dX_dxii_v2 = @(xi,eta,elx,ely,xi2,eta2) mesh.crazy_mesh.dx_dxii_element_v2(obj.domain.dX_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta, xi2, eta2);
            obj.el.dX_deta_v2 = @(xi,eta,elx,ely,xi2,eta2) mesh.crazy_mesh.dx_deta_element_v2(obj.domain.dX_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta, xi2, eta2);
            obj.el.dY_dxii_v2 = @(xi,eta,elx,ely,xi2,eta2) mesh.crazy_mesh.dy_dxii_element_v2(obj.domain.dY_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta, xi2, eta2);
            obj.el.dY_deta_v2 = @(xi,eta,elx,ely,xi2,eta2) mesh.crazy_mesh.dy_deta_element_v2(obj.domain.dY_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta, xi2, eta2);
            
        end
        
        function obj = eval_p_der(obj)
            [xp, ~] = GLLnodes(obj.p);
            [xip, etap] = meshgrid(xp);
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
%                     eln
                    obj.eval.p.dx_dxii(:,:,eln) = obj.el.dX_dxii(xip,etap,elx,ely);
                    obj.eval.p.dx_deta(:,:,eln) = obj.el.dX_deta(xip,etap,elx,ely);
                    obj.eval.p.dy_dxii(:,:,eln) = obj.el.dY_dxii(xip,etap,elx,ely);
                    obj.eval.p.dy_deta(:,:,eln) = obj.el.dY_deta(xip,etap,elx,ely);
                end
            end
            obj.eval.p.ggg = metric.ggg_v2(obj.eval.p);
        end
        
        %% check element mapping
        function obj = check_element_mapping(obj)
            cc = 0.6;
%             s = linspace(-1,1,31);
            [s,~] = GLLnodes(6);
            [xii, eta] = meshgrid(s);
            
            figure
            hold on
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                
                [x, y] = obj.el.mapping(xii, eta, elx, ely);
                plot(x, y,'Color', [cc cc cc], 'linewidth', 2)
                plot(x',y','Color', [cc cc cc], 'linewidth', 2)
                
                % bottom boundary
                [xB, yB] = obj.el.mapping(s, -1.*ones(size(s)), elx, ely);
                plot(xB, yB,'k','linewidth',2)
                
                % left boundary
                [xL, yL] = obj.el.mapping(1.*ones(size(s)), s, elx, ely);
                plot(xL, yL,'k','linewidth',2)
                
                % top boundary
                [xT, yT] = obj.el.mapping(s, 1.*ones(size(s)), elx, ely);
                plot(xT, yT,'k','linewidth',2)
                
                % right boundary
                [xR, yR] = obj.el.mapping(-1.*ones(size(s)), s, elx, ely);
                plot(xR, yR,'k','linewidth',2)
                end
            end
            set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
        end
        
        function obj = eval_p_der_v2(obj)
            [xp, ~] = GLLnodes(obj.p);
            [xip, etap] = meshgrid(xp);
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln)] = obj.el.mapping_xii_eta(xip,etap,elx,ely);
                    obj.eval.p.dx_dxii(:,:,eln) = obj.el.dX_dxii_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dx_deta(:,:,eln) = obj.el.dX_deta_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dy_dxii(:,:,eln) = obj.el.dY_dxii_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dy_deta(:,:,eln) = obj.el.dY_deta_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                end
            end
            obj.eval.p.ggg = metric.ggg_v2(obj.eval.p);
        end
        
        function obj = eval_jac_der(obj, xiip, etap)
            % this function calculates jacobian derviatives at all xiip and
            % etap of all the elements
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln)] = obj.el.mapping_xii_eta(xiip,etap,elx,ely);
                    obj.eval.p.dx_dxii(:,:,eln) = obj.el.dX_dxii_v2(xiip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dx_deta(:,:,eln) = obj.el.dX_deta_v2(xiip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dy_dxii(:,:,eln) = obj.el.dY_dxii_v2(xiip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dy_deta(:,:,eln) = obj.el.dY_deta_v2(xiip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                end
            end
            obj.eval.p.ggg = metric.ggg_v2(obj.eval.p);
        end
        
        function eval = eval_jac_der2(obj, xiip, etap)
            % this function calculates jacobian derviatives at all xiip and
            % etap of all the elements
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [eval.xii(:,:,eln),eval.eta(:,:,eln)] = obj.el.mapping_xii_eta(xiip,etap,elx,ely);
                    eval.dx_dxii(:,:,eln) = obj.el.dX_dxii_v2(xiip,etap,elx,ely,eval.xii(:,:,eln),eval.eta(:,:,eln));
                    eval.dx_deta(:,:,eln) = obj.el.dX_deta_v2(xiip,etap,elx,ely,eval.xii(:,:,eln),eval.eta(:,:,eln));
                    eval.dy_dxii(:,:,eln) = obj.el.dY_dxii_v2(xiip,etap,elx,ely,eval.xii(:,:,eln),eval.eta(:,:,eln));
                    eval.dy_deta(:,:,eln) = obj.el.dY_deta_v2(xiip,etap,elx,ely,eval.xii(:,:,eln),eval.eta(:,:,eln));
                end
            end
            eval.ggg = metric.ggg_v2(eval);
        end
        
        function obj = eval_p_der_gauss(obj)
            [xpg] = Gnodes(obj.p + 1);
            [xip, etap] = meshgrid(xpg);
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln)] = obj.el.mapping_xii_eta(xip,etap,elx,ely);
                    obj.eval.p.dx_dxii(:,:,eln) = obj.el.dX_dxii_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dx_deta(:,:,eln) = obj.el.dX_deta_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dy_dxii(:,:,eln) = obj.el.dY_dxii_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                    obj.eval.p.dy_deta(:,:,eln) = obj.el.dY_deta_v2(xip,etap,elx,ely,obj.eval.p.xii(:,:,eln),obj.eval.p.eta(:,:,eln));
                end
            end
            obj.eval.p.ggg = metric.ggg_v2(obj.eval.p);
        end
        
        
        function obj = eval_pf_der(obj, pf)
            obj.pf = pf;
            [obj.xf, obj.wf] = GLLnodes(pf);
            [obj.basis.hf, obj.basis.ef] = MimeticpolyVal(obj.xf,obj.p,1);
%             [xp, ~] = GLLnodes(obj.p + 5);
%             [xp, ~] = GLLnodes(pf);
            [xip, etap] = meshgrid(obj.xf);
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K + ely;
%                     eln
                    obj.eval.pf.dx_dxii(:,:,eln) = obj.el.dX_dxii(xip,etap,elx,ely);
                    obj.eval.pf.dx_deta(:,:,eln) = obj.el.dX_deta(xip,etap,elx,ely);
                    obj.eval.pf.dy_dxii(:,:,eln) = obj.el.dY_dxii(xip,etap,elx,ely);
                    obj.eval.pf.dy_deta(:,:,eln) = obj.el.dY_deta(xip,etap,elx,ely);
                end
            end
            obj.eval.pf.ggg = metric.ggg_v2(obj.eval.pf);
        end
        
        function obj = eval_dom_xy(obj)
            [xp, ~] = GLLnodes(obj.p);
            [xip, etap] = meshgrid(xp);
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [obj.eval.xy.x(:,:,eln),obj.eval.xy.y(:,:,eln)] = obj.el.mapping(xip,etap,elx,ely);
                end
            end
        end
        
        function obj = eval_dom_xy_gauss(obj)
            [xp] = Gnodes(obj.p + 1);
            [xip, etap] = meshgrid(xp);
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [obj.eval.xy.x(:,:,eln),obj.eval.xy.y(:,:,eln)] = obj.el.mapping(xip,etap,elx,ely);
                end
            end
        end
        
        function obj = eval_dom_xyf(obj)
            % this has to be finished before using
            [xp, ~] = GLLnodes(obj.p);
            [xip, etap] = meshgrid(xp);
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
                    [obj.eval.xy.x(:,:,eln),obj.eval.xy.y(:,:,eln)] = obj.el.mapping(xip,etap,elx,ely);
                end
            end
        end
        
    end
    
end

