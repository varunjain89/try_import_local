classdef PorousAirfoilMesh < handle
    % Created on: 4th April, 2019
    
    properties
        domain
        eval
        p
        xp
        wp
        basis
        K
        el
        Kx
        Ky
        pf
        xf
        wf
    end
    
    methods
        function obj = PorousAirfoilMesh(mapping,dX_dxii,dX_deta,dY_dxii,dY_deta)
            obj.domain.mapping = @(xi,eta) mapping(xi,eta);
            obj.domain.dX_dxii = @(xi,eta) dX_dxii(xi,eta);
            obj.domain.dX_deta = @(xi,eta) dX_deta(xi,eta);
            obj.domain.dY_dxii = @(xi,eta) dY_dxii(xi,eta);
            obj.domain.dY_deta = @(xi,eta) dY_deta(xi,eta);
        end
        
        function outputArg = method1(obj,inputArg)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            outputArg = obj.Property1 + inputArg;
        end
        
        function obj = def_basis(obj,p)
            obj.p = p;
            
            [obj.xp, obj.wp] = GLLnodes(obj.p);
            [obj.basis.hp, obj.basis.ep] = MimeticpolyVal(obj.xp,obj.p,1);
        end
        
        function obj = discretize(obj,Kx,Ky, p)
            
            obj.K = Kx;
            obj.Kx = Kx;
            obj.Ky = Ky;
            
            obj.el.bounds.x = linspace(-1,1,Kx+1);
            obj.el.bounds.y = linspace(-1,1,Ky+1);
            
            obj.el.mapping = @(xi,eta,elx,ely) mesh.element.mapping(obj.domain.mapping, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dX_dxii = @(xi,eta,elx,ely) mesh.element.dx_dxii(obj.domain.dX_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dX_deta = @(xi,eta,elx,ely) mesh.element.dx_deta(obj.domain.dX_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dY_dxii = @(xi,eta,elx,ely) mesh.element.dy_dxii(obj.domain.dY_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            obj.el.dY_deta = @(xi,eta,elx,ely) mesh.element.dy_deta(obj.domain.dY_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            
            obj.p = p;
        end
        
        function obj = eval_jacobian(obj,p)
            obj.p = p;
            
            [obj.xp, obj.wp] = GLLnodes(obj.p);
            [obj.basis.hp, obj.basis.ep] = MimeticpolyVal(obj.xp,obj.p,1);
            
            [xp, ~] = GLLnodes(p);
            [xiip, etap] = meshgrid(xp);
            
            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx -1)*obj.K +ely;
%                     eln
                    obj.eval.jac.p.dx_dxii(:,:,eln) = obj.el.dX_dxii(xiip,etap,elx,ely);
                    obj.eval.jac.p.dx_deta(:,:,eln) = obj.el.dX_deta(xiip,etap,elx,ely);
                    obj.eval.jac.p.dy_dxii(:,:,eln) = obj.el.dY_dxii(xiip,etap,elx,ely);
                    obj.eval.jac.p.dy_deta(:,:,eln) = obj.el.dY_deta(xiip,etap,elx,ely);
                end
            end
            
%             obj.eval.jac.p.dx_dxii = obj.domain.dX_dxii(xiip,etap);
%             obj.eval.jac.p.dx_deta = obj.domain.dX_deta(xiip,etap);
%             obj.eval.jac.p.dy_dxii = obj.domain.dY_dxii(xiip,etap);
%             obj.eval.jac.p.dy_deta = obj.domain.dY_deta(xiip,etap);
        end
        
        function obj = eval_pf_jacobian(obj,pf)
            obj.pf = pf;
            
            [obj.xf, obj.wf] = GLLnodes(obj.pf);
            [obj.basis.hf, obj.basis.ef] = MimeticpolyVal(obj.xf,obj.p,1);
            
            [xiip, etap] = meshgrid(obj.xf);
            
            for elx = 1:obj.Kx
                for ely = 1:obj.Ky
                    eln = (elx -1)*obj.Ky + ely;
                    for xx = 1:pf+1
                        for yy = 1:pf+1
                            obj.eval.jac.pf.dx_dxii(xx,yy,eln) = obj.el.dX_dxii(xiip(xx,yy),etap(xx,yy),elx,ely);
                            obj.eval.jac.pf.dx_deta(xx,yy,eln) = obj.el.dX_deta(xiip(xx,yy),etap(xx,yy),elx,ely);
                            obj.eval.jac.pf.dy_dxii(xx,yy,eln) = obj.el.dY_dxii(xiip(xx,yy),etap(xx,yy),elx,ely);
                            obj.eval.jac.pf.dy_deta(xx,yy,eln) = obj.el.dY_deta(xiip(xx,yy),etap(xx,yy),elx,ely);
                        end
                    end
                end
            end

        end
        
        function obj = eval_metric(obj)
            obj.eval.metric.ggg = metric.ggg(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta);
            obj.eval.metric.g11 = metric.g11(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta,obj.eval.metric.ggg);
            obj.eval.metric.g12 = metric.g12(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta,obj.eval.metric.ggg);
            obj.eval.metric.g22 = metric.g22(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta,obj.eval.metric.ggg);
        end
        
        function obj = eval_pf_metric(obj)
            obj.eval.pf.ggg = metric.ggg(obj.eval.jac.pf.dx_dxii,obj.eval.jac.pf.dx_deta,obj.eval.jac.pf.dy_dxii,obj.eval.jac.pf.dy_deta);
        end
        
    end
end

