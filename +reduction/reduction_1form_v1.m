function [dof_u, Mf] = reduction_1form_v1(ux, uy, el_mapping, basis, M11, jac1, metric1)

hp = basis.hp;
ep = basis.ep;

p = size(ep, 1);

[xp, wp] = GLLnodes(p);

dx_dxii = jac1.dx_dxii(:,:,1)';
dy_deta = jac1.dy_deta(:,:,1)';
dy_dxii = jac1.dy_dxii(:,:,1)';
dx_deta = jac1.dx_deta(:,:,1)';

ggg = metric1.ggg';

Mf = zeros(1,2*p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        for ix = 1:p+1
            for iy = 1:p+1
                [ix2, iy2] = el_mapping(xp(ix),xp(iy));
                Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * dy_deta(ix,iy);
                
                Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * -dx_deta(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * -dy_dxii(ix,iy);
%                 
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * dx_deta(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * dy_dxii(ix,iy);
%                 
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * -dx_deta(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * -dy_dxii(ix,iy);
%                 
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * dx_deta(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + ep(i,ix)*hp(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * dy_dxii(ix,iy);
            end
        end
    end
end

for i = 1:p+1
    for j = 1:p
        edgeij = p*(p+1) + (i-1)*p + j;
        for ix = 1:p+1
            for iy = 1:p+1
                [ix2, iy2] = el_mapping(xp(ix),xp(iy));
                Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * dx_dxii(ix,iy);
                
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * -dy_dxii(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * -dx_deta(ix,iy);
%                 
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * dy_dxii(ix,iy);
                Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*ux(ix2,iy2)*wp(ix)*wp(iy) * dx_deta(ix,iy);
%                 
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * -dy_dxii(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * -dx_deta(ix,iy);
%                 
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * dy_dxii(ix,iy);
%                 Mf(edgeij) = Mf(edgeij) + hp(i,ix)*ep(j,iy)*uy(ix2,iy2)*wp(ix)*wp(iy) * dx_deta(ix,iy);
                
            end
        end
    end
end

dof_u = M11 \ Mf';

end

