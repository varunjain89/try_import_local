function [ f_h ] = of2form_multi_element_v9(f, p, domain, K, element_bounds_x, element_bounds_y)
%OF2FORM_MULTI_ELEMENT Summary of this function goes here
%   Detailed explanation goes here

% modified on 14 Oct, 2018

% nr_local_const = p^2;
% nr_total_el    = K^2;

[xp, ~] = GLLnodes(p);

% polynomial degree of fine integration
pint = p+5;

% integration points 1D
[xpint, wpint] = GLLnodes(pint);

% integration points 2D
[sf, tf] = meshgrid(xpint);

kron_w = kron(wpint,wpint');

% figure
% hold on
for elx = 1:K
    for ely = 1:K
        el = (elx - 1)*K + ely;
        
        % ortho bounds for local element
        x1 = element_bounds_x(elx);
        x2 = element_bounds_x(elx+1);
        y1 = element_bounds_y(ely);
        y2 = element_bounds_y(ely+1);
        
        
        % mapping lbatto points on local element
        xi  = 0.5*(x1 + x2) + 0.5*(x2 - x1).* xp;
        eta = 0.5*(y1 + y2) + 0.5*(y2 - y1).* xp;
        
%         dx_delx = 0.5*(x2 - x1);
%         dy_dely = 0.5*(y2 - y1);
        
        for i = 1:p
            for j = 1:p
                volij_l = (i-1)*p + j;
%                               
                % coordinates of the volume for which integral has to be
                % calculated
                xi_i1  = xi(i);
                xi_i2  = xi(i+1);
                eta_j1 = eta(j);
                eta_j2 = eta(j+1);
                
                % fine mapping on ref domain to calculate integral
                xif(:,:,volij_l,el)  = 0.5*(xi_i1 + xi_i2)   + 0.5*(xi_i2 - xi_i1).* sf;
                etaf(:,:,volij_l,el) = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* tf;
                
                dxi_ds(volij_l)  = 0.5*(xi_i2 - xi_i1);
                deta_dt(volij_l) = 0.5*(eta_j2 - eta_j1);
            end
        end
    end
end

xif2 = xif(:);
etaf2 = etaf(:);

[x_int, y_int] = domain.mapping(xif2 ,etaf2);

x_int = reshape(x_int,[size(etaf,1) size(etaf,2) size(etaf,3) size(etaf,4)]);
y_int = reshape(y_int,[size(etaf,1) size(etaf,2) size(etaf,3) size(etaf,4)]);

x_int(:,:,1,1)
y_int(:,:,1,1)

% for t1 = 1:size(etaf,1)
%     for t2 = 1:size(etaf,2)
%         for t3 = 1:size(etaf,3)
%             for t4 = 1:size(etaf,4)
%                 [x_int(t1,t2,t3,t4), y_int(t1,t2,t3,t4)] = domain.mapping(xif(t1,t2,t3,t4) ,etaf(t1,t2,t3,t4));
%             end
%         end
%     end
% end

tic
f_int = f(x_int, y_int);
toc

%% test 

% xif = xif(:);
% etaf = etaf(:);
% tic
% f_int = f(x_int, y_int);
% toc

%%

eval_dx_ds = domain.dX_dxii(xif2, etaf2);
eval_dy_ds = domain.dY_dxii(xif2, etaf2);
eval_dx_dt = domain.dX_deta(xif2, etaf2);
eval_dy_dt = domain.dY_deta(xif2, etaf2);

eval_dx_ds = reshape(eval_dx_ds,[size(etaf,1) size(etaf,2) size(etaf,3) size(etaf,4)]);
eval_dy_ds = reshape(eval_dy_ds,[size(etaf,1) size(etaf,2) size(etaf,3) size(etaf,4)]);
eval_dx_dt = reshape(eval_dx_dt,[size(etaf,1) size(etaf,2) size(etaf,3) size(etaf,4)]);
eval_dy_dt = reshape(eval_dy_dt,[size(etaf,1) size(etaf,2) size(etaf,3) size(etaf,4)]);

% for t1 = 1:size(etaf,1)
%     for t2 = 1:size(etaf,2)
%         for t3 = 1:size(etaf,3)
%             for t4 = 1:size(etaf,4)
%                 eval_dx_ds(t1,t2,t3,t4) = domain.dX_dxii(xif(t1,t2,t3,t4) ,etaf(t1,t2,t3,t4));
%                 eval_dy_ds(t1,t2,t3,t4) = domain.dY_dxii(xif(t1,t2,t3,t4) ,etaf(t1,t2,t3,t4));
%                 eval_dx_dt(t1,t2,t3,t4) = domain.dX_deta(xif(t1,t2,t3,t4) ,etaf(t1,t2,t3,t4));
%                 eval_dy_dt(t1,t2,t3,t4) = domain.dY_deta(xif(t1,t2,t3,t4) ,etaf(t1,t2,t3,t4));
%             end
%         end
%     end
% end

% eval_dx_ds = domain.dX_dxii(xif, etaf);
% eval_dy_ds = domain.dY_dxii(xif, etaf);
% eval_dx_dt = domain.dX_deta(xif, etaf);
% eval_dy_dt = domain.dY_deta(xif, etaf);

eval_dxi_ds = zeros(pint+1, pint+1, p^2,K^2);
eval_deta_dt = zeros(pint+1, pint+1, p^2,K^2);

for vol = 1:p^2
    eval_dxi_ds(:,:,vol,:) = dxi_ds(vol);
    eval_deta_dt(:,:,vol,:) = deta_dt(vol);
end

dx_ds = eval_dx_ds .* eval_dxi_ds;
dy_ds = eval_dy_ds .* eval_dxi_ds;
dx_dt = eval_dx_dt .* eval_deta_dt;
dy_dt = eval_dy_dt .* eval_deta_dt;

    
gf = dx_ds.*dy_dt - dx_dt .* dy_ds;

kron_w_4D = repmat(kron_w, 1, 1, p^2, K^2);

f_h = f_int.* gf .* kron_w_4D;
f_h = sum(sum(f_h));
f_h = permute(f_h,[3 4 1 2]);

end

