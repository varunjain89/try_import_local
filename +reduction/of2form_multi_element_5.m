function [ f_h ] = of2form_multi_element_v6(f, p, mapping, dX_dxi, dX_deta, dY_dxi, dY_deta, K, element_bounds_x, element_bounds_y)
%OF2FORM_MULTI_ELEMENT Summary of this function goes here
%   Detailed explanation goes here

% modified on 14 Oct, 2018

nr_local_const = p^2;
nr_total_el    = K^2;

[xp, ~] = GLLnodes(p);

% polynomial degree of fine integration
pint = p+3;
% integration points 1D
[xpint, wpint] = GLLnodes(pint);
% integration points 2D
[sf, tf] = meshgrid(xpint);

kron_w = kron(wpint,wpint');

% figure
% hold on
for elx = 1:K
    for ely = 1:K
        el = (elx - 1)*K + ely;
        
        % ortho bounds for local element
        x1 = element_bounds_x(elx);
        x2 = element_bounds_x(elx+1);
        y1 = element_bounds_y(ely);
        y2 = element_bounds_y(ely+1);
        
        
        % mapping lbatto points on local element
        xi  = 0.5*(x1 + x2) + 0.5*(x2 - x1).* xp;
        eta = 0.5*(y1 + y2) + 0.5*(y2 - y1).* xp;
        
%         dx_delx = 0.5*(x2 - x1);
%         dy_dely = 0.5*(y2 - y1);
        
        for i = 1:p
            for j = 1:p
                volij_l = (i-1)*p + j;
%                               
                % coordinates of the volume for which integral has to be
                % calculated
                xi_i1  = xi(i);
                xi_i2  = xi(i+1);
                eta_j1 = eta(j);
                eta_j2 = eta(j+1);
                
                % fine mapping on ref domain to calculate integral
                xif(:,:,volij_l,el)  = 0.5*(xi_i1 + xi_i2)   + 0.5*(xi_i2 - xi_i1).* sf;
                etaf(:,:,volij_l,el) = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* tf;
                
                dxi_ds(volij_l)  = 0.5*(xi_i2 - xi_i1);
                deta_dt(volij_l) = 0.5*(eta_j2 - eta_j1);
            end
        end
    end
end

[x_int, y_int] = mapping(xif ,etaf);
f_int = f(x_int, y_int);

eval_dx_ds = dX_dxi(xif, etaf);
eval_dy_ds = dY_dxi(xif, etaf);
eval_dx_dt = dX_deta(xif, etaf);
eval_dy_dt = dY_deta(xif, etaf);

eval_dxi_ds = ones(pint+1, pint+1, p^2,K^2);
eval_deta_dt = ones(pint+1, pint+1, p^2,K^2);

for vol = 1:p^2
    eval_dxi_ds(:,:,vol,:) = dxi_ds(vol);
    eval_deta_dt(:,:,vol,:) = deta_dt(vol);
end

dx_ds = eval_dx_ds .* eval_dxi_ds;
dy_ds = eval_dy_ds .* eval_dxi_ds;
dx_dt = eval_dx_dt .* eval_deta_dt;
dy_dt = eval_dy_dt .* eval_deta_dt;

    
gf = dx_ds.*dy_dt - dx_dt .* dy_ds;

kron_w_4D = repmat(kron_w, 1, 1, p^2, K^2);

f_h = f_int.* gf .* kron_w_4D;
f_h = sum(sum(f_h));
f_h = permute(f_h,[3 4 1 2]);

end

