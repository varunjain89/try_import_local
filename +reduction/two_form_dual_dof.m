function [FF] = two_form_dual_dof(ff_an, patch1)
%TWO_FORM_DUAL_DOF Summary of this function goes here
%   Detailed explanation goes here

% created on 15 March, 2019

p = patch1.p;

FF = zeros(p^2,1);

ep = patch1.basis.ep;
wp = patch1.wp;
xp = patch1.xp;

for i = 1:p
    for j = 1:p
        volij = (i-1)*p +j;
        for px = 1:p+1
            for py = 1:p+1
                FF(volij) = FF(volij) + ep(i,px)*ep(j,py)*ff_an(xp(px),xp(py))*wp(px)*wp(py);
            end
        end
    end
end


end

