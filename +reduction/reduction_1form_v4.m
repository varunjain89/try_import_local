function [dof_u] = reduction_1form_v4(ux, uy, el_mapping, basis, M11, jac1,eln)

hp = basis.hp;
ep = basis.ep;

p = size(ep, 1);

% [xp, wp] = GLLnodes(p);

xp = basis.xp;
wp = basis.wp;

[ixii , ieta] = meshgrid(xp);
[ix3, iy3] = el_mapping(ixii, ieta);

ix3 = ix3';
iy3 = iy3';

uxi = ux(ix3,iy3);
uyi = uy(ix3,iy3);

dx_dxii = jac1.dx_dxii(:,:,eln)';
dy_deta = jac1.dy_deta(:,:,eln)';
dy_dxii = jac1.dy_dxii(:,:,eln)';
dx_deta = jac1.dx_deta(:,:,eln)';
% ggg = metric1.ggg;

Mf = zeros(1,2*p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        for ix = 1:p+1
            for iy = 1:p+1
%                 [ix2, iy2] = el_mapping(xp(ix),xp(iy));
%                 Mf(edgeij) = Mf(edgeij) + (ux(ix2,iy2)* dy_deta(ix,iy)-uy(ix2,iy2)*dx_deta(ix,iy))*ep(i,ix)*hp(j,iy)*wp(ix)*wp(iy);
                Mf(edgeij) = Mf(edgeij) + (uxi(ix,iy)* dy_deta(ix,iy)-uyi(ix,iy)*dx_deta(ix,iy))*ep(i,ix)*hp(j,iy)*wp(ix)*wp(iy);
            end
        end
    end
end

for i = 1:p+1
    for j = 1:p
        edgeij = p*(p+1) + (i-1)*p + j;
        for ix = 1:p+1
            for iy = 1:p+1
%                 [ix2, iy2] = el_mapping(xp(ix),xp(iy));
%                 Mf(edgeij) = Mf(edgeij) + (uy(ix2,iy2)*dx_dxii(ix,iy)-ux(ix2,iy2)*dy_dxii(ix,iy))*hp(i,ix)*ep(j,iy)*wp(ix)*wp(iy);
                Mf(edgeij) = Mf(edgeij) + (uyi(ix,iy)*dx_dxii(ix,iy)-uxi(ix,iy)*dy_dxii(ix,iy))*hp(i,ix)*ep(j,iy)*wp(ix)*wp(iy);
            end
        end
    end
end

dof_u = M11 \ Mf';

end

