function [dof_f] = reduction_2form_v2(ff, el_mapping, basis, M22s)

ep = basis.ep;

shape = size(M22s,1);
p = sqrt(shape);

% [xp, wp] = GLLnodes(p);
% [xp, wp] = Gnodes2(p+2);

xp = basis.xp;
wp = basis.wp;

[ixii , ieta] = meshgrid(xp);
[ix3, iy3] = el_mapping(ixii, ieta);

ix3 = ix3';
iy3 = iy3';

ffi = ff(ix3,iy3);

Mf = zeros(1,shape);

for i = 1:p
    for j = 1:p
        volij = (i-1)*p +j;
        for ix = 1:size(xp,2)
            for iy = 1:size(xp,2)
%                 [ix2, iy2] = el_mapping(xp(ix),xp(iy));
%                 Mf(volij) = Mf(volij) + ep(i,ix)*ep(j,iy)*ff(ix2,iy2)*wp(ix)*wp(iy);
                Mf(volij) = Mf(volij) + ep(i,ix)*ep(j,iy)*ffi(ix,iy)*wp(ix)*wp(iy);
            end
        end
    end
end

dof_f = M22s \ Mf';

end

