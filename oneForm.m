classdef oneForm < handle
    %ONEFORM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Xcochain
        Ycochain
        p
        pf
        eval_pf_dx_dxii
        eval_pf_dx_deta
        eval_pf_dy_dxii
        eval_pf_dy_deta
        try45
        K
        
        cochain
        rec_qx
        rec_qy
    end
    
    methods
        %         function obj = oneForm(ttl_nr_el)
        %             obj.try45 = zeros(obj.p, );
        %         end
        
        function qx = reconstructionX(obj)
            qx = reconstruction.reconstruct1xform_2(obj.Xcochain, obj.Ycochain, obj.p, obj.pf, obj.eval_pf_dx_dxii, obj.eval_pf_dx_deta, obj.eval_pf_dy_dxii, obj.eval_pf_dy_deta);
        end
        
        function qy = reconstructionY(obj)
            qy = reconstruction.reconstruct1yform_2(obj.Xcochain, obj.Ycochain, obj.p, obj.pf, obj.eval_pf_dx_dxii, obj.eval_pf_dx_deta, obj.eval_pf_dy_dxii, obj.eval_pf_dy_deta);
        end
        
        function obj = reconstruction(obj, q_h, ttl_nr_el, mesh1)
            
%             q_h = cochain;
            half_edges = obj.p*(obj.p+1);
            
            for eln = 1:ttl_nr_el
                qx_h(:,eln) = q_h(1:half_edges,eln);
                qy_h(:,eln) = -q_h(half_edges+1:end,eln);
            end

            for elx = 1:obj.K
                for ely = 1:obj.K
                    eln = (elx-1)*obj.K + ely;
                    
                    temp2 = reconstruction.reconstruct1xform_v4(qx_h(:,eln), qy_h(:,eln), obj.p, obj.pf, mesh1, eln);
                    obj.rec_qx(:,:,eln) = full(temp2)';
                    
                    temp2 = reconstruction.reconstruct1yform_v4(qx_h(:,eln), qy_h(:,eln), obj.p, obj.pf, mesh1, eln);
                    obj.rec_qy(:,:,eln) = full(temp2)';
                end
            end
        end
        
    end
    
end

