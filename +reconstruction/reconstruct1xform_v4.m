function [ B2 ] = reconstruct1xform_v4(u1x, u1y, mesh1, eln)

% function [ B2 ] = reconstruct1xform_v4( u1x, u1y, p, pf, mesh1, eln, basis)

%RECONSTRUCT1X Summary of this function goes here
%   Detailed explanation goes here

% modified on 14 Oct, 2018
% modified on 17 Oct, 2018
% modified on 17 Nov, 2018

% nf = 40;
% xf = linspace(-1,1,nf);

% [xf, ~] = GLLnodes(pf);
% [hf,ef] = MimeticpolyVal(xf,p,1);
pf = mesh1.pf;
hf = mesh1.basis.hf;
ef = mesh1.basis.ef;

dx_dxii = mesh1.eval.pf.dx_dxii(:,:,eln);
dx_deta = mesh1.eval.pf.dx_deta(:,:,eln);
dy_dxii = mesh1.eval.pf.dy_dxii(:,:,eln);
dy_deta = mesh1.eval.pf.dy_deta(:,:,eln);

eval_dx_dxii = dx_dxii';
eval_dx_deta = dx_deta';
eval_dy_dxii = dy_dxii';
eval_dy_deta = dy_deta';

eval_g = metric.ggg(eval_dx_dxii, eval_dx_deta, eval_dy_dxii, eval_dy_deta);

% dx_dxii_g = eval_dx_dxii ./ eval_g;
% dx_deta_g = eval_dx_deta ./ eval_g;
dy_dxii_g = eval_dy_dxii ./ eval_g;
dy_deta_g = eval_dy_deta ./ eval_g;

% dx_dxii_g = dx_dxii_g(:);
% dx_deta_g = dx_deta_g(:);
dy_dxii_g = dy_dxii_g(:);
dy_deta_g = dy_deta_g(:);

for i = 1:pf+1
   O(i) = (i-1)*(pf+1) + 1;
end

O1 = O;

for i = 1:pf
    O1 = horzcat(O1, O+i);
end

%% reference basis
hex  = kron(hf,ef);   % basis functions in x-direction with quad pts. numbered horizontally
                      % these are also the basis functions in y-direction,
                      % but with quad pts numbered vertically. We want the
                      % quad pts to be numbered horizontally, therefore we
                      % will resructure their numbering.
hey  = hex(:,O1);

%% new basis after metric terms
hex11 = bsxfun(@times, hex, dy_deta_g');
hey11 = bsxfun(@times, hey, dy_dxii_g');

%% evaluation
ux_h_1 = bsxfun(@times,hex11,u1x);

ux_h_2 = bsxfun(@times,hey11,u1y);

ux_h = ux_h_1 - ux_h_2;

ux_h =sum(ux_h);
B2 = reshape(ux_h,[pf+1,pf+1]);

end

