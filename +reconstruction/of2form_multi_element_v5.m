function [ xf_3D, yf_3D, recf2 ] = of2form_multi_element_v5(f_h, p, nf, el_mapping, gf, K,basis)
%OF2FORM_MULTI_ELEMENT Summary of this function goes here
%   Detailed explanation goes here

% Created on 20th March
% modified on 16 October, 2018
% modified on 16 November, 2018

p = mesh1.p;

nr_total_el = K^2;

xf_3D = zeros(nf+1,nf+1, nr_total_el);
yf_3D = zeros(nf+1,nf+1, nr_total_el);
recf2 = zeros(nf+1,nf+1, nr_total_el);

[xf, ~] = GLLnodes(nf);

[xi_recn, eta_recn] = meshgrid(xf);

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;

        temp = reconstruction.reconstruct2form_v3(f_h(:,el)', p, nf, gf(:,:,el),basis);
        recf2(:,:,el) = full(temp)';
        
        [xf, yf] = el_mapping(xi_recn,eta_recn,elx,ely);

        xf_3D(:,:,el) = xf;
        yf_3D(:,:,el) = yf;
    end
end

end