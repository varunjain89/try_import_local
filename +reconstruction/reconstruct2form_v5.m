function [ rec2D ] = reconstruct2form_v5(cochain, basis1D, gf)

% modified on 13 July, 2019

shape = size(gf);

gf = gf(:)';
gf = 1 ./gf;

ef = basis1D.ef;
basis  = kron(ef,ef);

metric = bsxfun(@times,basis,gf);

rec = bsxfun(@times,metric,cochain);
rec = sum(rec,1);

rec2D = reshape(rec, shape);

end

