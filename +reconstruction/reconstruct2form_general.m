function [xf, yf, pf] = reconstruct2form_general(cochain, xii, eta, )

% Created on 2 June, 2019

% evaluate basis functions @ xiif, etaf
[~, e] = MimeticpolyVal(xiif,N,k);

basis = kron (e,e);

% evaluate jacobian @ xiif, etaf
jac = evalaluate_jacobian(obj, xiif, etaf, reg, elx, ely);

% evaluate metric terms @ xiif, etaf
gf = physics.poisson.eval_metric(jac);
gf = gf(:)';
gf = 1 ./gf;

% evaluate reconstruction nodes
basis2  = bsxfun(@times,basis,gf);
pf      = bsxfun(@times,basis2,cochain);
pf      = sum(pf);

[xf, yf] = mapping(xiif, etaf, reg, elx, ely);

end