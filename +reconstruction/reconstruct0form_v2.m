function [ f_plot ] = reconstruct0form_v2( f_0_h, p, nf )
%RECONSTRUCT0FORM_V2 Summary of this function goes here
%   Detailed explanation goes here

% Created on: 6th March, 2018

[xi_fine, w] = GLLnodes(nf);
[hf,ef] = MimeticpolyVal(xi_fine,p,1);

f_plot = zeros(nf+1);

for i = 1:p+1
    for j = 1:p+1
        nodeij = (i-1)*(p+1) + j;
        for quad_x = 1:nf+1
            for quad_y = 1:nf+1
                f_plot(quad_x, quad_y) = f_plot(quad_x, quad_y) + f_0_h(nodeij)*hf(i,quad_x)*hf(j,quad_y);
            end
        end
    end
end

% figure
% contourf(x_recn, y_recn, f_plot)
% 
% f_exact = f(x_recn, y_recn);
% 
% figure
% contourf(x_recn, y_recn, f_exact)

end

