function [ xf_3D, yf_3D, recf2 ] = of2form_multi_element_v6_salil(f_h, mesh)
%OF2FORM_MULTI_ELEMENT Summary of this function goes here
%   Detailed explanation goes here

% Created on 20th March
% modified on 16 October, 2018
% modified on 16 November, 2018

K = mesh.K;
p = mesh.p;
nf = mesh.pf;
basis = mesh.basis;
el_mapping = mesh.el.mapping;
gf = mesh.eval.pf.ggg;

nr_total_el = K^2;

xf_3D = zeros(nf+1,nf+1, nr_total_el);
yf_3D = zeros(nf+1,nf+1, nr_total_el);
recf2 = zeros(nf+1,nf+1, nr_total_el);

[xf, ~] = GLLnodes(nf);

[xii_recn, eta_recn] = meshgrid(xf);

xiif2 = xii_recn(:);
etaf2 = eta_recn(:);

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;

        temp = reconstruction.reconstruct2form_v3(f_h(:,el)', p, nf, gf(:,:,el),basis);
        recf2(:,:,el) = full(temp)';
        
        [xf, yf] = el_mapping(xiif2,etaf2,elx,ely);
        
        xf = reshape(xf,[size(xii_recn,1) size(xii_recn,2)]);
        yf = reshape(yf,[size(xii_recn,1) size(xii_recn,2)]);

        xf_3D(:,:,el) = xf;
        yf_3D(:,:,el) = yf;
    end
end

end