function [ B2 ] = reconstruct0form( phi, p, pf )
%RECONTRUCT0FORM Summary of this function goes here
%   Detailed explanation goes here

%% reconstruct 0-form 

% pf = 30;
% xf = linspace(-1,1,nf);
[xf, wf] = GLLnodes(pf);

[hf,~] = MimeticpolyVal(xf,p,1);

A = kron(hf,hf);

B = bsxfun(@times,A,phi');
B1=sum(B);
B2 = reshape(B1,[pf+1,pf+1]);

end

