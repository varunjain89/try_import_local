function [ xf_3D, yf_3D, recf2 ] = of2form_multi_element_v7(f_h, mesh)
%OF2FORM_MULTI_ELEMENT Summary of this function goes here
%   Detailed explanation goes here

% Created on 20th March
% modified on 16 October, 2018
% modified on 16 November, 2018

K = mesh.K;
p = mesh.p;
nf = mesh.pf;
basis = mesh.basis;
el_mapping = mesh.el.mapping;
gf = mesh.eval.pf.ggg;

nr_total_el = K^2;

xf_3D = zeros(nf+1,nf+1, nr_total_el);
yf_3D = zeros(nf+1,nf+1, nr_total_el);
recf2 = zeros(nf+1,nf+1, nr_total_el);

[xf, ~] = GLLnodes(nf);

[xi_recn, eta_recn] = meshgrid(xf);

for elx = 1:K
    for ely = 1:K
        el = (elx-1)*K + ely;

        temp = reconstruction.reconstruct2form_v3(f_h(:,el)', p, nf, gf(:,:,el),basis);
        recf2(:,:,el) = full(temp)';
        
        for t1 = 1:size(xi_recn,1)
            for t2 = 1:size(xi_recn,2)
                [xf(t1,t2), yf(t1,t2)] = el_mapping(xi_recn(t1,t2),eta_recn(t1,t2),elx,ely);
            end
        end

        xf_3D(:,:,el) = xf;
        yf_3D(:,:,el) = yf;
    end
end

end