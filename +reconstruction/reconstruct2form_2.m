function [ B2 ] = reconstruct2form_2( phi, p, nf, gf, basis)
%RECONSTRUCT2FORM Summary of this function goes here
%   Detailed explanation goes here

%% reconstruct 2-form 

% nf = 40;
% xf = linspace(-1,1,nf);
% xf = linspace(-1,1,nf);

gf =gf(:)';
gf = 1 ./gf;

[xf, ~] = GLLnodes(nf);
[~,ef] = MimeticpolyVal(xf,p,1);

A = kron(ef,ef);
A = bsxfun(@times,A,gf);

B = bsxfun(@times,A,phi');
B1=sum(B);
if p==1
    B1 = B;
end
B2 = reshape(B1,[nf+1,nf+1]);

end

