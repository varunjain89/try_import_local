function [B2 ] = reconstruct2form_v4(cochain, basis, gf)

% reconstruct2form_v4( phi, basis)

%RECONSTRUCT2FORM Summary of this function goes here
%   Detailed explanation goes here

% modified on 16 October, 2018
% modified on 1 June, 2019

%% reconstruct 2-form 

% nf = 40;
% xf = linspace(-1,1,nf);
% xf = linspace(-1,1,nf);

gf = gf(:)';
gf = 1 ./gf;

% ef = basis.ef;
% [xf, ~] = GLLnodes(nf);
% [~,ef] = MimeticpolyVal(xf,p,1);

% A = kron(ef,ef);
A = bsxfun(@times,basis,gf);

B = bsxfun(@times,A,cochain);
B1=sum(B);
% if p==1
%     B1 = B;
% end
B2 = reshape(B1,[30,30]);

end

