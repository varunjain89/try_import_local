function [ B2 ] = reconstruct1xform_v8( u1x, u1y, ~, pf, jac, metric2,eln,basis_rec)

% 15 July, 2019

% [xf, ~] = GLLnodes(pf);
% [hf,ef] = MimeticpolyVal(xf,p,1);

% s = linspace(-1,1,pf+1);
% [hf, ef] = MimeticpolyVal(s,p,1);

hf = basis_rec.hfx;
ef = basis_rec.efx;

dy_dxii = jac.dy_dxii(:,:,eln);
dy_deta = jac.dy_deta(:,:,eln);
ggg = metric2.ggg(:,:,eln);

dy_dxii_g = dy_dxii ./ ggg;
dy_deta_g = dy_deta ./ ggg;

dy_dxii_g = dy_dxii_g(:);
dy_deta_g = dy_deta_g(:);

for i = 1:pf+1
   O(i) = (i-1)*(pf+1) + 1;
end

O1 = O;

for i = 1:pf
    O1 = horzcat(O1, O+i);
end

%% reference basis
% hex = kron(basis.hfx, basis.efy);
% hey = kron(basis.efx, basis.hfy);

hex  = kron(hf,ef);   % basis functions in x-direction with quad pts. numbered horizontally
                      % these are also the basis functions in y-direction,
                      % but with quad pts numbered vertically. We want the
                      % quad pts to be numbered horizontally, therefore we
                      % will resructure their numbering.
hey  = hex(:,O1);

%% new basis after metric terms
hex11 = bsxfun(@times, hex, dy_deta_g');
hey11 = bsxfun(@times, hey, dy_dxii_g');

%% evaluation
ux_h_1 = bsxfun(@times,hex11,u1x);

ux_h_2 = bsxfun(@times,hey11,u1y);

ux_h = ux_h_1 - ux_h_2;

ux_h =sum(ux_h);
B2 = reshape(ux_h,[pf+1,pf+1]);

end

