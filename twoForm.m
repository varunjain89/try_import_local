classdef twoForm
    %TWOFORM Summary of this class goes here
    %   Detailed explanation goes here
    % created on 23 Sept
    
    properties
        cochain
        p
        pf
        el_mapping
        eval_pf_ggg
        K
        
        f_an
        domain
        el_bounds_x
        el_bounds_y
    end
    
    methods
%         function obj = twoForm(f_an, domain, el_bounds, K, p)
%             obj.K = K;
%             obj.p = p;
%             obj.f_an = @(xi,eta) f_an(xi, eta);
%             obj.domain = domain;
%             obj.el_bounds_x = el_bounds.x;
%             obj.el_bounds_y = el_bounds.y;
%         end
        
        function [xf_3D, yf_3D, uf_h] = reconstruction(obj, basis)
            [xf_3D, yf_3D, uf_h] = reconstruction.of2form_multi_element_v5(obj.cochain, obj.p, obj.pf, obj.el_mapping, obj.eval_pf_ggg, obj.K, basis);
        end
        function F_3D = reduction(obj, f_an)
            F_3D = reduction.of2form_multi_element_v7(f_an, obj.p, obj.domain, obj.K, obj.el_bounds_x, obj.el_bounds_y);
        end
    end
    
end

