function [ sol ] = ggg_v2(eval)
%G Summary of this function goes here
%   Detailed explanation goes here

% Created on : 23 March, 2018
% modified on 14 Oct, 2018

dx_dxi  = eval.dx_dxii;
dx_deta = eval.dx_deta;
dy_dxi  = eval.dy_dxii;
dy_deta = eval.dy_deta;

sol = dx_dxi.*dy_deta - dx_deta.*dy_dxi;

end

