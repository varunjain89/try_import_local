function [ sol ] = g12(dx_dxi, dx_deta, dy_dxi, dy_deta, g)
%G12 Summary of this function goes here
%   Detailed explanation goes here

% Created on 23 March, 2018

k11 = 1;
k12 = 0;
k22 = 1;

sol = (k11 .* dx_dxi .* dx_deta + k12 .* (dy_dxi .* dx_deta + dx_dxi .* dy_deta) + k22 .* dy_dxi .* dy_deta) ./ g ;

end

