function [ sol ] = ggg_element(dx_dxi, dx_deta, dy_dxi, dy_deta, element_bounds_x, element_bounds_y, elx, ely, s, t)
%G Summary of this function goes here
%   Detailed explanation goes here

% Created on : 23 March, 2018
% Updated on : 27 March, 2018

xi_i1 = element_bounds_x(elx);
xi_i2 = element_bounds_x(elx+1);
eta_j1 = element_bounds_y(ely);
eta_j2 = element_bounds_y(ely+1);

xi  = 0.5*(xi_i1 + xi_i2) + 0.5*(xi_i2 - xi_i1).* s;
eta = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* t;

dxi_ds  = 0.5*(xi_i2 - xi_i1);
deta_dt = 0.5*(eta_j2 - eta_j1);

sol = (dx_dxi(xi,eta).*dy_deta(xi,eta) - dx_deta(xi,eta).*dy_dxi(xi,eta)) .* dxi_ds .* deta_dt;

end

