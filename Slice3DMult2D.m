function [ b2 ] = Slice3DMult2D( F, x )
%SLICE3DMULT2D Summary of this function goes here
%   Detailed explanation goes here

% Created on : 12 Feb, 2018.
% Descripion : Multiplies each 2-D slices of 3-D matrix, F, with another
% 2-D matrix. 

% tic
% f1 = size(F);
% x1 = size(x);
% b = reshape(sum(bsxfun(@times,reshape(F,[f1(1),1,f1(2:3)]), reshape(x',1,x1(2),[])),3),f1(1),x1(2),[]);
% toc

% tic
b2 = zeros(size(F, 1), size(x, 2), size(F, 3));

for i = 1:size(F,3)
    b2(:,:,i) = F(:,:,i)*x;
end
% toc

% isequal(b2,b)

% max(max(max(b-b2)))

end

