function [ x, y ] = map_local_y_edge(x_bound, y_bound, c, eta1, eta2, quad_x, xi_i)
%MAP_LOCAL_Y_EDGE Summary of this function goes here
%   Detailed explanation goes here

x1 = x_bound(1);
x2 = x_bound(2);
y1 = y_bound(1);
y2 = y_bound(2);

xi  = xi_i;
eta = 0.5*(eta1 + eta2) + 0.5*(eta2 - eta1).* quad_x;

x = (x1 + x2)/2 + (x2 - x1)/2 .* (xi + c .* sin(pi .* xi) .* sin(pi .* eta));
y = (y1 + y2)/2 + (y2 - y1)/2 .* (eta + c .* sin(pi .* xi) .* sin(pi .* eta));

end

