classdef n_hybrid_meshv2 < handle
    % Created on: 8th April, 2019
    % this is a class for any general mesh
    
    properties
        domain
        ttl_nr_domain
        ttl_nr_el
        el_bounds
        el
        
        lcl_nr_el
        
        K
        
    end
    
    methods
        %% initialize the mesh object with domain mapping and jacobians
        function obj = n_hybrid_meshv2(region)
            obj.domain = region;
            obj.ttl_nr_domain = size(obj.domain,2);
        end
        
        %% check domain mapping
        function obj = check_domain_mapping(obj)
            s = linspace(-1,1,31);
            [xii, eta] = meshgrid(s);
                        
            figure
            hold on
            
            for i = 1:obj.ttl_nr_domain
                
                [x, y] = obj.domain(i).mapping(xii, eta);
                plot(x, y, '+')
                
                % bottom boundary
                [xB, yB] = obj.domain(i).mapping(s, -1.*ones(size(s)));
                plot(xB, yB, 'linewidth',2)
                
                % left boundary
                [xL, yL] = obj.domain(i).mapping(1.*ones(size(s)), s);
                plot(xL, yL, 'linewidth',2)
                
                % top boundary
                [xT, yT] = obj.domain(i).mapping(s, 1.*ones(size(s)));
                plot(xT, yT, 'linewidth',2)
                
                % right boundary
                [xR, yR] = obj.domain(i).mapping(-1.*ones(size(s)), s);
                plot(xR, yR, 'linewidth',2)
            end
        end
        
        %% discretize the mesh object in elements and polynomial degree
        function obj = discretize(obj,Kx,Ky)
            obj.K.Kx = Kx;
            obj.K.Ky = Ky;
            
            obj.lcl_nr_el = Kx * Ky;
            
            obj.ttl_nr_el = obj.ttl_nr_domain * Kx * Ky;
            
            obj.el_bounds.x = linspace(-1,1,Kx+1);
            obj.el_bounds.y = linspace(-1,1,Ky+1);
            
            for i = 1:size(obj.domain,2)
                obj.domain(i).el.mapping = @(xi,eta,elx,ely) mesh.element.mapping(obj.domain(i).mapping, obj.el_bounds.x, obj.el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_dxii = @(xi,eta,elx,ely) mesh.element.dx_dxii(obj.domain(i).dX_dxii, obj.el_bounds.x, obj.el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_deta = @(xi,eta,elx,ely) mesh.element.dx_deta(obj.domain(i).dX_deta, obj.el_bounds.x, obj.el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_dxii = @(xi,eta,elx,ely) mesh.element.dy_dxii(obj.domain(i).dY_dxii, obj.el_bounds.x, obj.el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_deta = @(xi,eta,elx,ely) mesh.element.dy_deta(obj.domain(i).dY_deta, obj.el_bounds.x, obj.el_bounds.y, elx, ely, xi, eta);
            end
            
            for reg = 1:size(obj.domain,2)
                for elx = 1:Kx
                    for ely = 1:Ky
                        eln = (reg-1) * Kx * Ky + (elx -1) * Ky + ely;
                        obj.el(eln).mapping = @(xii,eta,eln) obj.domain(reg).el.mapping(xii,eta,elx,ely);
                        obj.el(eln).dX_dxii = @(xii,eta,eln) obj.domain(reg).el.dX_dxii(xii,eta,elx,ely);
                        obj.el(eln).dX_deta = @(xii,eta,eln) obj.domain(reg).el.dX_deta(xii,eta,elx,ely);
                        obj.el(eln).dY_dxii = @(xii,eta,eln) obj.domain(reg).el.dY_dxii(xii,eta,elx,ely);
                        obj.el(eln).dY_deta = @(xii,eta,eln) obj.domain(reg).el.dY_deta(xii,eta,elx,ely);
                    end
                end
            end
            
        end
        
        %% check element mapping
        function obj = check_element_mapping(obj)
            cc = 0.6;
            s = linspace(-1,1,31);
            [s2,~] = GLLnodes(6);
%             [xii, eta] = meshgrid(s2);
            
            [xii1, eta1] = meshgrid(s,s2);
            [xii2, eta2] = meshgrid(s2,s);
            
            figure
            hold on
            
            for i = 1:obj.ttl_nr_el
                
%                 [x, y] = obj.el(i).mapping(xii, eta);
                [x1, y1] = obj.el(i).mapping(xii1, eta1);
                [x2, y2] = obj.el(i).mapping(xii2, eta2);
                
                plot(x2(:,2:end-1), y2(:,2:end-1),'Color', [cc cc cc], 'linewidth', 2)
                plot(x1(2:end-1,:)',y1(2:end-1,:)','Color', [cc cc cc], 'linewidth', 2)
                
                % bottom boundary
                [xB, yB] = obj.el(i).mapping(s, -1.*ones(size(s)));
                plot(xB, yB,'k','linewidth',2)
                
                % left boundary
                [xL, yL] = obj.el(i).mapping(1.*ones(size(s)), s);
                plot(xL, yL,'k','linewidth',2)
                
                % top boundary
                [xT, yT] = obj.el(i).mapping(s, 1.*ones(size(s)));
                plot(xT, yT,'k','linewidth',2)
                
                % right boundary
                [xR, yR] = obj.el(i).mapping(-1.*ones(size(s)), s);
                plot(xR, yR,'k','linewidth',2)
            end
            set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
        end
        
        %% plot broken domain
        function obj = plot_broken_domain(obj)
            factor = 0.96;
            cc = 0.6;
            s = linspace(-factor,factor,31);
            [s2,~] = GLLnodes(6);
            s2(1) = -factor;
            s2(end) = factor;
            [xii, eta] = meshgrid(s2);
            
            [xii1, eta1] = meshgrid(s,s2);
            [xii2, eta2] = meshgrid(s2,s);
            
            figure
            hold on
            
            for i = 1:obj.ttl_nr_el
                
                [x, y] = obj.el(i).mapping(xii, eta);
                [x1, y1] = obj.el(i).mapping(xii1, eta1);
                [x2, y2] = obj.el(i).mapping(xii2, eta2);
                
                plot(x2(:,2:end-1), y2(:,2:end-1),'Color', [cc cc cc], 'linewidth', 2)
                plot(x1(2:end-1,:)',y1(2:end-1,:)','Color', [cc cc cc], 'linewidth', 2)
                
                % bottom boundary
                [xB, yB] = obj.el(i).mapping(s, -factor.*ones(size(s)));
                plot(xB, yB,'k','linewidth',2)
                
                % left boundary
                [xL, yL] = obj.el(i).mapping(factor*ones(size(s)), s);
                plot(xL, yL,'k','linewidth',2)
                
                % top boundary
                [xT, yT] = obj.el(i).mapping(s, factor.*ones(size(s)));
                plot(xT, yT,'k','linewidth',2)
                
                % right boundary
                [xR, yR] = obj.el(i).mapping(-factor.*ones(size(s)), s);
                plot(xR, yR,'k','linewidth',2)
            
                xp4 = (s2(1:end-1) + s2(2:end))/2;    
                size_red = 2;
                
                if i>obj.K.Ky
                    % left lambda's
                    [x,y] = obj.el(i).mapping(-1*ones(size(xp4)),xp4);
                    plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
                end
                
                if rem(i-1,obj.K.Ky) ~= 0
                    % bottom lanbda's
                    [x,y] = obj.el(i).mapping(xp4,-1*ones(size(xp4)));
                    plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
                else
                    [x,y] = obj.el(i).mapping(xp4,-1*ones(size(xp4)));
%                     plot(x, y,'go','MarkerFaceColor','g','markerSize',size_red)
                end
            end
            set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
        end
        
        %% plot lagrange multipliers for macor elements
        
        function obj = plot_macro_lagrange(obj)
            factor = 0.96;
            cc = 0.6;
            s = linspace(-factor,factor,31);
            [s2,~] = GLLnodes(6);
            s2(1) = -factor;
            s2(end) = factor;
            
%             figure
%             hold on
            
            for i = 1:obj.ttl_nr_el
                xp4 = (s2(1:end-1) + s2(2:end))/2;    
                size_red = 2;
                
                Ky_sub_el = 6;
                
                if rem(i,36) == 0 || rem(i+1,Ky_sub_el^2) == 0 || rem(i+4,Ky_sub_el^2) == 0 || rem(i+5,Ky_sub_el^2) == 0 || rem(i+8,Ky_sub_el^2) == 0 || rem(i+9,Ky_sub_el^2) == 0
                    % right lambda's
                    [x,y] = obj.el(i).mapping(1*ones(size(xp4)),xp4);
                    plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
                end
                
                if rem(i,36) == 0 || rem(i+26,36) == 0 || rem(i+24,36) == 0 || rem(i+14,36) == 0 || rem(i+12,36) == 0 || rem(i+2,36) == 0 
                    % top lanbda's
                    [x,y] = obj.el(i).mapping(xp4,1*ones(size(xp4)));
                    plot(x, y,'ro','MarkerFaceColor','r','markerSize',size_red)
                end
            end
%             set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
        end
        
        
        
        %% evaluate jacobian for all elements
        function jac = evaluate_jacobian_all(obj, xii, eta)
%             jac(obj.ttl_nr_el).dX_dxii = obj.el(obj.ttl_nr_el).dX_dxii(xii,eta);
            for i = 1:obj.ttl_nr_el
                jac.dx_dxii(:,:,i) = obj.el(i).dX_dxii(xii,eta);
                jac.dx_deta(:,:,i) = obj.el(i).dX_deta(xii,eta);
                jac.dy_dxii(:,:,i) = obj.el(i).dY_dxii(xii,eta);
                jac.dy_deta(:,:,i) = obj.el(i).dY_deta(xii,eta);
            end
        end
        
    end
end

