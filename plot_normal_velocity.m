function [ ] = plot_normal_velocity(ux_ex,temp2x,uy_ex,temp2y,rec_x,rec_y,ttl_nr_el)

% plot u

figure
hold on
for eln = 1:ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln))
end
colorbar
title('exact solution u_x')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on
for eln = 1:ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2x(:,:,eln)')
end
colorbar
title('reconstructed solution u_x')
set(gcf, 'Position',  [0700, 500, 550, 400])

figure
hold on
for eln = 1:ttl_nr_el
    surf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln) - temp2x(:,:,eln)')
end
colorbar
title('difference u_x')
set(gcf, 'Position',  [1300, 500, 550, 400])

% plot v

figure
hold on
for eln = 1:ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln))
end
colorbar
title('exact solution u_y')
set(gcf, 'Position',  [0100, 050, 550, 400])

figure
hold on
for eln = 1:ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2y(:,:,eln)')
end
colorbar
title('reconstructed solution u_y')
set(gcf, 'Position',  [0700, 050, 550, 400])

figure
hold on
for eln = 1:ttl_nr_el
    surf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln) - temp2y(:,:,eln)')
end
colorbar
title('difference u_y')
set(gcf, 'Position',  [1300, 050, 550, 400])

end

