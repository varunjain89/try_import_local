function [ x, y ] = map_local_edge_x( x_bound, y_bound, c, xi1, xi2, quad_x, eta_j)
%MAP_LOCAL_EDGE Summary of this function goes here
%   Detailed explanation goes here

x1 = x_bound(1);
x2 = x_bound(2);
y1 = y_bound(1);
y2 = y_bound(2);

xi  = 0.5*(xi1 + xi2) + 0.5*(xi2 - xi1).* quad_x;
eta = eta_j;

[x,y] = mesh.crazy_mesh.mapping(x_bound, y_bound, c, xi, eta);
% x = (x1 + x2)/2 + (x2 - x1)/2 .* (xi + c .* sin(pi .* xi) .* sin(pi .* eta));
% y = (y1 + y2)/2 + (y2 - y1)/2 .* (eta + c .* sin(pi .* xi) .* sin(pi .* eta));

end

