function [sys_inv_3D] = inverse_gpu(M11,E211, ttl_nr_el, local_nr_ed, local_nr_pp)
%INVERSE_GPU Summary of this function goes here
%   Detailed explanation goes here
% outputArg1 = inputArg1;
% outputArg2 = inputArg2;

ttl_loc_dof = local_nr_ed + local_nr_pp;

sys_inv_3D = zeros(ttl_loc_dof,ttl_loc_dof,ttl_nr_el);

xx = sqrt(local_nr_pp);

sizeM11 = size(M11);
pckts = (sizeM11(1)*sizeM11(2)*ttl_nr_el*8)/((60-1.2*xx)*1024*1024)
pckts = floor(pckts)

if pckts == 0
    limit = ttl_nr_el
else
    limit = ttl_nr_el/pckts
    limit = floor(limit)
end

pckts = floor(ttl_nr_el/limit)

rem = ttl_nr_el - limit * pckts

temp_sys = [zeros(local_nr_ed) E211'; E211 zeros(local_nr_pp)];
SYS_3D_on_CPU = repmat(temp_sys,[1 1 limit]);

% reset(gpuDevice)
d = gpuDevice;

% pass incomplete system to GPU
SYS_3D_on_GPU = gpuArray(SYS_3D_on_CPU);

for in = 1:pckts
    in
    % add M11 to the 3-D system on gpu
    d.AvailableMemory
    SYS_3D_on_GPU(1:local_nr_ed,1:local_nr_ed,:) = gpuArray(M11(:,:,(in-1)*limit+1:in*limit));
    % do the inverse of the 3D system
    d.AvailableMemory
    SYSi_3DonGPU = pagefun(@inv,SYS_3D_on_GPU);
    % gather the inverse back on CPU
    sys_inv_3D(:,:,(in-1)*limit+1:in*limit) = gather(SYSi_3DonGPU);
    d.AvailableMemory
end

rem = ttl_nr_el - limit * pckts

if (rem ~= 0)
    temp2 = repmat(temp_sys,[1 1 rem]);
    temp2(1:local_nr_ed,1:local_nr_ed,:) = M11(:,:,limit * pckts+1:end);
    temp_g = gpuArray(temp2);
    temp_ig = pagefun(@inv,temp_g);
    sys_inv_3D(:,:,limit * pckts+1:end) = gather(temp_ig);
    rem
end

end

