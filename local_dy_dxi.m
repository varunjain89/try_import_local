function [ dy_dxi ] = local_dy_dxi( x_bound, y_bound, c, xi1, xi2, quad_x, eta_j )
%LOCAL_DY_DXI Summary of this function goes here
%   Detailed explanation goes here

x1 = x_bound(1);
x2 = x_bound(2);
y1 = y_bound(1);
y2 = y_bound(2);

xi  = 0.5*(xi1 + xi2) + 0.5*(xi2 - xi1).* quad_x;
eta = eta_j;

dxi = 0.5*(xi2 - xi1);

dy_dxi = (y2 - y1)/2 .* (c .* cos(pi .* xi) .* sin(pi .* eta) .* pi .* dxi);

end

