function [ N ] = OutwardNormalMatrix( p )
%OUTWARDNORMALMATRIX Summary of this function goes here
%   Detailed explanation goes here

% this program ceates outward normal matrix
% or the the incidence matrix for ghost volumes 
% that exist at the element boundaries

nr_bc = 4*p;
nr_edges = 2*p*(p+1);

N = zeros(nr_bc, nr_edges);

for i = 1:p
    bc_bot      = i;
    bc_right    = 1*p + i;
    bc_left     = 2*p + i;
    bc_top      = 3*p + i;

    edge_bot    = i ;
    edge_right  = 2*p*(p+1) - p + i ;
    edge_left   = p*(p+1) +i ;
    edge_top    = p*(p+1) - p + i ;

    N(bc_bot,edge_bot)      = -1;
    N(bc_right,edge_right)  = +1;
    N(bc_left,edge_left)    = -1;
    N(bc_top,edge_top)      = +1;
end

N = sparse(N);

end

