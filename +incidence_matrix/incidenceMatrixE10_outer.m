function [ E10 ] = incidenceMatrixE10_outer( p )
%INCIDENCEMATRIXE10_OUTER Summary of this function goes here
%   Detailed explanation goes here

nr_nodes = (p+1)^2;
nr_edges = 2*p*(p+1);

E10 = zeros(nr_edges, nr_nodes); 

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p+i;
        
        node_left  = (i-1)*(p+1) + j;
        node_right = i*(p+1) + j;
        
        E10(edgeij, node_left)  = +1;
        E10(edgeij, node_right) = -1;
    end
end

for i = 1:p+1
    for j = 1:p
        edgeij = p*(p+1) + (i-1)*p + j;
        
        node_bot = (i-1)*(p+1) + j;
        node_top = (i-1)*(p+1) + j + 1;
        
        E10(edgeij, node_top) = +1;
        E10(edgeij, node_bot) = -1;
    end
end

end

