function [connectivity_el] = connectivity_elements(nr_Region, K, p, nr_el_Rg, ttl_loc_dof, nr_ed_el)

% Created on 28 May, 2019

GM_local_dof    = gather_matrix.GM_total_local_dof_v3(nr_el_Rg, ttl_loc_dof);
GM_local_dof_q  = GM_local_dof(:,1:nr_ed_el);
GM_lambda       = gather_matrix.GM_boundary_LM_nodes(K,p);


N = incidence_matrix.OutwardNormalMatrix(p);
N_3D = repmat(full(N),[1 1 nr_el_Rg]);

ass_conn2 = AssembleMatrices2(GM_lambda, GM_local_dof_q', N_3D);
ass_conn2 = [ass_conn2 zeros(2*K*p*(K+1),p^2)];

ass_conn3 = ass_conn2(1:2*K*p*(K-1),:);

for reg = 1:nr_Region
    connectivity(reg) = {ass_conn3};
end

connectivity_el = blkdiag(connectivity{:});

end

