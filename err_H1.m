function [ error ] = err_H1(l2_err_p_sqre, l2_err_gradPx_sqre, l2_err_gradPy_sqre)
%ERR_H1 Summary of this function goes here
%   Detailed explanation goes here

% Created on: 6th November, 2018

% let p be a zero form

error = l2_err_p_sqre + l2_err_gradPx_sqre + l2_err_gradPy_sqre;
error = sqrt(error);

end

