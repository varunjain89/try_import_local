function [ K ] = material(k11_an, k12_an, k22_an, xy)
%MATERIAL Summary of this function goes here
%   Detailed explanation goes here

% Created on : 17 Nov, 2018

K.k11 = k11_an(xy.x, xy.y);
K.k12 = k12_an(xy.x, xy.y);
K.k22 = k22_an(xy.x, xy.y);
K.detK = K.k11.*K.k22 - K.k12.^2;

end

