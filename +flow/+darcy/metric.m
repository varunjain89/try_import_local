function [ p ] = metric(jac, K)
%METRIC Summary of this function goes here
%   Detailed explanation goes here

% Created on : 17 Nov, 2018

% jac stands for jacobian

p.g11 = flow.darcy.g11K_v3(jac, K);
p.g12 = flow.darcy.g12K_v3(jac, K);
p.g22 = flow.darcy.g22K_v3(jac, K);

end

