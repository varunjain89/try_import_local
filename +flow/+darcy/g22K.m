function [ sol ] = g22K(dx_dxi, ~, dy_dxi, ~, g, K11, K12, K22, detK)

% function [ sol ] = g22K(dx_dxi, dx_deta, dy_dxi, dy_deta, g)

%G22 Summary of this function goes here
%   Detailed explanation goes here

% Created on 23 March, 2018

k11i =  K22./detK;
k12i = -K12./detK;
k22i =  K11./detK;

sol = (k11i.*dx_dxi.^2 + 2.* k12i.*dy_dxi.*dx_dxi + k22i.*dy_dxi.^2)./g;

end

