function [ sol ] = g12K_v2(eval, g, evalK, detK)
%G12 Summary of this function goes here
%   Detailed explanation goes here

% Created on 23 March, 2018
% Modified on 12 Oct, 2018

dx_dxi  = eval.dx_dxii;
dx_deta = eval.dx_deta;
dy_dxi  = eval.dy_dxii;
dy_deta = eval.dy_deta;

K11 = evalK.k11;
K12 = evalK.k12;
K22 = evalK.k22;

k11i =  K22./detK;
k12i = -K12./detK;
k22i =  K11./detK;

sol = (k11i.*dx_dxi.*dx_deta + k12i.*(dy_dxi.*dx_deta + dx_dxi.*dy_deta) + k22i.*dy_dxi.*dy_deta)./g ;

end

