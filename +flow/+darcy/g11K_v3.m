function [ sol ] = g11K_v3(eval, evalK)

% function [ sol ] = g11K(dx_dxi, dx_deta, dy_dxi, dy_deta, g, K11, K12, K22, detK)

%G11 Summary of this function goes here
%   Detailed explanation goes here

% Created on 23 March, 2018
% Modified on 12 Oct, 2018
% Modified on 17 Nov, 2018

dx_deta = eval.dx_deta;
dy_deta = eval.dy_deta;

K11 = evalK.k11;
K12 = evalK.k12;
K22 = evalK.k22;

k11i =  K22./evalK.detK;
k12i = -K12./evalK.detK;
k22i =  K11./evalK.detK;

sol = (k11i.*dx_deta.^2 + 2.*k12i.*dx_deta.*dy_deta + k22i.*dy_deta.^2)./eval.ggg;

end