function [ sol ] = g22K_v3(eval, evalK)

% function [ sol ] = g22K(dx_dxi, dx_deta, dy_dxi, dy_deta, g)

%G22 Summary of this function goes here
%   Detailed explanation goes here

% Created on 23 March, 2018
% Modified on 12 Oct, 2018

dx_dxi  = eval.dx_dxii;
dy_dxi  = eval.dy_dxii;

K11 = evalK.k11;
K12 = evalK.k12;
K22 = evalK.k22;

k11i =  K22./evalK.detK;
k12i = -K12./evalK.detK;
k22i =  K11./evalK.detK;

sol = (k11i.*dx_dxi.^2 + 2.* k12i.*dy_dxi.*dx_dxi + k22i.*dy_dxi.^2)./eval.ggg;

end

