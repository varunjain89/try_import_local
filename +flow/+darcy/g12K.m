function [ sol ] = g12K(dx_dxi, dx_deta, dy_dxi, dy_deta, g, K11, K12, K22, detK)
%G12 Summary of this function goes here
%   Detailed explanation goes here

% Created on 23 March, 2018

k11i =  K22./detK;
k12i = -K12./detK;
k22i =  K11./detK;

sol = (k11i.*dx_dxi.*dx_deta + k12i.*(dy_dxi.*dx_deta + dx_dxi.*dy_deta) + k22i.*dy_dxi.*dy_deta)./g ;

end

