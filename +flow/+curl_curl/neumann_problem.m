function [ F, norm_F ] = neumann_problem(M0, M11, E10, E_bc)
%NEUMANN_PROBLEM Summary of this function goes here
%   Detailed explanation goes here

% created on: 1st March, 2018
% the neumann problem is the curl curl problem from the braking spaces
% paper. curl curl F + F = 0. 
% This function solves it on a single element.
% As input you provide the polynomial degree and the metric terms g11, g12,
% and g12, and the RHS vector (the boundary conditions vector). 
% For the output you get the cochain F as the solution. 


% M0 = massMatrix_0form(p);
% M11 = M1(p, [g11, g12, g22]);
% E10 = incidenceMatrixE10_outer(p);

A = E10' * M11 * E10 + M0;
det(A);

F = A\E_bc;

norm_F = F' * M0 * F + F' * E10' * M11 * E10 * F;

sqrt(norm_F);
end

