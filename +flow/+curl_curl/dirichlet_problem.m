function [ E, norm_E ] = dirichlet_problem(M0, M11, E10, E_bc)
%DIRICHLET_PROBLEM Summary of this function goes here
%   Detailed explanation goes here

% created on: 1st March, 2018
% Description: this function solves the dirichlet part of the curl curl
% problem from the breaking spaces paper. curl curl E + E = 0. 
% As input you provide the polynomial degree p, the metric terms g11, g12
% and g22, and the boundary condition vector E_bc. The dimension of E_bc is
% equal to the dimension of gauss lobatto nodes on primal grid. 
% As output you get the cochain of E. 
% Please note: E is on the dual grid, and you need to multiply it with M1
% to get the values on primal grid. 

% M0 = massMatrix_0form(p);
% M11 = M1(p, [g11, g12, g22]);
% E10 = incidenceMatrixE10_outer(p);
E21_dual = E10';

B = E21_dual' * inv(M0) * E21_dual + inv(M11);
E_bc3 = E21_dual'*inv(M0)*E_bc;

E = B\E_bc3;

norm_E = E' * inv(M11) * E + E' * E21_dual' * inv(M0) * E21_dual * E - E_bc' * inv(M0) * E_bc;
sqrt(-norm_E);

end

