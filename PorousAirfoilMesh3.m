classdef PorousAirfoilMesh3 < handle
    % Created on: 4th April, 2019
    
    properties
         
        
        domain
        eval
        p
        xp
        wp
        basis
        K
        el
        Kx
        Ky
        pf
        xf
        wf
    end
    
    methods
        function obj = PorousAirfoilMesh3(region)
            obj.domain = region;
        end
        
        
        
        function obj = def_basis(obj,p)
            obj.p = p;
            
            [obj.xp, obj.wp] = GLLnodes(obj.p);
            [obj.basis.hp, obj.basis.ep] = MimeticpolyVal(obj.xp,obj.p,1);
        end
        
        function obj = discretize(obj,Kx,Ky, p)
            
            obj.K = Kx;
            obj.Kx = Kx;
            obj.Ky = Ky;
            
            obj.p = p;
            
            obj.el.bounds.x = linspace(-1,1,Kx+1);
            obj.el.bounds.y = linspace(-1,1,Ky+1);
            
            for i = 1:size(obj.domain,2)
                obj.domain(i).el.mapping = @(xi,eta,elx,ely) mesh.element.mapping(obj.domain(i).mapping, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_dxii = @(xi,eta,elx,ely) mesh.element.dx_dxii(obj.domain(i).dX_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_deta = @(xi,eta,elx,ely) mesh.element.dx_deta(obj.domain(i).dX_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_dxii = @(xi,eta,elx,ely) mesh.element.dy_dxii(obj.domain(i).dY_dxii, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_deta = @(xi,eta,elx,ely) mesh.element.dy_deta(obj.domain(i).dY_deta, obj.el.bounds.x, obj.el.bounds.y, elx, ely, xi, eta);
            end

        end
        
        function obj = eval_jacobian(obj,p)
            obj.p = p;
            
            [obj.xp, obj.wp] = GLLnodes(obj.p);
            [obj.basis.hp, obj.basis.ep] = MimeticpolyVal(obj.xp,obj.p,1);
            
            [xp, ~] = GLLnodes(p);
            [xiip, etap] = meshgrid(xp);
            
            for reg = 1:size(obj.domain,2)
                for elx = 1:obj.K
                    for ely = 1:obj.K
                        eln = (elx -1)*obj.K +ely;
                        obj.eval.jac.p.dx_dxii2(:,:,eln,reg) = obj.domain(reg).el.dX_dxii(xiip,etap,elx,ely);
                        obj.eval.jac.p.dx_deta2(:,:,eln,reg) = obj.domain(reg).el.dX_deta(xiip,etap,elx,ely);
                        obj.eval.jac.p.dy_dxii2(:,:,eln,reg) = obj.domain(reg).el.dY_dxii(xiip,etap,elx,ely);
                        obj.eval.jac.p.dy_deta2(:,:,eln,reg) = obj.domain(reg).el.dY_deta(xiip,etap,elx,ely);
                    end
                end
            end
            
            obj.eval.jac.p.dx_dxii = obj.eval.jac.p.dx_dxii2(:,:,:,1);
            obj.eval.jac.p.dx_deta = obj.eval.jac.p.dx_deta2(:,:,:,1);
            obj.eval.jac.p.dy_dxii = obj.eval.jac.p.dy_dxii2(:,:,:,1);
            obj.eval.jac.p.dy_deta = obj.eval.jac.p.dy_deta2(:,:,:,1);
            
            for reg = 2:size(obj.domain,2)
                obj.eval.jac.p.dx_dxii = cat(3, obj.eval.jac.p.dx_dxii, obj.eval.jac.p.dx_dxii2(:,:,:,reg));
                obj.eval.jac.p.dx_deta = cat(3, obj.eval.jac.p.dx_deta, obj.eval.jac.p.dx_deta2(:,:,:,reg));
                obj.eval.jac.p.dy_dxii = cat(3, obj.eval.jac.p.dy_dxii, obj.eval.jac.p.dy_dxii2(:,:,:,reg));
                obj.eval.jac.p.dy_deta = cat(3, obj.eval.jac.p.dy_deta, obj.eval.jac.p.dy_deta2(:,:,:,reg));
            end
%             obj.eval.jac.p.dx_dxii = obj.domain.dX_dxii(xiip,etap);
%             obj.eval.jac.p.dx_deta = obj.domain.dX_deta(xiip,etap);
%             obj.eval.jac.p.dy_dxii = obj.domain.dY_dxii(xiip,etap);
%             obj.eval.jac.p.dy_deta = obj.domain.dY_deta(xiip,etap);
        end
        
        function obj = eval_jacobian_gauss(obj,p)
            obj.p = p;
                        
            [obj.xp, obj.wp] = GLLnodes(obj.p);
            [obj.basis.hp, obj.basis.ep] = MimeticpolyVal(obj.xp,obj.p,1);
            
%             [xp, ~] = GLLnodes(p);
            
            [xp] = Gnodes(obj.p + 1);
            
            [xiip, etap] = meshgrid(xp);
            
%             figure
%             plot(xiip, etap, '+')
            
            for reg = 1:size(obj.domain,2)
                for elx = 1:obj.K
                    for ely = 1:obj.K
                        eln = (elx -1)*obj.K +ely;
                        obj.eval.jac.p.dx_dxii2(:,:,eln,reg) = obj.domain(reg).el.dX_dxii(xiip,etap,elx,ely);
                        obj.eval.jac.p.dx_deta2(:,:,eln,reg) = obj.domain(reg).el.dX_deta(xiip,etap,elx,ely);
                        obj.eval.jac.p.dy_dxii2(:,:,eln,reg) = obj.domain(reg).el.dY_dxii(xiip,etap,elx,ely);
                        obj.eval.jac.p.dy_deta2(:,:,eln,reg) = obj.domain(reg).el.dY_deta(xiip,etap,elx,ely);
                    end
                end
            end
            
            obj.eval.jac.p.dx_dxii = obj.eval.jac.p.dx_dxii2(:,:,:,1);
            obj.eval.jac.p.dx_deta = obj.eval.jac.p.dx_deta2(:,:,:,1);
            obj.eval.jac.p.dy_dxii = obj.eval.jac.p.dy_dxii2(:,:,:,1);
            obj.eval.jac.p.dy_deta = obj.eval.jac.p.dy_deta2(:,:,:,1);
            
            for reg = 2:size(obj.domain,2)
                obj.eval.jac.p.dx_dxii = cat(3, obj.eval.jac.p.dx_dxii, obj.eval.jac.p.dx_dxii2(:,:,:,reg));
                obj.eval.jac.p.dx_deta = cat(3, obj.eval.jac.p.dx_deta, obj.eval.jac.p.dx_deta2(:,:,:,reg));
                obj.eval.jac.p.dy_dxii = cat(3, obj.eval.jac.p.dy_dxii, obj.eval.jac.p.dy_dxii2(:,:,:,reg));
                obj.eval.jac.p.dy_deta = cat(3, obj.eval.jac.p.dy_deta, obj.eval.jac.p.dy_deta2(:,:,:,reg));
            end
%             obj.eval.jac.p.dx_dxii = obj.domain.dX_dxii(xiip,etap);
%             obj.eval.jac.p.dx_deta = obj.domain.dX_deta(xiip,etap);
%             obj.eval.jac.p.dy_dxii = obj.domain.dY_dxii(xiip,etap);
%             obj.eval.jac.p.dy_deta = obj.domain.dY_deta(xiip,etap);
        end
        
        function obj = eval_pf_jacobian(obj,pf)
            obj.pf = pf;
            
            [obj.xf, obj.wf] = GLLnodes(obj.pf);
            [obj.basis.hf, obj.basis.ef] = MimeticpolyVal(obj.xf,obj.p,1);
            
            [xiip, etap] = meshgrid(obj.xf);
            
            xiip2 = xiip(:);
            etap2 = etap(:);

            for elx = 1:obj.Kx
                for ely = 1:obj.Ky
                    eln = (elx -1)*obj.Ky + ely;
                    obj.eval.jac.pf.dx_dxii(:,:,eln) = obj.el.dX_dxii(xiip2,etap2,elx,ely);
                    obj.eval.jac.pf.dx_deta(:,:,eln) = obj.el.dX_deta(xiip2,etap2,elx,ely);
                    obj.eval.jac.pf.dy_dxii(:,:,eln) = obj.el.dY_dxii(xiip2,etap2,elx,ely);
                    obj.eval.jac.pf.dy_deta(:,:,eln) = obj.el.dY_deta(xiip2,etap2,elx,ely);
                end
            end
            
            obj.eval.jac.pf.dx_dxii = reshape(obj.eval.jac.pf.dx_dxii,[pf+1,pf+1, obj.Kx*obj.Ky]);
            obj.eval.jac.pf.dx_deta = reshape(obj.eval.jac.pf.dx_deta,[pf+1,pf+1, obj.Kx*obj.Ky]);
            obj.eval.jac.pf.dy_dxii = reshape(obj.eval.jac.pf.dy_dxii,[pf+1,pf+1, obj.Kx*obj.Ky]);
            obj.eval.jac.pf.dy_deta = reshape(obj.eval.jac.pf.dy_deta,[pf+1,pf+1, obj.Kx*obj.Ky]);

        end
        
        function obj = eval_metric(obj)
            obj.eval.metric.ggg = metric.ggg(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta);
            obj.eval.metric.g11 = metric.g11(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta,obj.eval.metric.ggg);
            obj.eval.metric.g12 = metric.g12(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta,obj.eval.metric.ggg);
            obj.eval.metric.g22 = metric.g22(obj.eval.jac.p.dx_dxii,obj.eval.jac.p.dx_deta,obj.eval.jac.p.dy_dxii,obj.eval.jac.p.dy_deta,obj.eval.metric.ggg);
        end
        
        function obj = eval_pf_metric(obj)
            obj.eval.pf.ggg = metric.ggg(obj.eval.jac.pf.dx_dxii,obj.eval.jac.pf.dx_deta,obj.eval.jac.pf.dy_dxii,obj.eval.jac.pf.dy_deta);
        end
        
        function obj = evaluate_jacobian_reconstruct_gauss(obj,pf)
%             obj.pf = pf;
            
%             [obj.xf, obj.wf] = GLLnodes(obj.pf);
%             [obj.basis.hf, obj.basis.ef] = MimeticpolyVal(obj.xf,obj.p,1);
            
%             [xiip, etap] = meshgrid(obj.xf);
            
            [xp] = Gnodes(obj.p);
            [xiip, etap] = meshgrid(xp);
            
%             xiip2 = xiip(:);
%             etap2 = etap(:);
            
            for reg = 1:size(obj.domain,2)
                for elx = 1:obj.K
                    for ely = 1:obj.K
                        eln = (elx -1)*obj.K +ely;
                        obj.eval.jac.pf.dx_dxii2(:,:,eln,reg) = obj.domain(reg).el.dX_dxii(xiip,etap,elx,ely);
                        obj.eval.jac.pf.dx_deta2(:,:,eln,reg) = obj.domain(reg).el.dX_deta(xiip,etap,elx,ely);
                        obj.eval.jac.pf.dy_dxii2(:,:,eln,reg) = obj.domain(reg).el.dY_dxii(xiip,etap,elx,ely);
                        obj.eval.jac.pf.dy_deta2(:,:,eln,reg) = obj.domain(reg).el.dY_deta(xiip,etap,elx,ely);
                    end
                end
            end
            
            obj.eval.jac.pf.dx_dxii = obj.eval.jac.pf.dx_dxii2(:,:,:,1);
            obj.eval.jac.pf.dx_deta = obj.eval.jac.pf.dx_deta2(:,:,:,1);
            obj.eval.jac.pf.dy_dxii = obj.eval.jac.pf.dy_dxii2(:,:,:,1);
            obj.eval.jac.pf.dy_deta = obj.eval.jac.pf.dy_deta2(:,:,:,1);
            
            for reg = 2:size(obj.domain,2)
                obj.eval.jac.pf.dx_dxii = cat(3, obj.eval.jac.pf.dx_dxii, obj.eval.jac.pf.dx_dxii2(:,:,:,reg));
                obj.eval.jac.pf.dx_deta = cat(3, obj.eval.jac.pf.dx_deta, obj.eval.jac.pf.dx_deta2(:,:,:,reg));
                obj.eval.jac.pf.dy_dxii = cat(3, obj.eval.jac.pf.dy_dxii, obj.eval.jac.pf.dy_dxii2(:,:,:,reg));
                obj.eval.jac.pf.dy_deta = cat(3, obj.eval.jac.pf.dy_deta, obj.eval.jac.pf.dy_deta2(:,:,:,reg));
            end
            
%             for elx = 1:obj.Kx
%                 for ely = 1:obj.Ky
%                     eln = (elx -1)*obj.Ky + ely;
%                     obj.eval.jac.pf.dx_dxii(:,:,eln) = obj.el.dX_dxii(xiip2,etap2,elx,ely);
%                     obj.eval.jac.pf.dx_deta(:,:,eln) = obj.el.dX_deta(xiip2,etap2,elx,ely);
%                     obj.eval.jac.pf.dy_dxii(:,:,eln) = obj.el.dY_dxii(xiip2,etap2,elx,ely);
%                     obj.eval.jac.pf.dy_deta(:,:,eln) = obj.el.dY_deta(xiip2,etap2,elx,ely);
%                 end
%             end
%             
%             obj.eval.jac.pf.dx_dxii = reshape(obj.eval.jac.pf.dx_dxii,[pf+1,pf+1, obj.Kx*obj.Ky]);
%             obj.eval.jac.pf.dx_deta = reshape(obj.eval.jac.pf.dx_deta,[pf+1,pf+1, obj.Kx*obj.Ky]);
%             obj.eval.jac.pf.dy_dxii = reshape(obj.eval.jac.pf.dy_dxii,[pf+1,pf+1, obj.Kx*obj.Ky]);
%             obj.eval.jac.pf.dy_deta = reshape(obj.eval.jac.pf.dy_deta,[pf+1,pf+1, obj.Kx*obj.Ky]);

        end
        
    end
end

