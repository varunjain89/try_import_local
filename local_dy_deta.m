function [dy_deta] = local_dy_deta(x_bound, y_bound, c, eta1, eta2, quad_x, xi_i)
%LOCAL_DY_DETA Summary of this function goes here
%   Detailed explanation goes here

x1 = x_bound(1);
x2 = x_bound(2);
y1 = y_bound(1);
y2 = y_bound(2);

xi  = xi_i;
eta = 0.5*(eta1 + eta2) + 0.5*(eta2 - eta1).* quad_x;

deta = 0.5*(eta2 - eta1);

dy_deta = (y2 - y1)/2 .* (deta + c .* sin(pi .* xi) .* cos(pi .* eta) .* pi .* deta);

end

