classdef mmesh4 < handle
    % Created on: 8th April, 2019
    % this is a class for any general mesh
    
    properties
        domain
        ttl_nr_domain
        ttl_nr_el
        el_bounds
        el
        
        K
        p
    end
    
    methods
        %% initialize the mesh object with domain mapping and jacobians
        function obj = mmesh4(region)
            obj.domain = region;
            obj.ttl_nr_domain = size(obj.domain,2);
        end
        
        %% check domain mapping
        function obj = check_domain_mapping(obj)
            s = linspace(-1,1,31);
            [xii, eta] = meshgrid(s);
                        
            figure
            hold on
            
            for i = 1:obj.ttl_nr_domain
                
                [x, y] = obj.domain(i).mapping(xii, eta);
                plot(x, y, '+')
                
                % bottom boundary
                [xB, yB] = obj.domain(i).mapping(s, -1.*ones(size(s)));
                plot(xB, yB, 'linewidth',2)
                
                % left boundary
                [xL, yL] = obj.domain(i).mapping(1.*ones(size(s)), s);
                plot(xL, yL, 'linewidth',2)
                
                % top boundary
                [xT, yT] = obj.domain(i).mapping(s, 1.*ones(size(s)));
                plot(xT, yT, 'linewidth',2)
                
                % right boundary
                [xR, yR] = obj.domain(i).mapping(-1.*ones(size(s)), s);
                plot(xR, yR, 'linewidth',2)
            end
        end
        
        %% discretize the mesh object in elements and polynomial degree
        function obj = discretize(obj,Kx,Ky)
            obj.ttl_nr_el = obj.ttl_nr_domain * Kx * Ky;
            
            obj.el_bounds.x = linspace(-1,1,Kx+1);
            obj.el_bounds.y = linspace(-1,1,Ky+1);
            
            temp = [0 logspace(-1,0,Ky)];
            
            edge_map   = @(s) -1*(1-s) + s;
            
            temp2 = edge_map(temp);
            temp2 = linspace(-1,1,Kx+1);
            
            temp3 = -flip(temp2);
            
            obj.domain(1).el_bounds.x = temp3;
            obj.domain(1).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(2).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(2).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(3).el_bounds.x = temp3;
            obj.domain(3).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(4).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(4).el_bounds.y = temp2;
            
            obj.domain(5).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(5).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(6).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(6).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(7).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(7).el_bounds.y = temp3;
            
            obj.domain(8).el_bounds.x = temp2;
            obj.domain(8).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(9).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(9).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(10).el_bounds.x = temp2;
            obj.domain(10).el_bounds.y = linspace(-1,1,Ky+1);
            
            for i = 1:size(obj.domain,2)
                obj.domain(i).el.mapping = @(xi,eta,elx,ely) mesh.element.mapping(obj.domain(i).mapping, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_dxii = @(xi,eta,elx,ely) mesh.element.dx_dxii(obj.domain(i).dX_dxii, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_deta = @(xi,eta,elx,ely) mesh.element.dx_deta(obj.domain(i).dX_deta, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_dxii = @(xi,eta,elx,ely) mesh.element.dy_dxii(obj.domain(i).dY_dxii, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_deta = @(xi,eta,elx,ely) mesh.element.dy_deta(obj.domain(i).dY_deta, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
            end
            
            for reg = 1:size(obj.domain,2)
                for elx = 1:Kx
                    for ely = 1:Ky
                        eln = (reg-1) * Kx * Ky + (elx -1) * Ky + ely;
                        obj.el(eln).mapping = @(xii,eta,eln) obj.domain(reg).el.mapping(xii,eta,elx,ely);
                        obj.el(eln).dX_dxii = @(xii,eta,eln) obj.domain(reg).el.dX_dxii(xii,eta,elx,ely);
                        obj.el(eln).dX_deta = @(xii,eta,eln) obj.domain(reg).el.dX_deta(xii,eta,elx,ely);
                        obj.el(eln).dY_dxii = @(xii,eta,eln) obj.domain(reg).el.dY_dxii(xii,eta,elx,ely);
                        obj.el(eln).dY_deta = @(xii,eta,eln) obj.domain(reg).el.dY_deta(xii,eta,elx,ely);
                    end
                end
            end
            
        end
        
        %% check element mapping
        function obj = check_element_mapping(obj)
            cc = 0.6;
            s = linspace(-1,1,31);
            [s2,~] = GLLnodes(6);
%             [xii, eta] = meshgrid(s2);
            
            [xii1, eta1] = meshgrid(s,s2);
            [xii2, eta2] = meshgrid(s2,s);
            
            figure
            hold on
            
            for i = 1:obj.ttl_nr_el
                
%                 [x, y] = obj.el(i).mapping(xii, eta);
                [x1, y1] = obj.el(i).mapping(xii1, eta1);
                [x2, y2] = obj.el(i).mapping(xii2, eta2);
                
                plot(x2(:,2:end-1), y2(:,2:end-1),'Color', [cc cc cc], 'linewidth', 2)
                plot(x1(2:end-1,:)',y1(2:end-1,:)','Color', [cc cc cc], 'linewidth', 2)
                
                % bottom boundary
                [xB, yB] = obj.el(i).mapping(s, -1.*ones(size(s)));
                plot(xB, yB,'k','linewidth',2)
                
                % left boundary
                [xL, yL] = obj.el(i).mapping(1.*ones(size(s)), s);
                plot(xL, yL,'k','linewidth',2)
                
                % top boundary
                [xT, yT] = obj.el(i).mapping(s, 1.*ones(size(s)));
                plot(xT, yT,'k','linewidth',2)
                
                % right boundary
                [xR, yR] = obj.el(i).mapping(-1.*ones(size(s)), s);
                plot(xR, yR,'k','linewidth',2)
            end
            set(gca,'TickLabelInterpreter','latex','FontSize',20,'XColor','k','YColor','k');
        end
        
        %% evaluate jacobian for all elements
        function jac = evaluate_jacobian_all(obj, xii, eta)
%             jac(obj.ttl_nr_el).dX_dxii = obj.el(obj.ttl_nr_el).dX_dxii(xii,eta);
            for i = 1:obj.ttl_nr_el
                jac.dx_dxii(:,:,i) = obj.el(i).dX_dxii(xii,eta);
                jac.dx_deta(:,:,i) = obj.el(i).dX_deta(xii,eta);
                jac.dy_dxii(:,:,i) = obj.el(i).dY_dxii(xii,eta);
                jac.dy_deta(:,:,i) = obj.el(i).dY_deta(xii,eta);
            end
        end
        
    end
end

