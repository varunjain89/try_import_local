function obj = discretize2(obj,Kx,Ky)
            obj.ttl_nr_el = obj.ttl_nr_domain * Kx * Ky;
            
            obj.el_bounds.x = linspace(-1,1,Kx+1);
            obj.el_bounds.y = linspace(-1,1,Ky+1);
            
            temp = [0 logspace(-1,0,Ky)];
            
            edge_map   = @(s) -1*(1-s) + s;
            
            temp2 = edge_map(temp);
            temp2 = linspace(-1,1,Kx+1);
            
            temp3 = -flip(temp2);
            
            obj.domain(1).el_bounds.x = temp3;
            obj.domain(1).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(2).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(2).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(3).el_bounds.x = temp3;
            obj.domain(3).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(4).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(4).el_bounds.y = temp2;
            
            obj.domain(5).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(5).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(6).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(6).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(7).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(7).el_bounds.y = temp3;
            
            obj.domain(8).el_bounds.x = temp2;
            obj.domain(8).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(9).el_bounds.x = linspace(-1,1,Kx+1);
            obj.domain(9).el_bounds.y = linspace(-1,1,Ky+1);
            
            obj.domain(10).el_bounds.x = temp2;
            obj.domain(10).el_bounds.y = linspace(-1,1,Ky+1);
            
            for i = 1:size(obj.domain,2)
                obj.domain(i).el.mapping = @(xi,eta,elx,ely) mesh.element.mapping(obj.domain(i).mapping, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_dxii = @(xi,eta,elx,ely) mesh.element.dx_dxii(obj.domain(i).dX_dxii, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dX_deta = @(xi,eta,elx,ely) mesh.element.dx_deta(obj.domain(i).dX_deta, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_dxii = @(xi,eta,elx,ely) mesh.element.dy_dxii(obj.domain(i).dY_dxii, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
                obj.domain(i).el.dY_deta = @(xi,eta,elx,ely) mesh.element.dy_deta(obj.domain(i).dY_deta, obj.domain(i).el_bounds.x, obj.domain(i).el_bounds.y, elx, ely, xi, eta);
            end
            
            for reg = 1:size(obj.domain,2)
                for elx = 1:Kx
                    for ely = 1:Ky
                        eln = (reg-1) * Kx * Ky + (elx -1) * Ky + ely;
                        obj.el(eln).mapping = @(xii,eta,eln) obj.domain(reg).el.mapping(xii,eta,elx,ely);
                        obj.el(eln).dX_dxii = @(xii,eta,eln) obj.domain(reg).el.dX_dxii(xii,eta,elx,ely);
                        obj.el(eln).dX_deta = @(xii,eta,eln) obj.domain(reg).el.dX_deta(xii,eta,elx,ely);
                        obj.el(eln).dY_dxii = @(xii,eta,eln) obj.domain(reg).el.dY_dxii(xii,eta,elx,ely);
                        obj.el(eln).dY_deta = @(xii,eta,eln) obj.domain(reg).el.dY_deta(xii,eta,elx,ely);
                    end
                end
            end
            
        end

