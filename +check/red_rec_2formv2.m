function [pf] = red_rec_2formv2(f_an,mesh00,p)

% Created on : 22 October, 2019

% ------- reconstruction -------

loc_dof_p = p^2;

[basis_dis.xp, basis_dis.wp] = GLLnodes(p);
[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(basis_dis.xp,p,1);

[xii_dis, eta_dis] = meshgrid(basis_dis.xp);
jac_dis = mesh00.evaluate_jacobian_all(xii_dis, eta_dis);
metric_dis = flow.poisson.eval_metric(jac_dis);

dof_f = zeros(loc_dof_p, mesh00.ttl_nr_el);

for eln = 1:mesh00.ttl_nr_el
    M22 = mass_matrix.M2_6(basis_dis, metric_dis.ggg(:,:,eln));
    dof_f(:,eln) = reduction.reduction_2form_v2(f_an, mesh00.el(eln).mapping, basis_dis, M22);
end

% ------- reconstruction -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[~, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[~, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh00.evaluate_jacobian_all(rec_xii, rec_eta);
metric_rec = flow.poisson.eval_metric(jac_rec);

for eln = 1:mesh00.ttl_nr_el

    rec_f(:,:,eln) = reconstruction.reconstruct2form_v6(dof_f(:,eln), basis_rec, metric_rec.ggg(:,:,eln));

    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh00.el(eln).mapping(rec_xii, rec_eta);

    ff_ex(:,:,eln) = f_an(rec_x(:,:,eln), rec_y(:,:,eln));

end

figure
hold on

for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),ff_ex(:,:,eln),'linecolor','none')
end

colorbar
title('check; 2-form; exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on

for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln),rec_y(:,:,eln),rec_f(:,:,eln))
end
colorbar
title('check; 2-form; reconstructed solution')
set(gcf, 'Position',  [700, 500, 550, 400])

figure
hold on

for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), rec_f(:,:,eln) - ff_ex(:,:,eln),'linecolor','none')
end
colorbar
title('check; 2-form; difference')
set(gcf, 'Position',  [1300, 500, 550, 400])

end

