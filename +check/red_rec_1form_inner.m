function [pf] = red_rec_1form_inner(ux,uy,mesh00,p)

% ------- reduction -------

[xp, wp] = GLLnodes(p);
[xii, eta] = meshgrid(xp);

jac1_dis = mesh00.evaluate_jacobian_all(xii, eta);
metric_dis = flow.poisson.eval_metric(jac1_dis);

[basis_dis.hp, basis_dis.ep] = MimeticpolyVal(xp,p,1);
basis_dis.xp = xp;
basis_dis.wp = wp;

for eln = 1:mesh00.ttl_nr_el
    M11 = mass_matrix.M1_11(p, metric_dis.g11(:,:,eln), -metric_dis.g12(:,:,eln), metric_dis.g22(:,:,eln), basis_dis);
    dof_u(:,eln) = reduction.reduction_1form_v4(ux, uy, mesh00.el(eln).mapping, basis_dis, M11, jac1_dis,eln);
end

% ------- reconstruction -------

pf = 30;

sx = linspace(-1,1,pf+1);
sy = linspace(-1,1,pf+1);

[basis_rec.hfx, basis_rec.efx] = MimeticpolyVal(sx,p,1);
[basis_rec.hfy, basis_rec.efy] = MimeticpolyVal(sy,p,1);

[rec_xii, rec_eta] = meshgrid(sx, sy);
jac_rec = mesh00.evaluate_jacobian_all(rec_xii', rec_eta');
metric_rec = flow.poisson.eval_metric(jac_rec);

for eln = 1:mesh00.ttl_nr_el
    temp2x(:,:,eln) = reconstruction.reconstruct1xform_v7(dof_u(1:p*(p+1),eln), dof_u(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln);
    temp2y(:,:,eln) = reconstruction.reconstruct1yform_v7(dof_u(1:p*(p+1),eln), dof_u(p*(p+1)+1:2*p*(p+1),eln), p, pf, jac_rec, metric_rec,eln);
    [rec_x(:,:,eln),rec_y(:,:,eln)] = mesh00.el(eln).mapping(rec_xii, rec_eta);
end

ux_ex = ux(rec_x,rec_y);
uy_ex = uy(rec_x,rec_y);

%

figure
hold on
for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln))
end
colorbar
title('check; 1-form; ux, exact solution')
set(gcf, 'Position',  [0100, 500, 550, 400])

figure
hold on
for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2x(:,:,eln)')
end
colorbar
title('check; 1-form; ux, reconstructed solution')
set(gcf, 'Position',  [0700, 500, 550, 400])

figure
hold on
for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), ux_ex(:,:,eln) - temp2x(:,:,eln)','linecolor','none')
end
colorbar
title('check; 1-form; ux, difference')
set(gcf, 'Position',  [1300, 500, 550, 400])

%

figure
hold on
for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln))
end
colorbar
title('check; 1-form; uy, exact solution')
set(gcf, 'Position',  [0100, 050, 550, 400])

figure
hold on
for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), temp2y(:,:,eln)')
end
colorbar
title('check; 1-form; uy, reconstructed solution')
set(gcf, 'Position',  [0700, 050, 550, 400])

figure
hold on
for eln = 1:mesh00.ttl_nr_el
    contourf(rec_x(:,:,eln), rec_y(:,:,eln), uy_ex(:,:,eln) - temp2y(:,:,eln)','linecolor','none')
end
colorbar
title('check; 1-form; uy, difference')
set(gcf, 'Position',  [1300, 050, 550, 400])

end

