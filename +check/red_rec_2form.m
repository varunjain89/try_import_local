function [pf] = red_rec_2form(f_an,p_foil)

% Created on : 16 April, 2019

%% define 2-form

volForm = twoForm_v2(p_foil);

%% reduction of source term

F_3D = volForm.reduction(f_an);

%% reconstruction of source term

pf = 20;
p_foil.eval_pf_jacobian(pf);
p_foil.eval_pf_metric();

%% plot calculated source term

[xf_3D, yf_3D, massf_h] = volForm.reconstruction(F_3D);

figure
hold on

for elx = 1:p_foil.Kx
    for ely = 1:p_foil.Ky
        eln = (elx -1) * p_foil.Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), massf_h(:,:,eln)')
    end
end

colorbar
title('calculated solution')

%% plot exact source term

f_ex = f_an(xf_3D, yf_3D);

figure
hold on

for elx = 1:p_foil.Kx
    for ely = 1:p_foil.Ky
        eln = (elx -1) * p_foil.Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln))
    end
end

title('exact solution')
colorbar

%% plot velocity vector 

figure
hold on

for elx = 1:p_foil.Kx
    for ely = 1:p_foil.Ky
        eln = (elx -1) * p_foil.Ky + ely;
        contourf(xf_3D(:,:,eln), yf_3D(:,:,eln), f_ex(:,:,eln)-massf_h(:,:,eln)')
    end
end

title('difference')
colorbar

end

