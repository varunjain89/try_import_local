function [ Ex ] = linear_momentum_x(p, LN_tau_xx, LN_tau_yx)
%LINEAR_MOMENTUM_X Summary of this function goes here
%   Detailed explanation goes here

% Created On : 8 May, 2018

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

Ex =  zeros(p^2, local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + local_nr_tau_yy);

for i = 1:p
    for j = 1:p
        volij = (i-1)*p+j;
        
        bottom = local_nr_tau_xx + LN_tau_yx(i,j);
        top    = local_nr_tau_xx + LN_tau_yx(i,j+1);
        left   = LN_tau_xx(i,j);
        right  = LN_tau_xx(i+1,j);
        
%         Ex(volij, bottom) = +1;
%         Ex(volij, top)    = -1;
        Ex(volij, bottom) = -1;
        Ex(volij, top)    = +1;
        Ex(volij, left)   = -1;
        Ex(volij, right)  = +1;
    end
end


end

