function [ Ey ] = linear_momentum_y(p, LN_tau_yy, LN_tau_xy)
%LINEAR_MOMENTUM_Y Summary of this function goes here
%   Detailed explanation goes here

% Created on :  May, 2018

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

Ey =  zeros(p^2, local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + local_nr_tau_yy);

for i = 1:p
    for j = 1:p
        volij = (i-1)*p+j;
        
        bottom = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,j);
        top    = local_nr_tau_xx + local_nr_tau_yx + local_nr_tau_xy + LN_tau_yy(i,j+1);
        left   = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(i,j);
        right  = local_nr_tau_xx + local_nr_tau_yx + LN_tau_xy(i+1,j);
        
        Ey(volij, bottom) = -1;
        Ey(volij, top)    = +1;
        Ey(volij, left)   = -1;
        Ey(volij, right)  = +1;
        
%         Ey(volij, bottom) = +1;
%         Ey(volij, top)    = -1;
%         Ey(volij, left)   = +1;
%         Ey(volij, right)  = -1;
        
    end
end

end

