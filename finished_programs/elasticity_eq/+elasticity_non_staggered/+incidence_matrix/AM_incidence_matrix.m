function [ E21 ] = AM_incidence_matrix( p )
%AM_INCIDENCE_MATRIX Summary of this function goes here
%   Detailed explanation goes here

volumes = p^2;
edges = 2*p*(p+1);

E21 = zeros(volumes, edges);

for i=1:p
    for j=1:p
        eleid = (i-1)*p + j;
        
        edge_bot = (j-1)*p + i;
        edge_top = j*p + i;
        edge_lef = edges/2 + (i-1) * p + j;
        edge_rig = edges/2 + i * p + j;

        %         TQ_bot - TQ_top + TQ_left - TQ_right;
%         E21(eleid, edge_bot) = +1;
%         E21(eleid, edge_top) = -1;
%         E21(eleid, edge_lef) = +1;
%         E21(eleid, edge_rig) = -1;
        
        E21(eleid, edge_bot) = -1;
        E21(eleid, edge_top) = +1;
        E21(eleid, edge_lef) = -1;
        E21(eleid, edge_rig) = +1;
        
    end
end

end

