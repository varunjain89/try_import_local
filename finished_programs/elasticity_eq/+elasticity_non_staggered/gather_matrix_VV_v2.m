function [ GM ] = gather_matrix_VV_v2(K, p)
%GATHER_MATRIX_VV Summary of this function goes here
%   Detailed explanation goes here

% Created on 9th May, 2018

% internal top edges

for elx = 1:K
    for ely = 1:K-1
        el = (elx-1)*K + ely;
        for i = 1:p
            for j = p+1
                local_top = p+i;
                GM(el, local_top) = (ely-1) * p * K + (elx-1)*p + i;
            end
        end
    end
end

% internal bottom edges

for elx = 1:K
    for ely = 2:K
        el = (elx-1)*K + ely;
        for i= 1:p
            for j = 1
                local_bot = i;
                GM(el, local_bot) = GM(el-1, p+i);
            end
        end
    end
end

% internal right edges

for elx = 1:K-1
    for ely = 1:K
        el=(elx-1)*K + ely;
        for i = p+1
            for j = 1:p
                local_right = 3*p + j;
                GM(el, local_right) = (K-1) * p * K + (elx-1) * p * K + (ely-1)*p +j;
            end
        end
    end
end

% internal left edges

for elx = 2:K
    for ely = 1:K
        el = (elx-1)*K + ely;
        for i = 1
            for j = 1:p
                local_left = 2*p + j;
                GM(el, local_left) = GM(el-K, 3*p +j);
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% add missing virtual volumes
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% internal top edges

for elx = 1:K
    for ely = 1:K-1
        el = (elx-1)*K + ely;
        for i = 1:p
            for j = p+1
                local_top = 4*p + p+i;
                GM(el, local_top) = 2*K*(K-1)*p + (ely-1) * p * K + (elx-1)*p + i;
            end
        end
    end
end

% internal bottom edges

for elx = 1:K
    for ely = 2:K
        el = (elx-1)*K + ely;
        for i= 1:p
            for j = 1
                local_bot = 4*p + i;
                GM(el, local_bot) = 2*K*(K-1)*p + GM(el-1, p+i);
            end
        end
    end
end

% internal right edges

for elx = 1:K-1
    for ely = 1:K
        el=(elx-1)*K + ely;
        for i = p+1
            for j = 1:p
                local_right = 4*p + 3*p + j;
                GM(el, local_right) = 2*K*(K-1)*p + (K-1) * p * K + (elx-1) * p * K + (ely-1)*p +j;
            end
        end
    end
end

% internal left edges

for elx = 2:K
    for ely = 1:K
        el = (elx-1)*K + ely;
        for i = 1
            for j = 1:p
                local_left = 4*p + 2*p + j;
                GM(el, local_left) = 2*K*(K-1)*p + GM(el-K, 3*p +j);
            end
        end
    end
end



% external bottom boundary

for elx = 1:K
    for ely = 1
        el = (elx-1)*K + ely;
        for i=1:p
            for j=1
                local_bot = i;
                GM(el, local_bot) = 4*K*(K-1)*p + (elx-1)*p + i;
            end
        end
    end
end

% external top boundary

for elx = 1:K
    for ely = K
        el = (elx-1)*K + ely;
        for i = 1:p
            local_top = p + i;
            GM(el, local_top) = 4*K*(K-1)*p + K*p + (elx-1)*p + i;
        end
    end
end

% external left boundary

for elx = 1
    for ely = 1:K
        el = (elx-1)*K + ely;
        for j = 1:p
            local_left = 2*p + j;
            GM(el, local_left) = 4*K*(K-1)*p + 2*K*p + (ely-1)*p + j;
        end
    end
end

% external right boundary

for elx = K
    for ely = 1:K
        el = (elx-1)*K + ely;
        for j = 1:p
            local_right = 3*p + j;
            GM(el, local_right) = 4*K*(K-1)*p + 3*K*p + (ely-1)*p + j;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% add missing virtual volumes
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% external bottom boundary

for elx = 1:K
    for ely = 1
        el = (elx-1)*K + ely;
        for i=1:p
            for j=1
                local_bot = 4*p + i;
                GM(el, local_bot) = 4*K*p + 4*K*(K-1)*p + (elx-1)*p + i;
            end
        end
    end
end

% external top boundary

for elx = 1:K
    for ely = K
        el = (elx-1)*K + ely;
        for i = 1:p
            local_top = 4*p + p + i;
            GM(el, local_top) = 4*K*p + 4*K*(K-1)*p + K*p + (elx-1)*p + i;
        end
    end
end

% external left boundary

for elx = 1
    for ely = 1:K
        el = (elx-1)*K + ely;
        for j = 1:p
            local_left = 4*p + 2*p + j;
            GM(el, local_left) = 4*K*p + 4*K*(K-1)*p + 2*K*p + (ely-1)*p + j;
        end
    end
end

% external right boundary

for elx = K
    for ely = 1:K
        el = (elx-1)*K + ely;
        for j = 1:p
            local_right = 4*p + 3*p + j;
            GM(el, local_right) = 4*K*p + 4*K*(K-1)*p + 3*K*p + (ely-1)*p + j;
        end
    end
end

end

