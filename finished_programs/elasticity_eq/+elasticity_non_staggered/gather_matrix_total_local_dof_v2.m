function [ GM ] = gather_matrix_total_local_dof_v2(K, p)
%GATHER_MATRIX_TOTAL_LOCAL_DOF Summary of this function goes here
%   Detailed explanation goes here

local_nr_tau_xx = p*(p+1);
local_nr_tau_yx = p*(p+1);
local_nr_tau_xy = p*(p+1);
local_nr_tau_yy = p*(p+1);

total_local_dof = local_nr_tau_xx + local_nr_tau_xy + local_nr_tau_yx + local_nr_tau_yy + 3*p^2;

temp = 1:total_local_dof;

for el = 1:K^2
    GM(el,:) = temp+(el-1)*total_local_dof;
end

end

