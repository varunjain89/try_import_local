function [LN_tau_xx, LN_tau_yx, LN_tau_xy, LN_tau_yy] = local_numbering( p )
%LOCAL_NUMBERING Summary of this function goes here
%   Detailed explanation goes here

%% local numbering

% local numbering t_xx

LN_tau_xx = zeros(p+1,p);

for i = 1:p+1
    for j = 1:p
        LN_tau_xx(i,j) = (i-1)*p + j;
    end
end

% local numbering t_yx

LN_tau_yx = zeros(p, p+1);

for i = 1:p
    for j = 1:p+1
        LN_tau_yx(i,j) = (j-1)*p + i;
    end
end

% local numbering t_xy

LN_tau_xy = zeros(p+1, p);

for i = 1:p+1
    for j = 1:p
        LN_tau_xy(i,j) = (i-1)*p + j;
    end
end

% local numbering t_yy

LN_tau_yy = zeros(p, p+1);

for i = 1:p
    for j = 1:p+1
        LN_tau_yy(i,j) = (j-1)*p + i;
    end
end

end

