function [ MS ] = full_mass_matrix_g(p, g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44)
%FULL_MASS_MATRIX Summary of this function goes here
%   Detailed explanation goes here

% Created on: 9th May, 2018

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

M11 = zeros(p*(p+1), p*(p+1));

for i = 1:p+1
    for j = 1:p
        edgeij = (i-1)*p + j;
        for k = 1:p+1
            for l = 1:p
                edgekl = (k-1)*p + l;
                for x = 1:p+1
                    for y = 1:p+1
                        M11(edgeij, edgekl) = M11(edgeij, edgekl) + g11(x,y)*hp(i,x)*ep(j,y)*hp(k,x)*ep(l,y)*wp(x)*wp(y);
                    end
                end
            end
        end
    end
end

M14 = zeros(p*(p+1), p*(p+1));

for i = 1:p+1
    for j = 1:p
        edgeij = (i-1)*p + j;
        for k = 1:p
            for l = 1:p+1
                edgekl = (l-1)*p + k;
                for x = 1:p+1
                    for y = 1:p+1
                        M14(edgeij, edgekl) = M14(edgeij, edgekl) + g14(x,y)*hp(i,x)*ep(j,y)*ep(k,x)*hp(l,y)*wp(x)*wp(y);
                    end
                end
            end
        end
    end
end

M22 = zeros(p*(p+1),p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p+i;
        for k = 1:p
            for l = 1:p+1
                edgekl = (l-1)*p+k;
                for x=1:p+1
                    for y = 1:p+1
                        M22(edgeij, edgekl) = M22(edgeij, edgekl) + g22(x,y)*ep(i,x)*hp(j,y)*ep(k,x)*hp(l,y)*wp(x)*wp(y);
                    end
                end
            end
        end
    end
end

M33 = zeros(p*(p+1),p*(p+1));

for i = 1:p+1
    for j = 1:p
        edgeij = (i-1)*p + j;
        for k = 1:p+1
            for l = 1:p
                edgekl = (k-1)*p + l;
                for x = 1:p+1
                    for y = 1:p+1
                        M33(edgeij, edgekl) = M33(edgeij, edgekl) + g33(x,y)*hp(i,x)*ep(j,y)*hp(k,x)*ep(l,y)*wp(x)*wp(y);
                    end
                end
            end
        end
    end
end

M41 = zeros(p*(p+1),p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        for k = 1:p+1
            for l = 1:p
                edgekl = (k-1)*p + l;
                for x = 1:p+1
                    for y = 1:p+1
                        M41(edgeij, edgekl) = M41(edgeij, edgekl) + g41(x,y)*ep(i,x)*hp(j,y)*hp(k,x)*ep(l,y)*wp(x)*wp(y);
                    end
                end
            end
        end
    end
end

M44 = zeros(p*(p+1),p*(p+1));

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        for k = 1:p
            for l = 1:p+1
                edgekl = (l-1)*p + k;
                for x = 1:p+1
                    for y = 1:p+1
                        M44(edgeij, edgekl) = M44(edgeij, edgekl) + g44(x,y)*ep(i,x)*hp(j,y)*ep(k,x)*hp(l,y)*wp(x)*wp(y);
                    end
                end
            end
        end
    end
end

ZM = zeros(p*(p+1),p*(p+1));

% M14 = -nu * M14;
% M22 = (1+nu) * M22;
% M33 = (1+nu) * M33;
% M41 = -nu * M41;

MS = [M11 ZM ZM M14; ZM M22 ZM ZM; ZM ZM M33 ZM; M41 ZM ZM M44];

end

