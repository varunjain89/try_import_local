%% april 19, 2018

% clc
close all
clear variables

addpath('../..')
addpath('../../../mimeticFEM2.0/MSEM')
addpath('../../../mimeticFEM2.0/export_fig')

for p = 1:15
    [E_err(p), F_err(p)] = func_curl_curl(p);
    p
end

lw = 1.5;

%% 

figure
semilogy(1:p, F_err, 'k','LineWidth',lw)
hold on
semilogy(1:p, E_err, 'r','LineWidth',lw)

hlegend = legend('$$ \left\| \mathsf{F}_{ex} - \mathsf{F}^{h}\right\|_{H(\mathsf{\textbf{curl}})} $$','$$ \left\| \textbf{E}_{ex} - \textbf{E}^{h} \right\|_{H(\mathsf{curl})} $$','location','southwest');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('Error in H(curl; $$\Omega$$)', 'Interpreter','latex','FontSize',14);
% 
set(gca,'TickLabelInterpreter','latex','FontSize',20);

ylim([1e-13 100])

% export_fig('H_curl_err_July2018.pdf','-pdf','-r864','-painters','-transparent');