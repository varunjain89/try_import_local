function [err_E_curl_norm, err_F_curl_norm ] = func_curl_curl( p )
%FUNC_CURL_CURL Summary of this function goes here
%   Detailed explanation goes here

% clc
% close all
% clear variables

% addpath('../../mimeticFEM2.0')
% addpath('../../mimeticFEM2.0/MSEM')
% addpath('../../mimeticFEM2.0/export_fig')

%% analytical solution 

% F_an = @(x,y) -sin(x/sqrt(2)).*sin(y/sqrt(2));
F_an  = @(x,y) exp(x) + exp(y);
Ey_an = @(x,y) exp(y);
Ex_an = @(x,y) -exp(x);

%% domain

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

%% number of elements 

K = 1;
% p = 10;

element_bounds_x = linspace(-1,1,K+1);
element_bounds_y = linspace(-1,1,K+1);

ttl_nr_el = K^2;

local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% mesh mapping and derivatives

domain_mapping = @(xi,eta) mesh.crazy_mesh.mapping(xbound, ybound, c, xi, eta);
domain_dX_dxii = @(xi,eta) mesh.crazy_mesh.dx_dxii(xbound, c, xi, eta);
domain_dX_deta = @(xi,eta) mesh.crazy_mesh.dx_deta(xbound, c, xi, eta);
domain_dY_dxii = @(xi,eta) mesh.crazy_mesh.dy_dxii(ybound, c, xi, eta);
domain_dY_deta = @(xi,eta) mesh.crazy_mesh.dy_deta(ybound, c, xi, eta);

el_mapping = @(xi,eta,elx,ely) mesh.crazy_mesh.mapping_element(domain_mapping, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_dxii_element(domain_dX_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dX_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dx_deta_element(domain_dX_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_dxii = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_dxii_element(domain_dY_dxii, element_bounds_x, element_bounds_y, elx, ely, xi, eta);
el_dY_deta = @(xi,eta,elx,ely) mesh.crazy_mesh.dy_deta_element(domain_dY_deta, element_bounds_x, element_bounds_y, elx, ely, xi, eta);

%% calculate metric terms

[xp, wp] = GLLnodes(p);
[xip, etap] = meshgrid(xp);

eval_p_dx_dxii = zeros(p+1,p+1,K^2);
eval_p_dx_deta = zeros(p+1,p+1,K^2);
eval_p_dy_dxii = zeros(p+1,p+1,K^2);
eval_p_dy_deta = zeros(p+1,p+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_p_dx_dxii(:,:,el) = el_dX_dxii(xip,etap,elx,ely);
        eval_p_dx_deta(:,:,el) = el_dX_deta(xip,etap,elx,ely);
        eval_p_dy_dxii(:,:,el) = el_dY_dxii(xip,etap,elx,ely);
        eval_p_dy_deta(:,:,el) = el_dY_deta(xip,etap,elx,ely);
    end
end

eval_p_ggg = metric.ggg(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta);
eval_p_g11 = metric.g11(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g12 = metric.g12(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);
eval_p_g22 = metric.g22(eval_p_dx_dxii, eval_p_dx_deta, eval_p_dy_dxii, eval_p_dy_deta, eval_p_ggg);

%% calculating mass matrices

M11 = zeros(local_nr_ed, local_nr_ed, ttl_nr_el);

for el = 1:ttl_nr_el
    M11(:,:,el) = mass_matrix.M1_3(p, eval_p_g11(:,:,el), eval_p_g12(:,:,el), eval_p_g22(:,:,el));
end

M0 = mass_matrix.massMatrix_0form_v2(p, eval_p_ggg);

E10 = incidence_matrix.incidenceMatrixE10_outer(p);

%% boundary conditions

E_bot   = -exp(-1);
E_top   = -exp(1);
E_left  = exp(-1);
E_right = exp(1);

E_bot_1   = E_bot*ones(size(xp));            % E_bot at lobatto pts
E_top_1   = E_top*ones(size(xp));            % E_top at lobatto pts
E_left_1  = E_left*ones(size(xp));           % E_left at lobatto pts
E_right_1 = E_right*ones(size(xp));          % E_right at lobatto pts

E_bc = bound_cond.curl_curl.bound_cond(p, E_bot_1, E_top_1, E_left_1, E_right_1);

%% solve curl curl F + F = 0

[F, norm_F] = flow.curl_curl.neumann_problem(M0, M11, E10, E_bc);

%% solve curl curl E + E =0

[E, norm_E] = flow.curl_curl.dirichlet_problem(M0, M11, E10, E_bc);

E_or = E;
E = M11\E;

%% check validity of condition curl F = E

check  = max(max(E - E10 * F));
check2 = E - E10 * F;

%% comparing norm 

norm_F = sqrt(+norm_F);
norm_E = sqrt(-norm_E);

%% post processing 

pf = 35;

gf = zeros(pf+1,pf+1,K^2);

[xf, wf] = GLLnodes(pf);
[xif, etaf] = meshgrid(xf);
[wfx, wfy] = meshgrid(wf);

eval_pf_dx_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dx_deta = zeros(pf+1,pf+1,K^2);
eval_pf_dy_dxii = zeros(pf+1,pf+1,K^2);
eval_pf_dy_deta = zeros(pf+1,pf+1,K^2);

for elx = 1:K
    for ely = 1:K
        el = (elx -1)*K +ely;
        eval_pf_dx_dxii(:,:,el) = el_dX_dxii(xif,etaf,elx,ely);
        eval_pf_dx_deta(:,:,el) = el_dX_deta(xif,etaf,elx,ely);
        eval_pf_dy_dxii(:,:,el) = el_dY_dxii(xif,etaf,elx,ely);
        eval_pf_dy_deta(:,:,el) = el_dY_deta(xif,etaf,elx,ely);
    end
end

%% calculating error in F

recF = reconstruction.reconstruct0form( F', p, pf );

% exact
F_an_h = F_an(xif, etaf);

err_F = (recF - F_an_h).^2 .* wfx .*wfy;
err_F = sum(sum(err_F));
% err_F = sqrt(err_F)

%% calculating error in E 

Ex = +E(1:local_nr_ed/2);
Ey = -E(local_nr_ed/2+1:end);

recEx = reconstruction.reconstruct1xform_2(Ey, Ex, p, pf, eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
recEy = -reconstruction.reconstruct1yform_2(Ey, Ex, p, pf, eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);

% exact
Ex_an_h = Ex_an(xif, etaf);
Ey_an_h = Ey_an(xif, etaf);

err_Ex = (recEx - Ex_an_h').^2 .* wfx .* wfy;
err_Ex = sum(sum(err_Ex));

err_Ey = (recEy - Ey_an_h').^2 .* wfx .* wfy;
err_Ey = sum(sum(err_Ey));

err_E = sqrt(err_Ex + err_Ey);

%% post process condition 

checkx = check2(1:local_nr_ed/2);
checky = check2(local_nr_ed/2+1:end);

checkx_h = reconstruction.reconstruct1xform_2(checky, checkx, p, pf, eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
checky_h = reconstruction.reconstruct1yform_2(checky, checkx, p, pf, eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);

err_cond_x = (checkx_h).^2 .*wfx .*wfy;
err_cond_x = sum(sum(err_cond_x));

err_cond_y = (checky_h).^2 .*wfx .*wfy;
err_cond_y = sum(sum(err_cond_y));

err_cond = sqrt(err_cond_x + err_cond_y);

%% plot scalar field F

% figure
% 
% subplot(1,3,1)   
% contourf(xif, etaf, F_an_h')
% title('analytical')
% colorbar
% axis equal 
% 
% subplot(1,3,2)   
% contourf(xif, etaf, recF')
% title('computed')
% colorbar
% axis equal
% 
% subplot(1,3,3)   
% contourf(xif, etaf, F_an_h'-recF)
% title('error')
% colorbar
% axis equal
% 
% x0=1000;
% y0=1000;
% width=2000;
% height=600;
% set(gcf,'units','points','position',[x0,y0,width,height])

%% plot vector field Ex

% figure
% 
% subplot(1,3,1)   
% contourf(xif, etaf, Ex_an_h')
% title('analytical')
% colorbar
% axis equal 
% 
% subplot(1,3,2)   
% contourf(xif, etaf, recEx)
% title('computed')
% colorbar
% axis equal
% 
% subplot(1,3,3)   
% contourf(xif, etaf, (Ex_an_h - recEx')')
% title('error')
% colorbar
% axis equal
% 
% x0=1000;
% y0=1000;
% width=2000;
% height=600;
% set(gcf,'units','points','position',[x0,y0,width,height])

%% plot vector field Ey

% figure
% 
% subplot(1,3,1)   
% contourf(xif, etaf, Ey_an_h')
% title('analytical')
% colorbar
% axis equal 
% 
% subplot(1,3,2)   
% contourf(xif, etaf, recEy)
% title('computed')
% colorbar
% axis equal
% 
% subplot(1,3,3)   
% contourf(xif, etaf, (Ey_an_h - recEy')')
% title('error')
% colorbar
% axis equal
% 
% x0=1000;
% y0=1000;
% width=2000;
% height=600;
% set(gcf,'units','points','position',[x0,y0,width,height])

%% plot vector field for condition (E - curl F)

% figure
% contourf(xif, etaf, checkx_h)
% colorbar
% xlabel('$$\xi$$', 'Interpreter','latex','FontSize',20);
% ylabel('$$\eta$$', 'Interpreter','latex','FontSize',20);
% % export_fig('error_x_equality.pdf','-png','-r300','-painters','-transparent');
% 
% figure
% contourf(xif, etaf, checky_h)
% colorbar
% xlabel('$$\xi$$', 'Interpreter','latex','FontSize',20);
% ylabel('$$\eta$$', 'Interpreter','latex','FontSize',20);
% % export_fig('error_y_equality.pdf','-png','-r300','-painters','-transparent');

%% curl F

curlF = E10 * F;

curlFx_an_h = Ex_an_h;
curlFy_an_h = Ey_an_h;

curlFx = curlF(1:local_nr_ed/2);
curlFy = -curlF(local_nr_ed/2 + 1:end);

rec_curlFx = reconstruction.reconstruct1xform_2(curlFy, curlFx, p, pf, eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);
rec_curlFy = -reconstruction.reconstruct1xform_2(curlFy, curlFx, p, pf, eval_pf_dx_dxii, eval_pf_dx_deta, eval_pf_dy_dxii, eval_pf_dy_deta);

err_curlFx_h = (curlFx_an_h' - rec_curlFx).^2 * wfx .* wfy;
err_curlFx_h = sum(sum(err_curlFx_h));

err_curlFy_h = (curlFy_an_h' - rec_curlFy').^2 * wfx .* wfy;
err_curlFy_h = sum(sum(err_curlFy_h));

err_curlF = sqrt(err_curlFx_h + err_curlFy_h);

err_F_curl_norm = sqrt(err_F + err_curlFx_h + err_curlFy_h)

% figure
% contourf(xif, etaf, rec_curlFx)
% colorbar
% 
% figure
% contourf(xif, etaf, curlFx_an_h')
% colorbar

% figure
% contourf(xif, etaf, rec_curlFy')
% colorbar
% 
% figure
% contourf(xif, etaf, curlFy_an_h')
% colorbar

%% curl E 

curlE = E10' * E_or;
curlE = curlE - E_bc;

curlE = M0 \ curlE;

% E21_dual'*inv(M0)*E_bc

rec_curlE = -reconstruction.reconstruct0form(curlE', p, pf );

% figure
% contourf(xif, etaf, rec_curlE)
% title('computed')
% colorbar

err_curlE = (rec_curlE - F_an_h).^2 .* wfx .*wfy;
err_curlE = sum(sum(err_curlE));

err_E_curl_norm = sqrt(err_Ex + err_Ey + err_curlE)



end

