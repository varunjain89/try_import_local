function [loc2glo, compass_sequence] = list_edges(edge_anno, connectivity_manual);
    % So the 1st, 2nd, 3rd, and 4th rows correspond to the designated
    % sequence
    compass_sequence = {'S','E','N','W'};
    edge_outer_all = [];
    edge_inner_all = [];
    for i = 1:size(connectivity_manual,1)
        for j = 1:length(compass_sequence)
            idx = find(ismember({edge_anno{i,:}}, compass_sequence{j}));
            if connectivity_manual(i,idx) == 0
                edge_outer_all = [edge_outer_all; i, j];
            else
                edge_inner_all = [edge_inner_all; i, j];
            end
        end
    end
    % format [region number, edge direction wrt. compass sequence]
    % inner edges come first, then outer edges
    edge_all = [edge_inner_all; edge_outer_all];
    edge_count = 1;
    % number the edges, know which edge is shared, and make sure the same
    % number is used for that
    edge_numbering = ones(1,size(edge_all,1));
    for i = 2:size(edge_all,1)
        current_edge = edge_all(i,:);
        % find the edge direction being connected to the different region
        idx = find(ismember({edge_anno{current_edge(1,1),:}}, compass_sequence{current_edge(1,2)}));
        connected_edge = connectivity_manual(current_edge(1,1),idx);
        if connected_edge == 0
            action = 'boundary';
        else
            % find which edge direction of the region being connected is
            connected_dir = find(ismember(compass_sequence,...
                edge_anno{connected_edge,find(connectivity_manual(connected_edge,:)==current_edge(1,1))}));
            % has the edge already been identified?
            [q, idx] = ismember([connected_edge connected_dir], edge_all(1:i-1,:), 'rows');
            if q == 1
                action = 'repeated_edge';
            else
                action = 'new_edge';
                
            end
        end
        switch action
            case 'boundary'
                edge_count = edge_count + 1;
                edge_numbering(i) = edge_count;
            case 'new_edge'
                edge_count = edge_count + 1;
                edge_numbering(i) = edge_count;
            case 'repeated_edge'
                edge_numbering(i) = edge_numbering(idx);
        end            
    end
    % row = edges in the same order as 
    loc2glo = zeros(4,size(connectivity_manual,1));
    for i = 1:size(edge_all,1);
        loc2glo(edge_all(i,2),edge_all(i,1)) = edge_numbering(i);
    end
end

    