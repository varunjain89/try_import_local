function [ C1 ] = cochain1y( fy, p )
%COCHAIN1Y Summary of this function goes here
%   Detailed explanation goes here

[x,w] = GLLnodes(p);

for i = 1:p+1
    for j= 1:p
        edgeij = (i-1) * p + j;
        xi = x(i);
        y1 = x(j);
        y2 = x(j+1);
        C1(edgeij) = integral(@(y) fy(xi,y) , y1, y2);
    end
end

end

