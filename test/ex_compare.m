
tic; a1 = ex_serial(50,10000); t1 = toc;

gcp;
tic; a2 = ex_parallel(50,10000); t2 = toc;

disp(['Serial processing time:' num2str(t1)])
disp(['parallel processing time:' num2str(t2)])