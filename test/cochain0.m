function [ C0 ] = cochain0( f, p )
%COCHAIN0 Summary of this function goes here
%   Detailed explanation goes here

%% construct 0-form cochain 

[x,w] = GLLnodes(p);
% f = @(x,y) sin(2*pi*x)*sin(2*pi*y);

for i = 1:p+1
    for j= 1:p+1
        nodeij = (i-1)*(p+1) + j;
        C0(nodeij) = f(x(i),x(j));
    end
end

end