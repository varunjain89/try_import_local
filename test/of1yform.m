function [ u_1y_h ] = of1yform( u_an, v_an, x_bound, y_bound, c, p, mapping, dX_deta, dY_deta )
%OF1YFORM Summary of this function goes here
%   Detailed explanation goes here

% Created on : 12 March, 2018

[x_gll, w] = GLLnodes(p);

p_in = p+10;
[quad_int_xi, w_int_xi] = GLLnodes(p_in);

nr_edges = 2*p*(p+1);
u_1y_h = zeros(nr_edges/2,1);

% figure
% hold on
for i = 1:p+1
    for j = 1:p
        edgeij = (i-1)*p + j;
        
        xi_i  = x_gll(i);
        eta_j1 = x_gll(j);
        eta_j2 = x_gll(j+1);
        
        xi = xi_i;
        eta = 0.5*(eta_j1 + eta_j2) + 0.5*(eta_j2 - eta_j1).* quad_int_xi;
        
        [x_int, y_int] = mapping(x_bound, y_bound, c, xi ,eta);
%         [x_int, y_int] = map_local_y_edge(x_bound, y_bound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         plot(x_int, y_int,'+')          % this line plots and make
                                        % comparison of integration pts
        u_int = u_an(x_int, y_int);
        v_int = v_an(x_int, y_int);
        
        deta_dt = 0.5*(eta_j2 - eta_j1);
        
        dx_dt = dX_deta(xi, eta)*deta_dt;
        dy_dt = dY_deta(xi, eta)*deta_dt;
        
%         dx_deta_int = local_dx_deta(x_bound, y_bound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
%         dy_deta_int = local_dy_deta(x_bound, y_bound, c, eta_j1, eta_j2, quad_int_xi, xi_i);
        
        for k = 1:p_in+1
            u_1y_h(edgeij) = u_1y_h(edgeij)+ (u_int(k)*dx_dt(k) + v_int(k)*dy_dt(k)) * w_int_xi(k);
        end
    end
end


end

