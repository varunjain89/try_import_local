function [ a ] = ex_parallel(M,N)
%EX_SERIAL Summary of this function goes here
%   Detailed explanation goes here

a = zeros(N,1);

parfor I = 1:N
    a(I) = max(eig(rand(M)));
end

end

