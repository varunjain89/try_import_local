function [ C1 ] = cochain1x( fx, p )
%COCHAIN1X Summary of this function goes here
%   Detailed explanation goes here

[x,w] = GLLnodes(p);

for i = 1:p
    for j= 1:p+1
        edgeij = (j-1)*p + i;
        y = x(j);
        x1 = x(i);
        x2 = x(i+1);
        C1(edgeij) = integral(@(x) fx(x,y) , x1, x2);
    end
end

end

