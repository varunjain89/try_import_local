function [ C1 ] = cochain1( fx, fy, p )
%COCHAIN1 Summary of this function goes here
%   Detailed explanation goes here

[x,w] = GLLnodes(p);

% fx = @(x,y) sin(2*pi*x).*sin(2*pi*y);
% fy = @(x,y) sin(2*pi*x).*sin(2*pi*y);

%% x - direction

for i = 1:p
    for j= 1:p+1
        edgeij = (j-1)*p + i;
        y = x(j);
        x1 = x(i);
        x2 = x(i+1);
        C1(edgeij) = integral(@(x) fx(x,y) , x1, x2);
    end
end

%% y - direction 

for i = 1:p+1
    for j= 1:p
        edgeij = p*(p+1) + (i-1)*p + j;
        xi = x(i);
        y1 = x(j);
        y2 = x(j+1);
        C1(edgeij) = integral(@(y) fy(xi,y) , y1, y2);
    end
end

end

