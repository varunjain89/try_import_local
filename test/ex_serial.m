function [ a ] = ex_serial(M,N)
%EX_SERIAL Summary of this function goes here
%   Detailed explanation goes here

a = zeros(N,1);

for I = 1:N
    a(I) = max(eig(rand(M)));
end

end

