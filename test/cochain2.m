function [ C2 ] = cochain2( f, p )
%COCHAIN2 Summary of this function goes here
%   Detailed explanation goes here

%% construct 2-form cochain 

[x,w] = GLLnodes(p);
% f = @(x,y) sin(2*pi*x)*sin(2*pi*y);

for i = 1:p
    for j= 1:p
        volij = (i-1)*p + j;
        xmin = x(i);
        xmax = x(i+1);
        ymin = x(j);
        ymax = x(j+1);
        C2(volij) = integral2(f,xmin,xmax,ymin,ymax);
    end
end

end

