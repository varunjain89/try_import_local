clc
clear all
close all

x = rand(100);
y = zeros(100);
y(x<.025) = x(x<.025);

cmap = [1 1 1;jet];
imagesc(y)
colormap(cmap)
colorbar