clc
close all
clear variables

addpath('..')
addpath('../MSEM')

xbound = [0 1];
ybound = [0 1];
c = 0.3;

p = 3;

[xp, ~] = GLLnodes(p);
[xf, ~] = GLLnodes(p+15);

[xip, etap] = meshgrid(xp);

figure
hold on

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p+i;
        xi1 = xp(i);
        xi2 = xp(i+1);
        
        eta_j = xp(j);
        
        quad_x = xf;
        [ x, y ] = map_local_edge_x(xbound, ybound, c, xi1, xi2, quad_x, eta_j);
        
        mesh.edges.horz.global.x(edgeij,:) = x;
        mesh.edges.horz.global.y(edgeij,:) = y;
        
%         mesh.edges.horz.local.xii(edgeij,:) = 
%         mesh.edges.horz.local.eta(edgeij,:) =
        
        plot(x,y,'+')
    end
end

figure
hold on
for i = 1:p+1
    for j = 1:p
        edgeij = (i-1)*p+j;
        xiii = xp(i);
        
        eta1 = xp(j);
        eta2 = xp(j+1);
        
        quad_x = xf;
        [ x, y ] = map_local_edge_y(xbound, ybound, c, eta1, eta2, quad_x, xiii);
        
        mesh.edges.vert.global.x(edgeij,:) = x;
        mesh.edges.vert.global.y(edgeij,:) = y;
        
%         mesh.edges.horz.local.xii(edgeij,:) = 
%         mesh.edges.horz.local.eta(edgeij,:) =
        
        plot(x,y,'+')
    end
end

figure
hold on
for ed = 1:p*(p+1)
    plot(mesh.edges.horz.global.x(ed,:),mesh.edges.horz.global.y(ed,:),'+')
end

figure
hold on
for ed = 1:p*(p+1)
    plot(mesh.edges.vert.global.x(ed,:),mesh.edges.vert.global.y(ed,:),'+')
end