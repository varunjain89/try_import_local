clc

N = 4;    % Size of each A
m = 300;  % Amount of matrices

% Create sparse, block diagonal matrix, and a solution vector
C = cellfun(@(~)rand(4), cell(m,1), 'UniformOutput', false);
A = sparse(blkdiag(C{:}));
b = randn(m*N,1);


% Block processing: use inv()
tic
for ii = 1:1e3
    for jj = 1:m
        inds = (1:4) + 4*(jj-1);
        inv(A(inds,inds)) * b(inds); %#ok<MINV>
    end    
end
toc

% Block processing: use mldivide()
tic
for ii = 1:1e3
    for jj = 1:m
        inds = (1:4) + 4*(jj-1);
        A(inds,inds) \ b(inds);
    end
end
toc

% All in one go: use inv()
tic
for ii = 1:1e3
    inv(A)*b;
end
toc

% All in one go: use mldivide()
tic
for ii = 1:1e3
    A\b;
end
toc