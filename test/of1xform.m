function [ u_1x_h ] = of1xform( u_an, v_an, x_bound, y_bound, c, p, mapping, dX_dxi, dY_dxi )
%OF1XFORM Summary of this function goes here
%   Detailed explanation goes here

% Created On : 12 March, 2018

[x_gll, ~] = GLLnodes(p);

p_in = p+10;
[quad_int_xi, w_int_xi] = GLLnodes(p_in);

nr_edges = 2*p*(p+1);
u_1x_h = zeros(nr_edges/2,1);

for i = 1:p
    for j = 1:p+1
        edgeij = (j-1)*p + i;
        
        xi_i1 = x_gll(i);
        xi_i2 = x_gll(i+1);
        eta_j = x_gll(j);
        
        xi  = 0.5*(xi_i1 + xi_i2)   + 0.5*(xi_i2 - xi_i1).* quad_int_xi;
        eta = eta_j;
        
        [x_int, y_int] = mapping(x_bound, y_bound, c, xi ,eta);
%         [x_int, y_int] = map_local_edge(x_bound, y_bound, c, xi_i1, xi_i2, quad_int_xi, eta_j);
%         plot(x_int, y_int,'+')          % this line plots and make
%                                         comparison of integration pts
        u_int = u_an(x_int, y_int);
        v_int = v_an(x_int, y_int);
        
        dxi_ds  = 0.5*(xi_i2 - xi_i1);
        
        dx_ds  = dX_dxi(xi, eta)*dxi_ds;
        dy_ds  = dY_dxi(xi, eta)*dxi_ds;
        
        for k = 1:p_in+1
            u_1x_h(edgeij) = u_1x_h(edgeij)+ (u_int(k)*dx_ds(k) + v_int(k)*dy_ds(k)) * w_int_xi(k);
        end
    end
end

end

