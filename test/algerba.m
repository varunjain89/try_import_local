classdef algerba
    %ALGERBA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        a
        b
    end
    
    methods
        function r = sum(obj)
            r = obj.a + obj.b;
        end
        function [x,y] = swap(obj)
            x = obj.b;
            y = obj.a;
        end
    end
    
end

