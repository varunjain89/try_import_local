clc
close all
% clear variables

% addpath('export_fig')
addpath('poisson/single_grid_old_basis')
addpath('poisson/dual_grid_new_basis')

% addpath('../../../../../poisson/single_grid_old_basis')
% addpath('poisson/dual_grid_new_basis')

%% importing single grid files

single_c00_p_divu = importdata('single_grid_old_basis_p_c_00_div_u_err.mat');
single_c00_p_flux = importdata('single_grid_old_basis_p_c_00_flux_err.mat');
single_c00_p_phi  = importdata('single_grid_old_basis_p_c_00_phi_err.mat');
single_c00_p_cond = importdata('single_grid_old_basis_p_c_00_cond_nr.mat');
%single_c00_p_spy  = importdata('single_grid_old_basis_p_10_c_00_LHS_spy.mat');

single_c03_p_divu = importdata('single_grid_old_basis_p_c_03_div_u_err.mat');
single_c03_p_flux = importdata('single_grid_old_basis_p_c_03_flux_err.mat');
single_c03_p_phi  = importdata('single_grid_old_basis_p_c_03_phi_err.mat');
single_c03_p_cond = importdata('single_grid_old_basis_p_c_03_cond_nr.mat');

%% importing Primal-Dual files

dual_c00_p_divu = importdata('dual_grid_new_basis_p_c_00_div_u_err.mat');
dual_c00_p_flux = importdata('dual_grid_new_basis_p_c_00_flux_err.mat');
dual_c00_p_phi  = importdata('dual_grid_new_basis_p_c_00_phi_err.mat');
dual_c00_p_cond = importdata('dual_grid_new_basis_p_c_00_cond_nr.mat');

dual_c03_p_divu = importdata('dual_grid_new_basis_p_c_03_div_u_err.mat');
dual_c03_p_flux = importdata('dual_grid_new_basis_p_c_03_flux_err.mat');
dual_c03_p_phi  = importdata('dual_grid_new_basis_p_c_03_phi_err.mat');
dual_c03_p_cond = importdata('dual_grid_new_basis_p_c_03_cond_nr.mat');


%%

num_pd  = 50;
lw      = 1.5; % this is line width for plots
ms      = 10;  % this is marker size
fig     = 0;

%% for c = 0, l2 norm of div u - h convergence

 %% for c = 0 l2 norm of div u - p convergence 
fig = fig + 1;
figure(fig)

semilogy(3:2:num_pd, dual_c00_p_divu(1,3:2:num_pd), '-ko', 'MarkerFaceColor','k','LineWidth',lw)
hold on 
semilogy(3:2:num_pd, single_c00_p_divu(1,3:2:num_pd), '-ro', 'MarkerFaceColor','r','LineWidth',lw)
hold on
semilogy(3:2:num_pd, dual_c03_p_divu(1,3:2:num_pd), '-k<', 'MarkerFaceColor','k','LineWidth',lw)
hold on 
semilogy(3:2:num_pd, single_c03_p_divu(1,3:2:num_pd), '-r<', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('Primal Dual, c=0.0','Primal-Primal, c=0.0','Primal-Dual, c=0.3','Primal-Primal, c=0.3','location','northwest');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$$\Vert \nabla \cdot \mathbf{q}^h - f^h \Vert _{L^2(\Omega)}$$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');

% export_fig('l2norm_divu_p_convergence.pdf','-pdf','-r864','-painters','-transparent');

 %% for c = 0 l2 err flux - p convergence

fig = fig + 1;
figure(fig)

semilogy(3:2:num_pd, dual_c00_p_flux(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
hold on
semilogy(3:2:num_pd, single_c00_p_flux(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
hold on
semilogy(3:2:num_pd, dual_c03_p_flux(1,3:2:num_pd),'-k<', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
hold on
semilogy(3:2:num_pd, single_c03_p_flux(1,3:2:num_pd),'-r<', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('Primal-Dual, c=0.0','Primal-Primal, c=0.0','Primal-Dual, c=0.3','Primal-Primal, c=0.3','location','northeast');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$\Vert \mathbf{q} ^h - \mathbf{q} _{ex} \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');
ylim([1e-15 1e+1])
% export_fig('l2norm_flux_p_convergence.pdf','-pdf','-r864','-painters','-transparent');

 %% for c = 0 l2 err phi - p convergence
 % dual and primal 
 
fig = fig + 1;
figure(fig)

semilogy(3:2:num_pd, dual_c00_p_phi(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
hold on
semilogy(3:2:num_pd, single_c00_p_phi(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
hold on
semilogy(3:2:num_pd, dual_c03_p_phi(1,3:2:num_pd),'-k>', 'MarkerFaceColor','k','MarkerSize',ms,'LineWidth',lw)
hold on
semilogy(3:2:num_pd, single_c03_p_phi(1,3:2:num_pd),'-r>', 'MarkerFaceColor','r','LineWidth',lw)

hlegend = legend('Primal-Dual, c=0.0','Primal-Primal, c=0.0','Primal-Dual, c=0.3','Primal-Primal, c=0.3','location','northeast');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$\Vert \phi ^h - \phi _{ex} \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');
ylim([1e-15 1e+1])
% export_fig('l2norm_phi_p_convergence.pdf','-pdf','-r864','-painters','-transparent');

%% condition number
 % dual and primal 
 
fig = fig + 1;
figure(fig)

loglog(3:2:num_pd, dual_c00_p_cond(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','LineWidth',lw)
hold on
loglog(3:2:num_pd, single_c00_p_cond(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
hold on
loglog(3:2:num_pd, dual_c03_p_cond(1,3:2:num_pd),'-k>', 'MarkerFaceColor','k','LineWidth',lw)
hold on
loglog(3:2:num_pd, single_c03_p_cond(1,3:2:num_pd),'-r>', 'MarkerFaceColor','r','LineWidth',lw)
hold on


hlegend = legend('Primal-Dual, c=0.0','Primal-Primal, c=0.0','Primal-Dual, c=0.3','Primal-Primal, c=0.3','location','northwest');
set(hlegend,'Interpreter','latex','FontSize',14);

xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
ylabel('$$\mbox{Condition Number}$$', 'Interpreter','latex','FontSize',14);

set(gca,'TickLabelInterpreter','latex','FontSize',20,'Xcolor','k','Ycolor','k');
% ylim([1e-15 1e+1])
% export_fig('condition_number_p.pdf','-pdf','-r864','-painters','-transparent');

%%

% N = 3:2:num_pd;
% d1 = dual_c00_p_cond(1,3:2:num_pd);
% s1 = single_c00_p_cond(1,3:2:num_pd);
% d2 = dual_c03_p_cond(1,3:2:num_pd);
% s2 = single_c03_p_cond(1,3:2:num_pd);

%% spy plots

% fig = fig + 1;
% figure(fig)
% 
% loglog(3:2:num_pd, dual_c00_p_cond(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','LineWidth',lw)
% hold on
% loglog(3:2:num_pd, single_c00_p_cond(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
% hold on
% loglog(3:2:num_pd, dual_c03_p_cond(1,3:2:num_pd),'-k>', 'MarkerFaceColor','k','LineWidth',lw)
% hold on
% loglog(3:2:num_pd, single_c03_p_cond(1,3:2:num_pd),'-r>', 'MarkerFaceColor','r','LineWidth',lw)
% 
% hlegend = legend('Primal-Dual, c=0.0','Primal-Primal, c=0.0','Primal-Dual, c=0.3','Primal-Primal, c=0.3','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',14);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$Condition\ Number$', 'Interpreter','latex','FontSize',14);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',20);
% % ylim([1e-15 1e+1])
% export_fig('poisson/plots/condition_number_p.pdf','-pdf','-r864','-painters','-transparent');


%% for c = 0.3 l2 norm of div u - p convergence
% 
% fig = fig + 1;
% figure(fig)
% 
% semilogy(3:2:num_pd, dual_c03_p_divu(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','MarkerSize',8,'LineWidth',lw)
% hold on
% semilogy(3:2:num_pd, single_c03_p_divu(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
% 
% hlegend = legend('Primal-Dual, $K \times K$ = $2 \times 2$','Primal-Dual, $K \times K$ = $6 \times 6$','Primal-Primal, $K \times K$ = $2 \times 2$','Primal-Primal, $K \times K$ = $6 \times 6$','location','southeast');
% set(hlegend,'Interpreter','latex','FontSize',14);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$\Vert dq_h - f_h \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',14);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',20);
% 
% export_fig('poisson/plots/c03_l2norm_divu_p_convergence.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% for c = 0.3 l2 err flux - p convergence
%  
% fig = fig + 1;
% figure(fig)
%  
% semilogy(3:2:num_pd, dual_c03_p_flux(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','MarkerSize',8,'LineWidth',lw)
% hold on
% semilogy(3:2:num_pd, single_c03_p_flux(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
% 
% hlegend = legend('Primal-Dual, $K \times K$ = $2 \times 2$','Primal-Dual, $K \times K$ = $6 \times 6$','Primal-Primal, $K \times K$ = $2 \times 2$','Primal-Primal, $K \times K$ = $6 \times 6$','location','southwest');
% set(hlegend,'Interpreter','latex','FontSize',14);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$\Vert q _h - q _{ex} \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',14);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',20);
% 
% export_fig('poisson/plots/c03_l2norm_flux_p_convergence.pdf','-pdf','-r864','-painters','-transparent');
% 
% %% for c = 0.3 l2 err flux, phi - p convergence
%  
% fig = fig + 1;
% figure(fig)
%  
% semilogy(3:2:num_pd, dual_c03_p_phi(1,3:2:num_pd),'-ko', 'MarkerFaceColor','k','MarkerSize',8,'LineWidth',lw)
% hold on
% semilogy(3:2:num_pd, single_c03_p_phi(1,3:2:num_pd),'-ro', 'MarkerFaceColor','r','LineWidth',lw)
% 
% hlegend = legend('Primal-Dual, $K \times K$ = $2 \times 2$','Primal-Dual, $K \times K$ = $6 \times 6$','Primal-Primal, $K \times K$ = $2 \times 2$','Primal-Primal, $K \times K$ = $6 \times 6$','location','southwest');
% set(hlegend,'Interpreter','latex','FontSize',14);
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$\Vert \phi _h - \phi _{ex} \Vert _{L^2(\Omega)}$', 'Interpreter','latex','FontSize',14);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',20);
% 
% export_fig('poisson/plots/c03_l2norm_phi_p_convergence.pdf','-pdf','-r864','-painters','-transparent');

%%

% figure(1)
% 
% axis equal
% 
% semilogy(1:num_pd, c_00_err_div_u(2,:), '-ro', 'MarkerFaceColor',[1 0 0])
% hold on 
% semilogy(1:num_pd, c_00_err_div_u(6,:), '-bo', 'MarkerFaceColor',[0 0 1])
% hold on
% semilogy(1:num_pd, c_03_err_div_u(2,:), '-rd', 'MarkerFaceColor',[1 0 0])
% hold on 
% semilogy(1:num_pd, c_03_err_div_u(6,:), '-bd', 'MarkerFaceColor',[0 0 1])
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$$L^2 - error$$', 'Interpreter','latex','FontSize',14);
% % title('$$ k = 10^{-1}$$', 'Interpreter','latex','FontSize',14);
% set(gca,'TickLabelInterpreter','latex','FontSize',14);
% % ylim([0.45 0.75])
% hlegend = legend('c = 0.0, K $\times$ K = 2 $\times$ 2','c = 0.0, K $\times$ K = 6 $\times$ 6','c = 0.3, K $\times$ K = 2 $\times$ 2','c = 0.3, K $\times$ K = 6 $\times$ 6','Direct','location','southeast');
%     set(hlegend,'Interpreter','latex','FontSize',14);
%     
% export_fig('l2_norm_div_u_p_convergence.pdf','-pdf','-r864','-painters','-transparent'); % for lines plot

%% l - inf, norm
% 
% figure(2)
% 
% axis equal
% 
% semilogy(1:num_pd, c_00_err_div_u_max(2,:), '-ro', 'MarkerFaceColor',[1 0 0])
% hold on 
% semilogy(1:num_pd, c_00_err_div_u_max(6,:), '-bo', 'MarkerFaceColor',[0 0 1])
% hold on
% semilogy(1:num_pd, c_03_err_div_u_max(2,:), '-rd', 'MarkerFaceColor',[1 0 0])
% hold on 
% semilogy(1:num_pd, c_03_err_div_u_max(6,:), '-bd', 'MarkerFaceColor',[0 0 1])
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$$L^2 - error$$', 'Interpreter','latex','FontSize',14);
% % title('$$ k = 10^{-1}$$', 'Interpreter','latex','FontSize',14);
% set(gca,'TickLabelInterpreter','latex','FontSize',14);
% % ylim([0.45 0.75])
% hlegend = legend('c = 0.0, K $\times$ K = 2 $\times$ 2','c = 0.0, K $\times$ K = 6 $\times$ 6','c = 0.3, K $\times$ K = 2 $\times$ 2','c = 0.3, K $\times$ K = 6 $\times$ 6','Direct','location','southeast');
%     set(hlegend,'Interpreter','latex','FontSize',14);
%     
% export_fig('l2_norm_div_u_max_p_convergence.pdf','-pdf','-r864','-painters','-transparent'); % for lines plot
% 
%% h-convergence 


%% p - convergence - orhogonal mesh

% figure(3)

% for el = 2:2:6
%     semilogy(1:num_pd, err_phi(el,:))
%     hold on
%     semilogy(1:num_pd, err_flux(el,:))
% end

% semilogy(3:2:num_pd, c_00_err_phi(2,3:2:num_pd), '-ro')
% hold on
% semilogy(3:2:num_pd, c_00_err_flux(2,3:2:num_pd), '-rd')
% hold on
% % semilogy(3:num_pd, err_phi(4,3:num_pd), '-bo')
% % hold on
% % semilogy(3:num_pd, err_flux(4,3:num_pd), '-bd')
% % hold on
% semilogy(3:2:num_pd, c_00_err_phi(6,3:2:num_pd), '-bo')
% hold on
% semilogy(3:2:num_pd, c_00_err_flux(6,3:2:num_pd), '-bd')
% 
% set(gca,'xtick',3:2:num_pd)
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$$L^2 - norm$$', 'Interpreter','latex','FontSize',14);
% % title('$$ k = 10^{-1}$$', 'Interpreter','latex','FontSize',14);
% set(gca,'TickLabelInterpreter','latex','FontSize',14);
% % ylim([0.45 0.75])
% hlegend = legend('K $\times$ K = 2 $\times$ 2, $\| p_h - p_{exact} \|_{L^2}$','K $\times$ K = 2 $\times$ 2, $\| u_h - u_{exact} \|_{L^2}$','K $\times$ K = 6 $\times$ 6, $\| p_h - p_{exact} \|_{L^2}$','K $\times$ K = 6 $\times$ 6, $\| u_h - u_{exact} \|_{L^2}$','location','northeast');
%     set(hlegend,'Interpreter','latex','FontSize',14);
%     
% export_fig('c_00_phi_flux_p_convergence.pdf','-pdf','-r864','-painters','-transparent'); % for lines plot
% 
% %% p - convergence - curved mesh 
% 
% figure(4)
% 
% % for el = 2:2:6
% %     semilogy(1:num_pd, err_phi(el,:))
% %     hold on
% %     semilogy(1:num_pd, err_flux(el,:))
% % end
% 
% semilogy(3:2:num_pd, c_03_err_phi(2,3:2:num_pd), '-ro')
% hold on
% semilogy(3:2:num_pd, c_03_err_flux(2,3:2:num_pd), '-rd')
% hold on
% % semilogy(3:num_pd, err_phi(4,3:num_pd), '-bo')
% % hold on
% % semilogy(3:num_pd, err_flux(4,3:num_pd), '-bd')
% % hold on
% semilogy(3:2:num_pd, c_03_err_phi(6,3:2:num_pd), '-bo')
% hold on
% semilogy(3:2:num_pd, c_03_err_flux(6,3:2:num_pd), '-bd')
% 
% set(gca,'xtick',3:2:num_pd)
% 
% xlabel('$$N$$', 'Interpreter','latex','FontSize',14);
% ylabel('$$L^2 - norm$$', 'Interpreter','latex','FontSize',14);
% % title('$$ k = 10^{-1}$$', 'Interpreter','latex','FontSize',14);
% set(gca,'TickLabelInterpreter','latex','FontSize',14);
% % ylim([0.45 0.75])
% hlegend = legend('K $\times$ K = 2 $\times$ 2, $\| p_h - p_{exact} \|_{L^2}$','K $\times$ K = 2 $\times$ 2, $\| u_h - u_{exact} \|_{L^2}$','K $\times$ K = 6 $\times$ 6, $\| p_h - p_{exact} \|_{L^2}$','K $\times$ K = 6 $\times$ 6, $\| u_h - u_{exact} \|_{L^2}$','location','northeast');
%     set(hlegend,'Interpreter','latex','FontSize',14);
%     
% export_fig('c_03_phi_flux_p_convergence.pdf','-pdf','-r864','-painters','-transparent'); % for lines plot
% 
% %% h- convergence 
% 
% % figure(2)
% % 
% % h = 1./(1:num_el);
% % 
% % loglog(h(2:num_el), err_phi(2:num_el,2), '-ro')
% % hold on
% % loglog(h(2:num_el), err_flux(2:num_el,2), '-rd')
% % hold on
% % loglog(h(2:num_el), err_phi(2:num_el,4), '-bo')
% % hold on
% % loglog(h(2:num_el), err_flux(2:num_el,4), '-bd')
% % hold on
% % loglog(h(2:num_el), err_phi(2:num_el,6), '-go')
% % hold on
% % loglog(h(2:num_el), err_flux(2:num_el,6), '-gd')

x1 = pi;
y1 = sin(pi);
txt1 = '\leftarrow sin(\pi) = 0';
text(x1,y1,txt1)

