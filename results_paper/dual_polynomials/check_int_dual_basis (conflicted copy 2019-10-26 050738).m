clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
addpath('../../export_fig')

%% domain

xbound = [-1 1];
ybound = [-1 1];
c = 0.0;

%% discretization

K = 1;
p = 4;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% 

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

%% 1D primal basis

pf = 50;
[xii, wii] = GLLnodes(pf);

[hp_plot, ep_plot] = MimeticpolyVal(xii,p,1);

lw = 1.5;
fs = 16;

figure
hold on
grid on
for i = 1:p+1 
    plot(xii, hp_plot,'LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$h_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

% ep_plot2 = sin(ep_plot*pi/2);

figure
hold on
grid on
for i = 1:p
    plot(xii, ep_plot,'LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$e_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

% figure
% hold on
% grid on
% for i = 1:p
%     plot(xii, ep_plot2,'LineWidth',lw)
% end
% 
% xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
% ylabel('$$e_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);
% 
% set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% 1D dual nodal basis 

M0 = zeros(p+1);

% for i = 1:p+1
%     for k = 1:p+1
%         for xx = 1:p+1
%             M0(i,k) = M0(i,k) + hp(i,xx)*hp(k,xx)*wp(xx);
%         end
%     end
% end

for i = 1:p+1
    for k = 1:p+1
        for xx = 1:pf+1
            M0(i,k) = M0(i,k) + hp_plot(i,xx)*hp_plot(k,xx)*wii(xx);
        end
    end
end

hp_dual_plot = M0\hp_plot;

figure
hold on
grid on
for i = 1:p+1 
    plot(xii, hp_dual_plot,'LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$\widetilde{h}_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% 1D dual edge basis 

M1 = zeros(p);

% for i = 1:p+1
%     for k = 1:p+1
%         for xx = 1:p+1
%             M0(i,k) = M0(i,k) + hp(i,xx)*hp(k,xx)*wp(xx);
%         end
%     end
% end

for i = 1:p
    for k = 1:p
        for xx = 1:pf+1
            M1(i,k) = M1(i,k) + ep_plot(i,xx)*ep_plot(k,xx)*wii(xx);
        end
    end
end

ep_dual_plot = M1\ep_plot;

figure
hold on
grid on
for i = 1:p+1 
    plot(xii, ep_dual_plot,'LineWidth',lw)
end

xlabel('$$\xi$$', 'Interpreter','latex','FontSize',fs);
ylabel('$$\widetilde{e}_i \left( \xi \right) $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%% numerical integration 

sum_hp_dual = zeros(1,p+1);

for i = 1:p+1
    for xx = 1:pf+1
    sum_hp_dual(i) = sum_hp_dual(i) + hp_dual_plot(i,xx)*wii(xx);
    end
end

sum_ep_dual = zeros(1,p);

for i = 1:p
    for xx = 1:pf+1
    sum_ep_dual(i) = sum_ep_dual(i) + ep_dual_plot(i,xx)*wii(xx);
    end
end

sum_hp_prim = zeros(1,p+1);

for i = 1:p+1
    for xx = 1:pf+1
    sum_hp_prim(i) = sum_hp_prim(i) + hp_plot(i,xx)*wii(xx);
    end
end

%% edge integrals of dual polynomials

edge_map   = @(s,x1,x2) x1*0.5*(1-s) + x2*0.5*(1+s);

tt = -0.4463

xii2 = edge_map(xii,xp(1),tt);

hp_dual_plot2 = interp1(xii,hp_dual_plot(1,:),xii2,'spline');

plot(xii2, hp_dual_plot2,'o')

sum = 0;

dx_dxii = (tt-xp(1))/2;

for xx = 1:pf+1
    sum = sum + hp_dual_plot2(xx) * wii(xx) * dx_dxii;
end

sum