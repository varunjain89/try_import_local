clc
close all
clear variables

addpath('export_fig')

xbound = [0 1];
ybound = [0 1];
c = 0.0;

%% boundary conditions

bc_bott = @(x,y) 0*ones(size(x));
bc_topp = @(x,y) -log10(1-3*x.*(1-x));
bc_left = @(x,y) 0*ones(size(x));
bc_rght = @(x,y) sin(pi*y);

%% discretization

K = 1;
p = 3;

ttl_nr_el = K^2;
% local_nr_ed = 2*p*(p+1);
% local_nr_pp = p^2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% M1 mass matrix

% evaluate jacobian at GLL points
mesh1.eval_p_der_v2();

% evaluate metric terms for M11
eval.p = flow.poisson.eval_metric(mesh1.eval.p);

mesh1.eval.metric = eval.p;

M11 = zeros(2*p*(p+1),2*p*(p+1),ttl_nr_el);

for eln = 1:ttl_nr_el
    M11(:,:,eln) = mass_matrix.M1_v7(mesh1, eln);
end

%% M2 mass matrix

M22s = zeros(p^2,p^2,ttl_nr_el);

for eln = 1:ttl_nr_el
    M22s(:,:,eln) = mass_matrix.M2_v4(mesh1,eln);
end

%% E21 incidence matrix

E211    = incidence_matrix.E21(p);

%%

% M11 = M1(3);
% E21 = E21(3);

LHS1 = [M11 E211'; E211 zeros(9,9)];

figure(1)
spy(LHS1)

set(gca,'TickLabelInterpreter','latex','FontSize',14,'Xcolor','k','Ycolor','k');

% M2 = M2(3)

M2E21 = M22s*E211;

LHS2 = [M11 M2E21'; M2E21 zeros(9,9)]

% figure(2)
% spy(LHS2)
% % 
% set(gca,'TickLabelInterpreter','latex','FontSize',14,'Xcolor','k','Ycolor','k');