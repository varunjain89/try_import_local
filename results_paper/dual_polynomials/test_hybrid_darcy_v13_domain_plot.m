clc
close all
clear variables

addpath('../..')
addpath('../../MSEM')
% addpath('../../export_fig')


%% domain

xbound = [0 1];
ybound = [0 1];
c = 0.3;

%% discretization

K = 1;

Kx = K;
Ky = K;

p = 7;

ttl_nr_el = K^2;
local_nr_ed = 2*p*(p+1);
local_nr_pp = p^2;

%% define mesh

mesh1 = CrazyMesh(K,p,xbound,ybound,c);

%% LHS systems

% evaluate material properties at GLL points
mesh1.eval_dom_xy();

[xp, wp] = GLLnodes(p);
[hp, ep] = MimeticpolyVal(xp,p,1);

[xiip, etap] = meshgrid(xp);

% figure 
% hold on
% 
% plot(xiip, etap, '-k')
% plot(etap, xiip, '-k')

%% 

% % [xp, wp] = GLLnodes(pp);
% % [xiip, etap] = meshgrid(xp);
% 
% figure
% hold on
% 
% for elx = 1:K
%     for ely = 1:K
%         [x,y] = mesh1.el.mapping(xiip, etap, elx, ely);
%         
%         x = 0.95*x;
%         y = 0.95*y;
%         
%         plot(x, y, '-k')
%         plot(x', y', '-k')
%     end
% end
% 
% title('domain mapping')
% 
% % check.red_rec_2form(phi_an, p_foil);

%% plot domain

% figure
% hold on
% 
% for elx = 1:Kx
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(xiip, etap, elx, ely);
%         plot(x, y, '+')
%     end
% end
% 
% title('domain mapping')

%% plot region

xp2 = linspace(-1,1,50);
[xL,yL] = mesh1.domain.mapping(-1, xp2);
[xR,yR] = mesh1.domain.mapping(+1, xp2);
[xB,yB] = mesh1.domain.mapping(xp2, -1);
[xT,yT] = mesh1.domain.mapping(xp2, +1);
% 
% figure
% hold on 
% 
% plot(xL, yL,'k')
% plot(xR, yR,'k')
% plot(xB, yB,'k')
% plot(xT, yT,'k')
% 
% title('region/domain boundary ')

%% plot elements

tt = 1;
xp3 = linspace(-tt,tt,50);

fs = 16;

figure
hold on
axis square

for elx = 1:Kx
    for ely = 1:Ky
        
        [xL,yL] = mesh1.el.mapping(-tt, xp3, elx, ely);
        [xR,yR] = mesh1.el.mapping(+tt, xp3, elx, ely);
        [xB,yB] = mesh1.el.mapping(xp3, -tt, elx, ely);
        [xT,yT] = mesh1.el.mapping(xp3, +tt, elx, ely);
        
        plot(xL, yL,'k', 'linewidth', 1.5)
        plot(xR, yR,'k', 'linewidth', 1.5)
        plot(xB, yB,'k', 'linewidth', 1.5)
        plot(xT, yT,'k', 'linewidth', 1.5)
    end
end

% set(gca,'visible','off')
% title('domain mapping')

%% 

for elx = 1:Kx
    for ely = 1:Ky
        for pp = 2:p
            [xS,yS] = mesh1.el.mapping(xp(pp),xp2, elx, ely);
            [xS2,yS2] = mesh1.el.mapping(xp2,xp(pp), elx, ely);
            plot(xS,yS,'Color', [0.3 0.3 0.3], 'linewidth', 1.0)
            plot(xS2,yS2,'Color', [0.3 0.3 0.3], 'linewidth', 1.0)
        end
    end
end

xlabel('$$ x $$', 'Interpreter','latex','FontSize',fs);
ylabel('$$ y $$', 'Interpreter','latex','FontSize',fs);

set(gca,'TickLabelInterpreter','latex','FontSize',fs,'Xcolor','k','Ycolor','k');

%%

% xp(1)   = -tt;
% xp(end) = +tt;
% 
% [xiip2, etap2] = meshgrid(xp);
% 
% for elx = 1:Kx
%     for ely = 1:Ky
%         
%         [xS,yS] = mesh1.el.mapping(xiip2, etap2, elx, ely);
%         
%         plot(xS(:,2:end-1), yS(:,2:end-1),'Color', [0.4 0.4 0.4], 'linewidth', 2)
%         plot(xS(2:end-1,:)', yS(2:end-1,:)','Color', [0.4 0.4 0.4], 'linewidth', 2)
%     end
% end
% 
% axis square

%% 

% for elx = 1:Kx
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(-1, xp, elx, ely);
%         
%         yL2 = (y(1:end-1)+y(2:end))/2;
%         plot(x(1:end-1), yL2,'ro','MarkerFaceColor','r','markerSize',6)
%     end
% end
% 
% for elx = 1:Kx
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(1, xp, elx, ely);
%         
%         yL2 = (y(1:end-1)+y(2:end))/2;
%         plot(x(1:end-1), yL2,'ro','MarkerFaceColor','r','markerSize',6)
%     end
% end
% 
% for elx = 1:Kx
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(xp, -1, elx, ely);
%         
%         xL2 = (x(1:end-1)+x(2:end))/2;
%         plot(xL2, y(1:end-1),'ro','MarkerFaceColor','r','markerSize',6)
%     end
% end
% 
% for elx = 1:Kx
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(xp, 1, elx, ely);
%         
%         xL2 = (x(1:end-1)+x(2:end))/2;
%         plot(xL2, y(1:end-1),'ro','MarkerFaceColor','r','markerSize',6)
%     end
% end
% 
% axis square
% 
% annotation('textbox', [0.49 0.94 0.05 0.06], 'String','$$\hat{\mathbf{u}}_\mathbf{n}$$','FontSize',16,'LineStyle','none','Interpreter','latex');
% annotation('textbox', [0.84 0.50 0.05 0.06], 'String','$$\hat{p}$$','FontSize',16,'LineStyle','none','Interpreter','latex');
% 
% annotation('line',[0.22 0.26],[0.95 0.95],'color','blue','linewidth', 2);
% annotation('line',[0.27 0.35],[0.95 0.95],'color','blue','linewidth', 2);
% annotation('line',[0.36 0.40],[0.95 0.95],'color','blue','linewidth', 2);
% 
% annotation('line',[0.43 0.47],[0.95 0.95],'color','blue','linewidth', 2);
% annotation('line',[0.48 0.56],[0.95 0.95],'color','blue','linewidth', 2);
% annotation('line',[0.57 0.61],[0.95 0.95],'color','blue','linewidth', 2);
% 
% annotation('line',[0.64 0.68],[0.95 0.95],'color','blue','linewidth', 2);
% annotation('line',[0.69 0.77],[0.95 0.95],'color','blue','linewidth', 2);
% annotation('line',[0.78 0.82],[0.95 0.95],'color','blue','linewidth', 2);
% 
% 
% for elx = 1:1
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(-1, xp, elx, ely);
%         
%         yL2 = (y(1:end-1)+y(2:end))/2;
%         plot(x(1:end-1), yL2,'bo','MarkerFaceColor','b','markerSize',6)
%     end
% end
% 
% for elx = Kx:Kx
%     for ely = 1:Ky
%         [x,y] = mesh1.el.mapping(1, xp, elx, ely);
%         
%         yL2 = (y(1:end-1)+y(2:end))/2;
%         plot(x(1:end-1), yL2,'bo','MarkerFaceColor','b','markerSize',6)
%     end
% end
% 
% for elx = 1:Kx
%     for ely = 1:1
%         [x,y] = mesh1.el.mapping(xp, -1, elx, ely);
%         
%         xL2 = (x(1:end-1)+x(2:end))/2;
%         plot(xL2, y(1:end-1),'bo','MarkerFaceColor','b','markerSize',6)
%     end
% end
% 
% annotation('line',[0.27 0.27],[0.73 0.85],'color','black','linewidth', 4);
% annotation('line',[0.27 0.36],[0.46 0.46],'color','black','linewidth', 4);
% 
% annotation('arrow',[0.31 0.31],[0.42 0.51],'linewidth', 1);
% annotation('arrow',[0.24 0.31],[0.78 0.78],'linewidth', 1);
% 
% annotation('textbox', [0.30 0.76 0.16 0.07], 'String','$${\mathsf{u}_x}_{i,j}$$','FontSize',16,'LineStyle','none','Interpreter','latex');
% annotation('textbox', [0.30 0.49 0.16 0.07], 'String','$${\mathsf{u}_y}_{i,j}$$','FontSize',16,'LineStyle','none','Interpreter','latex');
% annotation('textbox', [0.42 0.76 0.16 0.07], 'String','$${\lambda}_{i,j}$$','FontSize',16,'LineStyle','none','Interpreter','latex');
% 
% %% add spectral lines
% 
% % figure
% % hold on
% % 
% % tt = 1;
% % xp3 = linspace(-tt,tt,30);
% % 
% % for elx = 1:Kx
% %     for ely = 1:Ky
% %         
% %         [xL,yL] = mesh1.el.mapping(-tt, xp3, elx, ely);
% %         [xR,yR] = mesh1.el.mapping(+tt, xp3, elx, ely);
% %         [xB,yB] = mesh1.el.mapping(xp3, -tt, elx, ely);
% %         [xT,yT] = mesh1.el.mapping(xp3, +tt, elx, ely);
% %         
% %         plot(xL, yL,'k', 'linewidth', 3)
% %         plot(xR, yR,'k', 'linewidth', 3)
% %         plot(xB, yB,'k', 'linewidth', 3)
% %         plot(xT, yT,'k', 'linewidth', 3)
% %     end
% % end
% % 
% % for elx = 1:Kx
% %     for ely = 1:Ky
% %         
% %         [xS,yS] = mesh1.el.mapping(xiip, etap, elx, ely);
% %         
% %         plot(xS, yS,'k')
% %         plot(xS', yS','k')
% %     end
% % end
% % 
% % tsl = 20;
% % 
% % xlabel('$$x$$', 'Interpreter','latex','FontSize',tsl);
% % ylabel('$$y$$', 'Interpreter','latex','FontSize',tsl);
% % 
% % set(gca,'TickLabelInterpreter','latex','FontSize',tsl,'Xcolor','k','Ycolor','k');
% % 
% % axis square
