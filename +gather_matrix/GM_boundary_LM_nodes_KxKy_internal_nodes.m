function [ ggv ] = GM_boundary_LM_nodes_KxKy_internal_nodes(Kx, Ky, p)
%GM_BOUNDARY_LM_NODES Summary of this function goes here
%   Detailed explanation goes here

% Created on : 31 Oct, 2019
% Description : This is the gather matrix for ghost volumes / lagrange
% multipliers for Dirichlet BC. The lagrange multipliers are the same as
% the lambda or what we used to call ghost nodes. 

num_el = Kx*Ky;
num_local_boundary = 4*p;

nr_int_lambda = Kx*p*(Ky-1) + Ky*p*(Kx-1);

ggv = zeros(num_local_boundary, num_el);

A = 1:p;

% bottom edges
count = 0;
for i = 1:Kx
    for j = 2:Ky
        eleid = (i-1)*Ky + j;
        ggv(1:p, eleid) = A + p*count;
        count = count+1;
    end
end

% left edges
count = 0;
for i = 2:Kx
    for j = 1:Ky
        eleid = (i-1)*Ky + j;
        ggv(2*p+1:3*p, eleid) = A + Kx*p*(Ky-1) + count*p;
        count = count +1;
    end
end

% right edges
for i = 1:Kx-1
    for j = 1:Ky
        eleid = (i-1)*Ky + j;
        ggv(1*p+1:2*p, eleid) = ggv(2*p+1:3*p, eleid+Ky);
    end
end

% top edges
for i = 1:Kx
    for j = 1:Ky-1
        eleid = (i-1)*Ky + j;
        ggv(3*p+1:4*p, eleid) = ggv(1:p, eleid+1);
    end
end

% for i = 1:Kx
%     eleid_bot = (i-1)*Ky + 1;
%     ggv(1:p, eleid_bot) = A + nr_int_lambda + (i-1)*p;
% end
%     
% for i = 1:Ky
%     eleid_right = Ky*(Kx-1) + i;
%     ggv(p+1:2*p, eleid_right) = A + nr_int_lambda + Kx*p + (i-1)*p;
% end
% 
% for i = 1:Ky
%     eleid_left = i;
%     ggv(2*p+1:3*p, eleid_left) = A + nr_int_lambda + Kx*p + Ky*p + (i-1)*p;
% end
% 
% for i = 1:Kx
%     eleid_top = i*Ky;
%     ggv(3*p+1:4*p, eleid_top) = A + nr_int_lambda + Kx*p + 2*Ky*p + (i-1)*p;
% end

% for i = 1:K
%     eleid_bot = (i-1)*K + 1;
%     ggv(1:p, eleid_bot) = A + 2*K*(K-1)*p + (i-1)*p;
%     
%     eleid_right = K*(K-1) + i;
%     ggv(p+1:2*p, eleid_right) = A + 2*K*(K-1)*p + K*p + (i-1)*p;
%     
%     eleid_left = i;
%     ggv(2*p+1:3*p, eleid_left) = A + 2*K*(K-1)*p + 2*K*p + (i-1)*p;
%     
%     eleid_top = K + (i-1)*K;
%     ggv(3*p+1:4*p, eleid_top) = A + 2*K*(K-1)*p + 3*K*p + (i-1)*p;
% end

end

