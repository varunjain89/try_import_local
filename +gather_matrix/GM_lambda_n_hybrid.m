function [temp_GM_lam2] = GM_lambda_n_hybrid(r,c,p)

% Created on 1 April, 2019

% r is discretization of refined mesh
% c is discretization of coarse mesh

temp_GM_lam = gather_matrix.GM_boundary_LM_nodes_v2_internal(r,p);

R = full(spones(temp_GM_lam));

nr_coarse_el = c^2;
nr_refine_el = r^2;
nr_lambda_coarse = 4*r*p;
nr_local_lambda = 4*p;
nr_toal_elements = r*c;

nr_int_lambda_refine = 2*r*p*(r-1)

temp_GM_lam2 = zeros(nr_local_lambda, nr_toal_elements);

for i = 1:nr_coarse_el
    temp_GM_lam2(:,(i-1)*nr_refine_el +1: i * nr_refine_el) = ((i-1) * nr_int_lambda_refine + temp_GM_lam).*R;
end

xx = max(max(temp_GM_lam2));

count = 0;

ccx = c;
ccy = c;

rx = r;
ry = r;

% bottom edges
for i = 1:ccx
    for j = 2:ccy
        reg = (i-1)*ccy + j;
        count = count + 1;
        for Ri = 1:rx
            eleID = (reg-1)*(rx*ry) + (Ri-1)*ry + 1;
            temp_GM_lam2(1:p,eleID) = (xx + (count-1)* 3 * p + (Ri-1)*p + 1 : xx + (count-1)* 3 * p + (Ri-1)*p + p);
        end
    end
end

xx = max(max(temp_GM_lam2));

% left edges
count = 0;
for i = 2:ccx
    for j = 1:ccy
        reg = (i-1)*ccy + j;
        count = count + 1;
        for Ri = 1:ry
            eleID = (reg-1)*(rx*ry) + Ri;
            temp_GM_lam2(2*p+1:3*p,eleID) = (xx + (count-1)*3*p + (Ri-1)*p + 1 : xx + (count-1)*3*p + (Ri-1)*p + p);
        end
    end
end

% right edges
for i = 1:ccx-1
    for j = 1:ccy
        reg = (i-1)*ccy + j;
        for Ri = 1:ry
            eleIDR = (reg-1)*(rx*ry) + (rx-1)*ry + Ri;
            eleIDL = (reg-1 + ccy )*(rx*ry) + Ri;        
            temp_GM_lam2(1*p+1:2*p,eleIDR) = temp_GM_lam2(2*p+1:3*p,eleIDL);
        end
    end
end

% top edges
for i = 1:ccx
    for j = 1:ccy-1
        reg = (i-1)*ccy + j;
        for Ri = 1:rx
            eleIDT = (reg-1)*(rx*ry) + Ri * ry;
            eleIDB = reg * (rx*ry) + (Ri-1)*ry + 1;
            temp_GM_lam2(3*p+1:4*p,eleIDT) = temp_GM_lam2(1:p,eleIDB);
        end
    end
end



end

