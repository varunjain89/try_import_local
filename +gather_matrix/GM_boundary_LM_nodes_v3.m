function [ ggv ] = GM_boundary_LM_nodes_v3(K, r, p)
%GM_BOUNDARY_LM_NODES Summary of this function goes here
%   Detailed explanation goes here

% Created on : 7th Feb, 2018. 
% Modified on: 9 Apr, 2019 for n- level hybrid method
% Description : This is the gather matrix for ghost volumes / lagrange
% multipliers for Dirichlet BC. The lagrange multipliers are the same as
% the lambda or what we used to call ghost nodes. 

Kx = K;
Ky = K;

rx = r;
ry = r;

dof_edge_x = rx * p;
dof_edge_y = ry * p;

num_el = Kx * Ky;
num_local_boundary = 2 * dof_edge_x + 2 * dof_edge_y;

ggv = zeros(num_local_boundary, num_el);

Ax = 1:dof_edge_x;

% bottom edges
count = 0;
for i = 1:Kx
    for j = 2:Ky
        eleid = (i-1)*Ky + j;
        ggv(1:dof_edge_x, eleid) = Ax + dof_edge_x *count;
        count = count+1;
    end
end

Ay = 1:dof_edge_y;

% left edges
count = 0;
for i = 2:Kx
    for j = 1:Ky
        eleid = (i-1)*Ky + j;
        ggv(dof_edge_x + dof_edge_y +1:dof_edge_x + 2*dof_edge_y, eleid) = Ay + Kx * (Ky-1)* dof_edge_x + count* dof_edge_y;
        count = count +1;
    end
end

% right edges
for i = 1:Kx-1
    for j = 1:Ky
        eleid = (i-1)*Ky + j;
        ggv(dof_edge_x+1:dof_edge_x +dof_edge_y, eleid) = ggv(dof_edge_x + dof_edge_y +1:dof_edge_x + 2*dof_edge_y, eleid+Ky);
    end
end

% top edges
for i = 1:Kx
    for j = 1:Ky-1
        eleid = (i-1)*Ky + j;
        ggv(dof_edge_x + 2*dof_edge_y + 1:2*dof_edge_x + 2*dof_edge_y, eleid) = ggv(1:dof_edge_x, eleid+1);
    end
end

% boundary edges
for i = 1:Kx
    eleid_bot = (i-1)*Ky + 1;
    ggv(1:dof_edge_x, eleid_bot) = Ax + Kx*(Ky-1)*dof_edge_x + Ky*(Kx-1)*dof_edge_y + (i-1)*dof_edge_x;
end 

for i = 1:Ky
    eleid_right = Ky*(Kx-1) + i;
    ggv(dof_edge_x+1:dof_edge_x + dof_edge_y, eleid_right) = Ay + Kx*(Ky-1)*dof_edge_x + Ky*(Kx-1)*dof_edge_y + Kx*dof_edge_x + (i-1)*dof_edge_y;
end 
    
for i = 1:Ky
    eleid_left = i;
    ggv(dof_edge_x + dof_edge_y +1:dof_edge_x + 2*dof_edge_y, eleid_left) = Ay + Kx*(Ky-1)*dof_edge_x + Ky*(Kx-1)*dof_edge_y + Kx*dof_edge_x + Ky*dof_edge_y + (i-1)*dof_edge_y;
end 
    
for i = 1:Kx
    eleid_top = Ky + (i-1)*Ky;
    ggv(dof_edge_x + 2*dof_edge_y +1:2*dof_edge_x + 2*dof_edge_y, eleid_top) = Ax + Kx*(Ky-1)*dof_edge_x + Ky*(Kx-1)*dof_edge_y + Kx*dof_edge_x + 2*Ky*dof_edge_y + (i-1)*dof_edge_x;
end

end

