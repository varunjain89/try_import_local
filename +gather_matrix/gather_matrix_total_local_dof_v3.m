function [ GM ] = gather_matrix_total_local_dof_v3(ttl_nr_el, ttl_loc_dof)
%GATHER_MATRIX_TOTAL_LOCAL_DOF Summary of this function goes here
%   Detailed explanation goes here

% Created on : 9th October, 2018

% local_nr_tau_xx = p*(p+1);
% local_nr_tau_yx = p*(p+1);
% local_nr_tau_xy = p*(p+1);
% local_nr_tau_yy = p*(p+1);

% total_local_dof = local_nr_tau_xx + local_nr_tau_xy + local_nr_tau_yx + local_nr_tau_yy + 3*p^2;

temp = 1:ttl_loc_dof;
GM = zeros(ttl_nr_el,ttl_loc_dof);

for el = 1:ttl_nr_el
    GM(el,:) = temp+(el-1)*ttl_loc_dof;
end

end

