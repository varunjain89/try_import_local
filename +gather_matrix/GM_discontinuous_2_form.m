function [ gf ] = GM_discontinuous_2_form( p, K )
%GM_DISCONTINUOUS_1_FORM Summary of this function goes here
%   Detailed explanation goes here

% Created on : 9 Oct, 2018

disp('this program is not complete')

gf = zeros(2*p*(p+1), K^2);

el_total_dof = 2*p*(p+1) + p^2;
num_el_dof_q = 2*p*(p+1);

q = 1:2*p*(p+1);

for ele = 1:K^2
    gf(1:num_el_dof_q,ele) = q + el_total_dof*(ele-1);
end

end

