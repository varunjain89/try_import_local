function [ ggv ] = GM_boundary_LM_nodes( K, p )
%GM_BOUNDARY_LM_NODES Summary of this function goes here
%   Detailed explanation goes here

% Created on : 7th Feb, 2018. 
% Description : This is the gather matrix for ghost volumes / lagrange
% multipliers for Dirichlet BC. The lagrange multipliers are the same as
% the lambda or what we used to call ghost nodes. 

num_el = K^2;
num_local_boundary = 4*p;

ggv = zeros(num_local_boundary, num_el);

A = 1:p;

% bottom edges
count = 0;
for i = 1:K
    for j = 2:K
        eleid = (i-1)*K + j;
        ggv(1:p, eleid) = A + p*count;
        count = count+1;
    end
end

% left edges
count = 0;
for i = 2:K
    for j = 1:K
        eleid = (i-1)*K + j;
        ggv(2*p+1:3*p, eleid) = A + K*(K-1)*p + count*p;
        count = count +1;
    end
end

% right edges
for i = 1:K-1
    for j = 1:K
        eleid = (i-1)*K + j;
        ggv(1*p+1:2*p, eleid) = ggv(2*p+1:3*p, eleid+K);
    end
end

% top edges
for i = 1:K
    for j = 1:K-1
        eleid = (i-1)*K + j;
        ggv(3*p+1:4*p, eleid) = ggv(1:p, eleid+1);
    end
end

for i = 1:K
    eleid_bot = (i-1)*K + 1;
    ggv(1:p, eleid_bot) = A + 2*K*(K-1)*p + (i-1)*p;
    
    eleid_right = K*(K-1) + i;
    ggv(p+1:2*p, eleid_right) = A + 2*K*(K-1)*p + K*p + (i-1)*p;
    
    eleid_left = i;
    ggv(2*p+1:3*p, eleid_left) = A + 2*K*(K-1)*p + 2*K*p + (i-1)*p;
    
    eleid_top = K + (i-1)*K;
    ggv(3*p+1:4*p, eleid_top) = A + 2*K*(K-1)*p + 3*K*p + (i-1)*p;
end

end

