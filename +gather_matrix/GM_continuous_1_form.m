function [ gcf ] = GM_continuous_1_form( K, p )
%GM_CONTINUOUS_1_FORM Summary of this function goes here
%   Detailed explanation goes here

% Created on : 7th Feb, 2018
% Description : 

gcf = zeros(2*p*(p+1), K^2);

B = 1:p*(p-1);
C = p*(p-1) + 1: 2*p*(p-1);

for el = 1:K^2
    gcf(p+1:p^2,el) = B + 2*p*(p-1)*(el-1);
    gcf(p*(p+1)+p+1:p*(p+1)+p+p*(p-1),el) = C + 2*p*(p-1)*(el-1);
end

ggv = gather_matrix.GM_boundary_LM_nodes(K,p);
ggv2 = ggv + K^2 * 2 * p * (p-1);

gcf(1:p,:) = ggv2(1:p,:);
gcf(p^2+1:p^2+p,:) = ggv2(3*p+1:4*p,:);
gcf(p*(p+1) + 1: p*(p+1) + p,:) = ggv2(2*p+1:3*p,:);
gcf(p*(p+1) + p^2 +1: 2*p*(p+1),:) = ggv2(p+1:2*p,:);

end

