function [BC] = dirichlet_bc(mapping,dtize,bc_value,p,nn)

% mapping is the mapping of the edge.
% For example for Left edge user should give: mapping(-1,tt) as input
% argument

% bc_value is the value of BC at the corresponsing edge

% nn is the unit normal vector : +1 or -1

[xx,yy] = mapping(dtize.xp);
phi_bc = bc_value(xx,yy);

BC = zeros(p,1);

for j=1:p
    edge_id = j;
    for tt=1:p+1
        BC(edge_id) = BC(edge_id) + nn * phi_bc(tt) * dtize.ep(j,tt) * dtize.wp(tt);
    end
end

end

