function [x,y] = mapping(xii,eta,F_L_x,F_R_x,F_B_x,F_T_x,F_L_y,F_R_y,F_B_y,F_T_y)
%MAPPING Summary of this function goes here
%   Detailed explanation goes here

% Created on 5 April, 2019

s = (xii+1)/2;
t = (eta+1)/2;

F00_x = F_L_x(0,0);
F0h_x = F_L_x(0,1);
Fh0_x = F_R_x(1,0);
F11_x = F_R_x(1,1);

F00_y = F_L_y(0,0);
F0h_y = F_L_y(0,1);
Fh0_y = F_R_y(1,0);
F11_y = F_R_y(1,1);

x = (1-s).*F_L_x(0,t)     + s.*F_R_x(1,t)   + (1-t).*F_B_x(s,0)     + t.*F_T_x(s,1)...
    - (1-s).*(1-t)*F00_x  - (1-s).*t*F0h_x  - (1-t).*s*Fh0_x        - t.*s*F11_x;

y = (1-s).*F_L_y(0,t)     + s.*F_R_y(1,t)   + (1-t).*F_B_y(s,0)     + t.*F_T_y(s,1)...
    - (1-s).*(1-t)*F00_y  - (1-s).*t*F0h_y  - (1-t).*s*Fh0_y        - t.*s*F11_y;

end