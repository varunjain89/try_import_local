classdef twoForm_v3
    %TWOFORM Summary of this class goes here
    %   Detailed explanation goes here
    % created  on 23 Sept
    % modified on 16 Nov
    
    properties
%         cochain
        p
%         pf
        el_mapping
%         eval_pf_ggg
        K
        mesh
        
%         f_an
%         domain
        el_bounds_x
        el_bounds_y
    end
    
    methods
        function obj = twoForm_v3(mesh1)
            obj.mesh = mesh1;
            obj.K = mesh1.K;
            obj.p = mesh1.p;
            obj.el_bounds_x = mesh1.el.bounds.x;
            obj.el_bounds_y = mesh1.el.bounds.y;
%             obj.el_mapping  = mesh1.el.mapping;
        end
        
        function [xf_3D, yf_3D, uf_h] = reconstruction(obj, cochain)
            [xf_3D, yf_3D, uf_h] = reconstruction.of2form_multi_element_v6_salil(cochain, obj.mesh);
        end
        
        function F_3D = reduction(obj, f_an)
            F_3D = reduction.of2form_multi_element_v9(f_an, obj.mesh.p, obj.mesh.domain, obj.mesh.K, obj.mesh.el.bounds.x, obj.mesh.el.bounds.y);
        end
    end
    
end

