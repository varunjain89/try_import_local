classdef poisson < handle
%     Created on 1 June, 2019
    
    properties
        eval
    end
    
    methods
        
        function obj = eval_metric(obj, jac)
            obj.eval.metric.ggg = metric.ggg(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta);
            obj.eval.metric.g11 = metric.g11(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta,obj.eval.metric.ggg);
            obj.eval.metric.g12 = metric.g12(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta,obj.eval.metric.ggg);
            obj.eval.metric.g22 = metric.g22(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta,obj.eval.metric.ggg);
        end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end
        
    end
end

