function [pmetric] = eval_metric(jac)

% Created on 2 June, 2019

pmetric.ggg = metric.ggg(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta);
pmetric.g11 = metric.g11(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta,pmetric.ggg);
pmetric.g12 = metric.g12(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta,pmetric.ggg);
pmetric.g22 = metric.g22(jac.dx_dxii,jac.dx_deta,jac.dy_dxii,jac.dy_deta,pmetric.ggg);

end

